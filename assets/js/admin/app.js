import $ from 'jquery';
// Shop templates actions
$(document).ready(function () {
    // On load shop templates, if any
    if ($(document).find('[id^="field_container_"][id$="_templates"]').length) {
        if ($(document).find('[id^="field_widget_"][id$="_templates"] table').length) {
            // Disable previous selected attributes
            disableSelectedAttributes();

            // init attribute values selects
            initAllAttributesValues();
        }

        // Add new Shop template row actions
        $(document).on('sonata.add_element', '[id^="field_container_"][id$="_templates"]', function () {
            $.each($(document).find('select.admin-shop-attribute-select'), function () {
                if (!$(this).val()) {
                    $(document).find('#' + $(this).attr('id') + 'Value').empty().append($("<option></option>")).select2({
                        placeholder: 'Select an Attribute first...',
                        dropdownAutoWidth: true,
                        minimumResultsForSearch: 10,
                    }).trigger('change');
                }
            });

            // Disable previous selected attributes
            disableSelectedAttributes();

            // init attribute values selects
            initAllAttributesValues();
        });

        // On change shop attribute options
        $(document).on('change', 'select.admin-shop-attribute-select', function () {
            initAttributeValues($(this), true);

            // Disable previous selected attributes
            disableSelectedAttributes();
        });

        // On change shop attribute value options
        $(document).on('change', 'select.admin-shop-attribute-value-select', function () {
            // Change image for Attribute Value
            var selectedOption = $(this).find(':selected'),
                attributeValueImg = selectedOption.data('image'),
                attributeValueHelp = $(this).parent().find('.help-block');

            if (attributeValueImg) {
                if (attributeValueHelp.length) {
                    attributeValueHelp.find('img.attribute-image-select').attr('src', selectedOption.data('url')).attr('alt', attributeValueImg);
                } else {
                    $(this).parent().append(
                        '<span class="help-block sonata-ba-field-widget-help"><img src="' + selectedOption.data('url') + '" alt="' + attributeValueImg + '" class="attribute-image-select"></span>'
                    );
                }
            } else {
                if (attributeValueHelp.length) {
                    attributeValueHelp.remove();
                }
            }
        });
    }

    function initAllAttributesValues() {
        $.each($(document).find('select.admin-shop-attribute-select'), function () {
            initAttributeValues($(this), false);
        });
    }

    function initAttributeValues(attributeSelect, change) {
        var attributeValueSelect = $(document).find('#' + attributeSelect.attr('id') + 'Value');

        if (attributeSelect.val()) {
            $.ajax({
                url: attributeSelect.find(':selected').data('url'),
                type: 'POST',
                success: function (response) {
                    if (change) {
                        attributeValueSelect.empty().append($('<option></option>'));

                        if (response && response.length) {
                            // Change select options for Attribute Value
                            $.each(response, function (key, data) {
                                var option = $("<option></option>")
                                    .attr('value', data.id)
                                    .data('image', data.image)
                                    .data('url', data.url)
                                    .text(data.name);

                                attributeValueSelect.append(option);
                            });
                        }

                        attributeValueSelect.select2({
                            placeholder: 'Select a Value for Attribute...',
                            dropdownAutoWidth: true,
                            minimumResultsForSearch: 10,
                        }).trigger('change');
                    } else {
                        var attributeValueOptions = [];
                        if (response && response.length) {
                            $.each(response, function (key, data) {
                                attributeValueOptions.push(data.id);
                            });
                        }

                        $.each(attributeValueSelect.find('option'), function () {
                            var optionValue = $(this).attr('value');
                            if (!optionValue || $.inArray(parseInt(optionValue), attributeValueOptions) === -1) {
                                $(this).remove();
                            }
                        });

                        attributeValueSelect.select2({
                            dropdownAutoWidth: true,
                            minimumResultsForSearch: 10,
                        }).trigger('change');
                    }
                }
            });
        } else {
            if (attributeValueSelect.length) {
                attributeValueSelect.empty().append($("<option></option>")).select2({
                    placeholder: 'Select an Attribute first...',
                    dropdownAutoWidth: true,
                    minimumResultsForSearch: 10,
                }).trigger('change');
            }
        }
    }

    function disableSelectedAttributes() {
        var selectedAttributes = [];
        $.each($(document).find('select.admin-shop-attribute-select option:selected'), function () {
            selectedAttributes.push($(this).val());
        });

        $.each($(document).find('select.admin-shop-attribute-select'), function () {
            var selectValue = $(this).val();

            $.each($(this).find('option'), function () {
                var optionValue = $(this).attr('value');
                if (optionValue) {
                    if (selectValue !== optionValue && $.inArray(optionValue, selectedAttributes) !== -1) {
                        $(this).attr('disabled', 'disabled');
                    } else {
                        $(this).attr('disabled', false);
                    }
                }
            });
        });
    }
});
