import TemplateHelper from './../../../global/helper/template'

export default class TemplateMedimarket extends TemplateHelper {
    /**
     * @inheritDoc
     */
    _createTemplateFromPrototype(prototype) {
        let country = this._location.address.country;
        if (country && typeof globalData['countriesCode'][country] !== 'undefined') {
            country = globalData['countriesCode'][country];
        }
        prototype = prototype.replace(/__address__/g, this._locationAddress + '<br>' + country);

        let template = super._createTemplateFromPrototype(prototype);

        let templateElement = $('<div></div>').html(template);

        let elementPhone = templateElement.find('.mobilo_tel');
        if (elementPhone.length && !this._location.contact.phone) {
            elementPhone.hide();
        }

        return templateElement.html();
    }

}
