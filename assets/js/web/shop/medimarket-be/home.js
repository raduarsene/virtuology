import $ from 'jquery';
window.$ = $;
window.jQuery = $;

// Datetime/timezone
import moment from 'moment-timezone';
window.moment = moment;

// Import Template class for home page as global
import TemplateHelper from './helper/template';
window.TemplateHelper = TemplateHelper;

import OverlayInfoWindow from '../../block/OverlayInfoWindow/OverlayInfoWindow';

$(document).ready(function () {
  const MapObj = window.map;

  new OverlayInfoWindow({});

  /* mobilo_searchBox on click*/
  $('.mobilo_openFilters').on("click", function () {
    $('.mobilo_resultsBox').toggleClass("search-is-open");
  });


  if (MapObj && typeof MapObj.extendSettings === 'function') {
    let defaultSettings = {
      /**
       * On autocomplete place select
       * @param {*} ui 
       */
      onAutocompletePlaceSelect: (ui) => {
      },

      clearSearch: () => {},

      /**
       * Clear filters
       */
      onClearFilters() {},

      /**
       * On loader list change 
       * @param {*} action 
       */
      onLoaderListChange: (action, results) => {
        if (!action) {
          $('.search-results-no-loading').html(results.length)
        }
      },
      /**
       * On marker click
       * @param {*} infoWindow 
       * @param {*} marker 
       */
      onMarkerClick: (infoWindow, marker) => {},
      /**
       * On info window close
       * @param {*} marker 
       */
      onInfoWindowClose: (marker) => {},

      /**
       * On info window selected
       * @param {*} el 
       * @param {*} infoWindow 
       * @param {*} marker 
       */
      onShopSelected: (el, infoWindow, marker, externalId) => {}
    };

    MapObj.extendSettings(defaultSettings);
  }
});
