import OverlayInfoWindow from '../../block/OverlayInfoWindow/OverlayInfoWindow';

$(document).ready(function () {
    _loadReviews();
    new OverlayInfoWindow({});
});

function _loadReviews() {
    var reviewsOutputContainer = $("#trusted_shop_content");
    if (typeof reviewsOutputContainer === "undefined" || !reviewsOutputContainer || typeof reviewsOutputContainer.html() === "undefined") {
        return false;
    }

    if (reviewsOutputContainer.html().length > 0) {
        return false;
    }

    var externalId = $("#externalId").val(),
        _tsid = $("#trusted_shop_id").val();

    //totals
    if (sessionStorage.getItem("medi-market-" + externalId)) {
        reviewsOutputContainer.html(sessionStorage.getItem("medi-market-" + externalId));
    } else {
        var apiURL = 'https://api.trustedshops.com/rest/public/v2/shops/' + _tsid + '/quality/reviews.json';

        $.get(apiURL, function (data) {
            var result = data.response.data.shop.qualityIndicators.reviewIndicator.overallMark;
            var count = data.response.data.shop.qualityIndicators.reviewIndicator.activeReviewCount;
            var shopName = data.response.data.shop.name;
            var max = "5.00";

            if (count > 0) {
                var content = '<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">' +
                    '<small>' +
                    '<span itemprop="ratingValue">' + result + '</span> /' +
                    '<span itemprop="bestRating">' + max + '</span> of ' +
                    '<span itemprop="ratingCount">' + count + '</span> ' +
                    '</small>' +
                    '<a href="https://www.trustedshops.com/buyerrating/info_' + _tsid + '.html" title="' + shopName + ' custom reviews" target="_blank"><small>' + shopName + ' customer reviews</small></a>' +
                    '</div>';
                reviewsOutputContainer.html(content);
                sessionStorage.setItem("medi-market-" + externalId, content);
            }
        });
    }
}
