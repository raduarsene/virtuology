$( document ).ready(function() {
  $(window).width() < 1024 &&
      $(document).on("click", ".first-lvl li", function (t) {
          $(".third-lvl li").removeClass("d-inline-block"),
          $(".second-lvl li").removeClass("d-block"),
          $(".second-lvl li[data-first-lvl=" + $(this).data("first-lvl") + "]").addClass("d-block"),
          $(this).data("first-lvl") && ($(".second-lvl").addClass("open"), $(".nav-fly-out").removeClass("d-none"), t.preventDefault());
      }),
      $(window).width() < 1024 &&
        $(document).on("click", ".second-lvl li", function (t) {
          $(".third-lvl li").removeClass("d-inline-block"),
          $(".third-lvl li[data-second-lvl=" + $(this).data("second-lvl") + "]").addClass("d-inline-block"),
          $(this).data("second-lvl") && ($(".third-lvl").addClass("open"), t.preventDefault());
        }),
      $(document).on("click", ".js-menu-button", function () {
        $(".first-lvl").addClass("open"),
        setTimeout(function () {
            $("body").delay(400).addClass("no-scroll");
        }, 400);
      }),
      $(document).on("click", ".js-back", function () {
          $(this).closest(".lvl").removeClass("open");
      }),
      $(document).on("click", ".js-close", function () {
      $(".second-lvl li").removeClass("d-block"), 
      $(".nav-fly-out").addClass("d-none"), 
      $(".lvl").removeClass("open"), 
      $("body").removeClass("no-scroll");
  });
});