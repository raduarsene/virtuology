$( document ).ready(function() {
  $('.mobile-trigger').click(function () {
    $(this).toggleClass('open');
    $('body').toggleClass('overflow');
    $('.active-branch.hs-menu-flow-horizontal').slideToggle(300);
  });
    // open submenu 
  $('.child-trigger').click(function () {
    $(this).toggleClass('open');
    $('body').toggleClass('overflow');
    $('.hs-menu-children-wrapper').slideToggle(300);
  });
});