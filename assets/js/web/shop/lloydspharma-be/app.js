$( document ).ready(function() {
  $('.main-nav-trigger').on( "click", function() {
    $('body').toggleClass("header-open");
  });

  $('#autocomplete-search').on( "click", function() {
    $('.autocomplete-site').addClass("active");
  });

  $('.cancel-search').on( "click", function(e) {
    e.preventDefault();
    $('.autocomplete-site').removeClass("active");
  });

  $('.toggle-search').on( "click", function(e) {
    e.preventDefault();
    $('.autocomplete-site').addClass("active");
  });



  $(document).on("click", "#fr_BE", function (event) {


    event.preventDefault();
    let currentUrl = window.location.href;
    currentUrl = currentUrl.replace('nl', 'fr');
    window.location.href = currentUrl;
    return false;

  });


  $(document).on("click", "#nl_BE", function (event) {

    event.preventDefault();
    let currentUrl = window.location.href;
    currentUrl = currentUrl.replace('fr', 'nl');
    window.location.href = currentUrl;

  });

});

