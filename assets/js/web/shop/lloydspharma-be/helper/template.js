import TemplateHelper from './../../../global/helper/template'

export default class TemplateLloyds extends TemplateHelper {
    /**
     * @inheritDoc
     */
    _createTemplateFromPrototype(prototype) {
        let template = super._createTemplateFromPrototype(prototype);

        if (typeof this._locationKey !== 'undefined') {
            template = template.replace(/__index__/g, this._locationKey + 1);
        }

        let commanderUrl = '',
            commanderClass = 'hidden',
            prescriptionUrl = '',
            prescriptionClass = 'hidden';

        $.each(this._location.locationExtraData, function (index, value) {
            if (value.key === 'commander' || value.key === 'Commander') {
                commanderUrl = value.value;
                commanderClass = '';
            }
            if (value.key.trim() === 'ma-prescription' || value.key.trim() === 'Ma prescription') {
                prescriptionUrl = value.value;
                prescriptionClass = '';
            }
        });

        template = template.replace(/__commander_link__/g, commanderUrl)
            .replace(/__commander_class__/g, commanderClass)
            .replace(/__prescription_link__/g, prescriptionUrl)
            .replace(/__prescription_class__/g, prescriptionClass)
        ;

        return template;
    }
}
