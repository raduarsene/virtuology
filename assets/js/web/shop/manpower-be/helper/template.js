import TemplateHelper from './../../../global/helper/template'

export default class TemplateManpower extends TemplateHelper {
    /**
     * @inheritDoc
     */
    _createTemplateFromPrototype(prototype) {
        let template = super._createTemplateFromPrototype(prototype);


        let appointment = '',
            appointmentClass = '',
            byAppointmentClass = 'hidden open-by-appointment';
        $.each(this._location.locationExtraData, function (index, value) {
            if (value.key === 'By Appointment') {
                appointment = value.value;
                if(appointment) {
                    appointmentClass = 'hidden';
                    byAppointmentClass = 'open-by-appointment'
                }

            }
        });

        template = template.replace(/__appointmentClass__/g, appointmentClass)
        template = template.replace(/__byAppointmentClass__/g, byAppointmentClass)

        ;

        return template;
    }
}
