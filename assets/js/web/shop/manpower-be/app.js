$(document).ready(function () {
  $('.menuToggle').click(function () {
    $('body').toggleClass('overflow');
    $('.pane-main-menu').slideToggle();
  });
});