// Import local classes for home page as global
import TemplateHelper from './helper/template';
window.TemplateHelper = TemplateHelper;
import $ from "jquery";

$(document).ready(() => {
  $('.navbar-toggle').on('click', (event) => {
    event.preventDefault();
    $('.navbar .collapse').toggleClass('active');
  });
});