import TemplateHelper from './../../../global/helper/template'

export default class TemplateLouisdelhaize extends TemplateHelper {
    /**
     * @inheritDoc
     */
    _createTemplateFromPrototype(prototype) {


        let template = super._createTemplateFromPrototype(prototype);

        if (typeof this._locationKey !== 'undefined') {
            template = template.replace(/__index__/g, this._locationKey + 1);
        }


        return template;
    }


}
