$(document).ready(function () {
  $('.elementor-menu-toggle .eicon-menu-bar').click(function () {
    $(this).toggleClass('elementor-active');
    $('body').toggleClass('overflow');
    $('.elementor-nav-menu--dropdown').slideToggle('fast');
  });
});
