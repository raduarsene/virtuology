import Collection from '../../block/components/Collection';

$( document ).ready(function() {
  let defaultSettings = {};
  if (typeof globalSettings !== 'undefined') {
    defaultSettings = {...defaultSettings, ...globalSettings};
  }

  const itemTemplate = (item) => {
    const vehicleDetails = item.vehicleDetails;
    let pictureUrl = vehicleDetails.picture_list.picture;
    let date = new Date(vehicleDetails.build_date);

    if (Array.isArray(pictureUrl)) {
      pictureUrl = pictureUrl[0] ? pictureUrl[0].picture_url : null;
    }

    return `<div class="car-grid-item ">
        <div class="car-item">
          <div class="car-header">
            <a href="#" class="car-header-title">${date.getFullYear()} ${vehicleDetails.make_description} ${vehicleDetails.model_description}</a>
          </div>
          <div class="car-img">
            <a href="${vehicleDetails.condition_url}" target="_blank">
              <img src="${pictureUrl}">
            </a>
          </div>
          <div class="car-body">
            <div class="buy-method">
              <icon class="fa icon fa-gavel"></icon>Fixed Price
            </div>
            <p class="price">$${vehicleDetails.fixed_price}</p>
            <ul>
              <li>
                <i class="picon picon_location"></i>
                <span>${vehicleDetails.organisation}</span>
              </li>
              <li>
                <i class="picon picon_transmission"></i>
                <span>${vehicleDetails.trans_description}</span>
              </li>
              <li>
                <i class="picon picon_mileage"></i>
                <span>${vehicleDetails.odometer} Km</span>
              </li>
              <li>
                <i class="picon picon_engine"></i>
                <span>${vehicleDetails.engine_capacity} Ltr</span>
              </li>
              <li>
                <i class="picon picon_body"></i>
                <span>${vehicleDetails.body_description}</span>
              </li>
              <li>
                <i class="picon picon_warranty"></i>
                <span>${vehicleDetails.type_description}</span>
              </li>
            </ul>
          </div>
          <div class="car-footer">
            <a class="car-btn" href="${vehicleDetails.condition_url}" target="_blank">Condition report</a>
          </div>
        </div>
      </div>`
  };

  if ($('[data-collection]').get(0)) {
    new Collection({
      params: defaultSettings.auctionParam,
      apiUrl: defaultSettings.auctionsUrl,
      perPagePrefix: 'n',
      itemTemplate,
    });
  }

  $('.open-c-search').click(function () {
    $(this).parent().addClass('show');
  });

  $('.close-c-search').click(function () {
    $('.sliding-search').removeClass('show');
  });

  $('.input-group-prepend .dropdown-toggle').click(function () {
    $(this).parent().toggleClass('show');
  });

  $('.header-container-nav-mob .off-canvas-menu-button').click(function () {
    $('body').toggleClass('isOpen');
  });

  $('.mobile-menu .nav-expand-link').click(function () {
    $(this).closest('.nav-item').addClass('active');
  });

  $('.nav-back-link').click(function(){
    $(this).closest('.nav-item.active').removeClass('active');
  })
});
