$(document).ready(function () {
    $('.searchFormBtn').click(function (event) {
        event.preventDefault();
        $('.navMenu li ul.sub-nav').slideUp();
        $('.navMenu .search form').slideToggle();
        $(this).closest('li').toggleClass('active');
    });

    $('.nav li').hover(function () {
        if ($(window).width() > 990) {
            $(this).find('.sub-nav').slideToggle();

            $(this).parent().siblings().children().next().slideUp();
            return false;
        }
    });

    $('.nav-toggle.mobileMenu').click(function (event) {
        event.preventDefault();
        $('.nav').slideToggle();
    });

    /* footer acrodion*/
    $('.footerAccordion h3').click(function () {
        if ($(window).width() < 992) {
            $(this).next('.footerAccordionContent').slideToggle();
            $(this).addClass('open');

            $(this).parent().siblings().children().next().slideUp();
            return false;
        }
    });

    /* search-filters */
    $(".search-filters").click(function (e) {
        $('.map-search-box').toggleClass('open');
    });

    /* font size change functionality */
    const sectionBar = $('section.bar');
    const sizeDown = $("#sizeDown");
    const sizeUp = $("#sizeUp");
    const body = $("body");
    let zoomVolume = 0;

    const zoomFont = function (e) {
        e.preventDefault();

        const el = $(e.target);
        sectionBar.find('.active').removeClass('active');

        el.addClass('active');

        if (el.attr('id') === 'sizeUp') {
            if (zoomVolume < 2) {
                zoomVolume++;
            }
        } else if (el.attr('id') === 'sizeDown') {
            if (zoomVolume >= 0) {
                zoomVolume--;
            }
        }

        body.attr('data-zoom', zoomVolume);
    };

    sizeDown.on('click', zoomFont);
    sizeUp.on('click', zoomFont);
});
