import TemplateHelper from './../../../global/helper/template'

export default class TemplateLeon extends TemplateHelper {
    /**
     * @inheritDoc
     */
    _createTemplateFromPrototype(prototype) {



        let template = super._createTemplateFromPrototype(prototype);

        if (typeof this._locationKey !== 'undefined') {
            template = template.replace(/__index__/g, this._locationKey + 1);
        }

        let reservationUrl = '',
            reservationClass = 'hidden';
        $.each(this._location.locationExtraData, function (index, value) {
            if (value.key === 'reserver') {
                reservationUrl = value.value;
                reservationClass = '';
            }
        });

        template = template.replace(/__reservationUrl__/g, reservationUrl)
            .replace(/__reservationClass__/g, reservationClass)
            // .replace(/__phone_format__/g, this._phoneFormat(this._location.contact.phone))
            .replace(/__phone_format__/g, this._location.contact.phone)

        ;

        return template;
    }


    /**
     * @returns {string}
     * @protected
     */
    get _locationAddress() {
        let address = '';

        let addressData = this._location.address.street.replace(',,', ',').split(',');
        if (addressData.length > 1) {
            if(addressData[1].length) {
                address += addressData[1].trim() + ', ' + addressData[0].trim();
            }
            else {
                address = addressData[0].trim();
            }

        } else {
            address += this._location.address.street;
        }

        if (typeof this._location.address.extraInfo !== 'undefined' && this._location.address.extraInfo) {
            address += ', '+ this._location.address.extraInfo ;
        }

        address += '<br>' + this._location.address.locality + ', ' + this._location.address.zipCode;

        return address;
    }


    _phoneFormat(phone) {

        var phoneFormat = phone.replace('+33', '');
        phoneFormat = "0"+phoneFormat;

        return this._chunk(phoneFormat, 2).join(' ');


    }

    _chunk(str, n) {
        var ret = [];
        var i;
        var len;

        for (i = 0, len = str.length; i < len; i += n) {
            ret.push(str.substr(i, n))
        }

        return ret
    }


}
