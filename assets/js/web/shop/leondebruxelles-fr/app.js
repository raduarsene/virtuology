import $ from "jquery";

$(document).ready(() => {
  $('.menu-toggle').on('click', (event) => {
    event.preventDefault();
    const el = event.currentTarget;
    $(el).toggleClass("active_menu");
    $( ".wrapper-scroll" ).slideToggle();
  });

  $('.select-item-container').on('click', (event) => {
    const el = event.currentTarget;
    $(el).toggleClass("open");
    $( ".selet-item" ).toggleClass("show-item");;
  });

  var stickyTop = $('.sticky-menu').offset().top;
  $(window).scroll(function() {
    var windowTop = $(window).scrollTop();

    if (stickyTop < windowTop) {
      $('.sticky-wrapper').addClass('is-sticky');
      $('.mobi_content').addClass('content-sticky');
    } else {
      $('.sticky-wrapper').removeClass('is-sticky');
      $('.mobi_content').removeClass('content-sticky');
    }
  });

  $(document).on('click','.open_pop', (event) => {

    event.preventDefault();

    const el = event.currentTarget;
    var href = $(el).data('modal');

    var hrefIframe = $(el).prop('href');
    $("#command").prop('src', hrefIframe);

    $('#'+href).addClass("show-popup");
    $( "body" ).addClass("overflow");
    $(".fyb_header").css('z-index', 0);
  });

  $(document).on('click','.close_pop', (event) => {
    const el = event.currentTarget;
    $( ".container-popup" ).removeClass("show-popup");
    $( "body" ).removeClass("overflow");
    $(".fyb_header").css('z-index', 20);
  });

   /*scroll animation home page*/
    $(document).on('click','.event-list-title a', (event) => {
    event.preventDefault();
    const el = event.currentTarget;
    const hash = el.getAttribute('href').replace('#', '');
    const offset = $(`a[name='${ hash }']`).offset();

    $('body, html').animate({
      scrollTop: offset.top - 91
    });
    });

  $(document).on('click','.wrap-title', () => {
    $( ".event-list-block" ).toggleClass("open");
  });



  //slider
  var promotionSlider = $('.promotion-slider');

  if(typeof promotionSlider!== "undefined" && promotionSlider.length)
  {


    promotionSlider.slick({

      slidesToScroll: 3,
      slidesToShow: 3,
      dots: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 1140,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 720,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 540,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
  }

});



