import TemplateHelper from './../../../global/helper/template'

export default class TemplateCarrefour extends TemplateHelper {
    /**
     * @inheritDoc
     */
    _addOpenHoursTitle(data, template) {
        let commaClass = 'comma';
        if (data.opens.label === data.opens.sentence) {
            commaClass = 'hidden';
            data.opens.sentence = '';
        }

        template = super._addOpenHoursTitle(data, template);

        return template.replace(/__commaClass__/g, commaClass);
    }

    /**
     * @inheritDoc
     */
    get _locationBrandSlug() {
        let brandSlug = this._defaultBrandSlug;
        if (typeof this._location.brandSlug !== 'undefined' && this._location.brandSlug) {
            brandSlug = this._location.brandSlug;
        } else if (typeof globalData.brandSlugs !== 'undefined' && globalData.brandSlugs.length) {
            // Random brand slug
            brandSlug = globalData.brandSlugs[Math.floor((Math.random() * globalData.brandSlugs.length))];
        }

        return brandSlug;
    }

    /**
     *
     * @param prototype
     * @returns {string}
     * @private
     */
    _createTemplateFromPrototype(prototype) {
        let template = super._createTemplateFromPrototype(prototype);

        var externalIdDrive = this._location.externalId;
        if (externalIdDrive.length < 4) {
            for (var i = 0; i < (4 - this._location.externalId.length); i++) {
                externalIdDrive = "0" + externalIdDrive;
            }
        }

        return template.replace(/__externalIdDrive__/g, externalIdDrive);
    }
}
