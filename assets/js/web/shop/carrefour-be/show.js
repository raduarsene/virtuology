// require jQuery
import $ from 'jquery';
window.$ = $;
window.jQuery = $;

import 'bootstrap';
let owl_carousel = require('owl.carousel');
window.fn = owl_carousel;

// Datetime/timezone
import moment from 'moment-timezone';
window.moment = moment;

// Import google map class
import GoogleMap from './../../global/map/google';
import TemplateCarrefour from './helper/template';
import MapListView from './components/MapListView';
import BrowserDetect from './components/BrowserDetect';

$(document).ready(function () {
    const browserDetect = new BrowserDetect();
    var location = globalData.currentLocation,
        TemplateClass = new TemplateCarrefour();
    const mapListView = new MapListView();

    // Map actions
    var mapContainer = $('#details_map_container');
    if (mapContainer.length) {
        var defaultSettings = {
            credentials: mapContainer.data('credentials'),
            language: mapContainer.data('language'),
            data: location,
        };

        // Combine shop settings with default settings
        if (typeof globalSettings !== 'undefined') {
            defaultSettings = {...defaultSettings, ...globalSettings};
        }

        // Create map
        var Map = new GoogleMap(defaultSettings);
        Map.createMap(mapContainer.get(0));
    }

    $(document).on('click','[data-itineraire]' , (event) => {
        const mobileOperationSystem = browserDetect.getMobileOperatingSystem();
        const el = $(event.currentTarget);
        const itineraire = el.data('itineraire').split(',');
        const location = {
            lat: 0,
            lng: 0,
        };

        if (browserDetect._isMobile) {
            if (itineraire.length) {
                location.lat = itineraire[0];
                location.lng = itineraire[1];
                
                event.preventDefault();

                if (mobileOperationSystem === 'android') {
                    window.location.href = `geo:${location.lat},${location.lng}`
                } else if(mobileOperationSystem === 'ios') {
                    window.location.href = `maps://maps.google.com/maps?daddr=${location.lat},${location.lng}&amp;ll=`
                }
            }   
        }
    });
    
    // header with promotions
    const headerPromotion = $('.header_promotion');
    const body = $('body');
    
    if (headerPromotion.get(0)) {
        if (window.sessionStorage) {
            const showPromotion = !window.sessionStorage.getItem("promotion");
            const isVisible = headerPromotion.css('display') !== 'none';

            body[showPromotion && isVisible ?'addClass' : 'removeClass']('banner-promotion');
        }

        headerPromotion.find('a.btn.link').on('click', () => {
            body.removeClass('banner-promotion');
        })
    }

    // Sidebar actions
    var sidebarBlock = $('.detail-page-aside');
    if (sidebarBlock.length) {
        sidebarBlock.find('.open-hours-show').click(function (e) {
            e.preventDefault();

            $(this).find('.icon').toggleClass('icon-arrow-down').toggleClass('icon-arrow-up');
            sidebarBlock.find('.opening-hours-week').slideToggle('500');
        });
    }

    // Promotions actions
    var promotionsBlock = $('#block_details_promotions .owl-carousel');
    if (promotionsBlock.length) {
        var items = promotionsBlock.hasClass('col-12') ? 3 : 2;
        promotionsBlock.owlCarousel({
            margin: 10,
            // items: promotionsBlock.hasClass('col-12') ? 3 : 2,
            nav: false,
            items:3,
            // responsiveRefreshRate: 10,

            responsive:{
                0:{
                    items:1,
                },

                600:{
                    items:2,
                },
                1200:{
                    items:2,
                },
                1201:{
                    items:3,
                }
            }

        });
    }

    // Campaigns actions
    var campaignsBlock = $('#block_details_campaigns .owl-carousel');
    if (campaignsBlock.length) {
        campaignsBlock.owlCarousel({
            margin: 10,
            items: 3,
            nav: false,
            responsiveClass: true,
            responsiveRefreshRate: 10,
            responsive: {0: {items: 1}, 600: {items: 2}, 1000: {items: 3}}
        });
    }

    // Locations reviews action
    var reviewsBlock = $('#location_reviews_block');
    if (reviewsBlock.length) {

        var externalId = reviewsBlock.attr("data-externalid");
        var externalIdTracking = externalId;
        var diff = 4-externalId.length*1;
        for(var i=0; i<diff; i++) {
            externalIdTracking = "0"+externalIdTracking;
        }

        let digitalData = window.digitalData || {}
        digitalData.store = digitalData.store || {}
        digitalData.store.id = externalIdTracking;

        $.get(reviewsBlock.data('url'), function (response) {
            if (response && typeof response['reviews'] !== 'undefined' && typeof response['rating'] !== 'undefined') {
                var rating = response['rating'];
                reviewsBlock.find('.review-rating').text(rating);
                reviewsBlock.find('.review-count').text(response['reviews'].length);

                reviewsBlock.find('.stars-container').addClass('stars-' + parseInt(rating * 20));
                reviewsBlock.css("display", "flex");




                digitalData.store.reviewsCount=response['reviews'].length;
                digitalData.store.reviewsRating=rating;
            }

            digitalData.events=digitalData.events || []
            digitalData.events.push({
                "eventName":"pageView",
                "eventAction":"dataLayerComplete"
            });

            // console.log(digitalData);

        })
        .fail(function () {


                digitalData.events=digitalData.events || []
                digitalData.events.push({
                    "eventName":"pageView",
                    "eventAction":"dataLayerComplete"
                });

                // console.log(digitalData);

        });

    }

    // Locations near action
    var nearLocationsBlock = $('#near_location_block > ul'),
        nearLocationsButton = $('#near_location_block #near_location_button');
    if (nearLocationsBlock.length) {
        var query = {near: location.address.latitude + ',' + location.address.longitude};
        if (typeof globalSettings.near_query !== "undefined") {
            if (typeof globalSettings.near_query.size != "undefined") {
                query.size = globalSettings.near_query.size;
            }

            if (typeof globalSettings.near_query.radius != "undefined") {
                query.radius = globalSettings.near_query.radius;
            }
        }

        $.get(nearLocationsBlock.data('url'), query, function (locations) {
            if (locations && locations.length) {
                for (var locationKey in locations) {
                    var template = TemplateClass.getLocationTemplate(locations[locationKey], nearLocationsBlock.data('prototype'), false);

                    var templateBlock = $(template);
                    if (typeof locations[locationKey]['locationItemIds'] !== 'undefined' && locations[locationKey]['locationItemIds']
                        && (locations[locationKey]['locationItemIds'].includes('service.carrefour.store')
                            || locations[locationKey]['locationItemIds'].includes('service.carrefour.car.trunk'))
                    ) {
                        templateBlock.find('.external-details-link').show();
                    }

                    if (locationKey > 2) {
                        templateBlock.hide();
                    }

                    nearLocationsBlock.append(templateBlock);
                }

                if (locations.length > 3) {
                    nearLocationsButton.show();
                    nearLocationsButton.on('click', function (e) {
                        e.preventDefault();

                        $(this).hide();
                        nearLocationsBlock.find('> li').show();
                    })
                }

                nearLocationsBlock.parent().show();
            }
        });
    }

    //add tracking
    $(document).on("click", ".click-tracking", function (event) {

        var eventOrigin = $(event.target).data("eventtracking");

        var useStorage = false;
        if(typeof $(event.target).data("usestorage") !== "undefined") {
            useStorage = $(event.target).data("usestorage");
        }

        let digitalData = window.digitalData || [];
        digitalData.events = digitalData.events || [];
        digitalData.events.push({
            "eventName": "storelocatorInteraction",
            "eventOrigin": eventOrigin,
            "useStorage": useStorage,
        });

    });


    //promotions show more
    var showChar = 169;
    var ellipsestext = "...";
    var moretext = $("#show_more_trans").val();
    var lesstext = $("#show_less_trans").val();
    $('.more').each(function() {
        var content = $(this).html();

        if(content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);

            var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<div><a href="" class="read-more">' + moretext + '</a></div></span>';

            $(this).html(html);
        }

    });

    $(".read-more").click(function(){
        var content = $(this).closest('.content');
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        content.find('.moreellipses').toggle();
        content.find('.morecontent > span').toggle();
        return false;
    });
    //end promotion more


    
    $("#accordion").on('show.bs.collapse', function () {

        var eventOrigin = "storelocator store detail:service";

        let digitalData = window.digitalData || [];
        digitalData.events = digitalData.events || [];
        digitalData.events.push({
            "eventName": "storelocatorInteraction",
            "eventOrigin": eventOrigin,
            "useStorage": false,
        });
    });


});
