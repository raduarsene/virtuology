import GoogleMapsApi from 'load-google-maps-api';
import MarkerClusterer from './../../../block/map/google/google-markerclusterer';
import {GaEvents} from '../../../global/ga-events';
import Cookies from 'js-cookie';

export default class GoogleMap {
    // Default Google Maps options
    static get DEFAULT_SETTINGS() {
        return {
            maxMarkersSizeForClustering: 1,
            maxClusterZoom: 13,
            fitBounce: true,
            showMarkersClusterOnSearch: true,
            credentials: {key: null, key_premium: null},
            refreshControlCheckbox: true,
            language: null,
            markersShow: 10,
            map: {
                center: {lat: null, lng: null},
                zoom: null,
            },
            searchType: 'FIT_ALL',
            mapLibraries: ['geometry', 'places'],
            mapMouse: true, // Set mapMouse: false - if you want to not be able to drag and zoom the map with the mouse
            mapZoom: {position: null}, // Set mapZoom: false - if you want to hide zoom in/out
            mapFullScreen: true,
            restriction: null,
            marker: {
                icon: '/build/images/web/block/map/marker/icon.png',
                icon_width: 33,
                icon_height: 53,
                icon_center: '/build/images/web/block/map/marker/center.png',
                icon_width_center: 30,
                icon_height_center: 30,
                icon_selected: '/build/images/web/block/map/marker/icon.png',
                icon_width_selected: 53,
                icon_height_selected: 73,
            },
            marker_cluster: {
                images: '/build/images/web/block/map/marker/m',
                sizes: [{w: 53, h: 52}, {w: 56, h: 55}, {w: 66, h: 65}, {w: 78, h: 77}, {w: 90, h: 89}],
                textColor: '#ffffff',
            },
            autocompleteUrl: null,
            geoLocationCountry: null, // country name or a two letter ISO 3166-1 country code (it can be an array of countries up to 5)
            query: {
                page: null,
                size: null,
                near: null,
                radius: null,
                onEmptyRadius: null,
                within: null,

                orServiceKeys: null, //array
                brandSlugs: null, //array

                openOn: null, //string
                openNow: null, // boolean
                openOnSundays: null //boolean
            },

            // Events
            onAutocompletePlaceSelect: () => null,
            onCloseAllInfoWindows: () => null,
            onAddDefaultFilters: () => null,
            onLoaderListChange: () => null,
            onInfoWindowClose: () => null,
            onStorageDataLoad: () => null,
            onShopSelected: () => null,
            onClearFilters: () => null,
            beforeLoadData: () => null,
            onMarkerClick: () => null,
            clearSearch: () => null,
            onInit: () => null,

            currentSearchPosition: null,
            searchGaEvents: null,
        }
    };

    /**
     * Class constructor
     * @param {Object} settings
     * @param {TemplateCarrefour} TemplateClass
     */
    constructor(settings, TemplateClass) {
        // Classes
        this.TemplateClass = TemplateClass;

        // Objects
        this.refreshControlEl = $('.map-search-options');
        this.loadMoreBtn = $('[data-load-more]');

        // GOOGLE API objects
        this._googleMapApi = null;
        this._markersCluster = null;
        this._geocoder = null;
        this._map = null;



        // Var
        this._currentSearchAddress = '';
        this.proccessedMarkers = [];
        this.centerMarker = null;
        this.infoWindows = [];
        this.showOnSearch = false;
        this.selectedMarker = null;
        this.isFirstAPICall = true;
        this.queryData = {};

        this.settings = {...GoogleMap.DEFAULT_SETTINGS, ...settings};
        this.settings.visible_markers = this.settings.markersShow;
        this.locations = [];

        // Call on init class
        this.settings.onInit(this);

        //current tracking event
        this._trackingSearchEvent = "storelocator search:default";

        this.searchGaEvents = new GaEvents();
    }

    /**
     * Set refresh control checkbox
     */
    set refreshControlCheckbox(value) {
        this.settings.refreshControlCheckbox = value;
    }

    /**
     * Getter map credentials
     */
    get _mapCredentials() {
        var options = {};

        // Google map libraries
        if (this.settings.mapLibraries) {
            options.libraries = this.settings.mapLibraries;
        }

        // Google map language
        if (this.settings.language) {
            options.language = this.settings.language;
        }

        // Google map credentials
        var credentials = this.settings.credentials;
        if (typeof credentials.key_premium !== 'undefined' && credentials.key_premium) {
            options.client = credentials.key_premium;
        } else {
            options.key = credentials.key;
        }

        return options;
    }

    /**
     * Getter map options
     */
    get _mapOptions() {
        var mapOptions = {
            zoom: this.settings.map.zoom,
            center: this.settings.map.center,
            mapTypeId: this._googleMapApi.MapTypeId.ROADMAP,
            disableDefaultUI: true,
        };

        if (this.settings.mapMouse === false) {
            mapOptions.gestureHandling = 'none';
        }

        if (this.settings.mapZoom) {
            mapOptions.zoomControl = true;

            var mapPosition = this._googleMapApi.ControlPosition.LEFT_BOTTOM;
            if (typeof this.settings.mapZoom.position !== 'undefined' && this.settings.mapZoom.position) {
                mapPosition = this._googleMapApi.ControlPosition[this.settings.mapZoom.position.replace('-', '_').toUpperCase()];
            }
            mapOptions.zoomControlOptions = {position: mapPosition};
        }

        if (this.settings.mapFullScreen) {
            mapOptions.fullscreenControl = this.settings.mapFullScreen;
        }

        if (typeof this.settings.restriction !== 'undefined' && this.settings.restriction) {
            mapOptions.restriction = this.settings.restriction;
        }

        if (typeof this.settings.styles !== 'undefined' && this.settings.styles) {
            mapOptions.styles = this.settings.styles;
        }

        return mapOptions;
    }

    /**
     * Getter cluser options
     */
    get _clusterOptions() {
        var clusterStyles = [];

        for (var index = 0; index < 5; index++) {
            var clusterStyle = {
                url: this.settings.marker_cluster.images + (index + 1) + '.png',
                width: this.settings.marker_cluster.sizes[index].w,
                height: this.settings.marker_cluster.sizes[index].h,
            };

            if (typeof this.settings.marker_cluster.textColor !== 'undefined' && this.settings.marker_cluster.textColor) {
                clusterStyle.textColor = this.settings.marker_cluster.textColor;
            }

            clusterStyles.push(clusterStyle);
        }

        return {
            imagePath: this.settings.marker_cluster.images,
            styles: clusterStyles,
            averageCenter: true,
            maxZoom: this.settings.maxClusterZoom,
        };
    }

    /**
     * Getter is mobile
     */
    get _isMobile() {
        return navigator && navigator.userAgent 
            && /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    }

    /**
     * Create google map
     * @param {*} mapElement 
     */
    createMap(mapElement) {
        var _this = this;

        // Load Google Map API
        this._loadMapApi().then(function (MapApi) {
            // Initialize the map
            _this._initMap(MapApi, mapElement);

            // Add map events
            _this._addMapEvents();
        });
    }

    /**
     * Load map api
     */
    _loadMapApi() {
        return GoogleMapsApi(this._mapCredentials);
    }

    /**
     * Init map
     * @param {*} MapApi 
     * @param {*} mapElement 
     */
    _initMap(MapApi, mapElement) {
        this._googleMapApi = MapApi;

        this._map = new this._googleMapApi.Map(mapElement, this._mapOptions);

        this._geocoder = new this._googleMapApi.Geocoder();
    }

    /**
     * Attach map events  
     */
    _addMapEvents() {
        const _that = this;
        
        if (this._isMobile) {
            let lastSearchQuery = this._getCookieSearch();

            this.lastMobileZoom = this._map.getZoom();
            this._googleMapApi.event.addListener(this._map, 'idle', (event) => {
                const currentZoom = this._map.getZoom();
                if (_that.isFitBoundsCentred || _that.isFirstAPICall && !lastSearchQuery) {
                    _that.isFitBoundsCentred = false;
                    return;
                }

                _that._changeSearchType('WITHIN');

                if (_that.zoomChanged && 
                    typeof _that.zoomChanged === 'function' && 
                    this.lastMobileZoom !== currentZoom
                ) {
                    _that.zoomChanged();
                }

                if (_that.settings.refreshControlCheckbox || _that.isFirstAPICall && lastSearchQuery) {
                    _that._addMapData();
                } else {
                    this.refreshControlEl.addClass('need-map-refresh');
                }
            });
        } else {
            // Init map on load
            this._googleMapApi.event.addListenerOnce(this._map, 'tilesloaded', () => {
                _that._addMapData(true);
            });

            // On drag map
            this._googleMapApi.event.addListener(this._map, 'dragend', () => {
                _that._changeSearchType('WITHIN');
                
                if (_that.settings.refreshControlCheckbox) {
                    _that._addMapData();
                } else {
                    this.refreshControlEl.addClass('need-map-refresh');
                }
            });

            // On change zoom
            this._googleMapApi.event.addListener(this._map, 'zoom_changed', () => {
                if (_that.zoomChanged && typeof _that.zoomChanged === 'function') {
                    _that.zoomChanged();
                }

                if (_that.isFitBoundsCentred) {
                    _that.isFitBoundsCentred = false;
                    return;
                }

                _that._changeSearchType('WITHIN');

                if (_that.settings.refreshControlCheckbox) {
                    _that._addMapData();
                } else {
                    _that.refreshControlEl.addClass('need-map-refresh');
                }



            });

            //close info window on click
            this._googleMapApi.event.addListener(this._map, 'click', () => {

                _that._closeInfoWindows();
                return false;
            });

        }

        /**
         * Map refresh control events
         */
        this.refreshControlInput = $('#map_refresh_on_drag');
        this.refreshControlInput.on('change', this._onRefreshControlInputChange.bind(this));
        this.refreshControlInput.prop('checked', this.settings.refreshControlCheckbox);
        this.loadMoreBtn.on('click', this._onLoadMoreClick.bind(this));

        $(document).on('click', '.need-map-refresh', () => {
            _that._trackingSearchEvent = "storelocator map:search this area";
            _that._changeSearchType('WITHIN', true);
            _that.settings.clearSearch();
            _that.isMapRefresh = true;
            _that._addMapData();

            let digitalData = window.digitalData || [];
            digitalData.events=digitalData.events || [];
            digitalData.events.push({
                "eventName" : "storelocatorInteraction",
                "eventOrigin" : "storelocator map:search this area", //see "table 3" below
                "useStorage": false
            })

        });

        //zoom adobe events
        $(document).on('click', 'button[title="Zoom avant"]', function () {
            _that._changeZoomEvent("in");
        });
        $(document).on('click', 'button[title="Inzoomen"]', function () {
            _that._changeZoomEvent("in");
        });
        $(document).on('click', 'button[title="Zoom arrière"]', function () {
            _that._changeZoomEvent("out");
        });
        $(document).on('click', 'button[title="Uitzoomen"]', function () {
            _that._changeZoomEvent("out");
        });

        // Map search events
        this._addSearchEvents();
    }

    /**
     *
     * @param type
     * @private
     */
    _changeZoomEvent(type) {

        let digitalData = window.digitalData || [];
        digitalData.events=digitalData.events || [];
        digitalData.events.push({
            "eventName":"storelocatorInteraction",
            "eventOrigin":"storelocator map:zoom "+type,
            "useStorage": false
        });
    }

    /**
     * Change search type
     * @param {*} type 
     * @param canChange
     */
    _changeSearchType(type, action) {
        const canChangeType = this.settings.refreshControlCheckbox || action;

        if (
            !this.rejectChangeType && canChangeType || 
            !this.rejectChangeType && type !== 'WITHIN'
        ) {
            this.settings.searchType = type;
        }

        this.rejectChangeType = false;
    }

    /**
     * On load more click
     * @param {*} event 
     */
    _onLoadMoreClick(event) {
        this.settings.visible_markers = this.settings.visible_markers + this.settings.markersShow;
        let markers = this._getVisibleMarkers(this.settings.visible_markers);

        this._initMarkersCluster();
        this._addMarkersToMap(markers, true);
        this._updateSearchList(markers);
        this._setVisibleSearchListFooter(true);
        this._saveCookieSearch();
    }

    /**
     * On refresh control input change
     * @param {*} event 
     */
    _onRefreshControlInputChange(event) {
        const checked = event.currentTarget.checked;

        this.refreshControlCheckbox = checked;
    }

    /**
     * Add map data
     * @param animateMarkers
     * @param queries
     */
    _addMapData(animateMarkers, queries={}) {
        this._getAPILocationsData(animateMarkers, queries);
    }

    /**
     * Generate filters for search
     * @param {*} queries 
     */
    _getFiltersForSearch(queries = {}) {
        if (this.settings.searchType === 'FIT_ALL') {
            this.queryData = {};
        } else {
            this.queryData.page = this.settings.query.page;

            if (this.settings.searchType === 'NEAR') {
                const center = this._map.getCenter();

                delete this.queryData.within;

                if (!this.queryData.near) {
                    this.queryData.near = [center.lat(), center.lng()].join();
                }
            } else {
                delete this.queryData.near;
                delete this.queryData.radius;
                delete this.queryData.onEmptyRadius;

                // Set query bounds
                var bounds = this._map.getBounds();

                this.queryData.within = [
                    bounds.getNorthEast().lat(), 
                    bounds.getSouthWest().lng(), 
                    bounds.getSouthWest().lat(), 
                    bounds.getNorthEast().lng(),
                ].join();
            }

            this.queryData = $.extend(this.queryData, queries);
        }

        const lastSearchQuery = this._getCookieSearch();
       
        if (this.isFirstAPICall) {         
            this._setQueryesFromCookies(lastSearchQuery);
            this.queryData = this.serialaizeQueryData(this.queryData);
            this.settings.onAddDefaultFilters(this.queryData);
            return 
        }

        this.queryData = this.serialaizeQueryData(this.queryData);

        if (this.settings.searchType !== 'FIT_ALL') {
            this._saveCookieSearch();   
        }
    }

    /**
     * Serialize querry data
     * @param {*} queries 
     */
    serialaizeQueryData(queries) {
        let results = {};

        for (let prop in queries) {
            if (Array.isArray(queries[prop])) {
                results[prop] = queries[prop];
            } else {
                if (queries[prop]) {
                    results[prop] = queries[prop];
                }
            }
        }

        return results;
    }

    /**
     * Set queries from cookies
     * @param {*} lastSearchQuery 
     */
    _setQueryesFromCookies(lastSearchQuery) {
        if (lastSearchQuery && lastSearchQuery.query) {
            this.settings.searchType = lastSearchQuery.searchType;
            this.queryData = lastSearchQuery.query;
        }
    }

    /**
     * Get cooikes last search
     */
    _getCookieSearch() {
        const lastSearchQuery = Cookies.get('last_search_query');
        const data = lastSearchQuery ? JSON.parse(lastSearchQuery) : null;

        return data ? {
            currentSerachAddress: data.currentSerachAddress ? data.currentSerachAddress : '',
            visible_markers: data ? data.visible_markers : this.settings.visible_markers,
            searchType: data.searchType,
            query: data.query,
        } : null;
    }

    /**
     * Save last search data to cookies
     */
    _saveCookieSearch() {
        const now = new Date();

        now.setHours(now.getHours() + 1);

        Cookies.set('last_search_query', JSON.stringify({
            currentSerachAddress: this._currentSearchAddress ? this._currentSearchAddress : '',
            visible_markers: this.settings.visible_markers,
            searchType: this.settings.searchType,
            query: this.queryData,
        }), {
            expires: now,
        });
    }

    /**
     * Remove last search data from cookies
     */
    _removeCookieSearch() {
        Cookies.remove('last_search_query');
    }

    /**
     * Update filtres
     * @param {*} queries 
     */
    _updateFiltres(queries={}) {
        if (queries.openOnSundays) {
            this.queryData.openNow = false;
            this.queryData.openOn = null;
        } else if (queries.openNow) {
            this.queryData.openOnSundays = false;
            this.queryData.openOn = null;
        } else if (queries.openOn) {
            this.queryData.openOnSundays = false;
            this.queryData.openNow = false;
        }

        this._getFiltersForSearch(queries);
        
        if (!this.settings.refreshControlCheckbox) {
            this.refreshControlEl.addClass('need-map-refresh');
        } else {
            this._addMapData();
        }
    }

    /**
     * Get api locations
     * @param animateMarkers
     * @param queries
     */
    _getAPILocationsData(animateMarkers, queries = {}) {
        const _this = this;

        this._getFiltersForSearch(queries);

        if (this.getLocationsRequest && typeof this.getLocationsRequest.abort === 'function') {
            this.getLocationsRequest.abort();
        }
        
        this.settings.beforeLoadData(this.settings.searchType);
        this.isFitBoundsCentred = false;
        this._toggleResultsLoader(true);
        this._clearSearchList();

        const lastSearchQuery = this._getCookieSearch();

        if (this.isFirstAPICall) {
            this._addLocalStorgeMarkers(lastSearchQuery);
        } else {
            this._setVisibleSearchListFooter(false);
            this.settings.visible_markers = this.settings.markersShow;
        }

        // Get Locations from API
        this.getLocationsRequest = $.get(globalData.routes.apiLocations, this.queryData, function (data) {
            let visible_markers = _this.settings.visible_markers;
            _this.locations = data;

            if (!_this._fromLocalStorge && _this.settings.searchType === 'FIT_ALL') {
                visible_markers = _this.locations.length;
            }

            let markers = _this._getVisibleMarkers(visible_markers);
            
            // Init markers on map
            _this.getLocationsRequest = null;
            _this._initMarkersCluster();

            if (data && data.length) {
                // Add markers to map
                _this._addMarkersToMap(markers, _this.wasAddedLocalStorgeMarkers ? false : animateMarkers);
            } else {
                if (this.wasAddedLocalStorgeMarkers) {
                    _this._initMarkersCluster();
                    _this._clearSearchList();
                }

                _this._addCenterMarker();
            }

            if (_this.settings.searchType !== 'FIT_ALL') {
                _this._setLocalStorageData(data);


                if(!_this.isFirstAPICall) {
                    _this.searchGaEvents.gaSearchInputEvent(_this._currentSearchAddress, data.length);
                }


            }
            
            _this._updateSearchList(markers);
            _this._toggleResultsLoader(false, data);
            _this._setVisibleSearchListFooter(true);
            _this.wasAddedLocalStorgeMarkers = false;
            _this.rejectChangeType = false;
            _this.isFirstAPICall = false;
            _this.isMapRefresh = false;

            //track search
            _this._trackSearchLocations(queries);
        });

        this.getLocationsRequest.catch(() => {
            _this._toggleResultsLoader(false, [], true);
            _this.wasAddedLocalStorgeMarkers = false;
            _this._updateSearchList(_this.locations);
            _this.isFirstAPICall = false;
        })
    }

    /**
     * Get visible markers
     * @param {*} number 
     */
    _getVisibleMarkers(number) {
        let results = [];

        this.settings.visible_markers = number;

        if (number < this.locations.length) {
            results = this.locations.slice(0, this.settings.visible_markers);
        } else {
            results = this.locations;
        }

        return results;
    }

    /**
     * Set search list footer visible/hidden
     * @param {*} action 
     */
    _setVisibleSearchListFooter(action) {
        const parent = this.loadMoreBtn.closest('.search-list-actions');
        
        parent[action ? 'removeClass' : 'addClass']('hidden-searchlist-footer');

        if (this.settings.visible_markers < this.locations.length) {
            parent.find('.button[data-load-more]').removeClass('hidden');
            parent.find('.button[data-change-screen]').addClass('hidden');
            parent.find('.no-results-text').addClass('hidden');
        } else {
            parent.find('.button[data-load-more]').addClass('hidden');
            parent.find('.button[data-change-screen]').removeClass('hidden');
            parent.find('.no-results-text').removeClass('hidden');
        }
    }

    /**
     * Set localstorge data
     * @param {*} data 
     */
    _setLocalStorageData(data = []) {
        if (window.localStorage) {
            if (data.length) {
                window.localStorage.setItem('last_search', JSON.stringify({
                    search_type: this.settings.searchType,
                    items: data,
                }));
            } else {
                window.localStorage.removeItem('last_search');
            }
        }
    }

    /**
     * Add local storge markers
     * @param {*} query 
     */
    _addLocalStorgeMarkers(query) {
        if (window.localStorage) {
            const lastSearch = window.localStorage.getItem('last_search');
            const data = window.localStorage.getItem('last_search') ? JSON.parse(lastSearch) : { items: [] };

            if (Array.isArray(data.items) && data.items.length && (query || data.search_type === 'FIT_ALL')) {
                this.wasAddedLocalStorgeMarkers = true;
                this.locations = data.items;
                this.showOnSearch = true;

                let visibleMarkers = query ? query.visible_markers : this.settings.visible_markers;
                let markers = this._getVisibleMarkers(data.search_type === 'FIT_ALL' ? this.locations.length : visibleMarkers);

                this._initMarkersCluster();
                this._addMarkersToMap(markers, false);
                this._updateSearchList(markers);
                this._toggleResultsLoader(false, data.items);
                this.settings.onStorageDataLoad({
                    searchType: this.settings.searchType,
                    ui: {
                        item: {
                            value: query ? query.currentSerachAddress : '',
                        },
                    },
                    items: data.items,
                    query,
                });

                this._setVisibleSearchListFooter(true);
                this._fromLocalStorge = true;
            }
        }
    }

    /**
     * Center map depend of markers
     * @param list
     */
    _fitBounds(list = [], callback = () => {}, centerPosition = null) {
        const now = new Date();
        const markers = list.length ? list : this.proccessedMarkers;
        const bounds = new this._googleMapApi.LatLngBounds();
        let timeAnimation = now.getTime();
        
        if (!markers.length) {
            return;
        }
        
        markers.map((marker) => {
            bounds.extend(marker.getPosition());
            return marker;
        });

        if(centerPosition) {

            bounds.extend(centerPosition);
        }

        this.isFitBoundsCentred = true;
        this.zoomChanged = () => {
            const newdate = new Date();

            timeAnimation = newdate.getTime() - timeAnimation;

            callback(timeAnimation);
            this.zoomChanged = null;
        };

        if (this._isMobile) {
            callback(200);
            this.zoomChanged = null;
        }

        this._map.fitBounds(bounds);
    }

    /**
     * Clear search list
     */
    _clearSearchList() {
        var searchResultsList = $('.search-results ul');

        searchResultsList.html('');
    }

    /**
     * Add markers to map
     * @param {*} data 
     * @param animateMarkers
     */
    _addMarkersToMap(data, animateMarkers) {
        var _this = this;

        if (!this.markersLayer) {
            this.markersLayer = new this._googleMapApi.OverlayView();
            this.markersLayer.draw = function () {
                this.getPanes().markerLayer.id='markerLayer';
            };
            
            this.markersLayer.setMap(this._map);
        }

        this.proccessedMarkers = data.map(function (item, index) {

            var marker = new _this._googleMapApi.Marker(_this._getMarkerOptions(item));

            if (animateMarkers) {
                marker.setAnimation(_this._googleMapApi.Animation.DROP);
            }

            _this._addMarkerInfoWindow(marker, item);
        
            return marker;
        });

        // Add a marker cluster to manage the markers.
        this._markersClusterActions();
    }

    /**
     * Detect if it's clouster or fit bounce and set map zoom
     */
    _markersClusterActions() {
        const clusteringMarkers = this.settings.maxMarkersSizeForClustering < this.proccessedMarkers.length;

        // Fit all marker bounds on map
        if (clusteringMarkers && this.settings.showMarkersClusterOnSearch || this.settings.searchType === 'FIT_ALL') {
            this.isFitBoundsCentred = true;
            this._markersCluster = null;

            if (
                this.settings.searchType === 'WITHIN' && this.isFirstAPICall ||
                this.settings.searchType === 'NEAR'
            ) {
                this._markersCluster = new MarkerClusterer(this._map, [], this._clusterOptions);
                let allMarkers = this.proccessedMarkers;
                const prevZoom = this._map.getZoom();

                if (this.centerMarker) {
                    allMarkers = allMarkers.concat([this.centerMarker]);
                }

                this._fitBounds(allMarkers, (time) => {
                    const nextZoom = this._map.getZoom();

                    this._markersCluster = new MarkerClusterer(this._map, [], this._clusterOptions);
                    setTimeout(()=> {
                        this._markersCluster.addMarkers(this.proccessedMarkers, false);
                    }, prevZoom !== nextZoom ? (time * 100) + 250 : 0);
                });
            } else {
                this._markersCluster = new MarkerClusterer(this._map, this.proccessedMarkers, this._clusterOptions);

                if (this.settings.searchType === 'FIT_ALL') {
                    this._markersCluster.fitMapToMarkers();
                }

                this._addCenterMarker(this._map.getBounds().getCenter());
                this._centerMap();
            }
        } else if (!clusteringMarkers) {
            this._markersCluster = null;

            var centerPosition = this._addCenterMarker(this._map.getBounds().getCenter());

            if (this.settings.searchType !== 'WITHIN') {
                this._fitBounds(this.proccessedMarkers, () => {}, centerPosition);
            }

        }
    }

    /**
     * Center map
     */
    _addCenterMarker(centerPosition) {
        if (this.centerMarker) {
            this.centerMarker.setMap(null);
        }

        if (Boolean(this.queryData.within) || Boolean(this.queryData.near)) {
            if (!this.centerMarker) {
                this.centerMarker = new this._googleMapApi.Marker();

                if (Boolean(this.settings.marker.icon_center)) {
                    this.centerMarker.setIcon({
                        url: this.settings.marker.icon_center,
                        scaledSize: new this._googleMapApi.Size(
                            this.settings.marker.icon_width_center, 
                            this.settings.marker.icon_height_center,
                        ),
                    });
                }
            }

            var position;

            if (Boolean(this.queryData.near)) {
                var locations = this.queryData.near.split(',');

                position = new this._googleMapApi.LatLng(locations[0], locations[1])
            } else {
                position = this._map.getCenter();
            }

            this.centerMarker.setMap(this._map);
            this.centerMarker.setPosition(centerPosition || position);

            return centerPosition || position;

        }
    }

    /**
     * Get marker options
     * @param {*} item 
     */
    _getMarkerOptions(item) {
        var markerOptions = {
            map: this._map,
            position: new this._googleMapApi.LatLng(item.address.latitude, item.address.longitude),
            title: item.name,
            externalId: item.externalId,
            optimized: false,
        };

        if (typeof this.settings.marker.icon !== 'undefined') {
            var markerUrl = this.settings.marker.icon,
                markerName = 'default';

            if (typeof globalData.brandSlugs !== 'undefined') {
                if (item.brandSlug && globalData.brandSlugs.includes(item.brandSlug)) {
                    markerName = item.brandSlug;
                } else {
                    // Random marker icon
                    markerName = globalData.brandSlugs[Math.floor((Math.random() * globalData.brandSlugs.length))];
                }
            }

            markerOptions.markerName = markerName;
            markerOptions.icon = {
                url: `${markerUrl.replace('default', markerName)}?markerID=${item.externalId}!`,
                scaledSize: new this._googleMapApi.Size(this.settings.marker.icon_width, this.settings.marker.icon_height),
            };
        }

        return markerOptions;
    }

    /**
     * Change selected marker
     * @param {*} marker 
     */
    _changeSelectedMarker(marker) {
        if (typeof this.settings.marker.icon_selected !== 'undefined' && this.settings.marker.icon_selected) {
            if (this.selectedMarker) {
                this._addDefaultMarkerIcon(this.selectedMarker);
            }
            this.selectedMarker = marker;

            var markerUrl = this.settings.marker.icon_selected,
                markerName = 'default';

            if (typeof marker.markerName !== 'undefined' && marker.markerName && typeof globalData.brandSlugs !== 'undefined'
                && globalData.brandSlugs.includes(marker.markerName)
            ) {
                markerName = marker.markerName;
            }

            marker.setIcon({
                url: `${markerUrl.replace('default', markerName)}?markerID=${marker.externalId}!`,
                scaledSize: new this._googleMapApi.Size(this.settings.marker.icon_width_selected, this.settings.marker.icon_height_selected),
            });
        }
    }

    /**
     * Add default marker icon
     * @param {*} marker 
     */
    _addDefaultMarkerIcon(marker) {
        if (typeof this.settings.marker.icon !== 'undefined') {
            var markerUrl = this.settings.marker.icon,
                markerName = 'default';

            if (typeof marker.markerName !== 'undefined' && marker.markerName && typeof globalData.brandSlugs !== 'undefined'
                && globalData.brandSlugs.includes(marker.markerName)
            ) {
                markerName = marker.markerName;
            }

            marker.setIcon({
                url: `${markerUrl.replace('default', markerName)}?markerID=${marker.externalId}!`,
                scaledSize: new this._googleMapApi.Size(this.settings.marker.icon_width, this.settings.marker.icon_height),
            });
        }
    }

    /**
     * Init marker cluster
     */
    _initMarkersCluster() {
        this.proccessedMarkers.map((marker) => {
            marker.setMap(null);

            return marker;
        })

        this.proccessedMarkers = [];
        this.infoWindows = [];

        if (this._markersCluster) {
            this._markersCluster.clearMarkers();
        }
    }

    /**
     * Add marker infow window
     * @param {*} marker 
     * @param {*} item 
     */
    _addMarkerInfoWindow(marker, item) {
        const infoWindowContent = this.TemplateClass.getLocationTemplate(item, $('#map_container').data('prototype'), false);
        const _that = this;
        const infoWindow = new this._googleMapApi.InfoWindow({
            content: infoWindowContent,
            externalId: item.externalId,
        });

        marker.addListener('click', () => _that._onMarkerClick(infoWindow, marker));
        infoWindow.addListener('closeclick', this._onInfoWindowCloseClick.bind(this));

        this.infoWindows.push(infoWindow);
    }

    /**
     * On marker click
     * @param {*} infoWindow 
     * @param {*} marker 
     */
    _onMarkerClick(infoWindow, marker) {
        this._showInfoWindow(infoWindow, marker);
        this.settings.onMarkerClick(infoWindow, marker);

        //send also event on marker click
        var eventOrigin = "storelocator map:pin";
        let digitalData = window.digitalData || [];
        digitalData.events = digitalData.events || [];
        digitalData.events.push({
            "eventName": "storelocatorInteraction",
            "eventOrigin": eventOrigin,
            "useStorage": false
        });


    }

    /**
     * On info window close click
     */
    _onInfoWindowCloseClick() {
        if (this.selectedMarker) {
            this.settings.onInfoWindowClose(this.selectedMarker);
            this._addDefaultMarkerIcon(this.selectedMarker);
            this.selectedMarker = null;
        }
    }

    /**
     * Show info window
     * @param {*} infoWindow 
     * @param {*} marker 
     */
    _showInfoWindow(infoWindow, marker) {
        this._closeInfoWindows();
        
        this._map.setCenter(marker.getPosition());
        marker.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);

        // change marker to selected
        this._changeSelectedMarker(marker);

        infoWindow.open(this._map, marker);
    }

    /**
     * On close all info windows
     */
    _closeInfoWindows() {
        for (var index = 0; index < this.infoWindows.length; index++) {
            this.infoWindows[index].close();
        }

        if (this.selectedMarker) {
            this._addDefaultMarkerIcon(this.selectedMarker);
            this.selectedMarker.setZIndex(1);
        }
    }

    /**
     * Stop all animations on markers
     */
    _stopAnimationOnMarkers() {
        this.proccessedMarkers.map((marker) => {
            marker.setAnimation(null);
            return marker;
        });
    }

    /**
     * Update search list
     * @param {*} data 
     */
    _updateSearchList(data =[]) {
        var searchResultsBlock = $('#block_map .search-results');

        if (searchResultsBlock.length && (this.showOnSearch
            || typeof this.queryData.near !== 'undefined' && this.queryData.near
            || typeof this.queryData.within !== 'undefined' && this.queryData.within)
        ) {
            this.showOnSearch = false;

            // Init location search results
            var searchResultsList = searchResultsBlock.find('.search-results-list ul');
            searchResultsList.html('');

            var resultsBlock = this.TemplateClass.getLocationsTemplates(data, searchResultsBlock.data('prototype'), false);
            searchResultsList.append(resultsBlock);

            searchResultsList.find('.show-open-window').on('click', this.onShopListClick.bind(this));
        }
    }

    /**
     * On shop list click
     */
    onShopListClick(e) {
        if ($(e.currentTarget).is('a') || $(e.currentTarget).parent().is('a')) {
            return;
        }

        e.preventDefault();

        const externalId = $(e.currentTarget).data('id').toString();

        if (this.proccessedMarkers.length) {
            $.each(this.proccessedMarkers, (index, proccessedMarker) => {
                const markerExternalId = proccessedMarker.externalId.toString();

                if (markerExternalId === externalId) {
                    $.each(this.infoWindows, (key, infoWindow) => {
                        if (infoWindow.externalId.toString() === externalId) {
                            this._stopAnimationOnMarkers();
                            this._zoomCluster(proccessedMarker, () => {
                                this.settings.onShopSelected($(this), infoWindow, proccessedMarker, externalId);
                                this._showInfoWindow(infoWindow, proccessedMarker);
                            });
                        
                            return false;
                        }
                    });

                    return false;
                }
            })
        }

        let digitalData = window.digitalData || [];
        digitalData.events=digitalData.events || [];
        digitalData.events.push({
            "eventName": "storelocatorInteraction",
            "eventOrigin": "storelocator store list:store card"
        });

    }

    /**
     * Zoom clouser markers
     * @param {*} callback 
     */
    _zoomCluster(proccessedMarker, callback = () => {}) {
        if (this._markersCluster) {
            this._markersCluster.clusters_.map((cluster) => {
                if (cluster) {
                    const markers = cluster.getMarkers() || [];

                    markers.map((marker) => {
                        if (marker.externalId === proccessedMarker.externalId) {
                            this.refreshControlEl.addClass('need-map-refresh');
                            this._map.setZoom(this.settings.maxClusterZoom+1);
                            this._centerMap(marker);
                        }

                        return marker;
                    });
                }

                return cluster;
            });
        }

        callback();
    }

    /**
     * Add search events
     */
    _addSearchEvents() {
        this._currentPosition();
        this._searchAutocomplete();
    }

    /**
     * Get current position
     */
    _currentPosition() {
        var _this = this;

        var currentPositionButton = $('#map_current_position');
        if (currentPositionButton.length) {
            currentPositionButton.on('click', function (e) {
                e.preventDefault();

                if (typeof navigator !== 'undefined' && typeof navigator.geolocation !== 'undefined' && navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        _this._geocoder.geocode({location: {lat: position.coords.latitude, lng: position.coords.longitude}}, function (results, status) {
                            if (_this._googleMapApi.GeocoderStatus.OK === status) {
                                var geolocationCountry = results[0].address_components.filter(
                                    components => {return components.types.includes('country');}
                                )[0].short_name;
                                
                                if (geolocationCountry === _this.settings.geoLocationCountry) {
                                    _this._showLocationsNear(position.coords.latitude, position.coords.longitude);
                                    const resultItem = _this._getGeocodeFormatedAddress(results[0]);

                                    if (resultItem) {

                                        $("#autocomplete_search_error_geo").hide();

                                        _this._trackingSearchEvent="storelocator search:geolocalisation";
                                        _this._centerMapBySearch(resultItem.parsedName, true);
                                        _this.settings.onAutocompletePlaceSelect({
                                            item: {
                                                value: resultItem.parsedName,
                                            },
                                        });
                                    }
                                } else {
                                    _this._showAllLocations();

                                    $("#autocomplete_search_error_geo").show();
                                }
                            } else {
                                _this._showAllLocations();
                            }
                        });
                    }, function () {
                        _this._showAllLocations();
                    });
                } else {
                    _this._showAllLocations();
                }
            })
        }
    }

    _showAllLocations() {
        this._initMarkersCluster();
        this._markersCluster = null;
        this._addMapData(true);
    }

    /**
     * On autocomplete focus
     * @param {*} event 
     */
    _onAutocompleteInputFocus(event) {
        this._closeInfoWindows();
    }

    /**
     * Search locations
     */
    _searchAutocomplete() {
        const searchAutocompleteInput = $('#map_search_autocomplete');
        const _this = this;

        if (searchAutocompleteInput.length) {

            searchAutocompleteInput.keypress(function (e) {
                var key = e.which;
                if(key == 13) {
                    return false;

                }
            });

            searchAutocompleteInput.on('focus', this._onAutocompleteInputFocus.bind(this));

            var requestTerm = '';
            searchAutocompleteInput.autocomplete({
                minLength: 2,
                autoFocus: true,
                focus: (event) => {
                    event.preventDefault();
                },
                source: (request, response) => {
                    requestTerm = request.term;
                    $.get(_this.settings.autocompleteUrl, {
                        language: _this.settings.language,
                        zipcity: request.term,
                    }, (data) => {
                        if (data && data.length) {
                            $('#autocomplete_search_error').hide();
                            $('#autocomplete_search_info').show();
                        } else {
                            searchAutocompleteInput.removeClass('ui-autocomplete-loading');
                            $('#autocomplete_search_info').hide();
                            $('#autocomplete_search_error').show();
                        }

                        response($.map(data, function (item) {
                            return {
                                value: item.value,
                                label: item.label,
                            };
                        }));
                    }).fail(()=> {
                        searchAutocompleteInput.removeClass('ui-autocomplete-loading');
                        $('#autocomplete_search_info').hide();
                        $('#autocomplete_search_error').show();

                        response($.map([], function (item) {
                            return {
                                value: item.value,
                                label: item.label,
                            };
                        }));
                    });
                },
                select: (event, ui) => {

                    $("#autocomplete_search_error_geo").hide();

                    if(requestTerm*1 > 0) {
                        _this._trackingSearchEvent = "storelocator search:numbers";
                    }
                    else {
                        _this._trackingSearchEvent = "storelocator search:words";
                    }

                    let location = ui.item.value.replace(/[0-9]/g, '');

                    if (_this._isMobile) {
                        _this.isFirstAPICall = false;
                    }

                    _this.settings.visible_markers = _this.settings.markersShow;
                    _this._currentSearchAddress = ui.item.value;
                    _this.settings.searchType = 'NEAR';
                    _this._clearFilters();
                    _this._initMarkersCluster();
                    _this._clearSearchList();
                    _this._centerMapBySearch(location, true);
                    _this._setVisibleSearchListFooter(false);
                    _this.settings.onAutocompletePlaceSelect(ui);
                }
            }).data("ui-autocomplete")._renderItem = (ul, item) => {
                return $('<li class="ui-menu-item"></li>')
                    .data('value', item)
                    .append('<div class=\"ui-menu-item-wrapper\">' + item.label + '</div>')
                    .appendTo(ul);
            };
        }
    }

    /**
     * Clear search filters
     */
    _clearFilters() {
        this.queryData.openOnSundays = null;
        this.queryData.orServiceKeys = null;
        this.queryData.brandSlugs = null;
        this.queryData.openNow = null;
        this.queryData.openOn = null;
        this.settings.onClearFilters();
        
        Cookies.remove('last_search_query');
    }

    /**
     * Center map
     * @param {*} address 
     */
    _centerMapBySearch(address, animateMarkers) {
        var _this = this;

        this._geocoder.geocode({'address': address}, function (results, status) {
            if (_this._googleMapApi.GeocoderStatus.OK === status) {
                var position = results[0].geometry.location;
                _this.currentSearchPosition = position;
                _this._showLocationsNear(position.lat(), position.lng(), animateMarkers);
            }
        });
    }

    /**
     * Get geocode dormated address
     * @param {*} result 
     */
    _getGeocodeFormatedAddress(result = { address_components: [] }) {
        const address = {
            zipCode: null,
            country: null,
            city: null,
        };

        result.address_components.map((item) => {
            if (item.types.includes('country')) {
                address.country = item.long_name;
            }

            if (item.types.includes('locality')) {
                address.city = item.long_name;
            }

            if (item.types.includes('postal_code')) {
                address.zipCode = item.long_name;
            }
            return item;
        });

        return {
            parsedName: `${address.zipCode ? `${address.zipCode}, ` : ''}${address.city ? `${address.city}, ` : ''}${address.country ? `${address.country}` : ''}`,
            address,
        }
    }

    /**
     * Center marker
     */
    _centerMap(item) {
        const marker = item ? item : this.centerMarker;

        if (marker) {
            this._map.setCenter(marker.getPosition());
        }
    }

    /**
     * Show near location
     * @param {*} latitude 
     * @param {*} longitude 
     */
    _showLocationsNear(latitude, longitude, animateMarkers) {
        this.queryData.near = [latitude, longitude].join();
        this.queryData.radius = this.settings.query.radius;
        this.queryData.onEmptyRadius = this.settings.query.onEmptyRadius;
        this.settings.searchType = 'NEAR';
        this.rejectChangeType = true;
        this.showOnSearch = true;

        this._addCenterMarker(this.currentSearchPosition);
        this._centerMap(this.centerMarker);
        this._map.setZoom(10);
        this._addMapData(animateMarkers);
    }

    /**
     * Toggle show result laoder
     * @param {*} el 
     * @param {*} show 
     */
    _toggleResultsLoader(show, data, isError) {
        this.refreshControlEl[ show ? 'addClass' : 'removeClass']('loading');
        this.settings.onLoaderListChange(show, data, isError);

        if (!show && this.refreshControlEl.hasClass('need-map-refresh')) {
            this.refreshControlEl.removeClass('need-map-refresh');
        }
    }

    _trackSearchLocations(queries) {


        if(this.settings.searchType == 'FIT_ALL') {
            return;
        }



        var _this = this;

        let digitalData = window.digitalData || [];
        digitalData.events=digitalData.events || [];

        if($("#map_search_results_block").hasClass('show')) {
            var pageName = "storelocator store list";
        }
        else {
            var pageName = "storelocator search";
        }

        let storeIDs = [];
        let refinements = {};

        $.each(this.locations, function(index, value) {

            if(index < _this.settings.visible_markers) {

                var externalId = _this._transformExternalId(value.externalId);

                storeIDs.push(externalId);
            }

        });

        if(typeof queries.brandSlugs !== "undefined" && queries.brandSlugs) {
            refinements.storeTypes = queries.brandSlugs;
        }
        if(typeof queries.orServiceKeys !== "undefined" && queries.orServiceKeys) {

            var serviceKeysTracking = [];
            $.each(queries.orServiceKeys, function (index, value) {

                //services:[]//an array with the values that have been ticked:"online shopping","simply you box","shipTo"

                if(value == "CAR_TRUNK") {
                    serviceKeysTracking.push("online shopping");
                }
                if(value == "SMPYBOX") {
                    serviceKeysTracking.push("simply you box");
                }
                if(value == "SHIPTO") {
                    serviceKeysTracking.push("shipTo");
                }

            });

            refinements.services = serviceKeysTracking;
        }

        //open date
        if(typeof queries.openOn !== "undefined" && queries.openOn) {
            refinements.openingTimes = queries.openOn.substring(0,10);
        }
        if(typeof queries.openOnSundays !== "undefined" && queries.openOnSundays) {
            refinements.openingTimes = "sunday";
        }
        if(typeof queries.openNow !== "undefined" && queries.openNow) {
            refinements.openingTimes = "now";
        }

        digitalData.events.push({

            "eventName":"pageView",
            "eventAction":"click",
            "pageInfo":{
                "pageName": pageName,
                "pageType": "storelocator",
                "language": this.settings.language
            },
            "eventOrigin": this._trackingSearchEvent,
            "eventDetails": {
                storeIDs: storeIDs,
                refinements: refinements
            }
        });

        // console.log(digitalData);

    }

    /**
     *
     * @param externalId
     * @returns {*}
     * @private
     */
    _transformExternalId(externalId) {

        var externalIdLength = externalId.length;
        var externalIdReturn = externalId;
        for(var i=0; i<(4-externalIdLength); i++) {
            externalIdReturn = "0"+externalIdReturn;
        }

        return externalIdReturn;
    }
}
