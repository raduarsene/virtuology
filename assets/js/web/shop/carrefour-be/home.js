// require jQuery
import $ from 'jquery';
window.$ = $;
window.jQuery = $;

// Datetime/timezone
import moment from 'moment-timezone';
window.moment = moment;

// Jquery ui autocomplete
import 'jquery-ui/ui/widgets/autocomplete';

import 'bootstrap';

// Import map class
import BrowserDetect from './components/BrowserDetect';
import MapListView from './components/MapListView';
import GoogleMap from './map/google';
import TemplateCarrefour from './helper/template';

$(document).ready(function () {
    const mapContainer = $('#map_container');
    const mapElement = mapContainer.find('#map_element');
	let defaultSettings = {}, MapObj;
    const customLoader = $('.custom-loading-block');
    const mapListView = new MapListView();
    const browserDetect = new BrowserDetect();
    const headerPromotion = $('.header_promotion');
    const body = $('body');

    if (headerPromotion.get(0)) {
        if (window.sessionStorage) {
            const showPromotion = !window.sessionStorage.getItem("promotion");
            const isVisible = headerPromotion.css('display') !== 'none';

            body[showPromotion && isVisible ?'addClass' : 'removeClass']('banner-promotion');
        }

        headerPromotion.find('a.btn.link').on('click', () => {
            body.removeClass('banner-promotion');
        })
    }

    if (mapElement.get(0)) {
        const mapElement = mapContainer.find('#map_element');

        defaultSettings = {
            credentials: mapContainer.data('credentials'),
            language: mapContainer.data('language'),
            /**
             * On google map class initialization
             */
            onInit: () => {
                mapListView.showResultsIfHaveCacheData();
            },
            /**
             * On autocomplete place select
             * @param {*} ui 
             */
			onAutocompletePlaceSelect: (ui) => {
                mapListView.activateListScreen('results', true);
                mapListView.addCurrentSearchValue(ui);
            },

            onAddDefaultFilters: (filters) => {
                mapListView.addDefaultFilters(filters);
            },
            /**
             * Before load data
             */
            beforeLoadData: (searchType) => {
                if (searchType === 'WITHIN') {
                    mapListView.clearSearch();
                }
            },
            clearSearch: () => {
                mapListView.clearSearch();
            },
            /**
             * On storge data loaded
             */
            onStorageDataLoad: ({searchType, ui}) => {
                if (searchType !== 'FIT_ALL') {
                    mapListView.activateListScreen('results', true);
                    mapListView.addCurrentSearchValue(ui);
                }
            },

            /**
             * Clear filters
             */
            onClearFilters() {
                mapListView.clearFilters();
            },

            /**
             * On loader list change 
             * @param {*} action 
             */
            onLoaderListChange: (action, results, isError) => {
                customLoader[action ? 'addClass' : 'removeClass']('show');
                mapListView.onLoaderListChange(action, results, isError);
            },
            /**
             * On marker click
             * @param {*} infoWindow 
             * @param {*} marker 
             */
            onMarkerClick: (infoWindow, marker) => {
                mapListView.selectItemFromList({
                    externalId: marker.externalId, 
                    needScroll: true,
                    marker, 
                });
            },
            /**
             * On info window close
             * @param {*} marker 
             */
            onInfoWindowClose: (marker) => {
                mapListView.unselectItemFromList({
                    externalId: marker.externalId,
                    marker,
                });
            },

            /**
             * On info window selected
             * @param {*} el 
             * @param {*} infoWindow 
             * @param {*} marker 
             */
            onShopSelected: (el, infoWindow, marker, externalId) => {
                mapListView.selectItemFromList({
                    animateMarker: true,
                    needScroll: false,
                    externalId, 
                    marker, 
                });
            }
        };

        // Combine shop settings with default settings
        if (typeof globalSettings !== 'undefined') {
            defaultSettings = {
                ...defaultSettings,
                ...globalSettings,
            };
        }

        MapObj = new GoogleMap(defaultSettings, new TemplateCarrefour());

        MapObj.createMap(mapElement.get(0));
        mapListView.setMapObj(MapObj);

        $(document).on('click','[data-itineraire]' , (event) => {
            const mobileOperationSystem = browserDetect.getMobileOperatingSystem();
            const el = $(event.currentTarget);
            const itineraire = el.data('itineraire').split(',');
            const location = {
                lat: 0,
                lng: 0,
            };

            if (MapObj._isMobile) {
                if (itineraire.length) {
                    location.lat = itineraire[0];
                    location.lng = itineraire[1];
                    
                    event.preventDefault();

                    if (mobileOperationSystem === 'android') {
                        window.location.href = `geo:${location.lat},${location.lng}`
                    } else if(mobileOperationSystem === 'ios') {
                        window.location.href = `maps://maps.google.com/maps?daddr=${location.lat},${location.lng}&amp;ll=`
                    }
                }   
            }
        });
    }
});
