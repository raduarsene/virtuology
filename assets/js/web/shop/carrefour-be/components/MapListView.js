import MapListFilters from './MapListFilters';
import BrowserDetect from './BrowserDetect';
import Cookies from 'js-cookie';

export default class MapListView extends MapListFilters {
  /**
   * Class constructor
   * @param {*} props 
   */
  constructor(props = {}) {
    super(props);

    this.elements = {
      searchListInput: $("#search_field_store input[type=text]"),
      autocompleteInput: $('#map_search_autocomplete'),
      mapElement: $('#map_container #map_element'),
      submitFiltersBtn: $('[data-submit-filters]'),
      resultMobileHeader: $('.results-mobile-top'),
      customLoader: $('.custom-loading-block'),
      resetSearchBtn: $('#map_search_reset'),
      resultWrapper: $('.results-wrapper'),
      searchWrapper: $('.search-wrapper'),
      searchResults: $('.search-results'),
      mapContainer: $('#map_container'),
      homePageSection: $('.home-page'),
      searchValue: $('.search-value'),
      filterBlock: $('.filter-block'),
      contentPage: $('.content-page'),
      sidebar: $('.sidebar'),
      headerFixed: $('.fixed-top'),
      window: $(window),
      body: $('body'),
    };

    this.settings = {
      ...props,
      mapObj: null,
    };

    this.browseDetect = new BrowserDetect();
    this.startPoint = { x: 0, y: 0 };
    this.endPoint = { x: 0, y: 0 };

    this.attachEvents();
  }
  
  /**
   * Get scroll offset
   */
  get getScrollOffset() {
    const {elements: {sidebar, resultMobileHeader, contentPage, body}, browseDetect: {_isMobile}} = this;
    let height = _isMobile ? sidebar.offset().top : resultMobileHeader.outerHeight(true);
  
    if (_isMobile && contentPage.get(0)) {
      height = (sidebar.next('.main-content').outerHeight(true) - 146) + contentPage.outerHeight(true);
      if (body.hasClass('banner-promotion')) {
        height= height - 48;
      }
    }

    // console.log(height, 'test', $(window).scrollTop(), $('.header_promotion').outerHeight(true));
    return height;
  }

  /**
   * windows scroll
   */
  onWindowScroll() {
    const { elements: {headerFixed, contentPage, homePageSection, body}} = this;

    if (body.hasClass('no-header')) {
      return;
    }

    if (window.pageYOffset > 0 ) {
      setTimeout(() => {
        body.addClass('scrolled');
      }, 10)
    } else {
      body.removeClass('scrolled');
    }

    if (window.pageYOffset === 0 && this.browseDetect._isMobile) {
      contentPage.removeClass('scrolled');
    }

    if (headerFixed.hasClass('scrolled') && window.pageYOffset > 0 ) {
      contentPage.addClass('scrolled');
      homePageSection.addClass('header-fixed');
    } else {
      contentPage.removeClass('scrolled');
      homePageSection.removeClass('header-fixed');
    }
  }

  /**
   * Show results screen if exist search in cache
   */
  showResultsIfHaveCacheData() {
    const lastSearchQuery = Cookies.get('last_search_query');
    const data = lastSearchQuery ? JSON.parse(lastSearchQuery) : null;

    if (data) {
      this.activateListScreen('results');
    } else {
      this.activateListScreen('search');
    }
  }

  /**
   * Clear search
   */
  clearSearch() {
    const { elements: {searchValue, autocompleteInput}} = this;

    searchValue.addClass('hide');
    autocompleteInput.val('');
  }

  /**
   * On toggle show ios bar from safari
   */
  iosHackBottomBarResize() {
    const height = window.innerHeight;
    const {body} = this.elements;
    
    this.browseDetect.getiPhoneModel();


    // if (this.isIphoneX()) {
    //   body.classList.add('is-iphone-x');
    // } else {
    //   body.classList.remove('is-iphone-x');
    // }

    if (body.hasClass('is-iphone-x')) {
      if (height > 720) {
        body.removeClass('is-small-view');
      } else {
        body.addClass('is-small-view');
      }
    } else if (body.hasClass('iphone_6_8')) {
      // console.log(height)
      if (height > 553) {
        body.removeClass('is-small-view');
      } else {
        body.addClass('is-small-view');
      }
    }  
  }

  /**
   * On loader list change
   * @param {*} action 
   * @param {*} data 
   */
  onLoaderListChange(action, data, isError) {
    const { elements: {searchResults, resultWrapper}} = this;

    resultWrapper[action ? 'addClass' : 'removeClass']('loading');
    if (isError) {
      return;
    }

    if (action && searchResults.hasClass('no-results')) {
      searchResults.removeClass('no-results');
    } else if (!action && !data.length) {
      searchResults.addClass('no-results');
    }
  }

  /**
   * Set map obj in list
   * @param {*} instance 
   */
  setMapObj(instance) {
    this.settings.mapObj = instance;
  }

  /**
   * Add current search value
   * @param {*} ui 
   */
  addCurrentSearchValue(ui) {
    const { elements: { searchValue, autocompleteInput }} = this;

    searchValue.text(ui.item.value);
    searchValue.removeClass('hide');
    
    setTimeout(() => {
      autocompleteInput.val('');
      autocompleteInput.blur();
    }, 0);
  }

  /**
   * Attach list events
   */
  attachEvents() {
    const { elements } = this;

    var _this = this;

    // List search input animation
    elements.searchListInput.on('focusout', this.listSearchInputAnimation.bind(null, false));
    elements.searchListInput.on('focus', this.listSearchInputAnimation.bind(null, true));
    
    //key up
    elements.searchListInput.on('keyup', this.onKeyDownSearchInput.bind(this));

    // Reset search
    elements.resetSearchBtn.on('click', this.resetSearchAutocompleteInput.bind(this));

    // On submit filters data
    elements.submitFiltersBtn.on('click', this.submitFilters.bind(this));

    // Autocomplete input focus
    elements.autocompleteInput
      .on('focus', this.onToggleFocus.bind(this, true))
      .on('blur', this.onToggleFocus.bind(this, false));

    // On scroll list
    if (this.browseDetect._isMobile) {
      elements.window.on('scroll', this.onScrollList.bind(this));
    } else {
      elements.sidebar.on('scroll', this.onScrollList.bind(this));
    }
    

    // windows on scroll
     $(window).on('scroll', this.onWindowScroll.bind(this));

    // Map container touch events
    elements.mapContainer.on('touchstart', this.onMapTouchStart.bind(this));

    // Sidebar touch events
    elements.sidebar.on('touchstart', this.onSidebarTouchStart.bind(this));
    elements.sidebar.on('touchmove', this.onSidebarTouchMove.bind(this));
    elements.sidebar.on('touchend', this.onSidebarTouchEnd.bind(this));


    this.iosHackBottomBarResize();
    window.addEventListener('resize', this.iosHackBottomBarResize.bind(this));

    // Hover element from list
    $(document)
      .on('mouseover',"#storeloc_list > li", this.onHoverListItem.bind(null, true))
      .on('mouseleave',"#storeloc_list > li", this.onHoverListItem.bind(null, false));

    // Handle screen change event
    $('[data-change-screen]').on('click', this.onHandleScreenChange.bind(this));

    $(document)
      .on('click',"#storeloc_list > li", () => {
          if (elements.homePageSection.hasClass('is-results-wrapper')) {
            elements.homePageSection.addClass('mobile');
          }
      });

    $('#accordion [data-toggle="collapse"]').on('click', () => {  
      $($(this).data('target')).collapse('toggle');
    });



    $(document).on('click', ".more_details", function (event) {

      var element = $(event.target);
      var eventOrigin = "";
      if(element.hasClass('cardInfo')) {
        eventOrigin = "storelocator store list: card info"
      }
      else {
        eventOrigin = "storelocator map:pin info"
      }


      //track more info events
      //track change screens
      var digitalData = window.digitalData || [];
      digitalData.events = digitalData.events || [];

      digitalData.events.push({
        "eventName":"storelocatorInteraction",
        "eventOrigin": eventOrigin,
        "useStorage": true,
      });

      // console.log(digitalData);
      // event.preventDefault();
      // return false;
    });
  }

  /**
   * On toggle focus
   * @param isFocus
   * @param {*} event 
   */
  onToggleFocus(isFocus, event) {
    const { elements: {body}} = this;

    body.toggleClass('keyboard-open', isFocus);
  }



  // Touch map events
  onMapTouchStart() {
    this.setMapMobileSearchResultFullHeight();
  }

  // Sidebar touch events
  onSidebarTouchStart(event) {
    const touches = event.touches ? event.touches[0] : {};

    this.startPoint.x = touches.pageX || 0;
    this.startPoint.y = touches.pageY || 0;
  }

  /**
   * On sidebar touch move
   * @param {*} event 
   */
  onSidebarTouchMove(event) {
    const touches = event.touches ? event.touches[0] : {};
    const {elements: {homePageSection}} = this;

    this.endPoint.x = touches.pageX || 0;
    this.endPoint.y = touches.pageY || 0;

    if (  
      homePageSection.hasClass('is-results-wrapper') &&
      this.startPoint.y < this.endPoint.y && 
      window.pageYOffset === 0
    ) {
      this.setMapMobileSearchResultFullHeight();
      event.stopImmediatePropagation()
      event.preventDefault();
    }
  }

  /**
   * On sidebar touch end
   * @param {*} event 
   */
  onSidebarTouchEnd(event) {}

  /**
   * Set mobile search resutl full height
   */
  setMapMobileSearchResultFullHeight() {
    const {elements: {homePageSection}} = this;

    if (homePageSection.hasClass('is-results-wrapper')) {
      homePageSection.addClass('mobile');
    }
  }

  isIphoneX() {
    const ratio = window.devicePixelRatio;
    const screen = {
      width: window.screen.width * ratio,
      height: window.screen.height * ratio,
    };

    return screen.width === 1125 && screen.height === 2436 || screen.width === 1242 && screen.height === 2688;
  }

    /**
   * Filter sticky on scroll
   */
  onScrollList() {
    const { elements: {filterBlock, sidebar, homePageSection, resultMobileHeader}} = this;
    const scrollItem = this.browseDetect._isMobile ? this.elements.window : sidebar;
    const height = this.getScrollOffset;

    if (!homePageSection.hasClass('is-results-wrapper') ) {
      return;
    }

    if (height <= scrollItem.scrollTop()) {
      filterBlock.addClass('fixed');
    } else if (filterBlock.hasClass('fixed')) {
      filterBlock.removeClass('fixed');
    }
  }

  /**
   * On input search key down handler
  */
  onKeyDownSearchInput(event) {
    const {elements: {searchListInput}} = this;
    const val = searchListInput.val();
    const inputContainer = searchListInput.closest('.relative');

    if (val && !inputContainer.hasClass('has-value')) {
      inputContainer.addClass('has-value');
    } else if (!val && inputContainer.hasClass('has-value')) {
      inputContainer.removeClass('has-value');
    }
  }

  /**
   * Submit filtres
   */
  submitFilters() {
    const {settings: {mapObj}} = this;
    const fields = this.formatData();

    if (mapObj && typeof mapObj._addMapData === 'function') {
      mapObj._addMapData(true, fields);
    }

    this.saveCurrentFiltres();
    this.activateListScreen('results');
  }

  /**
   * Activate screen name
   * @param {*} name 
   */
  activateListScreen(name) {
    const { elements: { homePageSection, searchResults } } = this;
    const next = $(`[data-screen="${name}"]`);
    const current = $('[data-screen].show');

    current.removeClass('show');
    next.addClass('show');

    if (name === 'results') {
      homePageSection
        .addClass('is-results-wrapper')
        .removeClass('mobile');
    } else {
      homePageSection
        .removeClass('is-results-wrapper mobile');
    }

    if (name === 'search' && searchResults.hasClass('no-results')) {
      searchResults.removeClass('no-results');
    }

    if (current.data('screen') === 'filters' && !this.filtersOn) {
      this.clearFilters();
    }
  }

  /**
   * On handle screen change
   */
  onHandleScreenChange() {
    const el = $(event.currentTarget);
    const screenName = el.attr('data-change-screen');

    this.activateListScreen(screenName);

    if(screenName == "search") {

      //track change screens
      var digitalData = window.digitalData || [];
      digitalData.events=digitalData.events || [];

      digitalData.events.push({
        "eventName":"pageView",
        "eventAction":"click",
        "pageInfo":{

          "pageName": "storelocator search",//a value describing the screen, see "table 1" below
          "pageType":"storelocator",
          "language" : this.settings.language// "FR" or "NL"
        },
        "eventOrigin": "storelocator store list:back", // a value describing what was clicked to arrive to the screen, see "table 1" below
        "eventDetails": {}//an object containing extra information, see "table2" below
    });

      $("#autocomplete_search_error_geo").hide();

    }
  }

  /**
   * Reset search autocomplete input
   */
  resetSearchAutocompleteInput(event) {
    const { elements: { autocompleteInput, searchListInput } } = this;

    event.preventDefault();

    if (!autocompleteInput.hasClass('ui-autocomplete-loading')) {
      searchListInput.closest('.relative').removeClass('has-value');
      autocompleteInput.val('');
    }
  }

  /**
   * Hover list item
   * @param {*} isHover 
   * @param {*} event 
   */
  onHoverListItem(isHover, event) {
    const el = $(event.currentTarget);
    const hasParentMap = el.closest('.map-element');

    if (hasParentMap.get(0) || el.hasClass('active')) {
        return;
    }

    el[isHover ? 'addClass' : 'removeClass']("selected"); 
  }

  /**
   * List search input animation on focus/focusout
   * @param {*} isFocus 
   */
  listSearchInputAnimation(isFocus) {
    const parent = $("#search_field_store");

    if (isFocus) {
      parent.addClass('focus');
    } else {
      parent.removeClass('focus');
    }
  }

  /**
   * On marker click select item from list
   * @param {*} externalId 
   */
  selectItemFromList({externalId, marker, needScroll, animateMarker, animationType = 1}) {
    const markerLayer = $('#markerLayer');
    const markerDom = markerLayer.find(`img[src*="?markerID=${externalId}!"]`).closest('div');
    const el = $(`[data-id="${externalId}"]`).parent('li');
    const { elements: {resultWrapper} } = this;

    markerLayer.find('.animate-marker').removeClass('animate-marker');
    resultWrapper.find('.selected').removeClass('selected active');
    el.addClass('selected active');

    if (needScroll) {
      this.animateScrollOnSelectedElement(el);
    }

    if (animateMarker) {
      markerDom.addClass('animate-marker');
    }
  }

  /**
   * On info window close unselect item
   * @param {*} externalId 
   */
  unselectItemFromList({externalId, marker}) {
    const el = $(`[data-id="${externalId}"]`).parent('li');
    const markerDom = $(`#markerLayer .animate-marker`);
    const { elements: {resultWrapper} } = this;

    resultWrapper.find('.selected').removeClass('selected active');
    markerDom.removeClass('animate-marker');
    el.removeClass('selected active');
  }

  /**
   * Calculate animation speed
   * @param {*} a 
   * @param {*} b 
   * @param minSeconds
   */
  calculateAnimationRatio(a, b, minSeconds = 500) {
    let max = Math.max(a,b);
    let min = Math.min(a,b);

    return ((1 - (min/max)) * 100) * minSeconds;
  }

  /**
   * Animate scroll on selected element
   * @param {*} el 
   */
  animateScrollOnSelectedElement(el) {
    const { elements: {filterBlock, sidebar, resultMobileHeader} } = this;

    if (!el.get(0)) {
      return;
    }

    let headerHeight = resultMobileHeader.outerHeight(true);
    let positionTop = el.position().top;

    if (positionTop < 0) {
      positionTop = Math.abs(sidebar.scrollTop() - (Math.abs(positionTop) + 30));
    }

    const modul = Math.abs(sidebar.scrollTop() - positionTop);
    let speed = modul < 20 ? 0 : 500;

    if (speed => 500) {
      speed = this.calculateAnimationRatio(sidebar.scrollTop(), positionTop) + speed;

      if (speed > 800) {
        speed = 800;
      }
    }

    sidebar.animate({
      scrollTop: positionTop + headerHeight + (filterBlock.outerHeight(true)/2),
    }, speed);
  }
}