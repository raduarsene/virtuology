export default class BrowserDetect {
  /**
   * Class constructor
   * @param {*} props 
   */
  constructor() {
    this.body = document.querySelector('body');
    this.dataBrowser = [
      {string: navigator.userAgent, subString: "Edge", identity: "MS Edge"},
      {string: navigator.userAgent, subString: "MSIE", identity: "Explorer"},
      {string: navigator.userAgent, subString: "Trident", identity: "Explorer"},
      {string: navigator.userAgent, subString: "Firefox", identity: "Firefox"},
      {string: navigator.userAgent, subString: "Opera", identity: "Opera"},  
      {string: navigator.userAgent, subString: "OPR", identity: "Opera"},  
      {string: navigator.userAgent, subString: "Chrome", identity: "Chrome"}, 
     ];

     this.os = ["Win", "Mac", "X11", "Linux"]
     this.iphoneName;
     this.init();
  }

  /**
   * Get iphone modeel
   */
  getiPhoneModel() {
    this.body.classList.remove(this.iphoneName);
    
    // iPhone X
    if ((window.screen.height / window.screen.width == 812 / 375) && (window.devicePixelRatio == 3)) 
    {
      this.iphoneName = "is-iphone-x";
    }   
    // iPhone 6+/6s+/7+ and 8+    
    else if ((window.screen.height / window.screen.width == 736 / 414) && (window.devicePixelRatio == 3)) {
      this.iphoneName = "iphone_6_8_plus";
    }   
    // iPhone 6/6s/7 and 8  
    else if ((window.screen.height / window.screen.width == 667 / 375) && (window.devicePixelRatio == 2)) {
      this.iphoneName = "iphone_6_8";
    }  
    // iPhone 4/4s  
    else if ((window.screen.height / window.screen.width == 1.5) && (window.devicePixelRatio == 2)) {
      this.iphoneName = "iphone_4";
    }

    if (this.iphoneName && this.body) {
      this.body.classList.add(this.iphoneName);
    }

    return this.iphoneName;
  }

  /**
   * Getter is mobile
   */
  get _isMobile() {
    return navigator && navigator.userAgent 
        && /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  }

  /**
   * Get current mobile operation sistem
   */
  getMobileOperatingSystem() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
  
    if (/windows phone/i.test(userAgent)) {
        return "window_phone";
    }

    if (/android/i.test(userAgent)) {
        return "android";
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "ios";
    }

    return "unknown";
  }
  
  /**
   * Init
   */
  init() {
    this.browser = (this.searchString(this.dataBrowser) || "Other").toLowerCase();
    this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
    this.osType = this.detectOs();
    $('body').addClass(`${this.browser} ${this.osType} ${this.getMobileOperatingSystem()}`);
    
    this.getiPhoneModel();
  }

  /**
   * Detect os
   */
  detectOs() {
    let osName = '';

    this.os.map((item) => {
      if (navigator.appVersion.indexOf(item) > -1) {
        osName = item.toLowerCase();
      }

      return item;
    });

    return osName;
  }

  /**
   * Search in string
   * @param {*} data 
   */
  searchString(data) {
    for (let i = 0; i < data.length; i++) {
      let dataString = data[i].string;

      this.versionSearchString = data[i].subString;

      if (dataString.indexOf(data[i].subString) !== -1) {
        return data[i].identity;
      }
    }
  }

  /**
   * Search for browser version
   * @param {*} dataString 
   */
  searchVersion(dataString) {
    const index = dataString.indexOf(this.versionSearchString);
    const rv = dataString.indexOf("rv:");

    if (index === -1) {
      return;
    }

    if (this.versionSearchString === "Trident" && rv !== -1) {
      return parseFloat(dataString.substring(rv + 3));
    } else {
      return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
    }
  }
}
