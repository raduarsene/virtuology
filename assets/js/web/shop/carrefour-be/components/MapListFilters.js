import 'bootstrap-datepicker';
import moment from 'moment';

export default class MapListFilters {
  /**
   * Class constructor
   * @param {*} props 
   */
  constructor(props = {}) {
    this.activeFiltersNumber = 0;
    this.openHoursOldValue = '';
    this.items = {
      openHours: $(`[data-radio="open_hours"]`),
      activeFilters: $('[data-active-filters]'),
      inputsCheckbox: $(`[data-checkbox]`),
      brands: $(`[data-checkbox-brand]`),
      datepicker: $('[data-datepicker]'),
    };

    this.openHoursDate = moment().utc(true).toJSON();
    this.filtersOn = false;
    this.filters = {
      openHours: null,
      orServiceKeys: {},
      brandSlugs: {},
    };

    this.calculateActiveFilters();
    this.events();
  }

  /**
   * Add default filters
   * @param {*} filters 
   */
  addDefaultFilters(filters = {}) {
    const checkboxFilters = $(`[data-checkbox]`);
    const radioFilters = $(`[data-radio]`);
    const {items: {datepicker}} = this;

    for (let prop in filters) {
      if (filters[prop] && Array.isArray(filters[prop])) {
        filters[prop].map((value) => {
            checkboxFilters.each((index, item) => {
                const el = $(item);

                if (el.val() === value || el.val().indexOf(value) > -1) {
                    $(item).prop('checked', true);
                    this.filters[prop][`${item.name}`] = value;

                    if (item.name === 'orServiceKeys[1]') {
                      this.filters[prop][`${item.name}`] = el.val();
                    }
                }
            });
            return value;
        });
      } else {
        if (prop === 'openOnSundays' || prop === 'openNow' || prop === 'openOn') {
          if (filters[prop]) {
            this.filters[prop] = filters[prop];
            
            radioFilters.each((index, item) => {
              const el = $(item);

              if (el.val() === prop) {
                el.prop('checked', filters[prop]);
              }
            });

            if (prop === 'openOn' && filters[prop]) {
              let date = moment(filters[prop])

              datepicker.find('.calendar-selection').html(date.utc(true).locale(window.locale).format('dddd D MMMM'));
              datepicker.addClass('show-date');
            }
          }
        }
      }
    }

    this.calculateActiveFilters();
    this.filtersOn = true;
  }

  /**
   * Save current filtres
   */
  saveCurrentFiltres() {
    this.filtersOn = true;
  }

  /**
   * Clear filters
   */
  clearFilters() {
    const { items: {inputsCheckbox, openHours, datepicker, activeFilters}} = this;

    this.openHoursDate = moment().utc(true).toJSON();
    this.activeFiltersNumber = 0;
    this.filtersOn = false;
    this.filters = {
      openHours: null,
      orServiceKeys: {},
      brandSlugs: {},
    };

    datepicker.removeClass('show-date');
    datepicker.datepicker('hide');

    inputsCheckbox.each((index, item) => {
      $(item).prop('checked', false);
    });

    openHours.each((index, item) => {
      $(item).prop('checked', false);
    });

    activeFilters.find('span').text(this.activeFiltersNumber);
    activeFilters.hide();
  }

  /**
   * On date change
   */
  onDateChange(event) {
    const {items: {datepicker}} = this;
    const date = moment(event.date);
    const labelDoc = $('.calendar-filter');
    
    this.openHoursDate= date.utc(true).toJSON();

    datepicker.find('.calendar-selection').html(date.utc(true).locale(window.locale).format('dddd D MMMM'));
    datepicker.addClass('show-date');
    labelDoc.addClass('date-selected');
    datepicker.datepicker('hide');

    if (this.filtersOn && this.settings.mapObj) {
      this.settings.mapObj._updateFiltres(this.formatData());
    }
  }
    
  /**
   * Add filters events
   */
  events() {
    const {items: {inputsCheckbox, openHours, datepicker}} = this;

    datepicker.datepicker({
      maxViewMode: 1,
      keyboardNavigation: false,
      forceParse: false,
      todayHighlight: true,
      weekStart: 1,
      startDate: new Date(),
    }).on('changeDate', this.onDateChange.bind(this));

    inputsCheckbox.on('change', this.onCheckboxChange.bind(this));
    openHours.on('click', this.onOpenHoursClick.bind(this));
  }

  /**
   * On open hours event click
   * @param {*} event 
   */
  onOpenHoursClick(event) {
    const {items: {datepicker}} = this;
    const $el = $(event.currentTarget);
    const checked = $el.prop('checked');
    const labelDoc = $('.calendar-filter');
    const value = $el.val();

    if (this.filters.openHours === $el.val() && checked) {
      this.filters.openHours = null;
      $el.prop('checked', false);
    } else {
      this.filters.openHours = $el.val();
    }

    this.filters.openOnSundays = null;
    this.filters.openNow = null;

    if ($el.val() === 'openOn') {
      if($el.is(':checked')) {
        datepicker.datepicker('show');
      }
      else {
        $(".calendar-selection").html('');
        datepicker.datepicker('hide');
        datepicker.removeClass('show-date');
        labelDoc.removeClass('date-selected');
      }
    } else {
      this.filters.openOn = null;

      if($el.is(':checked')) {
        $(".calendar-selection").html('');
        datepicker.datepicker('hide');
        datepicker.removeClass('show-date');
        labelDoc.removeClass('date-selected');
      }

      if (value === 'openOnSundays') {
        this.filters.openOnSundays = $el.is(':checked');
      } else if(value === 'openNow') {
        this.filters.openNow = $el.is(':checked');
      }
    }

    this.calculateActiveFilters();

    if (this.filtersOn && this.settings.mapObj) {
      this.settings.mapObj._updateFiltres(this.formatData());
    }
  }

  /**
   * On checkbox change
   * @param {*} event 
   */
  onCheckboxChange(event) {
    const $el = $(event.currentTarget);
    const checked = $el.prop('checked');
    const type = $el.data('checkbox');
    const name = $el.prop('name'); 

    if (checked) {
      this.filters[type][name] = $el.val();
    } else {
      delete this.filters[type][name];
    }

    this.calculateActiveFilters();

    if (this.filtersOn && this.settings.mapObj) {
      this.settings.mapObj._updateFiltres(this.formatData());
    }
  }

  /**
   * Format data
   */
  formatData() {
    let results = {};

    for (let filterName in this.filters) {
      if (this.filters[filterName] && typeof this.filters[filterName] === 'object') {
        results[filterName] = [];
        for (let prop in this.filters[filterName]) {
          let parsedValue = this.filters[filterName][prop].replace(/\'/g,"").split(",")
          
          results[filterName] = results[filterName].concat(parsedValue);
        }
      } else if (this.filters[filterName] && typeof this.filters[filterName] === 'string') {
        if (this.filters[filterName] === 'openOn') {
          results.openOn = this.openHoursDate;
        } else {
          results[filterName] =this.filters[filterName]
        }
      } else if (this.filters[filterName] && typeof this.filters[filterName] === 'boolean') {
        results[filterName] = true
      }
    }

    return results;
  }

  /**
   * Calculate active filters
   */
  calculateActiveFilters() {
    const filters = this.formatData() || {};
    const {items: {activeFilters}} = this;
    let activeNumber = 0;

    for( let prop in filters) {
      if (Array.isArray(filters[prop]) && filters[prop].length > 0) {
        const stringValue = filters[prop].toString();

        activeNumber= activeNumber + filters[prop].length;

        if (stringValue.indexOf('CAR_TRUNK') > -1 && stringValue.indexOf('STORE') > -1) {
          --activeNumber;
        }
      } else if (filters[prop] && !Array.isArray(filters[prop]) && prop !== 'openHours') {
        activeNumber++;
      }
    }

    this.activeFiltersNumber = activeNumber;
    activeFilters.find('span').text(this.activeFiltersNumber);

    if (this.activeFiltersNumber) {
      activeFilters.show();
    } else {
      activeFilters.hide();
    }
  }
}