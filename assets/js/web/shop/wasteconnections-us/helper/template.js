import TemplateHelper from './../../../global/helper/template'

export default class TemplateWorkForceSolutions extends TemplateHelper {



    /**
     * @returns {string}
     * @protected
     */
    get _locationDistance() {
        let distance = '';
        if (this._location.address.distance > 0) {

            distance = this._location.address.distance*0.621371;
            distance = distance.toFixed(2);
        }

        return distance;
    }

    /**
     *
     * @returns {string | string}
     * @protected
     */
    get _locationPhone() {
        var phone = this._location.contact.phone;

        if(phone.indexOf('(')*1 != -1 ) {

            return phone;
        }

        phone = phone.replace(/\s/g,'');

        return "("+phone.substr(0, 3)+") "+phone.substr(3, 3)+"-"+phone.substr(6, 4);

    }


    /**
     * @returns {string}
     * @protected
     */
    get _locationAddress() {
        let address = '';



        let addressData = this._location.address.street.split(',');
        if (addressData.length > 1) {
            address += addressData[1].trim() + ' ' + addressData[0].trim();
        } else {
            address += this._location.address.street;
        }

        if (typeof this._location.address.extraInfo !== 'undefined' && this._location.address.extraInfo) {
            address += ', '+ this._location.address.extraInfo ;
        }

        address += '<br>' + this._location.address.locality + ', ' + this._location.address.zipCode;

        return address;
    }


    /**
     * @returns {string}
     * @protected
     */
    get _locationDistanceUnit() {
        let distanceUnit = '';
        if (this._location.address.distance > 0) {

            this._location.address.distance = this._location.address.distance*0.621371;

            distanceUnit = 'miles';

        }

        return distanceUnit;
    }
}
