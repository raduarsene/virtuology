import '../../home';
import Cookies from 'js-cookie';
import '../../block/all_blocks';

// Actions for Tom&CO page
$(document).ready(function () {
    $('.search-filters').on('click', function () {
        $('.search-results-list').toggleClass('small');
    });

    // Cookies
    var cookiesBlock = $('#cookies');
    if (cookiesBlock.length) {
        var cookiesTomandco = Cookies.get('mobilosoft_tomandco');
        if (!cookiesTomandco) {
            cookiesBlock.show();
        }

        $('.button-accept').on('click', function () {
            cookiesBlock.hide();

            Cookies.set('mobilosoft_tomandco', 1, {expires: 365});
        });
    }

    // Do actions only if the block event team is on page
    var blockEventTeam = $('#block_event_team');
    if (blockEventTeam.length) {
        // Images slider
        var imagesBlock = blockEventTeam.find('.events-slide');
        if (imagesBlock.length) {
            imagesBlock.slick({
                slidesToScroll: 3,
                slidesToShow: 3,
                dots: true,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 1140,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 720,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 540,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        }

        blockEventTeam.show();
    }
});
