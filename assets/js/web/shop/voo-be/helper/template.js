import TemplateHelper from './../../../global/helper/template'

export default class TemplateVoo extends TemplateHelper {
    /**
     * @inheritDoc
     */
    _createTemplateFromPrototype(prototype) {
        let template = super._createTemplateFromPrototype(prototype);

        let templateElement = $('<div></div>').html(template);

        let elementTitle = templateElement.find('h5'),
            elementDetails = templateElement.find('.infowindow-details'),
            elementPhone = templateElement.find('.location-phone'),
            elementCall = templateElement.find('.call-button');
        if (this._location.brandSlug === globalData['brandSlugBoutique'] || this._location.brandSlug === globalData['brandSlugRevendeur']) {
            if (elementPhone.length && (typeof this._location.contact.phone === 'undefined' || !this._location.contact.phone)) {
                elementPhone.hide();
            }

            if (elementCall.length && (typeof this._location.contact.phone === 'undefined' || !this._location.contact.phone)) {
                elementCall.hide();
            }
        } else {
            if (elementTitle.length) {
                elementTitle.html(this._location.name);
            }

            if (elementDetails.length) {
                elementDetails.hide();
            }

            if (elementPhone.length) {
                elementPhone.hide();
            }

            if (elementCall.length) {
                elementCall.hide();
            }
        }

        return templateElement.html();
    }

    /**
     * @inheritDoc
     */
    get _locationAddress() {
        let address = '';

        if (typeof this._location.address.extraInfo !== 'undefined' && this._location.address.extraInfo) {
            address += this._location.address.extraInfo + ' - ';
        }

        if (typeof this._location.address.number !== 'undefined' && this._location.address.number) {
            address += this._location.address.number + ' ';
        }

        address += this._location.address.street + '<br>' + 'B-' + this._location.address.zipCode + ' ' + this._location.address.locality;

        return address;
    }
}
