$(function(){
  $(".image-slider").slick({
    slidesToScroll: 1,
    slidesToShow: 1,
    dots: true,
    arrows: false,
  });

  $(".offers-slider").slick({
    slidesToScroll: 1,
    slidesToShow: 3,
    infinite: false,
    dots: true,
    arrows: false,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 540,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  /* close intineray popup detail page */
   $(document).on('click', '.intineray-close-popup-btn', function(e) {
     $('.intinerary-popup-container').removeClass("open");
     $('body').removeClass('hidden');
    e.preventDefault();    
  });

    /* open intineray popup detail page */
   $(document).on('click', '.btn-group .button-tracking', function(e) {
     $('.intinerary-popup-container').addClass("open");
     $('body').addClass('hidden');
    e.preventDefault();  
  });

  $(document).on("click", ".voo-accordion-header", function(){
    if($(this).find('.voo-open-accordion').hasClass('active')) {
      $(".voo-accordion-block").find(".voo-open-accordion").removeClass('active');
      $(".voo-accordion-block").find(".voo-accordion-content").slideUp(100);
      $(".voo-accordion-item").removeClass('open');
    } else {
      $(".voo-accordion-block").find(".voo-open-accordion").removeClass('active');
      $(".voo-accordion-block").find(".voo-accordion-content").slideUp(100);
      $(".voo-accordion-item").removeClass('open');
    
      $(this).find('.voo-open-accordion').toggleClass("active");
      $(this).find('.voo-open-accordion').closest('.voo-accordion-item').find('.voo-accordion-content').slideToggle(100);
    }
  });
});
