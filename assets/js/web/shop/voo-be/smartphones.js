// Block smartphones actions
window.blocksActions.defaultActions.push({
    defaultActions: function () {
        // Do actions only if the block is on page
        var blockSmartphones = $('#block_smartphones');
        if (blockSmartphones.length) {
            $.get(blockSmartphones.data('url'), function (smartphones) {
                if (smartphones && smartphones.length) {
                    var smartphonesSlide = blockSmartphones.find('.smartphones-slide');

                    var prototype = smartphonesSlide.data('prototype');
                    for (var key in smartphones) {
                        var template = prototype.replace(/__url__/g, smartphones[key].url)
                            .replace(/__image__/g, smartphones[key].image)
                            .replace(/__name__/g, smartphones[key].name)
                            .replace(/__brand__/g, smartphones[key].brand)
                            .replace(/__internalStorage__/g, smartphones[key].internalStorage)
                            .replace(/__price__/g, smartphones[key].price)
                            .replace(/__bundlePrice__/g, smartphones[key].bundlePrice);

                        smartphonesSlide.append(template);
                    }

                    // Images slide
                    smartphonesSlide.slick({
                        slidesToScroll: 1,
                        slidesToShow: 4,
                        autoplaySpeed: 5000,
                        autoplay: true,
                        dots: true,
                        arrows: false,
                        responsive: [
                            {
                                breakpoint: 1140,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 1,
                                    infinite: true,
                                    dots: true
                                }
                            },
                            {
                                breakpoint: 720,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 1
                                }
                            },
                            {
                                breakpoint: 480,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            }
                        ]
                    });

                    blockSmartphones.show();
                }
            });
        }
    }
});
