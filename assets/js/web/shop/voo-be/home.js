// require jQuery
import $ from 'jquery';
window.$ = $;
window.jQuery = $;

// Datetime/timezone
import moment from 'moment-timezone';
window.moment = moment;

// Jquery ui autocomplete
import 'jquery-ui/ui/widgets/autocomplete';

// Import local classes for home page as global
import TemplateHelper from './helper/template';
window.TemplateHelper = TemplateHelper;

import BrowserDetect from '../../block/map/components/BrowserDetect';

$(function(){
  const itineraryBlockMobile = $('.intinerary-popup-container');
  const browserDetect = new BrowserDetect();
  
  /* dropdown point events */
  $(document).on("click", ".wrap-title", function(){
    $('.results-by-letters').toggleClass("active"); 
    $('.search-alphabet').toggleClass("active");     
  });

   /* close intineray popup detail page */
   $(document).on('click', '.intineray-close-popup-btn', function(e) {
     $('.intinerary-popup-container').removeClass("open");
     $('body').removeClass('hidden');
    e.preventDefault();  
  });

  $(document).on('click','[data-itineraire]', (event) => {
    const el = $(event.currentTarget);

    if (browserDetect._isMobile) {
      event.preventDefault();

      itineraryBlockMobile.find('[data-url="google"]').attr('href', el.data('google'));
      itineraryBlockMobile.find('[data-url="waze"]').attr('href', el.data('waze'));
      itineraryBlockMobile.addClass('open');
    }
  })

  $('.search-filters').on("click",function(){
    if(!$(this).hasClass("closed-box")) {
      $('.section-map').addClass('expand');
    } else {
      $('.section-map').removeClass('expand');
    }
  });

  if (window.vooHeader && typeof window.init === 'function') {
    window.vooHeader.init();
  }
  
  setTimeout(() => {
    const MapObj = window.map;

    if (MapObj && typeof MapObj.extendSettings === 'function') {
      let defaultSettings = {
        /**
         * On autocomplete place select
         * @param {*} ui 
         */
        onAutocompletePlaceSelect: (ui) => {
          
          $('.search-results').addClass("show");
          $('.section-map').addClass('mobile-map-small');
        },

        clearSearch: () => {},

        /**
         * Clear filters
         */
        onClearFilters() {},

        /**
         * On loader list change 
         * @param {*} action 
         */
        onLoaderListChange: (action, results) => {
          
        },
        /**
         * On marker click
         * @param {*} infoWindow 
         * @param {*} marker 
         */
        onMarkerClick: (infoWindow, marker) => {},
        /**
         * On info window close
         * @param {*} marker 
         */
        onInfoWindowClose: (marker) => {},

        /**
         * On info window selected
         * @param {*} el 
         * @param {*} infoWindow 
         * @param {*} marker 
         */
        onShopSelected: (el, infoWindow, marker, externalId) => {}
      };

      MapObj.extendSettings(defaultSettings);
    }
  }, 0);
})
