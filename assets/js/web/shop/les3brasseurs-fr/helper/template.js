import TemplateHelper from './../../../global/helper/template'

export default class TemplateBrasseurs extends TemplateHelper {
    /**
     * @inheritDoc
     */
    _createTemplateFromPrototype(prototype) {
        let template = super._createTemplateFromPrototype(prototype);

        if (typeof this._locationKey !== 'undefined') {
            template = template.replace(/__index__/g, this._locationKey + 1);
        }

        let reservationUrl = '',
            reservationClass = 'hidden';
        $.each(this._location.locationExtraData, function (index, value) {
            if (value.key === 'reserver-une-table') {
                reservationUrl = value.value;
                reservationClass = '';
            }
        });

        let clickAndCollectUrl = '',
            clickAndCollectClass = 'hidden';
        $.each(this._location.locationExtraData, function (index, value) {
            if (value.key === "Click and Collect") {
                clickAndCollectUrl = value.value;
                clickAndCollectClass = '';
            }
        });

        let uberUrl = '',
            uberClass = 'hidden';
        $.each(this._location.locationExtraData, function (index, value) {
            if (value.key === 'Uber-eats') {
                uberUrl = value.value;
                uberClass = '';
            }
        });

        let deliverooUrl = '',
            deliveroooClass = 'hidden';
        $.each(this._location.locationExtraData, function (index, value) {
            if (value.key === 'Deliveroo') {
                deliverooUrl = value.value;
                deliveroooClass = '';
            }
        });

        template = template.replace(/__reservationUrl__/g, reservationUrl)
            .replace(/__reservationClass__/g, reservationClass)
            .replace(/__uberUrl__/g, uberUrl)
            .replace(/__uberClass__/g, uberClass)
            .replace(/__deliverooUrl__/g, uberUrl)
            .replace(/__deliverooClass__/g, uberClass)
            .replace(/__clickAndCollectUrl__/g, clickAndCollectUrl)
            .replace(/__clickAndCollectClass__/g, clickAndCollectClass)

        ;

        return template;
    }
}
