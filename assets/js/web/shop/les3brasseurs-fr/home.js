// require jQuery
import $ from 'jquery';
window.$ = $;
window.jQuery = $;

// Datetime/timezone
import moment from 'moment-timezone';
window.moment = moment;

// Jquery ui autocomplete
import 'jquery-ui/ui/widgets/autocomplete';

// Import local classes for home page as global
import TemplateHelper from './helper/template';
window.TemplateHelper = TemplateHelper;
