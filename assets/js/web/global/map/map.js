// Import map clases
import GoogleMap from './google';
import HereMap from './here';

var MapAction = function (sectionBlock, data) {
    // Set map actions
    var mapContainer = sectionBlock.find('.map-container');
    if (mapContainer.length) {
        var mapElement = mapContainer.find('.map-element');

        var defaultSettings = {
            type: mapContainer.data('type'),
            credentials: mapContainer.data('credentials'),
            language: mapContainer.data('language'),
            showMapRoute: mapContainer.data('map-route'),
            data: data,
        };

        // Combine shop settings with default settings
        if (typeof globalSettings !== 'undefined') {
            defaultSettings = {...defaultSettings, ...globalSettings};
        }

        var Map;
        switch (defaultSettings.type) {
            case mapContainer.data('type-here'):
                Map = new HereMap(defaultSettings);
                break;
            case mapContainer.data('type-google'):
            default:
                Map = new GoogleMap(defaultSettings);
        }

        // Create map
        Map.createMap(mapElement.get(0));
    }
};

export default MapAction;