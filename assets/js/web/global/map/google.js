import GoogleMapsApi from 'load-google-maps-api';

export default class GoogleMap {
    
    // Default Google Maps options
    static get DEFAULT_SETTINGS() {
        return {
            credentials: {key: null, key_premium: null},
            language: null,
            map: {
                zoom: 14,
            },
            mapLibraries: ['geometry', 'places'],
            mapMouse: true, // Set mapMouse: false - if you want to not be able to drag and zoom the map with the mouse
            mapZoom: {position: 'left-bottom'}, // Set mapZoom: false - if you want to hide zoom in/out
            mapFullScreen: true,
            marker: {
                icon: '/build/images/web/block/map/marker/icon.png',
                icon_width: 33,
                icon_height: 53,
            },
            showMapRoute: false,
        }
    };

    /**
     * @param {Object} settings
     */
    constructor(settings) {
        this._googleMapApi = null;
        this._directionsDisplay = null;
        this._directionsService = null;
        this._map = null;

        this._settings = {...GoogleMap.DEFAULT_SETTINGS, ...settings};
    }

    createMap(mapElement) {
        var _this = this;

        // Load Google Map API
        this._loadMapApi().then(function (MapApi) {
            // Initialize the map
            _this._initMap(MapApi, mapElement);

            // Add marker to map
            _this._addMarkerToMap();

            if (typeof _this._settings.showMapRoute !== 'undefined' && _this._settings.showMapRoute) {
                _this._mapRouteActions();
            }

            //autocomplete top
            _this._searchAutocompleteTop();

        });
    }

    _loadMapApi() {
        return GoogleMapsApi(this._mapCredentials);
    }

    _initMap(MapApi, mapElement) {
        this._googleMapApi = MapApi;

        this._map = new this._googleMapApi.Map(mapElement, this._mapOptions);
    }

    _addMarkerToMap() {
        var item = this._settings.data;

        var markerOptions = {
            map: this._map,
            position: new this._googleMapApi.LatLng(item.address.latitude, item.address.longitude),
            title: item.name,
        };

        if (this._settings.marker.icon) {


            if (typeof this._settings.marker.icon !== 'undefined') {
                var markerUrl = this._settings.marker.icon,
                    markerName = 'default';

                if (typeof globalData.brandSlugs !== 'undefined') {
                    if (item.brandSlug && globalData.brandSlugs.includes(item.brandSlug)) {
                        markerName = item.brandSlug;
                    } else {
                        // Random marker icon
                        markerName = globalData.brandSlugs[Math.floor((Math.random() * globalData.brandSlugs.length))];
                    }
                }

                markerOptions.markerName = markerName;
                markerOptions.icon = {
                    url: `${markerUrl.replace('default', markerName)}?markerID=${item.externalId}!`,
                    scaledSize: new this._googleMapApi.Size(this._settings.marker.icon_width, this._settings.marker.icon_height),
                };
            }


        }

        new this._googleMapApi.Marker(markerOptions);
    }

    _mapRouteActions() {
        this._directionsDisplay = new this._googleMapApi.DirectionsRenderer;
        this._directionsService = new this._googleMapApi.DirectionsService;

        this._directionsDisplay.setMap(this._map);
        this._directionsDisplay.setPanel($('#route_search_container').get(0));

        this._searchAutocomplete();

        this._submitSearchAddress();
    }

    _searchAutocomplete() {
        var _this = this;

        var searchAutocompleteInput = $('#map_route_search_destination');
        if (searchAutocompleteInput.length) {
            var autocomplete = new this._googleMapApi.places.Autocomplete(searchAutocompleteInput.get(0));

            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', this._map);

            // On select from autocomplete
            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();

                if (place.geometry) {
                    _this._showMapRoute(place.geometry.location);
                }
            });
        }
    }


    _searchAutocompleteTop() {
        var _this = this;

        var searchAutocompleteInput = $('#gm-autocomplete-top');
        if (searchAutocompleteInput.length) {
            var autocomplete = new this._googleMapApi.places.Autocomplete(searchAutocompleteInput.get(0));

            // Restrict autocomplete results to specific country/ies
            if (this._settings.geoLocationCountry) {
                autocomplete.setComponentRestrictions({'country': this._settings.geoLocationCountry});
            }

            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', this._map);

            // On select from autocomplete
            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();

                if (place.geometry) {

                    window.location.href = _this._settings.listUrl+"?location="+place.formatted_address;

                }
            });
        }
    }

    _submitSearchAddress() {
        var _this = this;

        var searchAddressButton = $('#map_route_search_button'),
            searchAutocompleteInput = $('#map_route_search_destination');
        if (searchAddressButton.length && searchAutocompleteInput.length) {
            searchAddressButton.on('click', function (e) {
                e.preventDefault();

                var address = searchAutocompleteInput.val();
                if (address && address.length > 2) {
                    _this._showMapRoute(null, address);
                }
            });
        }
    }

    _showMapRoute(location, address) {
        var _this = this;

        var destination = location ? location : address;

        this._directionsService.route({
            origin: new this._googleMapApi.LatLng(this._settings.data.address.latitude, this._settings.data.address.longitude),
            destination: destination,
            travelMode: this._googleMapApi.DirectionsTravelMode.DRIVING,
            unitSystem: this._googleMapApi.UnitSystem.METRIC
        }, function(response, status) {
            if (status === _this._googleMapApi.DirectionsStatus.OK) {

                _this._directionsDisplay.setDirections(response);
            } else {
                alert('Directions request failed due to ' + status);
            }
        });
    }

    get _mapCredentials() {
        var options = {};

        // Google map libraries
        if (this._settings.mapLibraries) {
            options.libraries = this._settings.mapLibraries;
        }

        // Google map language
        if (this._settings.language) {
            options.language = this._settings.language;
        }

        // Google map credentials
        var credentials = this._settings.credentials;
        if (typeof credentials.key_premium !== 'undefined' && credentials.key_premium) {
            options.client = credentials.key_premium;
        } else {
            options.key = credentials.key;
        }

        return options;
    }

    get _mapOptions() {
        var mapOptions = {
            zoom: this._settings.map.zoom,
            center: new this._googleMapApi.LatLng(this._settings.data.address.latitude, this._settings.data.address.longitude),
            mapTypeId: this._googleMapApi.MapTypeId.ROADMAP,
            disableDefaultUI: true,
        };

        if (this._settings.mapMouse === false) {
            mapOptions.gestureHandling = 'none';
        }

        if (this._settings.mapZoom) {
            mapOptions.zoomControl = true;

            var mapPosition = this._googleMapApi.ControlPosition.LEFT_BOTTOM;
            if (typeof this._settings.mapZoom.position !== 'undefined' && this._settings.mapZoom.position) {
                mapPosition = this._googleMapApi.ControlPosition[this._settings.mapZoom.position.replace('-', '_').toUpperCase()];
            }
            mapOptions.zoomControlOptions = {position: mapPosition};
        }

        if (this._settings.mapFullScreen) {
            mapOptions.fullscreenControl = this._settings.mapFullScreen;
            mapOptions.fullscreenControlOptions = {position: google.maps.ControlPosition.TOP_LEFT};
        }

        return mapOptions;
    }
}
