import '../../block/map/here/here-map.v3';

export default class HereMap {
    // Default Here Maps options
    static get DEFAULT_SETTINGS() {
        return {
            credentials: {id: null, code: null},
            language: null,
            map: {
                zoom: 14,
            },
            mapMouse: {zoom: false}, // Set mapMouse: false - if you want to disable drag map with mouse
            mapZoom: {position: 'left-bottom'}, // Set mapZoom: false - if you want to hide zoom in/out
            marker: {
                icon: '/build/images/web/block/map/marker/icon.png',
                icon_width: 33,
                icon_height: 53,
            },
            showMapRoute: false,
        }
    };

    /**
     * @param {Object} settings
     */
    constructor(settings) {
        // Const
        this.AUTOCOMPLETE_URL = 'https://discover.search.hereapi.com/v1/autosuggest';
        this.GEOCODER_URL = 'https://geocoder.api.here.com/6.2/geocode.json';

        this._platform = null;
        this._defaultLayers = null;
        this._pixelRatio = null;
        this._polyline = null;
        this._group = null;
        this._marker = null;
        this._infoWindow = null;
        this._ui = null;
        this._map = null;

        this._settings = {...HereMap.DEFAULT_SETTINGS, ...settings};
    }

    /**
     * Get iso3 countries
     */
    get _getCoutriesISO3() {
        let hereCountries = [];
        let languageCountriesISO3 = {'da':'DNK','nl':'BEL','en':'GBR','fi':'FIN','fr':'FRA','de':'DEU','it':'ITA','no':'NOR','pl':'POL','pt':'PRT','ru':'RUS','es':'ESP','sv':'SWE'};
        let countriesISO3 = {'DK':'DNK','BE':'BEL','GB':'GBR','FI':'FIN','FR':'FRA','DE':'DEU','IT':'ITA','NO':'NOR','PL':'POL','PT':'PRT','RU':'RUS','ES':'ESP','SE':'SWE'};

        if (this._settings.geoLocationCountry) {
            if (Array.isArray(this._settings.geoLocationCountry)) {
                this._settings.geoLocationCountry.map(function (countryCode) {
                    if (typeof countriesISO3[countryCode] !== 'undefined') {
                        hereCountries.push(countriesISO3[countryCode]);
                    }
                })
            } else {
                if (typeof countriesISO3[this._settings.geoLocationCountry] !== 'undefined') {
                    hereCountries.push(countriesISO3[this._settings.geoLocationCountry]);
                }
            }
        }

        if (!hereCountries.length && this._settings.language && typeof languageCountriesISO3[this._settings.language] !== 'undefined') {
            hereCountries.push(languageCountriesISO3[this._settings.language]);
        }

        return hereCountries;
    }

    get _getCoutry() {
        let hereCountry = 'MUL'; // Multiple Languages
        let allHereCountries = {'da':'DAN','nl':'DUT','en':'ENG','fi':'FIN','fr':'FRE','de':'GER','it':'ITA','no':'NOR','pl':'POL','pt':'POR','ru':'RUS','es':'SPA','sv':'SWE'};

        if (this._settings.language && typeof allHereCountries[this._settings.language] !== 'undefined') {
            hereCountry = allHereCountries[this._settings.language];
        }

        return hereCountry;
    }

    createMap(mapElement) {
        // Step 1: initialize communication with the platform
        this._loadMapApi();

        // Step 2: initialize the map  - not specificing a location will give a whole world view.
        this._initMap(mapElement);

        // Step 3: make the map interactive
        this._initMapSettings();

        // Map marker
        this._addMarkerToMap(this._settings.data.address, false);

        if (typeof this._settings.showMapRoute !== 'undefined' && this._settings.showMapRoute) {
            this._mapRouteActions();
        }

        var autocompleteTop = $("#gm-autocomplete-top");
        if(typeof autocompleteTop!=="undefined" && autocompleteTop.length) {
            this._searchAutocomplete("gm-autocomplete-top")
        }
    }

    _loadMapApi() {
        this._platform = new H.service.Platform({
            apikey: this._settings.credentials.apikey,
            useHTTPS: true
        });

        this._pixelRatio = window.devicePixelRatio || 1;

        this._defaultLayers = this._platform.createDefaultLayers({
            tileSize: this._pixelRatio === 1 ? 256 : 512,
            ppi: this._pixelRatio === 1 ? undefined : 320,
            language: this._getLanguage,
        });
    }

    _initMap(mapElement) {
        this._map = new H.Map(
            mapElement,
            this._defaultLayers.vector.normal.map,
            {
                pixelRatio: this._pixelRatio,
                engineType: H.map.render.RenderEngine.EngineType.P2D,
                zoom: this._settings.map.zoom,
            },
        );
    }

    _initMapSettings() {
        // MapEvents enables the event system
        this._mapSettingsBehavior();

        // Switches the map language
        this._mapSettingsTileLayer();

        // Create the default UI components
        this._mapSettingsUIComponents();

        // SET map custom settings
        this._mapSettingsCustom();

        // add a resize listener to make sure that the map occupies the whole container
        window.addEventListener('resize', () => this._map.getViewPort().resize());
    }

        /**
     * Stop here mouse wheel propagation
     * @private
     */
    _stopHereMouseWheelEvent() {
        const that = this;

        if (this.isScrollWheelActive) {
            clearTimeout(this.isScrollWheelActive);
        }

        this.mapOverlay.addClass('visible');
        this.isScrollWheelActive = setTimeout(function() {
            that.mapOverlay.removeClass('visible');
        }, 100);
    }

    _mapSettingsBehavior() {
        if (this._settings.mapMouse) {
            this.isScrollWheelActive = null;
            const mapElement = $('.map-element');

            this.mapOverlay = $('<span class="map-overlay" />');

            mapElement.prepend(this.mapOverlay);

            var events = new H.mapevents.MapEvents(this._map);
            var behavior = new H.mapevents.Behavior(events);
            
            if (typeof this._settings.mapMouse.zoom === 'undefined' || this._settings.mapMouse.zoom !== true) {
                behavior.disable(H.mapevents.Behavior.WHEELZOOM);
                $(window).on('wheel', this._stopHereMouseWheelEvent.bind(this));
            }

            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                behavior.disable(H.mapevents.Behavior.WHEELZOOM);

                var eventStartY;

                this._map.addEventListener('pointermove', function (e) {
                    if (!behavior.isEnabled(H.mapevents.Behavior.DRAGGING)) {
                        window.scrollBy(0, (eventStartY - e.currentPointer.viewportY));
                    }
                });

                this._map.addEventListener('pointerenter', function (e) {
                    eventStartY = e.currentPointer.viewportY;

                    if (e.pointers.length === 2) {
                        // This means there are two finger move gesture on screen
                        behavior.enable(H.mapevents.Behavior.DRAGGING);
                    } else {
                        behavior.disable(H.mapevents.Behavior.DRAGGING);
                    }
                });
            }
        }
    }

    _mapSettingsTileLayer() {
        var mapTileService = this._platform.getMapTileService({
                type: 'base'
            }),
            // Our layer will load tiles from the HERE Map Tile API
            mapLayer = mapTileService.createTileLayer(
                'maptile',
                'normal.day',
                this._pixelRatio === 1 ? 256 : 512,
                'png8',
                {lg: this._getCoutry, ppi: this._pixelRatio === 1 ? undefined : 320}
            );

        this._map.setBaseLayer(mapLayer);
    }

    _mapSettingsUIComponents() {
        if (this._settings.mapZoom) {
            this._ui = H.ui.UI.createDefault(this._map, this._defaultLayers);

            // Remove not needed settings control
            this._ui.removeControl('mapsettings');

            // Set zoom options position
            if (typeof this._settings.mapZoom.position !== 'undefined' && this._settings.mapZoom.position) {
                var zoom = this._ui.getControl('zoom');

                zoom.setAlignment(this._settings.mapZoom.position);
            }
        }
    }

    _mapSettingsCustom() {
        // Add map coordinates
        this._map.setCenter({lat: this._settings.data.address.latitude, lng: this._settings.data.address.longitude});
        this._map.setZoom(this._settings.map.zoom);
    }

    _addMarkerToMap(position, removeOld) {
        if (removeOld && this._marker) {
            this._map.removeObject(this._marker);
        }

        // Create a marker for the noisePoint
        var marker = new H.map.Marker({
            lat: position.latitude,
            lng: position.longitude
        }, {
            icon: new H.map.Icon(this._settings.marker.icon, {
                size: {w: this._settings.marker.icon_width, h: this._settings.marker.icon_height},
                anchor: {x: this._settings.marker.icon_width / 2, y: this._settings.marker.icon_height / 2}
            })
        });

        if (removeOld) {
            this._marker = marker;
        }

        this._map.addObject(marker);
    }

    _mapRouteActions() {
        this._searchAutocomplete();

        this._submitSearchAddress();
    }

    _searchAutocomplete(inputId) {
        var _this = this;

        if(typeof inputId!=="undefined") {
            var searchAutocompleteInput = $('#'+inputId);
        }
        else {
            var searchAutocompleteInput = $('#map_route_search_destination');
        }

        if (searchAutocompleteInput.length) {

            var boundries = this._map.getViewBounds();
            var top = boundries.getTop();
            var bottom = boundries.getBottom();
            var left = boundries.getLeft();
            var right = boundries.getRight();
            if(top == bottom ) {
                bottom+=0.1;
            }
            if(left == right) {
                right+=0.1
            }
            var viewPort = top+","+left+","+bottom+","+right;


            searchAutocompleteInput.autocomplete({
                minLength: 2,
                autoFocus: true,
                focus: function (event) {
                    event.preventDefault();
                },
                source: function (request, response) {
                    $.get(_this.AUTOCOMPLETE_URL, {
                        apikey: _this._settings.credentials.apikey,
                        in: `countryCode:${_this._getCoutriesISO3.join(",")}`,
                        resultTypes: 'locality,addressblock,place',
                        q: request.term,

                        limit: 10,
                        withExperiments: "PBAPI_3292_autosuggest_cuisines=true",
                        lang: _this._settings.language,
                    }, function (data) {

                        if (data && typeof data.results !== 'undefined' && data.results.length) {

                            response($.map(data.results, (item) => {

                                return {
                                    value: item.highlightedTitle+" "+item.highlightedVicinity.split("<br/>").join(" "),
                                    label: item.highlightedTitle+" "+item.highlightedVicinity.split("<br/>").join(" "),
                                    locationId: item.id,
                                    position: (typeof item.position!=="undefined")?item.position:null,

                                };

                            }));

                        }

                    });
                },
                select: function (event, ui) {
                    if(searchAutocompleteInput.prop('id')!=="map_route_search_destination") {

                        window.location.href = _this._settings.listUrl+"?location="+ui.item['value'];
                    }
                    else {
                        _this._showMapRoute(ui.item.locationId, ui.item.value);
                    }

                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li></li>")
                    .data('value', item)
                    .append('<div>' + item.label + '</div>')
                    .appendTo(ul);
            };
        }
    }

    _submitSearchAddress() {
        var _this = this;

        var searchAddressButton = $('#map_route_search_button'),
            searchAutocompleteInput = $('#map_route_search_destination');
        if (searchAddressButton.length && searchAutocompleteInput.length) {
            searchAddressButton.on('click', function (e) {
                e.preventDefault();

                var address = searchAutocompleteInput.val();
                if (address && address.length > 2) {
                    _this._showMapRoute(null, address);
                }
            });
        }
    }

    _showMapRoute(locationId, searchText) {
        var _this = this,
            urlParameters = {
                app_id: this._settings.credentials.id,
                app_code: this._settings.credentials.code,
                language: this._settings.language,
                gen: 9,
                jsonattributes: 1,
            };

        if (locationId) {
            urlParameters.locationid = locationId;
        } else {
            urlParameters.searchtext = searchText;
        }

        $.get(this.GEOCODER_URL, urlParameters, function (data) {
            if (data && data.response.view.length && data.response.view[0].result.length && typeof data.response.view[0].result[0].location !== 'undefined') {
                var position = data.response.view[0].result[0].location.displayPosition;

                _this._addMarkerToMap(position, true);

                _this._addMapRouteFromAToB(position);
            }
        });
    }

    _addMapRouteFromAToB(position) {
        var _this = this,
            router = this._platform.getRoutingService(),
            routeRequestParams = {
                mode: 'fastest;car',
                representation: 'display',
                routeattributes : 'waypoints,summary,shape,legs',
                maneuverattributes: 'direction,action',
                waypoint0: this._settings.data.address.latitude + ',' + this._settings.data.address.longitude,
                waypoint1: position.latitude + ',' + position.longitude,
                language: this._settings.language,
            };

        router.calculateRoute(
            routeRequestParams,
            function (result) {
                var route = result.response.route[0];

                _this._addRouteShapeToMap(route);
                _this._addManueversToMap(route);

                _this._showRouteDirections(route);
            },
            function () {
                alert('Can\'t reach the remote server');
            }
        );
    }

    /**
     * Creates a H.map.Polyline from the shape of the route and adds it to the map.
     *
     * @param {Object} route A route as received from the H.service.RoutingService
     */
    _addRouteShapeToMap(route) {
        var lineString = new H.geo.LineString(),
            routeShape = route.shape;

        routeShape.forEach(function (point) {
            var parts = point.split(',');

            lineString.pushLatLngAlt(parts[0], parts[1]);
        });

        if (this._polyline) {
            this._map.removeObject(this._polyline);
        }

        this._polyline = new H.map.Polyline(lineString, {
            style: {
                lineWidth: 4,
                strokeColor: 'rgba(0, 128, 255, 0.7)'
            }
        });

        // Add the polyline to the map
        this._map.addObject(this._polyline);

        // And zoom to its bounding rectangle
        this._map.setViewBounds(this._polyline.getBounds(), true);
    }

    /**
     * Creates a series of H.map.Marker points from the route and adds them to the map.
     *
     * @param {Object} route  A route as received from the H.service.RoutingService
     */
    _addManueversToMap(route) {
        var _this = this,
            svgMarkup = '<svg width="18" height="18" xmlns="http://www.w3.org/2000/svg"><circle cx="8" cy="8" r="8" fill="#1b468d" stroke="white" stroke-width="1"/></svg>',
            dotIcon = new H.map.Icon(svgMarkup, {anchor: {x: 8, y: 8}});

        if (this._group) {
            this._map.removeObject(this._group);
        }

        this._group = new H.map.Group();

        // Add a marker for each maneuver
        for (var i = 0; i < route.leg.length; i++) {
            for (var j = 0; j < route.leg[i].maneuver.length; j++) {
                // Get the next maneuver.
                var maneuver = route.leg[i].maneuver[j];
                // Add a marker to the maneuvers group
                var marker = new H.map.Marker(
                    {lat: maneuver.position.latitude, lng: maneuver.position.longitude},
                    {icon: dotIcon}
                );
                marker.instruction = maneuver.instruction;
                this._group.addObject(marker);
            }
        }

        this._group.addEventListener('tap', function (e) {
            var position = e.target.getPosition(),
                text = e.target.instruction;

            _this._map.setCenter(position);

            // Opens/Closes a info bubble
            if (!_this._infoWindow) {
                _this._infoWindow = new H.ui.InfoBubble(
                    position,
                    // The FO property holds the province name.
                    {content: text}
                );

                _this._ui.addBubble(_this._infoWindow);
            } else {
                _this._infoWindow.close();

                _this._infoWindow.setPosition(position);
                _this._infoWindow.setContent(text);
                _this._infoWindow.open();
            }
        }, false);

        // Add the maneuvers group to the map
        this._map.addObject(this._group);
    }

    _showRouteDirections(route) {
        var routeSearchContainer = $('#route_search_container');
        if (routeSearchContainer.length) {
            var templateHtml = '<div class="m-b-15"><div><b>Total distance:</b> ' + (route.summary.distance / 1000)
                + ' km.</div><div><b>Travel Time:</b> ';

            if (Math.floor(route.summary.travelTime / 3600) > 0) {
                templateHtml += Math.floor(route.summary.travelTime / 3600) + ' hours ';
            }

            if (Math.floor(route.summary.travelTime % 3600 / 60) > 0) {
                templateHtml += Math.floor(route.summary.travelTime % 3600 / 60) + ' minutes ';
            }

            templateHtml += (route.summary.travelTime % 60) + ' seconds.</div></div>';

            if (route.leg.length) {
                templateHtml += '<ol>';

                for (var i = 0; i < route.leg.length; i++) {
                    for (var j = 0; j < route.leg[i].maneuver.length; j++) {
                        var maneuver = route.leg[i].maneuver[j];

                        templateHtml += '<li><span class="arrow ' + maneuver.action + '"></span><span>' + maneuver.instruction + '</span></li>';
                    }
                }

                templateHtml += '</ol>';
            }

            routeSearchContainer.html(templateHtml);
        }
    }

    get _getLanguage() {
        var hereLanguage = 'en-US';

        if (this._settings.language && this._settings.language !== 'en') {
            hereLanguage = this._settings.language + '-' + this._settings.language.toUpperCase();
        }

        return hereLanguage;
    }

    get _getCoutry() {
        var hereCountry = 'MUL'; // Multiple Languages
        var allHereCountries = {'da':'DAN','nl':'DUT','en':'ENG','fi':'FIN','fr':'FRE','de':'GER','it':'ITA','no':'NOR','pl':'POL','pt':'POR','ru':'RUS','es':'SPA','sv':'SWE'};

        if (this._settings.language && typeof allHereCountries[this._settings.language] !== 'undefined') {
            hereCountry = allHereCountries[this._settings.language];
        }

        return hereCountry;
    }
}
