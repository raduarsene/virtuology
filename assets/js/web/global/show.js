import $ from 'jquery';

// Global variables
window.currentLocationAssets = null;
// Blocks Actions to execute on show page
window.blocksActions = {
    defaultActions: [],
    locationAssetsActions: [],
};

// Actions for show page
$(document).ready(function () {
    // If there are default block actions
    if (window.blocksActions.defaultActions.length) {
        // Call default block actions
        window.blocksActions.defaultActions.forEach(function (callbackAction) {
            try {
                callbackAction.defaultActions();
            } catch (e) {
                console.log(e.message);
            }
        });
    }

    // If there are location assets block actions
    if (window.blocksActions.locationAssetsActions.length) {
        // Get Location Assets API data
        $.get(globalData.routes.apiLocationAssets, {
            siteId: globalData.currentLocation.siteId,
            locationId: globalData.currentLocation.guid
        }, function (assets) {
            var descriptionImagesBlock = $('#block_location_description .image-slider'),
                detailsImagesBlock = $('#block_location_details .image-slider');

            if (assets && assets.length
                || descriptionImagesBlock.children() && descriptionImagesBlock.children().length
                || detailsImagesBlock.children() && detailsImagesBlock.children().length
            ) {
                window.currentLocationAssets = assets;

                // Call location assets block actions
                window.blocksActions.locationAssetsActions.forEach(function (callbackAction) {
                    try {
                        callbackAction.locationAssetsActions();
                    } catch (e) {
                        console.log(e.message);
                    }
                });
            }
        });
    }

    let stickyMenu = $('.sticky-menu');
    if (stickyMenu.length) {
        let stickyTop = stickyMenu.offset().top;

        $(window).scroll(function () {
            var windowTop = $(window).scrollTop();

            if (stickyTop < windowTop) {
                $('.sticky-wrapper').addClass('is-sticky');
                $('.mobi_content').addClass('content-sticky');
            } else {
                $('.sticky-wrapper').removeClass('is-sticky');
                $('.mobi_content').removeClass('content-sticky');
            }
        });
    }
});
