import $ from 'jquery';

export class GtmEvents  {
    /**
     * Search geolocation
     * @param {*} place 
     * @param {*} searchResultsNo 
     */
    gtmSearchInputEvent(place, searchResultsNo) {
        var dataLayer = window.dataLayer = window.dataLayer || [];
        
        if (window.canTriggereEvent) {
            dataLayer.push({
                "searchBar":place + "/" + searchResultsNo, 
                event:'searchBar', 
                'searchTerm':place, 
                'searchResultNumber':searchResultsNo
            });
        }
    }

    /**
     * Geolocation
     * @param {*} visitorGeolocStore 
     * @param {*} searchResultsNo 
     */
    gtmSearchGeoloc(visitorGeolocStore, searchResultsNo) {
        var dataLayer = window.dataLayer = window.dataLayer || [];

        if (window.canTriggereEvent) {
            dataLayer.push({
                "searchGeoloc":visitorGeolocStore.formatted_address + "/" + searchResultsNo, 
                event:'searchGeoloc', 
                'visitorGeolocStore':visitorGeolocStore, 
                'searchResultNumber':searchResultsNo
            });
        }
    }

    /**
     * Search filter
     * @param {*} queryData 
     * @param {*} searchResultsNo 
     */
    gtmSearchFilter(queryData, searchResultsNo) {
        var searchFilterCategory = '';
        
        if(queryData.serviceKeys.length || queryData.brandSlugs.length) {
            var filters = $.merge(queryData.serviceKeys, queryData.brandSlugs);

            searchFilterCategory = filters.join(",");
        }

        if(searchFilterCategory.length) {
            var dataLayer = window.dataLayer = window.dataLayer || [];

            if (window.canTriggereEvent) {
                dataLayer.push({
                    "searchStoreFilter":searchFilterCategory + "/" + searchResultsNo, 
                    event:'searchStoreFilter', 
                    'searchFilterCategory':searchFilterCategory, 
                    'searchResultNumber':searchResultsNo
                });
            }
        }
    }


    /**
     * Cta event
     * @param {*} event 
     */
    gtmCtaEvent(event) {
        const element = $(event.target);
        var eventName = element.attr('data-gtm_event'),
            eventValue = element.attr('data-gtm_event_value');


        if (eventName && eventValue) {
            var dataLayer = window.dataLayer = window.dataLayer || [];

            if (window.canTriggereEvent) {
                dataLayer.push({
                    [eventName]: eventValue, 
                    event: eventName, 
                    linkAnchor: element.prop('href')
                });
            }
        }

        return true;
    }

    events() {
        $(document).on('click', ".gtm-cta", this.gtmCtaEvent.bind(this));
    }
}

$(document).ready(function () {
    //attach general events
    const searchGtmEvents = new GtmEvents();

    searchGtmEvents.events();

    //switch language
    $(document).on('click', "a.lang", function (event) {
        var dataLayer = window.dataLayer = window.dataLayer || [];
        var language = $(this).prop('hreflang');
        var link = $(this).prop('href');

        if (window.canTriggereEvent)  {
            dataLayer.push({
                'clickLanguage': language, 
                event:'clickLanguage', 
                linkAnchor: link
            });
        }
    });

    $(document).on('click', ".to-shops > a", function (event) {
        var dataLayer = window.dataLayer = window.dataLayer || [];
        var link = $(this).prop('href');

        if (window.canTriggereEvent) {
            dataLayer.push({
                'clickHeader': link, 
                event: 'clickHeader', 
                linkAnchor: link
            });
        }
    });

    $(document).on('click', ".menu-item-object-page > a", function (event) {
        var dataLayer = window.dataLayer = window.dataLayer || [];
        var link = $(this).prop('href');

        if (window.canTriggereEvent) {
            dataLayer.push({
                'clickMenu': link, 
                event: 'clickMenu', 
                linkAnchor: link
            });
        }
    });

    $(document).on('click', ".header-aside > h5 >ul > li >a", function (event) {
        var dataLayer = window.dataLayer = window.dataLayer || [];
        var link = $(this).prop('href');

        if (window.canTriggereEvent) {
            dataLayer.push({
                'clickHeader': link, 
                event: 'clickHeader', 
                linkAnchor: link
            });
        }
    });
});
