import EE from '../global/helper/EventEmitter';
import Cookies from 'js-cookie';

window.canTriggereEvent = true;

/**
 * Can trigger events 
 * @param {*} status 
 */
const canTriggereEvents = (status) => {
  const cookiesStatus = status || Cookies.get('cookieconsent_status');

  return cookiesStatus === 'allow' ? true : false;
};

if (isNeededToAcceptGDPR) {
  window.canTriggereEvent = canTriggereEvents(status);
}

/**
 * Lissent for cookies status change
 * @param {*} status 
 */
window.onStatusChange = (status) => {
  EE.emit('cookies:status:change', status);

  if (isNeededToAcceptGDPR) {
    window.canTriggereEvent = canTriggereEvents(status);
  }
};