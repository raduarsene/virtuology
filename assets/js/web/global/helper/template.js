export default class TemplateHelper {
    /**
     * Default open hours options
     * @constructor
     */
    static get OPEN_HOURS_OPTIONS() {
        return {
            typeException: false,
            openClass: '',
            closedClass: '',
            openLabel: '',
            closeLabel: '',
            openSentence: '',
            closeSentence: '',
            dayClosed: '',
            exceptionOpen: '',
            exceptionClosed: '',
            openHoursSeparator: '',
            dateFormat: '',
            timeFormat: '',
            daysNumber: {1: '', 2: '', 3: '', 4: '', 5: '', 6: '', 7: ''}
        }
    };

    constructor() {
        this._defaultBrandSlug = 'default';

        /**
         * @type {{
         *  typeException: boolean,
         *  openClass: string,
         *  closedClass: string,
         *  openLabel: string,
         *  closeLabel: string,
         *  openSentence: string,
         *  closeSentence: string,
         *  dayClosed: string,
         *  exceptionOpen: string,
         *  exceptionClosed: string,
         *  openHoursSeparator: string,
         *  dateFormat: string,
         *  timeFormat: string,
         *  daysNumber: {"1": string, "2": string, "3": string, "4": string, "5": string, "6": string, "7": string}
         * }}
         * @protected
         */
        this._options = TemplateHelper.OPEN_HOURS_OPTIONS;
        if (typeof globalData.openHoursData !== 'undefined' && globalData.openHoursData) {
            this._options = {...this._options, ...globalData.openHoursData};
        }
    }

    /**
     * @param {Array<Object>} locations
     * @param {string} prototype
     * @param {boolean} withOpenHours
     * @returns {string} the html template
     */
    getLocationsTemplates(locations, prototype, withOpenHours = true) {
        let template = '';

        for (let key in locations) {
            this._currentLocationKey = key;

            template += this.getLocationTemplate(locations[key], prototype, withOpenHours);
        }

        return template;
    }

    /**
     * @param {Object} location
     * @param {string} prototype
     * @param {boolean} withOpenHours
     * @returns {string} the html template
     */
    getLocationTemplate(location, prototype, withOpenHours = true) {
        this._currentLocation = location;

        let template = this._createTemplateFromPrototype(prototype);

        let data = this._openHoursData;
        template = this._addOpenHoursTitle(data, template);

        if (withOpenHours) {

            if (this._options.typeException) {
                template = this._addExceptionOpenHours(data, template);
            } else {
                template = this._addWeekOpenHours(data, template);
            }
        }

        return template;
    }

    /**
     * @param {string} prototype
     * @returns {string}
     * @protected
     */
    _createTemplateFromPrototype(prototype) {
        return prototype.replace(/__externalId__/g, this._location.externalId)
            .replace(/__name__/g, this._location.name)
            .replace(/__slug__/g, this._location.slug)
            .replace(/__brand__/g, this._location.brand)
            .replace(/__brandSlug__/g, this._locationBrandSlug)
            .replace(/__latitude__/g, this._location.address.latitude)
            .replace(/__longitude__/g, this._location.address.longitude)
            .replace(/__address__/g, this._locationAddress)
            .replace(/__fullAddress__/g, this._location.address.fullAddress)
            .replace(/__distance__/g, this._locationDistance)
            .replace(/__distance_unit__/g, this._locationDistanceUnit)
            .replace(/__phone__/g, this._locationPhone)
            .replace(/__index__/g, this._locationKey+1);
    }

    /**
     * @param {Object} data
     * @param {string} template
     * @returns {string}
     * @protected
     */
    _addOpenHoursTitle(data, template) {

        if(this._location.status == 'OPEN') {
            return template.replace(/__openClass__/g, data.opens.class)
                .replace(/__openLabel__/g, data.opens.label)
                .replace(/__openSentence__/g, data.opens.sentence)
                .replace(/__openDelimiter__/g, ',');
        }
        else {

            var openingDate = moment.tz(this._location.openingDate, this._location.address.timezone);
            //
            return template.replace(/__openClass__/g, this._options.closedClass)
                .replace(/__openLabel__/g, this._options.closeLabel)

                .replace(/__openSentence__/g, this._options.openSoonSentence+' '+openingDate.format('DD/MM/YYYY'))
                .replace(/__openDelimiter__/g, ',');
            //
        }

    }

    /**
     * @param {Object} data
     * @param {string} template
     * @returns {string}
     * @protected
     */
    _addExceptionOpenHours(data, template) {
        let templateElement = $('<div></div>').html(template);

        if(this._location.status != 'OPEN')
        {
            return templateElement.html();
        }

        let table = templateElement.find('table.table-schedule');
        if (table.length) {
            let currentWeekDay = parseInt(moment().tz(this._location.address.timezone).format('E'));

            for (let weekDay = 1; weekDay <= 7; weekDay++) {
                let tr = $('<tr></tr>'),
                    th = $('<th></th>'),
                    td = $('<td></td>');
                th.text(this._options.daysNumber[weekDay]);
                tr.append(th);
                if (currentWeekDay === weekDay) {
                    tr.addClass('active');
                }

                if (typeof data.weekOpenHours[weekDay] !== 'undefined') {
                    let openHourDay = [];

                    for (let openHourKey in data.weekOpenHours[weekDay]) {
                        openHourDay.push(data.weekOpenHours[weekDay][openHourKey].openTimeFormat
                            + ' - ' + data.weekOpenHours[weekDay][openHourKey].closeTimeFormat);
                    }

                    td.text(openHourDay.join(' ' + this._options.openHoursSeparator + ' '));

                    tr.append(td);
                } else {
                    td.addClass('warning').text(this._options.dayClosed);
                    tr.append(td);
                }

                table.append(tr);
            }
        }

        let tableException = templateElement.find('.open-hours-exceptions');
        if (tableException.length) {
            let exceptionOpen = tableException.find('.is-open').parent(),
                exceptionClosed = tableException.find('.is-closed').parent();

            if (data.specialOpenHours.length) {
                for (let openKey in data.specialOpenHours) {
                    exceptionOpen.append('<p>' + data.specialOpenHours[openKey] + '</p>');
                }
            } else {
                exceptionOpen.remove();
            }

            if (data.specialClosedHours.length) {
                for (let closedKey in data.specialClosedHours) {
                    exceptionClosed.append('<p>' + data.specialClosedHours[closedKey] + '</p>');
                }
            } else {
                exceptionClosed.remove();
            }
        }

        return templateElement.html();
    }

    /**
     * @param {Object} data
     * @param {string} template
     * @returns {string}
     * @protected
     */
    _addWeekOpenHours(data, template) {
        let templateElement = $('<div></div>').html(template);

        let openHoursExceptions = templateElement.find('.open-hours-exceptions');
        if (openHoursExceptions.length) {
            openHoursExceptions.remove();
        }

        if(this._location.status != 'OPEN')
        {
            return templateElement.html();
        }

        let tableSchedule = templateElement.find('table.table-schedule');
        if (tableSchedule.length) {
            let currentWeekDay = parseInt(moment().tz(this._location.address.timezone).format('E'));

            for (let dayNumber = 1; dayNumber <= 7; dayNumber++) {
                let weekDay = currentWeekDay + dayNumber - 1;
                if (weekDay > 7) {
                    weekDay = weekDay - 7;
                }

                let tableTr = $('<tr></tr>'),
                    tableTh = $('<th></th>'),
                    tableTd = $('<td></td>');
                tableTh.text(this._options.daysNumber[weekDay]);
                tableTr.append(tableTh);
                if (currentWeekDay === weekDay) {
                    tableTr.addClass('active');
                }

                if (typeof data.weekSpecialHours[weekDay] !== 'undefined') {
                    if (data.weekSpecialHours[weekDay].closed) {
                        tableTd.html('<span class="warning">' + this._options.dayClosed + '</span>'
                            + ' (' + this._options.exceptionClosed + ')');
                    } else {
                        let specialOpenHoursString = data.weekSpecialHours[weekDay].openTimeFormat
                            + ' - ' + data.weekSpecialHours[weekDay].closeTimeFormat;

                        if (data.weekSpecialHours[weekDay].additionalOpenTime && data.weekSpecialHours[weekDay].additionalCloseTime) {
                            specialOpenHoursString += ' ' + this._options.openHoursSeparator
                                + ' ' + data.weekSpecialHours[weekDay].additionalOpenTimeFormat
                                + ' - ' + data.weekSpecialHours[weekDay].additionalCloseTimeFormat;
                        }

                        tableTd.text(specialOpenHoursString + ' (' + this._options.exceptionOpen + ')');
                    }
                } else if (typeof data.weekOpenHours[weekDay] !== 'undefined') {
                    let openHourDay = [];

                    for (let openHourKey in data.weekOpenHours[weekDay]) {
                        openHourDay.push(data.weekOpenHours[weekDay][openHourKey].openTimeFormat
                            + ' - ' + data.weekOpenHours[weekDay][openHourKey].closeTimeFormat);
                    }

                    tableTd.text(openHourDay.join(' ' + this._options.openHoursSeparator + ' '));
                } else {
                    tableTd.addClass('warning').text(this._options.dayClosed);
                }

                tableTr.append(tableTd);
                tableSchedule.append(tableTr);
            }
        }

        return templateElement.html();
    }

    /**
     * @param {Object} location
     * @protected
     */
    set _currentLocation(location) {
        /**
         * @type {Object}
         * @protected
         */
        this._location = location;
    }

    /**
     * @param {number} locationKey
     * @protected
     */
    set _currentLocationKey(locationKey) {
        /**
         * @type {number}
         * @protected
         */
        this._locationKey = parseInt(locationKey);
    }


    /**
     * @returns {string}
     * @protected
     */
    get _locationBrandSlug() {
        let brandSlug = this._defaultBrandSlug;
        if (typeof this._location.brandSlug !== 'undefined' && this._location.brandSlug) {
            brandSlug = this._location.brandSlug;
        }

        return brandSlug;
    }

    /**
     * @returns {string}
     * @protected
     */
    get _locationAddress() {
        let address = '';

        if (typeof this._location.address.extraInfo !== 'undefined' && this._location.address.extraInfo) {
            address += this._location.address.extraInfo + ' - ';
        }

        if (typeof this._location.address.number !== 'undefined' && this._location.address.number) {
            address += this._location.address.number + ' ';
        }

        address += this._location.address.street + '<br>' + this._location.address.zipCode + ' ' + this._location.address.locality;

        return address;
    }

    /**
     * @returns {string}
     * @protected
     */
    get _locationDistance() {
        let distance = '';
        if (this._location.address.distance > 0) {
            if (this._location.address.distance < 1) {
                distance = this._location.address.distance * 1000;
            } else {
                distance = this._location.address.distance;
            }
        }

        return distance;
    }

    /**
     *
     * @returns {string | string}
     * @protected
     */
    get _locationPhone() {
        return this._location.contact.phone
    }

    /**
     * @returns {string}
     * @protected
     */
    get _locationDistanceUnit() {
        let distanceUnit = '';
        if (this._location.address.distance > 0) {
            if (this._location.address.distance < 1) {
                distanceUnit = 'm';
            } else {
                distanceUnit = 'km';
            }
        }

        return distanceUnit;
    }

    /**
     * @returns {{
     *  opens: {isOpenNow: boolean, class: string, label: string, sentence: string, openTime: string, closeTime: string},
     *  weekOpenHours: Object,
     *  weekSpecialHours: Object,
     *  specialOpenHours: Array,
     *  specialClosedHours: Array
     * }}
     * @protected
     */
    get _openHoursData() {
        let opensData = {
                isOpenNow: false,
                class: this._options.closedClass,
                label: this._options.closeLabel,
                sentence: this._options.closeLabel,
                openTime: '',
                closeTime: ''
            },
            weekOpenHours = {},
            weekSpecialHours = {},
            specialOpenHours = [],
            specialClosedHours = [];

        let currentDate = moment().tz(this._location.address.timezone),
            currentDateString = currentDate.format('YYYY-MM-DD'),
            currentWeekDay = parseInt(currentDate.format('E'));

        let allSpecialHours = {},
            thisWeekDays = [],
            nextWeekDays = [],
            weekDays = {};

        // Init the week special hours with date of the next 7 days
        let weekDate = moment().tz(this._location.address.timezone);
        for (let index = 0; index < 7; index++) {
            weekDays[weekDate.format('YYYY-MM-DD')] = parseInt(weekDate.format('E'));

            weekDate.add(1, 'd');
        }

        if (this._location.specialHours.length) {
            for (let key in this._location.specialHours) {
                let specialStartDate = moment.tz(this._location.specialHours[key].startDate, this._location.address.timezone),
                    specialEndDate = moment.tz(this._location.specialHours[key].endDate, this._location.address.timezone),
                    specialEndDateString = specialEndDate.format('YYYY-MM-DD');

                for (; specialStartDate.format('YYYY-MM-DD') <= specialEndDateString; specialStartDate.add(1, 'd')) {
                    let specialDateString = specialStartDate.format('YYYY-MM-DD');

                    // Check if special working hours is not older that today
                    if (currentDateString <= specialDateString) {
                        allSpecialHours[specialDateString] = this._location.specialHours[key];

                        if (this._location.specialHours[key].closed) {
                            specialClosedHours.push(this._options.daysNumber[specialStartDate.format('E')]
                                + ' ' + specialStartDate.format(this._options.dateFormat));
                        } else {
                            let specialOpenHoursString = this._location.specialHours[key].openTimeFormat
                                + ' - ' + this._location.specialHours[key].closeTimeFormat;

                            if (this._location.specialHours[key].additionalOpenTime && this._location.specialHours[key].additionalCloseTime) {
                                specialOpenHoursString += ' ' + this._options.openHoursSeparator + ' '
                                    + this._location.specialHours[key].additionalOpenTimeFormat
                                    + ' - ' + this._location.specialHours[key].additionalCloseTimeFormat;
                            }

                            specialOpenHours.push(
                                this._options.daysNumber[specialStartDate.format('E')] + ' '
                                + specialStartDate.format(this._options.dateFormat) + ' (' + specialOpenHoursString + ')'
                            );
                        }
                    }

                    if (typeof weekDays[specialDateString] !== 'undefined') {
                        weekSpecialHours[weekDays[specialDateString]] = this._location.specialHours[key];

                        if (!this._location.specialHours[key].closed) {
                            if (weekDays[specialDateString] > currentWeekDay) {
                                thisWeekDays.push(weekDays[specialDateString]);
                            } else if (weekDays[specialDateString] < currentWeekDay) {
                                nextWeekDays.push(weekDays[specialDateString]);
                            }
                        }
                    }
                }
            }
        }

        if (this._location.businessHours.length) {
            // Get working days and hours
            for (let key in this._location.businessHours) {
                if (typeof weekOpenHours[this._location.businessHours[key].startDay] === 'undefined') {
                    weekOpenHours[this._location.businessHours[key].startDay] = {};
                }
                let openHoursKey = this._location.businessHours[key].openTime + '_' + this._location.businessHours[key].closeTime;
                weekOpenHours[this._location.businessHours[key].startDay][openHoursKey] = this._location.businessHours[key];

                // Check working days to get next working day if today is closed
                if (typeof weekSpecialHours[this._location.businessHours[key].startDay] === 'undefined') {
                    if (this._location.businessHours[key].startDay > currentWeekDay) {
                        thisWeekDays.push(this._location.businessHours[key].startDay);
                    } else if (this._location.businessHours[key].startDay < currentWeekDay) {
                        nextWeekDays.push(this._location.businessHours[key].startDay);
                    }
                }
            }
        }

        // If there is at least one day when the location is opened
        if (Object.keys(weekOpenHours).length || Object.keys(weekSpecialHours).length) {
            let todayOpenTime = '',
                todayOpenTimeFormat = '',
                todayCloseTime = '',
                todayCloseTimeFormat = '';
            if (typeof weekSpecialHours[currentWeekDay] !== 'undefined') {
                // Check if today is a special open day
                if (!weekSpecialHours[currentWeekDay].closed) {
                    todayOpenTime = weekSpecialHours[currentWeekDay].openTime;
                    todayOpenTimeFormat = weekSpecialHours[currentWeekDay].openTimeFormat;
                    todayCloseTime = weekSpecialHours[currentWeekDay].closeTime;
                    todayCloseTimeFormat = weekSpecialHours[currentWeekDay].closeTimeFormat;

                    if (weekSpecialHours[currentWeekDay].additionalOpenTime && weekSpecialHours[currentWeekDay].additionalOpenTime < todayOpenTime) {
                        todayOpenTime = weekSpecialHours[currentWeekDay].additionalOpenTime;
                        todayOpenTimeFormat = weekSpecialHours[currentWeekDay].additionalOpenTimeFormat;
                    }
                    if (weekSpecialHours[currentWeekDay].additionalCloseTime && weekSpecialHours[currentWeekDay].additionalCloseTime > todayCloseTime) {
                        todayCloseTime = weekSpecialHours[currentWeekDay].additionalCloseTime;
                        todayCloseTimeFormat = weekSpecialHours[currentWeekDay].additionalCloseTimeFormat;
                    }
                }
            } else if (typeof weekOpenHours[currentWeekDay] !== 'undefined') {
                // Check today usual open hours
                for (let openHourKey in weekOpenHours[currentWeekDay]) {
                    if (!todayOpenTime || weekOpenHours[currentWeekDay][openHourKey].openTime < todayOpenTime) {
                        todayOpenTime = weekOpenHours[currentWeekDay][openHourKey].openTime;
                        todayOpenTimeFormat = weekOpenHours[currentWeekDay][openHourKey].openTimeFormat;
                    }
                    if (!todayCloseTime || weekOpenHours[currentWeekDay][openHourKey].closeTime > todayCloseTime) {
                        todayCloseTime = weekOpenHours[currentWeekDay][openHourKey].closeTime;
                        todayCloseTimeFormat = weekOpenHours[currentWeekDay][openHourKey].closeTimeFormat;
                    }
                }
            }

            // Check next day when will be opened
            let nextOpenDate = currentDate,
                nextOpenDay = currentWeekDay;
            if (thisWeekDays.length || nextWeekDays.length) {
                // check this week open date
                if (thisWeekDays.length) {
                    nextOpenDay = Math.min.apply(null, thisWeekDays);
                } else {
                    if (nextWeekDays.length) {
                        nextOpenDay = Math.min.apply(null, nextWeekDays);
                    }
                }

                nextOpenDate = moment.tz(Object.keys(weekDays).find(key => weekDays[key] === nextOpenDay), this._location.address.timezone);
            } else {
                // check future weeks open date
                let possibleOpenDate = moment().tz(this._location.address.timezone).add(7, 'd'),
                    endOpenDate = moment().tz(this._location.address.timezone).add(1, 'y'),
                    endOpenDateString = endOpenDate.format('YYYY-MM-DD');

                for (; possibleOpenDate.format('YYYY-MM-DD') <= endOpenDateString; possibleOpenDate.add(1, 'd')) {
                    let openDateString = possibleOpenDate.format('YYYY-MM-DD'),
                        openDateDay = parseInt(possibleOpenDate.format('E'));
                    if (typeof weekOpenHours[openDateDay] !== 'undefined' && typeof allSpecialHours[openDateString] === 'undefined'
                        || typeof allSpecialHours[openDateString] !== 'undefined' && !allSpecialHours[openDateString]['closed']
                    ) {
                        nextOpenDate = possibleOpenDate;
                        nextOpenDay = openDateDay;
                        break;
                    }
                }
            }

            let nextOpenTime = '',
                nextOpenTimeFormat = '',
                nextCloseTime = '',
                nextCloseTimeFormat = '',
                nextOpenDateString = nextOpenDate.format('YYYY-MM-DD');
            if (typeof allSpecialHours[nextOpenDateString] !== 'undefined'
                && !allSpecialHours[nextOpenDateString].closed
            ) {
                nextOpenTime = allSpecialHours[nextOpenDateString].openTime;
                nextOpenTimeFormat = allSpecialHours[nextOpenDateString].openTimeFormat;
                nextCloseTime = allSpecialHours[nextOpenDateString].closeTime;
                nextCloseTimeFormat = allSpecialHours[nextOpenDateString].closeTimeFormat;

                if (allSpecialHours[nextOpenDateString].additionalOpenTime
                    && allSpecialHours[nextOpenDateString].additionalOpenTime < nextOpenTime
                ) {
                    nextOpenTime = allSpecialHours[nextOpenDateString].additionalOpenTime;
                    nextOpenTimeFormat = allSpecialHours[nextOpenDateString].additionalOpenTimeFormat;
                }
                if (allSpecialHours[nextOpenDateString].additionalCloseTime
                    && allSpecialHours[nextOpenDateString].additionalCloseTime > nextCloseTime
                ) {
                    nextCloseTime = allSpecialHours[nextOpenDateString].additionalCloseTime;
                    nextCloseTimeFormat = allSpecialHours[nextOpenDateString].additionalCloseTimeFormat;
                }
            } else if (typeof weekOpenHours[nextOpenDay] !== 'undefined') {
                for (let openHourKey in weekOpenHours[nextOpenDay]) {
                    if (!nextOpenTime || weekOpenHours[nextOpenDay][openHourKey].openTime < nextOpenTime) {
                        nextOpenTime = weekOpenHours[nextOpenDay][openHourKey].openTime;
                        nextOpenTimeFormat = weekOpenHours[nextOpenDay][openHourKey].openTimeFormat;
                    }
                    if (!nextCloseTime || weekOpenHours[nextOpenDay][openHourKey].closeTime > nextCloseTime) {
                        nextCloseTime = weekOpenHours[nextOpenDay][openHourKey].closeTime;
                        nextCloseTimeFormat = weekOpenHours[nextOpenDay][openHourKey].closeTimeFormat;
                    }
                }
            }

            opensData = this._getOpensData({
                todayOpenTime: todayOpenTime,
                todayOpenTimeFormat: todayOpenTimeFormat,
                todayCloseTime: todayCloseTime,
                todayCloseTimeFormat: todayCloseTimeFormat,
                nextOpenDate: nextOpenDate,
                nextOpenTime: nextOpenTime,
                nextOpenTimeFormat: nextOpenTimeFormat,
                nextCloseTime: nextCloseTime,
                nextCloseTimeFormat: nextCloseTimeFormat,
            });
        }

        return {
            opens: opensData,
            weekOpenHours: weekOpenHours,
            weekSpecialHours: weekSpecialHours,
            specialOpenHours: specialOpenHours,
            specialClosedHours: specialClosedHours,
        };
    }

    /**
     * @param {Object} opensCloseData
     * @param {string} opensCloseData.todayOpenTime
     * @param {string} opensCloseData.todayOpenTimeFormat
     * @param {string} opensCloseData.todayCloseTime
     * @param {string} opensCloseData.todayCloseTimeFormat
     * @param {Moment} opensCloseData.nextOpenDate
     * @param {string} opensCloseData.nextOpenTime
     * @param {string} opensCloseData.nextOpenTimeFormat
     * @param {string} opensCloseData.nextCloseTime
     * @param {string} opensCloseData.nextCloseTimeFormat
     * @returns {{isOpenNow: boolean, class: string, label: string, sentence: string, openTime: string, closeTime: string}}
     * @protected
     */
    _getOpensData(opensCloseData) {
        let currentHour = moment().tz(this._location.address.timezone).format('HHmm'),
            opensData;

        if (opensCloseData.todayOpenTime && currentHour < opensCloseData.todayOpenTime) {
            opensData = {
                isOpenNow: false,
                class: this._options.closedClass,
                label: this._options.closeLabel,
                sentence: this._options.closeSentence + ' ' + opensCloseData.todayOpenTimeFormat,
                openTime: opensCloseData.todayOpenTime,
                closeTime: opensCloseData.todayCloseTime,
            };
        } else if (opensCloseData.todayCloseTime && currentHour <= opensCloseData.todayCloseTime) {
            opensData = {
                isOpenNow: true,
                class: this._options.openClass,
                label: this._options.openLabel,
                sentence: this._options.openSentence + ' ' + opensCloseData.todayCloseTimeFormat,
                openTime: opensCloseData.todayOpenTime,
                closeTime: opensCloseData.todayCloseTime,
            };
        } else {
            opensData = {
                isOpenNow: false,
                class: this._options.closedClass,
                label: this._options.closeLabel,
                sentence: this._options.closeSentence + ' ' + opensCloseData.nextOpenTimeFormat
                    + ' ' + this._options.daysNumber[opensCloseData.nextOpenDate.format('E')].substring(0, 3),
                openTime: opensCloseData.nextOpenTime,
                closeTime: opensCloseData.nextCloseTime,
            };

            // if doesn't open this week
            if (opensCloseData.nextOpenDate.format('YYYY-MM-DD') >= moment().tz(this._location.address.timezone).add(7, 'd').format('YYYY-MM-DD')) {
                opensData['sentence'] += ' ' + opensCloseData.nextOpenDate.format(this._options.dateFormat);
            }
        }

        return opensData;
    }
}
