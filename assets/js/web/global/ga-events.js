import $ from 'jquery';

export class GaEvents  {


    /**
     * Cta event
     * @param {*} event
     */
    gaCtaEvent(event) {
        const element = $(event.target);
        var eventName = element.attr('data-gtm_event'),
            eventValue = element.attr('data-gtm_event_value');


        if (eventName && eventValue) {

            this.gaSendEvent(eventName, eventValue, 'CTA');
        }

        return true;
    }


    /**
     * Search geolocation
     * @param {*} place
     * @param {*} searchResultsNo
     */
    gaSearchInputEvent(place, searchResultsNo) {
        var dataLayer = window.dataLayer = window.dataLayer || [];

        if (typeof window.canTriggereEvent == "undefined" || (window.canTriggereEvent != "undefined" && window.canTriggereEvent)) {

            this.gaSendEvent('search', place + "/" + searchResultsNo, 'search');

        }
    }

    /**
     * Geolocation
     * @param {*} visitorGeolocStore
     * @param {*} searchResultsNo
     */
    gaSearchGeoloc(visitorGeolocStore, searchResultsNo) {
        var dataLayer = window.dataLayer = window.dataLayer || [];

        if (typeof window.canTriggereEvent == "undefined" || (window.canTriggereEvent != "undefined" && window.canTriggereEvent)) {

            this.gaSendEvent('search', visitorGeolocStore.formatted_address + "/" + searchResultsNo, 'search');

        }
    }

    /**
     * Search filter
     * @param {*} queryData
     * @param {*} searchResultsNo
     */
    gaSearchFilter(queryData, searchResultsNo) {
        var searchFilterCategory = '';

        if(queryData.serviceKeys.length || queryData.brandSlugs.length) {
            var filters = $.merge(queryData.serviceKeys, queryData.brandSlugs);

            searchFilterCategory = filters.join(",");
        }

        if(searchFilterCategory.length) {
            var dataLayer = window.dataLayer = window.dataLayer || [];

            if (typeof window.canTriggereEvent == "undefined" || (window.canTriggereEvent != "undefined" && window.canTriggereEvent)) {

                this.gaSendEvent('search', searchFilterCategory + "/" + searchResultsNo, 'search');
            }
        }
    }

    /**
     *
     * @param category
     * @param label
     * @param action
     */
    gaSendEvent(category, label, action) {

        if (typeof ga === 'function') {

            ga( 'gaUa.send', 'event', {

                eventCategory: category,
                eventLabel: label,
                eventAction: action,

            } );

            ga( 'gMobiCode.send', 'event', {

                eventCategory: category,
                eventLabel: label,
                eventAction: action,

            } );

            ga( 'gClientCode.send', 'event', {

                eventCategory: category,
                eventLabel: label,
                eventAction: action,

            } );
        }


    }

    events() {

        $(document).on('click', ".ga-cta", this.gaCtaEvent.bind(this));

    }








}

$(document).ready(function () {

    //attach general events
    const searchGaEvents = new GaEvents();
    searchGaEvents.events();


});
