import axios from 'axios';
import './Pagination';

const noop = () => {};

class Collection {
  /**
   * Class constructor
   * @param props
   */
  constructor(props = {}) {
    this.options = {
      paginatorContainer: $('[data-collection-pagination]'),
      collectionWrapper: $('[data-collection-wrapper]'),
      container: $('[data-collection]'),
      currentPagePrefix: 'currentPage',
      totalItemsPrefix: 'totalResults',
      perPagePrefix: 'per_page',
      paginationPrefix: 'pages',
      resultsPrefix: 'results',
      showPaginator: true,
      apiUrl: '',
      headers: {},
      params: {},
      itemTemplate: (item) => {
        return '<div></div>'
      },
      ...props,
    };

    this.init();
  }

  /**
   * Get api url
   * @returns {string}
   */
  get apiUrl()
  {
    return this.options.apiUrl;
  }

  /**
   * Init collection function
   */
  init()
  {
    this.loadItems({
      resolve: this.onResponseSuccess,
      reject: this.onResponseFailure,
      headers: this.options.headers,
      params: this.options.params,
      url: this.apiUrl,
    });
  }

  /**
   * On response success
   * @param response
   */
  onResponseSuccess(response)
  {
    const items = response.data[this.options.resultsPrefix];
    const pagination = response.data[this.options.paginationPrefix];

    this.updateItems(items, pagination);
    this.loadingData(false);
  }

  /**
   * on response error
   * @param error
   */
  onResponseFailure(error)
  {
    this.loadingData(false);
  }

  /**
   * Update items list
   * @param items
   * @param pagination
   */
  updateItems(items = [], pagination)
  {
    let template = '';

    items.map((item) => {
      template += this.options.itemTemplate(item);

      return item;
    });

    this.options.container.html(template);
    this.options.paginatorContainer.pagination({
      itemsOnPage: this.options.params[this.options.perPagePrefix] || 10,
      currentPage: parseInt(pagination[this.options.currentPagePrefix]),
      items: parseInt(pagination[this.options.totalItemsPrefix]),
      onPageClick: this.onPageChange.bind(this),
      prevText: '<icon class="fa fa-angle-left"></icon>',
      nextText: '<icon class="fa fa-angle-right"></icon>',
    });
  }

  /**
   * On page change
   * @param page
   */
  onPageChange(page)
  {
    this.loadingData(true);
    this.loadItems({
      resolve: this.onResponseSuccess,
      reject: this.onResponseFailure,
      headers: this.options.headers,
      params: {
        ...this.options.params,
        page,
      },
      url: this.apiUrl,
    });
  }

  /**
   * Load collection items
   * @param params
   * @param url
   * @param resolve
   * @param reject
   * @param headers
   */
  loadItems({ params = {}, url = '', resolve = noop, reject = noop, headers = {} })
  {
    let queryParams = '';

    for (let prop in params) {
      queryParams+= !queryParams ? `?${prop}=${params[prop]}` : `&${prop}=${params[prop]}`
    }

    axios.get(`${url}${queryParams}`, headers)
      .then(resolve.bind(this))
      .catch(reject.bind(this))
    ;
  }

  /**
   * Loading data actions
   * @param action
   */
  loadingData(action)
  {
    let $body = $('body');

    $body.toggleClass('loading', action);

    if (action) {
      $('html, body').animate({
        scrollTop: this.options.collectionWrapper.offset().top,
      }, 200);
    }
  }
}

export default Collection;
