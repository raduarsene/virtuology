// Block location services actions
window.blocksActions.defaultActions.push({
    defaultActions: function () {
        // Do actions only if the block is on page
        var blockLocationServices = $('#block_location_services');
        if (blockLocationServices.length) {
            var infoModal = blockLocationServices.find('#service_info_modal');

            if (infoModal.length) {
                blockLocationServices.on('click', '.modal-button', function () {
                    $('body').addClass('modal-open');

                    infoModal.find('.modal-title').text($(this).data('modal-title'));
                    infoModal.find('.modal-body').html($(this).data('modal-body'));

                    infoModal.addClass('show').show();
                });

                blockLocationServices.on('click', '#modal_close_button', function () {
                    $('body').removeClass('modal-open');

                    infoModal.removeClass('show').hide();
                });
            }
        }
    }
});
