import * as SmoothScroll from 'smooth-scroll';

// Block locations actions
$(document).ready(function () {
    // Do actions only if the block is on page
    let blockLocations = $('#block_locations');
    if (blockLocations.length) {
        // Init locations letter grouped
        $.get(globalData.routes.apiLocationsGrouped, function (locations) {
            // Set top alphabet
            let searchAlphabet = blockLocations.find('#search_alphabet');
            if (searchAlphabet.length) {
                let letterPrototypeDefault = searchAlphabet.data('prototype-default'),
                    letterPrototypeLink = searchAlphabet.data('prototype-link');

                let lastLetterCode = 'Z'.charCodeAt();
                for (let letterCode = 'A'.charCodeAt(); letterCode <= lastLetterCode; letterCode++) {
                    let letter = String.fromCharCode(letterCode);
                    if (locations && typeof locations[letter] !== 'undefined') {
                        searchAlphabet.append(letterPrototypeLink.replace(/__letter__/g, letter));
                    } else {
                        searchAlphabet.append(letterPrototypeDefault.replace(/__letter__/g, letter));
                    }
                }
            }

            // Set locations letter grouped
            let resultsLetter = blockLocations.find('#results_letters_content');
            if (resultsLetter.length && locations) {
                let TemplateClass = new TemplateHelper();

                let resultsLetterPrototype = resultsLetter.data('prototype'),
                    locationPrototype = resultsLetter.data('prototype-summary');

                $.each(locations, function (letter, locationData) {
                    let lettersContentsBlock = $(resultsLetterPrototype.replace(/__letter__/g, letter));

                    let contentBlock = lettersContentsBlock.find('.letter-content');
                    if (contentBlock.length) {
                        $.each(locationData, function (index, location) {
                            let template = TemplateClass.getLocationTemplate(location, locationPrototype);

                            contentBlock.append(template);
                        });
                    }

                    resultsLetter.append(lettersContentsBlock);
                });

                // Open hours action
                let openHoursBlock = resultsLetter.find('.open-close-box');
                if (openHoursBlock.length) {
                    openHoursBlock.on('click', '.open-sentence', function (e) {
                        e.stopPropagation();

                        $(this).closest('.open-close-box').toggleClass('box-opened');
                    });
                }
            }

            // Set locations letter grouped
            let searchAlphabetSelect = blockLocations.find('#search_alphabet_select');
            if (searchAlphabetSelect.length && locations) {
                $.each(locations, function (letter) {
                    searchAlphabetSelect.append('<option value="' + letter + '">' + letter + '</option>');
                });

                searchAlphabetSelect.on('change', function () {
                    window.location.hash = "#letter_" + $(this).val();

                    return false;
                })
            }
        });

        // Search action
        if (blockLocations.find('.location-search').length) {
            blockLocations.on('input paste', '.location-search-input', function () {
                let resultsLetter = blockLocations.find('#results_letters_content'),
                    locationSummary = resultsLetter.find('.location-summary');

                let search = $(this).val();
                if (search) {
                    locationSummary.each(function (index, item) {
                        var locationSummaryText = $(item).find('h5').text().trim().replace(/\s+/g, ' ');
                        locationSummaryText += ' ' + $(item).find('address').text().trim().replace(/\s+/g, ' ');

                        var regex = RegExp(search, 'i');
                        if (regex.test(locationSummaryText)) {
                            $(item).show();
                        } else {
                            $(item).hide();
                        }
                    });
                } else {
                    locationSummary.show();
                }

                resultsLetter.find('.result-one-letter').each(function (index, item) {
                    $(item).show();

                    if (!$(item).find('.location-summary:visible').length) {
                        $(item).hide();
                    }
                });

                let searchAlphabet = blockLocations.find('#search_alphabet');
                if (searchAlphabet.length) {
                    var defaultLetterPrototype = searchAlphabet.data('prototype-default'),
                        linkLetterPrototype = searchAlphabet.data('prototype-link');

                    searchAlphabet.find('li').each(function (index, item) {
                        var letter = $(item).text().trim().replace(/\s+/g, ' ');

                        if (resultsLetter.find('#letter_' + letter).parent().is(':visible')) {
                            $(item).html(linkLetterPrototype.replace(/__letter__/g, letter));
                        } else {
                            $(item).html(defaultLetterPrototype.replace(/__letter__/g, letter));
                        }
                    });
                }
            });
        }

        // Alphabet letters smooth scroll
        new SmoothScroll('a[href*="#"]', {speed: 100});
    }
});
