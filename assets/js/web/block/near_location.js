// Block near locations actions
window.blocksActions.defaultActions.push({
    defaultActions: function () {
        // Do actions only if the block is on page
        var blockNearLocations = $('#block_near_location');
        if (blockNearLocations.length) {
            var location = globalData.currentLocation;

            var locationsSlide = blockNearLocations.find('.locations-to-slide');

            var query = {near: location.address.latitude + ',' + location.address.longitude};
            if (typeof globalSettings.near_query !== "undefined") {
                if (typeof globalSettings.near_query.radius != "undefined") {
                    query.radius = globalSettings.near_query.radius;
                }

                if (typeof globalSettings.near_query.size != "undefined") {
                    query.size = globalSettings.near_query.size;
                }

                if (typeof globalSettings.near_query.brandSlugs != "undefined") {
                    query.brandSlugs = globalSettings.near_query.brandSlugs;
                }
            }

            var useSlider = 1;
            if (typeof locationsSlide.data('useslider') !== "undefined") {
                useSlider = locationsSlide.data('useslider');
            }

            $.get(locationsSlide.data('url'), query, function (locations) {
                if (!locations || !locations.length) {
                    return;
                }

                var TemplateClass = new TemplateHelper(),
                    template = TemplateClass.getLocationsTemplates(locations, locationsSlide.data('prototype'), false);
                locationsSlide.html(template);

                // Route button
                locationsSlide.find('.location-button').on('click', function (e) {
                    e.preventDefault();

                    if (typeof globalData.useLocationRouteMap !== 'undefined' && globalData.useLocationRouteMap) {
                        window.open($(this).data('show-route'), '_blank');
                    } else {
                        switch ($(this).data('type')) {
                            case $(this).data('type-here'):
                                window.open('https://www.here.com/directions/mix/end:'
                                    + $(this).data('latitude') + ',' + $(this).data('longitude'), '_blank');
                                break;
                            case $(this).data('type-google'):
                            default:
                                window.open('https://www.google.com/maps?saddr=My+Location&daddr='
                                    + $(this).data('latitude') + ',' + $(this).data('longitude'), '_blank');
                        }
                    }
                });

                if (useSlider) {
                    // Images slider
                    locationsSlide.slick({
                        slidesToScroll: 3,
                        slidesToShow: 3,
                        dots: true,
                        arrows: false,
                        responsive: [
                            {breakpoint: 1140, settings: {slidesToShow: 2, slidesToScroll: 1}},
                            {breakpoint: 720, settings: {slidesToShow: 2, slidesToScroll: 1}},
                            {breakpoint: 540, settings: {slidesToShow: 1, slidesToScroll: 1}}
                        ]
                    });
                }

                blockNearLocations.show();
            });
        }
    }
});
