// Block location events actions
window.blocksActions.defaultActions.push({
    defaultActions: function () {
        // Do actions only if the block is on page
        var blockLocationEvents = $('#block_location_events');
        if (blockLocationEvents.length) {
            // Images slider
            var imagesBlock = blockLocationEvents.find('.events-slide');
            if (imagesBlock.length) {
                imagesBlock.slick({
                    slidesToScroll: 3,
                    slidesToShow: 3,
                    dots: true,
                    arrows: false,
                    responsive: [
                        {
                            breakpoint: 1140,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 720,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 540,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });
            }

            blockLocationEvents.show();
        }
    }
});
