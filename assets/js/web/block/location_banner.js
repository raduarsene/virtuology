// Import map class
import MapAction from '../global/map/map';

window.blocksActions.defaultActions.push({
    defaultActions: function () {
        // Do actions only if the block is on page
        var blockLocationBanner = $('#block_location_banner');
        if (blockLocationBanner.length) {
            // Update banner map
            var mapContainer = blockLocationBanner.find('#map_container_banner');
            if (mapContainer.length) {
                MapAction(blockLocationBanner, globalData.currentLocation);
            }
        }
    }
});
