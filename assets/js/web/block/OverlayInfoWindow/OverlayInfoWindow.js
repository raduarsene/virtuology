import Cookies from 'js-cookie';

export default class OverlayInfoWindow {
  /**
   * Class constructor
   * @param {*} props 
   */
  constructor(props = {}) {
    this.elements = {
      overlayInfoWindowClose: $(props.closeSelector || '[data-overlay-close]'),
      overlayInfoWindow: $(props.container || '[data-overlay]'),
    };

    this.init();
  }

  /**
   * Init class
   */
  init() {
    this.attachEvents();
    this.showOverlay();
  }

  /**
   * Attach list events
   */
  attachEvents() {
    const { elements: {overlayInfoWindowClose} } = this;

    // Handle clsoe overlay 
    overlayInfoWindowClose.on('click', this.onCloseClick.bind(this));
  }

  /**
   * On close button click
   */
  onCloseClick() {
    this.hideOverlay();
  }

  /**
   * Show overlay
   */
  showOverlay() {
    const { elements: {overlayInfoWindow} } = this;

    if (overlayInfoWindow.get(0) && !Cookies.get('dissmisOverlay')) {
      overlayInfoWindow.addClass('open');
    }
  }

  /**
   * Hide overlay
   */
  hideOverlay() {
    const { elements: {overlayInfoWindow} } = this;

    overlayInfoWindow.removeClass('open');
    this.saveCookes();
  }

  /**
   * Save cookes from overlay
   */
  saveCookes() {
    const now = new Date();

    now.setHours(now.getHours() + 24);

    Cookies.set('dissmisOverlay', true, {
      expires: now,
    });
  }
}