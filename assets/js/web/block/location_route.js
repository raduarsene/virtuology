// Import map class
import MapAction from '../global/map/map';

window.blocksActions.defaultActions.push({
    defaultActions: function () {
        // Do actions only if the block is on page
        var blockLocationRoute = $('#block_location_route');
        if (blockLocationRoute.length) {
            var location = globalData.currentLocation;

            blockLocationRoute.find('#map_route_search_current')
                .val((location.address.number ? location.address.number + ' ' : '') + location.address.street + ', ' + location.address.zipCode + ' ' + location.address.locality + ', ' + location.address.country);

            MapAction(blockLocationRoute, location);
            blockLocationRoute.hide();

            if (typeof globalData.focusOnLocationRoute !== 'undefined' && globalData.focusOnLocationRoute) {
                blockLocationRoute.show();

                setTimeout(function () {
                    $('html, body').animate({scrollTop: document.body.scrollHeight + 4000}, 'slow');
                }, 2000);
            }
        }
    }
});
