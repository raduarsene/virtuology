// Import map class
import MapAction from '../global/map/map';

var locationDetailsActions = {
    addLocationDetails: function () {
        // Do actions only if the block is on page
        var blockLocationDetails = $('#block_location_details');
        if (blockLocationDetails.length) {
            // Update page locations list
            var location = globalData.currentLocation;

            var routeButton = blockLocationDetails.find('.location-button');
            if (routeButton.length) {
                routeButton.on('click', function (e) {
                    e.preventDefault();

                    var blockLocationRoute = $('#block_location_route');
                    if (typeof globalData.useLocationRouteMap !== 'undefined' && globalData.useLocationRouteMap && blockLocationRoute.length) {
                        blockLocationRoute.show();
                        $('html, body').animate({
                            scrollTop: blockLocationRoute.offset().top
                        }, 2000);
                    } else {
                        switch ($(this).data('type')) {
                            case $(this).data('type-here'):
                                window.open('https://www.here.com/directions/mix/end:'
                                    + location.address.latitude + ',' + location.address.longitude,'_blank');
                                break;
                            case $(this).data('type-google'):
                            default:
                                window.open('https://www.google.com/maps/dir/?api=1&destination='
                                    + location.address.latitude + ',' + location.address.longitude,'_blank');
                        }
                    }
                });
            }

            MapAction(blockLocationDetails, location);
        }
    },
    addDetailsImages: function () {
        var locationAssets = window.currentLocationAssets;

        var imagesBlock = $('#block_location_details .image-slider');
        
        imagesBlock.on('init', function (event, slick, direction) {
          if (slick.slideCount === 1) {
            $('.slick-dots').hide();
          } 
        });

        var startSlider = false;
        if (imagesBlock.length && locationAssets && locationAssets.length) {
            $.each(locationAssets, function (index, item) {
                if (item.url && item.externalId.indexOf('location_') === 0) {
                    imagesBlock.append('<img src="' + item.url + '" alt="' + item.name + '"/>');
                }
            });

            startSlider = true;
        }
        else {
            if(imagesBlock.length) {

                if(imagesBlock.children() && imagesBlock.children().length) {
                    startSlider = true;
                }
            }

        }

        if(startSlider) {

            // Images slider
            imagesBlock.slick({
                slidesToScroll: 1,
                slidesToShow: 1,
                dots: true,
                arrows: false,
            });

        }

    }
};

window.blocksActions.defaultActions.push({
    defaultActions: function () {
        locationDetailsActions.addLocationDetails();
    }
});

window.blocksActions.locationAssetsActions.push({
    locationAssetsActions: function () {
        locationDetailsActions.addDetailsImages();
    }
});
