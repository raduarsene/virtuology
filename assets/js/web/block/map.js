// Import map classes
import $ from 'jquery';
import GoogleMap from './map/google/google';
import HereMap from './map/here/here';
import SearchNoMap from './map/search';

// Block map actions
$(document).ready(function () {
    // Do actions only if the block is on page
    var blockMap = $('#block_map');
    if (blockMap.length) {
        var TemplateClass = new TemplateHelper();

        // Set map actions
        var mapContainer = blockMap.find('#map_container'),
            defaultSettings = {};
        if (mapContainer.length) {
            var mapElement = mapContainer.find('#map_element');

            defaultSettings = {
                type: mapContainer.data('type'),
                credentials: mapContainer.data('credentials'),
                language: mapContainer.data('language'),
            };

            // Combine shop settings with default settings
            if (typeof globalSettings !== 'undefined') {
                defaultSettings = {...defaultSettings, ...globalSettings};
            }

            var Map;
            switch (defaultSettings.type) {
                case mapContainer.data('type-here'):
                    Map = new HereMap(defaultSettings, TemplateClass);
                    break;
                case mapContainer.data('type-google'):
                default:
                    Map = new GoogleMap(defaultSettings, TemplateClass);
            }

            // Create map
            Map.createMap(mapElement.get(0));
            window.map = Map;

            if (Map.setMapObj) {
                Map.setMapObj(Map);
            }
        }

        // Search without map actions
        var searchBlock = blockMap.find('#map_image_search_block');
        if (searchBlock.length) {
            // Combine shop settings with default settings
            if (typeof globalSettings !== 'undefined') {
                defaultSettings = {...defaultSettings, ...globalSettings};
            }

            var SearchClass = new SearchNoMap(defaultSettings, TemplateClass);

            // Search actions
            SearchClass.searchActions(searchBlock);
        }
    }
});
