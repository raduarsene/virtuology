// Block location description actions
window.blocksActions.locationAssetsActions.push({
    locationAssetsActions: function () {

        var imagesBlock = $('#block_location_description .image-slider');

        imagesBlock.on('init', function (event, slick, direction) {
          if (slick.slideCount === 1) {
            $('.slick-dots').hide();
          } 
        });

        if(!imagesBlock.length) {
            this.showDefaultImage();
            return;
        }
        
        if(imagesBlock.children() && imagesBlock.children().length) {

            // Images slider
            imagesBlock.slick({
                slidesToScroll: 1,
                slidesToShow: 1,
                dots: true,
                arrows: false,
                autoplay: true,
            });

        }
        else {

            var locationAssets = window.currentLocationAssets;
            var locationImageNo = 0;
            var defaultImgWidth = 0;
            if(typeof $("#default_img_width") !== "undefined" && $("#default_img_width")) {
                defaultImgWidth = $("#default_img_width").val();
            }
            if (locationAssets && locationAssets.length) {
                $.each(locationAssets, function (index, item) {
                    if (item.url && item.externalId.indexOf('location_') === 0) {

                        var url = item.url;
                        if(defaultImgWidth*1 > 0) {
                            url+="-w"+defaultImgWidth;
                        }

                        imagesBlock.append('<img src="' + url + '" alt="' + item.name + '"/>');
                        locationImageNo++;
                    }
                });

                var moreShopImages = $(".more-shop-images");
                if(typeof moreShopImages !== "undefined" && moreShopImages.length) {
                    var moreImages = moreShopImages.children();
                    if(moreImages.length) {
                        locationImageNo += moreImages.length;
                        imagesBlock.append(moreImages)
                    }
                }

                if(locationImageNo) {
                    // Images slider
                    imagesBlock.slick({
                        slidesToScroll: 1,
                        slidesToShow: 1,
                        dots: true,
                        arrows: false,
                        autoplay: true,
                    });
                }
                else {

                    this.showDefaultImage();
                }

            }
            else {
                //show a default image if any
                this.showDefaultImage();

            }
        }

    },
    
    showDefaultImage: function () {

        var imagesBlock = $('#block_location_description .image-slider');

        if($(".default-shop-image") && $(".default-shop-image").length) {

            imagesBlock.css('display', 'none');
            $(".default-shop-image").css('display', 'block');

        }
    }
});
