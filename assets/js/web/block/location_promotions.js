// Block location promotions actions
window.blocksActions.defaultActions.push({
    defaultActions: function () {
        // Do actions only if the block is on page
        var blockLocationPromotions = $('#block_location_promotions');
        if (blockLocationPromotions.length) {
            // Images slide
            var imagesBlock = blockLocationPromotions.find('.promotions-slide');

            if(!imagesBlock.length) {
                var imagesBlock = blockLocationPromotions.find('.events-slide');
            }

            if (imagesBlock.length) {
                imagesBlock.slick({
                    slidesToScroll: 1,
                    slidesToShow: 3,
                    infinite: false,
                    dots: true,
                    arrows: false,
                    responsive: [
                        {
                            breakpoint: 1140,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 720,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 540,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });
            }

            blockLocationPromotions.show();
        }
    }
});
$(document).ready(function () {
    //promotions show more
    var showChar = 228;
    var ellipsestext = "...";
    var moretext = $("#show_more_trans").val();
    var lesstext = $("#show_less_trans").val();
    $('.show-more-text').each(function() {
        var content = $(this).html();

        if(content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar-1, content.length - showChar);

            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<div><a href="" class="read-more">' + moretext + '</a></div></span>';

            $(this).html(html);
        }

    });

    $(".read-more").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
    //end promotion more
});
