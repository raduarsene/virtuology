import StoreLocatorMap from '../StoreLocatorMap/StoreLocatorMap';
// import './here-map-api.min';
import './here-map.v3';

export default class HereMap extends StoreLocatorMap {
  // Default Here Maps options
  static get DEFAULT_SETTINGS() {
    return {
      ...StoreLocatorMap.DEFAULT_SETTINGS,
      here_marker_cluster: {
        template: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="-100 -100 200 200"><defs><g id="a" transform="rotate(45)"><path d="M0 47A47 47 0 0 0 47 0L62 0A62 62 0 0 1 0 62Z" fill-opacity="0.7"/><path d="M0 67A67 67 0 0 0 67 0L81 0A81 81 0 0 1 0 81Z" fill-opacity="0.5"/><path d="M0 86A86 86 0 0 0 86 0L100 0A100 100 0 0 1 0 100Z" fill-opacity="0.3"/></g></defs><g fill="__color__"><circle r="42"/><g><use xlink:href="#a"/></g><g transform="rotate(120)"><use xlink:href="#a"/></g><g transform="rotate(240)"><use xlink:href="#a"/></g><text x="-8%" y="8%" font-size="50" font-weight="bold" fill="__text_color__">__text__</text></g></svg>',
        textColor: '#ffffff',
        options: {
          1: {color: '#008BFE', width: 53},
          2: {color: '#FFBF00', width: 56},
          3: {color: '#FD0000', width: 66},
          4: {color: '#FF00ED', width: 78},
          5: {color: '#9C00FF', width: 90},
        },
      },
    };
  };

  /**
   * Class constructor
   * @param {Object} settings
   * @param {TemplateHelper} TemplateClass
   */
  constructor(settings, TemplateClass) {
    super(settings);
    // Const
    // this.AUTOCOMPLETE_URL = 'https://autocomplete.geocoder.api.here.com/6.2/suggest.json';
    this.AUTOCOMPLETE_URL = 'https://discover.search.hereapi.com/v1/autosuggest'//'https://places.cit.api.here.com/places/v1/autosuggest';
    // this.GEOCODER_URL = 'https://geocoder.api.here.com/6.2/geocode.json';

    this.GEOCODER_URL = 'https://geocode.search.hereapi.com/v1/geocode';

    // Objects

    this.settings = {...HereMap.DEFAULT_SETTINGS, ...settings,
      mapType: 'here',
    };

    this.TemplateClass = TemplateClass;
  }

  /**
   * Get iso3 countries
   */
  get _getCoutriesISO3() {
    let hereCountries = [];
    let languageCountriesISO3 = {'da':'DNK','nl':'BEL','en':'GBR','fi':'FIN','fr':'FRA','de':'DEU','it':'ITA','no':'NOR','pl':'POL','pt':'PRT','ru':'RUS','es':'ESP','sv':'SWE'};
    let countriesISO3 = {'DK':'DNK','BE':'BEL','GB':'GBR','FI':'FIN','FR':'FRA','DE':'DEU','IT':'ITA','NO':'NOR','PL':'POL','PT':'PRT','RU':'RUS','ES':'ESP','SE':'SWE', 'LU':'LUX'};

    if (this.settings.geoLocationCountry) {
      if (Array.isArray(this.settings.geoLocationCountry)) {
        this.settings.geoLocationCountry.map(function (countryCode) {
          if (typeof countriesISO3[countryCode] !== 'undefined') {
            hereCountries.push(countriesISO3[countryCode]);
          }
        })
      } else {
        if (typeof countriesISO3[this.settings.geoLocationCountry] !== 'undefined') {
          hereCountries.push(countriesISO3[this.settings.geoLocationCountry]);
        }
      }
    }

    if (!hereCountries.length && this.settings.language && typeof languageCountriesISO3[this.settings.language] !== 'undefined') {
      hereCountries.push(languageCountriesISO3[this.settings.language]);
    }

    return hereCountries;
  }

  /**
   * Get map bounds
   */
  get getMapBounds() {
    const bounds = this._map.getViewModel().getLookAtData().bounds;

    return [
      bounds.getTop(),
      bounds.getLeft(),
      bounds.getBottom(),
      bounds.getRight(),
    ];
  }

  get _getCoutry() {
    let hereCountry = 'MUL'; // Multiple Languages
    let allHereCountries = {'da':'DAN','nl':'DUT','en':'ENG','fi':'FIN','fr':'FRE','de':'GER','it':'ITA','no':'NOR','pl':'POL','pt':'POR','ru':'RUS','es':'SPA','sv':'SWE'};

    if (this.settings.language && typeof allHereCountries[this.settings.language] !== 'undefined') {
      hereCountry = allHereCountries[this.settings.language];
    }

    return hereCountry;
  }

  /**
   * Init map
   * @param {*} mapElement
   */
  _initMap(mapElement) {

    if(typeof this.settings.credentials.apikey !== "undefined") {
      this._map = new H.Map(
          mapElement,
          this._defaultLayers.vector.normal.map,
          {
            pixelRatio: this._pixelRatio,
            engineType: H.map.render.RenderEngine.EngineType.P2D,
            zoom: 5
          },
      );
    }
    else {
      this._map = new H.Map(
          mapElement,
          this._defaultLayers.normal.map,
          {
            pixelRatio: this._pixelRatio
          },
      );
    }

    this._initMapSettings();
  }

  /**
   * Load map api
   */
  _loadMapApi() {
    let _this = this;

    return new Promise((resolve, reject) => {
      var storeLocatorApi = new H.service.Platform({
        apikey: _this.settings.credentials.apikey,
        useHTTPS: true
      });

      // var storeLocatorApi = new H.service.Platform({
      //   app_id: _this.settings.credentials.id,
      //   app_code: _this.settings.credentials.code,
      //   useHTTPS: true
      // });

      _this._pixelRatio = 1; //window.devicePixelRatio || 1;
      _this._defaultLayers = storeLocatorApi.createDefaultLayers({
        tileSize: _this._pixelRatio === 1 ? 256 : 512,
        ppi: _this._pixelRatio === 1 ? undefined : 320,
        language: 'en-US',
      });

      // init map group
      _this._markersCluster = new H.map.Group();

      resolve(storeLocatorApi);
    });
  }

  /**
   * Init here map settings
   */
  _initMapSettings() {
    this._mapSettingsBehavior();
    this._mapSettingsTileLayer();
    this._mapSettingsUIComponents();
    this._mapSettingsCustom();
  }

  /**
   * On map zoomchange
   * @param {*} zoom
   */
  _zoomChange(zoom) {
    if (this._infoWindow) {
      if (zoom < this.settings.map.zoom) {
        this._infoWindow.close();
      } else {
        // this._map.setCenter(this._infoWindow.getPosition());
      }
    }

    this.settings.map.zoom = zoom;
  }

  /**
   * Add map events
   */
  _addMapEvents() {
    var _this = this;
    let countEvent = 0;

    // add a resize listener to make sure that the map occupies the whole container
    window.addEventListener('resize', () => _this._map.getViewPort().resize());
    this._map.addEventListener('mapviewchangeend', (event) => {
      const currentZoom = this._map.getZoom();
      countEvent++;

      if (this.settings.map.zoom !== currentZoom) {
        this._zoomChange(currentZoom);
      }

      if (_this.isFitBoundsCentred) {
        _this.isFitBoundsCentred = false;
        return;
      }

      if (_this.isFirstAPICall) {
        if (_this.settings.defaultGeolocationSearch && !_this.autocompleteInputValue) {
          return super._getCurrentPosition((position) => {
            if (position && position.coords) {
              _this._showLocationsNear(position.coords.latitude, position.coords.longitude, true);
              _this._map.setCenter({
                lat: position.coords.latitude,
                lng: position.coords.longitude
              });
            }
          });
        }

        if (_this.autocompleteInputValue) {
          if (countEvent === 1) {
            return _this._getPlace(_this.autocompleteInputValue);
          }
        } else {
          return _this._addMapData();
        }
      }

      _this._changeSearchType('WITHIN');

      if (_this.settings.refreshControlCheckbox) {
        _this._addMapData();
      } else {
        _this.refreshControlEl.addClass('need-map-refresh');
      }
    });

    super._addMapEvents();
  }

  /**
   * Search autocomplete
   */
  _searchAutocomplete() {
    const searchAutocompleteInput = $('#map_search_autocomplete');
    const _this = this;

    if (searchAutocompleteInput.length) {
      const center = _this._map.getCenter();

      searchAutocompleteInput.autocomplete({
        minLength: 2,
        autoFocus: true,
        focus: (event) => {
          event.preventDefault();
        },
        source: (request, response) => {
          $.get(_this.AUTOCOMPLETE_URL, {
            in: `countryCode:${_this._getCoutriesISO3.join(",")}`,
            resultTypes: 'locality,addressblock,place',
            apikey: _this.settings.credentials.apikey,
            at: `${center.lat},${center.lng}`,
            lang: _this.settings.language,
            q: request.term,
            limit: 10,
          },( data) => {
            if (data && typeof data.items !== 'undefined' && data.items.length) {
              response($.map(data.items, (item) => {
                return {
                  position: (typeof item.position!=="undefined")?item.position:null,
                  locationId: item.id,
                  value: item.title,
                  label: item.title,
                };
              }));
            }
          });
        },
        select: (event, ui) => {
          if(ui.item.position) {
            let place = {};

            place.formatted_address = ui.item.value;
            place.position = ui.item.position;
            place.getPosition = () => ({...place.position});

            super._searchAutocomplete(place);
          }
          else {
            let data = {
              ...ui.item,
              position: {},
            };

            super._searchAutocomplete({
              ...data,
              formatted_address: ui.item.value,
            });
          }
        }
      }).data("ui-autocomplete")._renderItem = (ul, item) => {
        return $("<li></li>")
          .data('value', item)
          .append('<div>' + item.label + '</div>')
          .appendTo(ul);
      };
    }
  }

  /**
   * Center map by search
   * @param {*} address
   * @param {*} animateMarkers
   * @param {*} locationId
   */
  _centerMapBySearch(address, animateMarkers, locationId) {
    const _this = this;
    const urlParameters = {
      // app_id: this.settings.credentials.id,
      // app_code: this.settings.credentials.code,
      apikey: _this.settings.credentials.apikey,
      lang: this.settings.language,
      countryCode: _this._getCoutriesISO3.join(','),
      // gen: 9,
      // jsonattributes: 1,
    };

    if (locationId) {
      // urlParameters.id = locationId;
      console.log('this method is not supported anymore');
    } else {
      urlParameters.q = address;
    }

    $.get(this.GEOCODER_URL, urlParameters, this._geocodeResult.bind(this, animateMarkers));
  }

  /**
   * Geocode results event
   * @param animateMarkers
   * @param {*} results
   * @param {*} status
   */
  _geocodeResult(animateMarkers, data) {
    if (data && data.items.length && typeof  data.items[0].position !== "undefined" ) {
      const position = data.items[0].position;
      const positionParameter = {};

      positionParameter.lat = () => position.lat;
      positionParameter.lng = () => position.lng;

      super._centerMapBySearch(positionParameter, animateMarkers);
    }
  }

  /**
   * Add center marker
   * @param centerPosition
   */
  _addCenterMarker(centerPosition) {
    if (this.centerMarker) {
      this._map.removeObject(this.centerMarker);
    }

    if (this.settings.searchType === 'WITHIN' || this.settings.searchType === 'NEAR') {
      let position;

      if (Boolean(this.queryData.near)) {
        var locations = this.queryData.near.split(',');

        position = new H.geo.Point(locations[0], locations[1])
      } else {
        position = this._map.getCenter();
      }

      this.centerMarker = new H.map.Marker(centerPosition || position, {
        icon: new H.map.Icon(StoreLocatorMap.DEFAULT_SETTINGS.marker.icon_center, {
          size: {
            h: StoreLocatorMap.DEFAULT_SETTINGS.marker.icon_width_center,
            w: StoreLocatorMap.DEFAULT_SETTINGS.marker.icon_height_center,
          }
        }),
      });

      if (!this.centerMarker.getPosition) {
        this.centerMarker.getPosition = () => {
          return {
            lat: locations[0],
            lng: locations[1],
          };
        }
      }
      this._map.addObject(this.centerMarker);
    }
  }

  /**
   * Add marker to map
   * @param {*} data
   */
  _addMarkersToMap(data) {
    const _this = this;

    // First we need to create an array of DataPoint objects for the ClusterProvider
    const dataPoints = data.map(function (item) {
      // Note that we pass "null" as value for the "altitude"
      // Last argument is a reference to the original data to associate with our DataPoint
      // We will need it later on when handling events on the clusters/noise points for showing
      // details of that point
      return new H.clustering.DataPoint(item.address.latitude, item.address.longitude, null, item);
    });

    if (_this._infoWindow) {
      _this._infoWindow.close();
    }

    // Create a clustering provider with a custom theme
    const clusteredDataProvider = new H.clustering.Provider(dataPoints, {
        clusteringOptions: {
          // Maximum radius of the neighborhood
          eps: 32,

          // minimum weight of points required to form a cluster
          minWeight: 3,
        },
        // Custom clustering theme description object.
        theme: {
          getClusterPresentation: (cluster) => {
            let data = [];

            // Iterate through all points which fall into the cluster and location references to them
            cluster.forEachDataPoint(dataPoints.push.bind(data));

            const clusterMarker = new H.map.Marker(cluster.getPosition(), {
              icon: _this._createClusterIcon(cluster),

              // Set min/max zoom with values from the cluster,
              // otherwise clusters will be shown at all zoom levels:
              min: cluster.getMinZoom(),
              max: cluster.getMaxZoom(),
            });

            // Link data from the random point from the cluster to the marker,
            // to make it accessible inside _onMarkerClick
            clusterMarker.setData(data);
            return clusterMarker;
          },
          getNoisePresentation: (noisePoint) => {
            // Get a reference to data object our noise points
            let data = noisePoint.getData();

            // Create a marker for the noisePoint

            var icon = new H.map.Icon(_this.settings.marker.icon, {
              size: {w: _this.settings.marker.icon_width, h: _this.settings.marker.icon_height},
              anchor: {x: _this.settings.marker.icon_width / 2, y: _this.settings.marker.icon_height / 2}
            });

            var markerName = 'default';
            var zIndex = 10
            if (typeof globalData.brandSlugs !== 'undefined') {
              if (data.brandSlug && globalData.brandSlugs.includes(data.brandSlug)) {
                markerName = data.brandSlug;

                var zIndex = 100*globalData.brandSlugs.indexOf(markerName);

                var icon = new H.map.Icon(_this.settings.marker.icon.replace('default', markerName), {
                  size: {w: _this.settings.marker.icon_width, h: _this.settings.marker.icon_height},
                  anchor: {x: _this.settings.marker.icon_width / 2, y: _this.settings.marker.icon_height / 2}
                });

              }
            }

            let noiseMarker = new H.map.Marker(noisePoint.getPosition(), {
              // Use min zoom from a noise point
              // to show it correctly at certain zoom levels:
              min: noisePoint.getMinZoom(),
              icon: icon
            });

            // Link a data from the point to the marker
            // to make it accessible inside onMarkerClick
            noiseMarker.setData(data);
            noiseMarker.setZIndex(noiseMarker.getZIndex()+zIndex);
            noiseMarker.addEventListener('tap', _this._addEventMarkerClick.bind(_this));

            // Add marker to group for map bounds
            _this._clusterMarkerFitAll(dataPoints, noiseMarker, clusteredDataProvider);

            return noiseMarker;
          }
        },
    });

    this._mapLayer = new H.map.layer.ObjectLayer(clusteredDataProvider);
    this._map.addLayer(this._mapLayer);
  }

  /**
   * Show info window
   * @param {*} infoWindow
   * @param {*} marker
   */
  _showInfoWindow(infoWindow, marker) {
    const position = marker.getPosition();
    const mapHeight = $('.map-container').height();
    const currScreenCenter = this._map.geoToScreen(position);
    const newY = (currScreenCenter.y - (mapHeight/2 - 40));
    const newX = (currScreenCenter.x - 0);
    const newGeoCenter = this._map.screenToGeo(newX, newY);

    infoWindow.setPosition(position);
    infoWindow.open();

    this.changeMapPoints = false;

    if (window.outerWidth < 767) {
      this._map.setCenter(newGeoCenter, true);
    } else {
      this._map.setCenter(position, true);
    }
  }

  /**
   * Create here map infowwindow
   * @param {*} marker
   */
  _createHereMapInfoWindow(marker) {
    const data = marker.getData();
    const infoWindowContent = this.TemplateClass.getLocationTemplate(data, $('#map_container').data('prototype'));
    const position = marker.b;

    marker.getPosition = () => marker.b;

    if (!this._infoWindow) {
      this._infoWindow = new H.ui.InfoBubble(position, {
        content: infoWindowContent
      });

      this._ui.addBubble(this._infoWindow);
    } else {
      this._infoWindow.setContent(infoWindowContent);
    }
  }

  /**
   * Add event on marker click/tab
   * @param {*} event
   */
  _addEventMarkerClick(event) {
    const marker = event.target;
    const position = marker.b;
    const data = marker.getData();

    marker.getPosition = () => marker.b;
    marker.externalId = data.externalId;

    if (data instanceof Object && !Array.isArray(data)) {
      this._createHereMapInfoWindow(marker);
      this._onMarkerClick(this._infoWindow, marker);
    } else {
      this._map.setZoom(this._map.getZoom() + 1);
      this._map.setCenter(position, true);
    }
  }

  /**
   * Cluster marker fit all
   * @param {*} dataPoints
   * @param {*} marker
   * @param {*} clusteredDataProvider
   */
  _clusterMarkerFitAll(dataPoints, marker, clusteredDataProvider) {
    this.proccessedMarkers.push(marker);
    if (dataPoints.length === this.proccessedMarkers.length) {
      this._markersCluster.addObjects(this.proccessedMarkers);
      this._map.addObject(this._markersCluster);

      const bounds = this._markersCluster.getBoundingBox();

      if (bounds && this.proccessedMarkers.length > 1) {
        this._map.getViewModel().setLookAtData({
          bounds: bounds
        });
      } else if (this.proccessedMarkers.length === 1) {
        const proccessedMarker = this.proccessedMarkers[0];

        this._map.setCenter(proccessedMarker.getPosition(), true);
      }
    }
  }

  /**
   * Create cluster icon
   * @param {*} cluster
   */
  _createClusterIcon(cluster) {
    // Set cluster size depending on the weight
    var clusterWeight = cluster.getWeight(),
      clusterNumber = Math.floor(clusterWeight / 10),
      iconTemplate;

    clusterNumber = clusterNumber < 1 ? 1 : clusterNumber > 5 ? 5 : clusterNumber;

    if (typeof this.settings.here_marker_cluster.template !== 'undefined' && this.settings.here_marker_cluster.template) {
      iconTemplate = this.settings.here_marker_cluster.template.replace(/__text__/g, clusterWeight)
        .replace(/__color__/g, this.settings.here_marker_cluster.options[clusterNumber].color)
        .replace(/__text_color__/g, this.settings.here_marker_cluster.textColor);
    } else {
      iconTemplate = this.settings.marker_cluster.images + clusterNumber + '.png';
    }

    return new H.map.Icon(iconTemplate, {
      size: {
        w: this.settings.here_marker_cluster.options[clusterNumber].width,
        h: this.settings.here_marker_cluster.options[clusterNumber].width - 1
      },
      anchor: {
        x: this.settings.here_marker_cluster.options[clusterNumber].width / 2,
        y: (this.settings.here_marker_cluster.options[clusterNumber].width - 1) / 2
      },
    });
  }

  /**
   * Current possition
   */
  _currentPosition() {
    const _this = this;

    super._currentPosition((position) => {
      if (position && position.coords) {
        _this._showLocationsNear(position.coords.latitude, position.coords.longitude, true);

        _this._map.setCenter({
          lat: position.coords.latitude,
          lng: position.coords.longitude
        });
      }
    });
  }

  /**
   * Stop here mouse wheel propagation
   * @private
   */
  _stopHereMouseWheelEvent() {
    const that = this;
    if (this.isScrollWheelActive) {
      clearTimeout(this.isScrollWheelActive);
    }

    this.mapOverlay.addClass('visible');
    this.isScrollWheelActive = setTimeout(function() {
      that.mapOverlay.removeClass('visible');
    }, 100);
  }

  /**
   * MapEvents enables the event system
   */
  _mapSettingsBehavior() {
    if (this.settings.mapMouse) {
      this.isScrollWheelActive = null;
      const mapElement = $('.map-element');
      this.mapOverlay = $('<span class="map-overlay" />');
      let events = new H.mapevents.MapEvents(this._map);
      let behavior = new H.mapevents.Behavior(events);

      mapElement.prepend(this.mapOverlay);

      if (typeof this.settings.mapMouse.zoom === 'undefined' || this.settings.mapMouse.zoom !== true) {
        behavior.disable(H.mapevents.Behavior.WHEELZOOM);
        $(window).on('wheel', this._stopHereMouseWheelEvent.bind(this));
      }

      if (this._isMobile) {
        behavior.disable(H.mapevents.Behavior.WHEELZOOM);
        var eventStartY;

        this._map.addEventListener(function (e) {
          if (!behavior.isEnabled(H.mapevents.Behavior.DRAGGING)) {
            window.scrollBy(0, (eventStartY - e.currentPointer.viewportY));
          }
        }, 'pointermove');

        this._map.addEventListener('pointerenter', function (e) {
          eventStartY = e.currentPointer.viewportY;

          if (e.pointers.length === 2) {
            behavior.enable(H.mapevents.Behavior.DRAGGING);
          } else {
            behavior.disable(H.mapevents.Behavior.DRAGGING);
          }
        });
      }
    }
  }

  /**
   * Switches the map language
   */
  _mapSettingsTileLayer() {
    let mapTileService = this._storeLocatorApi.getMapTileService({
      type: 'base'
    }),

    mapLayer = mapTileService.createTileLayer(
      'maptile',
      'normal.day',
      this._pixelRatio === 1 ? 256 : 512,
      'png8',
      {lg: this._getCoutry, ppi: this._pixelRatio === 1 ? undefined : 320}
    );

    this._map.setBaseLayer(mapLayer);
  }

  /**
   * Create the default UI components
   */
  _mapSettingsUIComponents() {
    if (this.settings.mapZoom) {
      this._ui = H.ui.UI.createDefault(this._map, this._defaultLayers, this._getLanguage);
      this._ui.removeControl('mapsettings');

      if (typeof this.settings.mapZoom.position !== 'undefined' && this.settings.mapZoom.position) {
        let zoom = this._ui.getControl('zoom');
        zoom.setAlignment(this.settings.mapZoom.position);
      }
    }
  }

  /**
   * SET map custom settings
   */
  _mapSettingsCustom() {
    this._map.setCenter(this.settings.map.center);
    this._map.setZoom(this.settings.map.zoom);
  }
}
