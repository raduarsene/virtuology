import './here/here-map-api.min';

export default class HereMap {
    // Default Here Maps options
    static get DEFAULT_SETTINGS() {
        return {
            credentials: {id: null, code: null},
            language: null,
            map: {
                center: {lat: null, lng: null},
                zoom: null,
                fitAll: false,
            },
            mapMouse: {zoom: false}, // Set mapMouse: false - if you want to disable drag map with mouse
            mapZoom: {position: 'left-bottom'}, // Set mapZoom: false - if you want to hide zoom in/out
            marker: {
                icon: '/build/images/web/block/map/marker/icon.png',
                icon_width: 33,
                icon_height: 53,
            },
            marker_cluster: {
                images: '/build/images/web/block/map/marker/m',
            },
            here_marker_cluster: {
                template: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="-100 -100 200 200"><defs><g id="a" transform="rotate(45)"><path d="M0 47A47 47 0 0 0 47 0L62 0A62 62 0 0 1 0 62Z" fill-opacity="0.7"/><path d="M0 67A67 67 0 0 0 67 0L81 0A81 81 0 0 1 0 81Z" fill-opacity="0.5"/><path d="M0 86A86 86 0 0 0 86 0L100 0A100 100 0 0 1 0 100Z" fill-opacity="0.3"/></g></defs><g fill="__color__"><circle r="42"/><g><use xlink:href="#a"/></g><g transform="rotate(120)"><use xlink:href="#a"/></g><g transform="rotate(240)"><use xlink:href="#a"/></g><text x="-8%" y="8%" font-size="50" font-weight="bold" fill="__text_color__">__text__</text></g></svg>',
                textColor: '#ffffff',
                options: {
                    1: {color: '#008BFE', width: 53},
                    2: {color: '#FFBF00', width: 56},
                    3: {color: '#FD0000', width: 66},
                    4: {color: '#FF00ED', width: 78},
                    5: {color: '#9C00FF', width: 90},
                },
            },
            geoLocationCountry: null, // country name or a two letter ISO 3166-1 country code
            query: {
                page: null,
                size: null,
                near: null,
                radius: null,
                within: null,
                itemList: null,
            },
        }
    };

    /**
     * @param {Object} settings
     * @param {TemplateHelper} TemplateClass
     */
    constructor(settings, TemplateClass) {
        // Const
        this.AUTOCOMPLETE_URL = 'https://autocomplete.geocoder.api.here.com/6.2/suggest.json';
        this.GEOCODER_URL = 'https://geocoder.api.here.com/6.2/geocode.json';

        // Classes
        this.TemplateClass = TemplateClass;

        // Objects
        this._platform = null;
        this._defaultLayers = null;
        this._pixelRatio = null;
        this._infoWindow = null;
        this._mapLayer = null;
        this._markersGroup = null;
        this._ui = null;
        this._map = null;

        // Var
        this.changeMapPoints = true;
        this.proccessedMarkers = [];
        this.showOnSearch = false;
        this.querySearch = false;
        this.querySearchGetData = false;
        this.queryData = {};

        this.settings = {...HereMap.DEFAULT_SETTINGS, ...settings};
    }

    createMap(mapElement) {
        // Step 1: initialize communication with the platform
        this._loadMapApi();

        // Step 2: initialize the map  - not specificing a location will give a whole world view.
        this._initMap(mapElement);

        // Step 3: make the map interactive
        this._initMapSettings();

        // Map events
        this._addMapEvents();
    }

    _loadMapApi() {
        this._platform = new H.service.Platform({
            app_id: this.settings.credentials.id,
            app_code: this.settings.credentials.code,
            useHTTPS: true
        });

        this._pixelRatio = window.devicePixelRatio || 1;

        this._defaultLayers = this._platform.createDefaultLayers({
            tileSize: this._pixelRatio === 1 ? 256 : 512,
            ppi: this._pixelRatio === 1 ? undefined : 320,
            language: this._getLanguage,
        });

        // init map group
        this._markersGroup = new H.map.Group();
    }

    _initMap(mapElement) {
        this._map = new H.Map(
            mapElement,
            this._defaultLayers.normal.map,
            {pixelRatio: this._pixelRatio},
        );
    }

    _initMapSettings() {
        // MapEvents enables the event system
        this._mapSettingsBehavior();

        // Switches the map language
        this._mapSettingsTileLayer();

        // Create the default UI components
        this._mapSettingsUIComponents();

        // SET map custom settings
        this._mapSettingsCustom();
    }

    /**
     * Stop here mouse wheel propagation
     * @private
     */
    _stopHereMouseWheelEvent() {
        const that = this;

        if (this.isScrollWheelActive) {
            clearTimeout(this.isScrollWheelActive);
        }

        this.mapOverlay.addClass('visible');
        this.isScrollWheelActive = setTimeout(function() {
            that.mapOverlay.removeClass('visible');
        }, 100);
    }

    _mapSettingsBehavior() {
        if (this.settings.mapMouse) {
            this.isScrollWheelActive = null;
            const mapElement = $('.map-element');

            this.mapOverlay = $('<span class="map-overlay" />');

            mapElement.prepend(this.mapOverlay);

            var events = new H.mapevents.MapEvents(this._map);
            var behavior = new H.mapevents.Behavior(events);

            if (typeof this.settings.mapMouse.zoom === 'undefined' || this.settings.mapMouse.zoom !== true) {
                behavior.disable(H.mapevents.Behavior.WHEELZOOM);
                $(window).on('wheel', this._stopHereMouseWheelEvent.bind(this));
            }

            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                behavior.disable(H.mapevents.Behavior.WHEELZOOM);

                var eventStartY;

                this._map.addEventListener('pointermove', function (e) {
                    if (!behavior.isEnabled(H.mapevents.Behavior.DRAGGING)) {
                        window.scrollBy(0, (eventStartY - e.currentPointer.viewportY));
                    }
                });

                this._map.addEventListener('pointerenter', function (e) {
                    eventStartY = e.currentPointer.viewportY;

                    if (e.pointers.length === 2) {
                        // This means there are two finger move gesture on screen
                        behavior.enable(H.mapevents.Behavior.DRAGGING);
                    } else {
                        behavior.disable(H.mapevents.Behavior.DRAGGING);
                    }
                });
            }
        }
    }

    _mapSettingsTileLayer() {
        var mapTileService = this._platform.getMapTileService({
                type: 'base'
            }),
            // Our layer will load tiles from the HERE Map Tile API
            mapLayer = mapTileService.createTileLayer(
                'maptile',
                'normal.day',
                this._pixelRatio === 1 ? 256 : 512,
                'png8',
                {lg: this._getCoutry, ppi: this._pixelRatio === 1 ? undefined : 320}
            );

        this._map.setBaseLayer(mapLayer);
    }

    _mapSettingsUIComponents() {
        if (this.settings.mapZoom) {
            this._ui = H.ui.UI.createDefault(this._map, this._defaultLayers, this._getLanguage);

            // Remove not needed settings control
            this._ui.removeControl('mapsettings');

            // Set zoom options position
            if (typeof this.settings.mapZoom.position !== 'undefined' && this.settings.mapZoom.position) {
                var zoom = this._ui.getControl('zoom');
                zoom.setAlignment(this.settings.mapZoom.position);
            }
        }
    }

    _mapSettingsCustom() {
        // Add map coordinates
        this._map.setCenter(this.settings.map.center);
        this._map.setZoom(this.settings.map.zoom);
    }

    _addMapEvents() {
        var _this = this;

        // add a resize listener to make sure that the map occupies the whole container
        window.addEventListener('resize', () => _this._map.getViewPort().resize());

        this._map.addEventListener('mapviewchangeend', function () {
            if (_this.changeMapPoints) {
                if (typeof _this.settings.map.fitAll !== 'undefined' && _this.settings.map.fitAll && _this.proccessedMarkers.length) {
                    _this.settings.map.fitAll = false;
                } else {
                    if (_this.querySearch && !_this.querySearchGetData) {
                        _this.querySearch = false;
                    } else {
                        // Get locations data from API
                        _this._getAPILocationsData();
                    }
                }
            } else {
                _this.changeMapPoints = true;
            }
        });

        this._addSearchEvents();
    }

    _getAPILocationsData() {
        var _this = this;

        if (typeof this.settings.map.fitAll !== 'undefined' && this.settings.map.fitAll) {
            this.queryData = {};
        } else {
            this.queryData.page = this.settings.query.page;

            if (this.querySearchGetData && typeof this.queryData.near !== 'undefined' && this.queryData.near) {
                this.queryData.within = null;

                this.querySearchGetData = false;
            } else {
                this.queryData.near = null;
                this.queryData.radius = null;
                if (this.showOnSearch) {
                    this.queryData.itemList = null;
                }

                var bounds = this._map.getViewBounds();
                // Set query bounds
                this.queryData.within = [bounds.getTop(), bounds.getLeft(), bounds.getBottom(), bounds.getRight()].join();
            }
        }

        if (this.getDataRequest && typeof this.getDataRequest.abort === 'function') {
            this.getDataRequest.abort();
        }

        // Get Locations from API
        this.getDataRequest = $.get(globalData.routes.apiLocations, this.queryData, function (data) {
            // Init markers on map
            _this._initMarkersCluster();
            this.getDataRequest = null;

            if (data && data.length) {
                // Add markers to map
                _this._addClusteringMarkers(data);

                // Show shops on search list
                _this._updateSearchList(data);
            } else {
                // Init search list
                _this._updateSearchList([]);
            }
        });
    }

    _addClusteringMarkers(data) {
        var _this = this;

        // First we need to create an array of DataPoint objects for the ClusterProvider
        var dataPoints = data.map(function (item) {
            // Note that we pass "null" as value for the "altitude"
            // Last argument is a reference to the original data to associate with our DataPoint
            // We will need it later on when handling events on the clusters/noise points for showing
            // details of that point
            return new H.clustering.DataPoint(item.address.latitude, item.address.longitude, null, item);
        });

        // Create a clustering provider with a custom theme
        var clusteredDataProvider = new H.clustering.Provider(dataPoints, {
            clusteringOptions: {
                // Maximum radius of the neighborhood
                eps: 32,
                // minimum weight of points required to form a cluster
                minWeight: 2
            },
            // Custom clustering theme description object.
            theme: {
                getClusterPresentation: function (cluster) {
                    var data = [];

                    // Iterate through all points which fall into the cluster and location references to them
                    cluster.forEachDataPoint(dataPoints.push.bind(data));

                    var clusterMarker = new H.map.Marker(cluster.getPosition(), {
                        icon: _this._createClusterIcon(cluster),

                        // Set min/max zoom with values from the cluster,
                        // otherwise clusters will be shown at all zoom levels:
                        min: cluster.getMinZoom(),
                        max: cluster.getMaxZoom(),
                    });

                    // Link data from the random point from the cluster to the marker,
                    // to make it accessible inside _onMarkerClick
                    clusterMarker.setData(data);

                    return clusterMarker;
                },
                getNoisePresentation: function (noisePoint) {
                    // Get a reference to data object our noise points
                    var data = noisePoint.getData(),
                        // Create a marker for the noisePoint
                        noiseMarker = new H.map.Marker(noisePoint.getPosition(), {
                            // Use min zoom from a noise point
                            // to show it correctly at certain zoom levels:
                            min: noisePoint.getMinZoom(),
                            icon: new H.map.Icon(_this.settings.marker.icon, {
                                size: {w: _this.settings.marker.icon_width, h: _this.settings.marker.icon_height},
                                anchor: {x: _this.settings.marker.icon_width / 2, y: _this.settings.marker.icon_height / 2}
                            })
                        });

                    // Link a data from the point to the marker
                    // to make it accessible inside onMarkerClick
                    noiseMarker.setData(data);

                    // Add marker to group for map bounds
                    _this._clusterMarkerFitAll(dataPoints, noiseMarker);

                    return noiseMarker;
                }
            },
        });

        if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            // We attach the event listener to the cluster provider, and not to the individual markers
            this._addEventMarkerClick(clusteredDataProvider);
        }

        // Create a layer that will consume objects from our clustering provider
        this._mapLayer = new H.map.layer.ObjectLayer(clusteredDataProvider);

        // To make objects from clustering provider visible,
        // we need to add our layer to the map
        this._map.addLayer(this._mapLayer);
    }

    _clusterMarkerFitAll(dataPoints, marker) {
        this.proccessedMarkers.push(marker);

        if (dataPoints.length === this.proccessedMarkers.length
            && (typeof this.settings.map.fitAll !== 'undefined' && this.settings.map.fitAll || this.querySearch)
        ) {
            this._markersGroup.addObjects(this.proccessedMarkers);

            if (this._markersGroup.getBounds()) {
                this._map.setViewBounds(this._markersGroup.getBounds(), true);
            }
        }
    }

    _addEventMarkerClick(clusteredDataProvider) {
        var _this = this;

        // CLICK/TAP event handler for our markers.
        clusteredDataProvider.addEventListener('tap', function (e) {
            // Get position of the "clicked" marker
            var position = e.target.getPosition(),
                // Get the data associated with that marker
                data = e.target.getData();

            // If a marker was clicked (not cluster)
            if (data instanceof Object && !Array.isArray(data)) {

                // Merge default template with the data and get HTML
                var infoWindowContent = _this.TemplateClass.getLocationTemplate(data, $('#map_container').data('prototype'));

                // For all markers create only one infoWindow, if created reuse
                if (!_this._infoWindow) {
                    _this._infoWindow = new H.ui.InfoBubble(position, {
                        content: infoWindowContent
                    });

                    _this._ui.addBubble(_this._infoWindow);
                } else {
                    // Reuse existing infoWindow object
                    _this._infoWindow.setPosition(position);
                    _this._infoWindow.setContent(infoWindowContent);
                    _this._infoWindow.open();
                }

                // Don't change map points after recenter
                _this.changeMapPoints = false;

                // Move map's center to a clicked marker
                _this._map.setCenter(position, true);

                // Open hours expand in infoWindow
                $(document).off('click', '.info-window-block .open-close-box .open-sentence')
                    .on('click', '.info-window-block .open-close-box .open-sentence', function () {
                        $(this).closest(".open-close-box").toggleClass('box-opened');
                    });

                // Marker location route
                $(document).off('click', '.info-window-block .location-button')
                    .on('click', '.info-window-block .location-button', function (e) {
                        e.preventDefault();

                        if (typeof globalData.useLocationRouteMap !== 'undefined' && globalData.useLocationRouteMap) {
                            window.open($(this).data('show-route'),'_blank');
                        } else {
                            window.open('https://www.here.com/directions/mix/end:'
                                + $(this).data('latitude') + ',' + $(this).data('longitude'),'_blank');
                        }
                    });
            } else {
                // Increase map zoom on clicking on cluster
                _this._map.setZoom(_this._map.getZoom() + 1);

                // Move map's center to a clicked marker
                _this._map.setCenter(position, true);
            }
        });
    }

    _createClusterIcon(cluster) {
        // Set cluster size depending on the weight
        var clusterWeight = cluster.getWeight(),
            clusterNumber = Math.floor(clusterWeight / 10),
            iconTemplate;

        clusterNumber = clusterNumber < 1 ? 1 : clusterNumber > 5 ? 5 : clusterNumber;

        if (typeof this.settings.here_marker_cluster.template !== 'undefined' && this.settings.here_marker_cluster.template) {
            iconTemplate = this.settings.here_marker_cluster.template.replace(/__text__/g, clusterWeight)
                .replace(/__color__/g, this.settings.here_marker_cluster.options[clusterNumber].color)
                .replace(/__text_color__/g, this.settings.here_marker_cluster.textColor);
        } else {
            iconTemplate = this.settings.marker_cluster.images + clusterNumber + '.png';
        }

        return new H.map.Icon(iconTemplate, {
            size: {w: this.settings.here_marker_cluster.options[clusterNumber].width, h: this.settings.here_marker_cluster.options[clusterNumber].width - 1},
            anchor: {x: this.settings.here_marker_cluster.options[clusterNumber].width / 2, y: (this.settings.here_marker_cluster.options[clusterNumber].width - 1) / 2},
        });
    }

    _updateSearchList(data) {
        var _this = this;

        var searchResultsBlock = $('.map-search-box .search-results');
        if (searchResultsBlock.length) {
            this.showOnSearch = searchResultsBlock.data('search');
            var searchFilters = $('.map-search-box #map_search_filters');

            // Init location search results
            var searchResultsList = searchResultsBlock.find('.search-results-list ul');
            searchResultsList.html('');

            if (!this.showOnSearch || typeof this.queryData.near !== 'undefined' && this.queryData.near) {
                var resultsBlock = this.TemplateClass.getLocationsTemplates(data, searchResultsBlock.data('prototype'));
                searchResultsList.append(resultsBlock);

                // Open hours expand in search list
                searchResultsList.on('click', '.open-close-box .open-sentence', function () {
                    $(this).closest('.open-close-box').toggleClass('box-opened');
                });

                if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    searchResultsList.on('click', '.show-open-window', function (e) {
                        e.preventDefault();

                        var externalId = $(this).data('id');
                        if (_this.proccessedMarkers.length) {
                            $.each(_this.proccessedMarkers, function (index, marker) {
                                var markerData = marker.getData();
                                if (parseInt(markerData.externalId) === externalId) {
                                    // Show marker info window
                                    marker.dispatchEvent('tap');

                                    return false;
                                }
                            })
                        }
                    });
                }

                if (this.showOnSearch) {
                    searchResultsBlock.show();

                    if (searchFilters.length) {
                        searchFilters.show();
                    }
                }
            } else {
                if (this.showOnSearch) {
                    searchResultsBlock.hide();

                    if (searchFilters.length) {
                        searchFilters.hide();

                        searchFilters.find('[id^=filter_itemList_]').prop('checked', false);
                    }
                }
            }
        }
    }

    _addSearchEvents() {
        // Go to current position event
        this._currentPosition();

        // Input search with autocomplete
        this._searchAutocomplete();

        // Go to autocomplete address on map
        this._submitSearchAddress();

        this._filtersActions();
    }

    _currentPosition() {
        var _this = this;

        var currentPositionButton = $('#map_current_position');
        if (currentPositionButton.length) {
            currentPositionButton.on('click', function (e) {
                e.preventDefault();

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        _this._map.setCenter({
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        });
                    });
                }
            })
        }
    }

    _searchAutocomplete() {
        var _this = this;

        var searchAutocompleteInput = $('#map_search_autocomplete');
        if (searchAutocompleteInput.length) {
            searchAutocompleteInput.autocomplete({
                minLength: 2,
                autoFocus: true,
                focus: function (event) {
                    event.preventDefault();
                },
                source: function (request, response) {
                    $.get(_this.AUTOCOMPLETE_URL, {
                        app_id: _this.settings.credentials.id,
                        app_code: _this.settings.credentials.code,
                        query: request.term,
                        language: _this.settings.language,
                        country: _this._getCoutriesISO3.join(','),
                        maxresults: 10,
                        beginHighlight: '<b>',
                        endHighlight: '</b>',
                    }, function (data) {
                        if (data && typeof data.suggestions !== 'undefined' && data.suggestions.length) {
                            response($.map(data.suggestions, function (item) {
                                return {
                                    value: item.label.replace(/<b>/g, '').replace(/<\/b>/g, ''),
                                    label: item.label,
                                    locationId: item.locationId,
                                };
                            }));
                        }
                    });
                },
                select: function (event, ui) {
                    _this._centerMapBySearch(ui.item.locationId, ui.item.value);
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li></li>")
                    .data('value', item)
                    .append('<div>' + item.label + '</div>')
                    .appendTo(ul);
            };
        }
    }

    _submitSearchAddress() {
        var _this = this;

        var searchAddressButton = $('#map_search_address_button'),
            searchAutocompleteInput = $('#map_search_autocomplete');
        if (searchAddressButton.length && searchAutocompleteInput.length) {
            searchAddressButton.on('click', function (e) {
                e.preventDefault();

                var address = searchAutocompleteInput.val();
                if (address && address.length > 2) {
                    _this._centerMapBySearch(null, address);
                }
            });
        }
    }

    _centerMapBySearch(locationId, searchText) {
        var _this = this,
            urlParameters = {
                app_id: this.settings.credentials.id,
                app_code: this.settings.credentials.code,
                language: this.settings.language,
                country: _this._getCoutriesISO3.join(','),
                gen: 9,
                jsonattributes: 1,
            };

        if (locationId) {
            urlParameters.locationid = locationId;
        } else {
            urlParameters.searchtext = searchText;
        }

        $.get(this.GEOCODER_URL, urlParameters, function (data) {
            if (data && data.response.view.length && data.response.view[0].result.length && typeof data.response.view[0].result[0].location !== 'undefined') {
                var position = data.response.view[0].result[0].location.displayPosition;

                _this.querySearch = true;
                _this.querySearchGetData = true;
                _this.queryData.near = [position.latitude, position.longitude].join();
                _this.queryData.radius = _this.settings.query.radius;

                _this._getAPILocationsData();
            }
        });
    }

    _filtersActions() {
        var _this = this;

        // Map search filters
        var mapSearchFilters = $('#map_search_filters .search-filters');
        if (mapSearchFilters.length) {
            mapSearchFilters.on('click', '.open-filters', function () {
                $(this).parent('.search-filters').toggleClass('closed-box');
            });

            var filterItemsList = mapSearchFilters.find('[id^=filter_itemList_]');
            filterItemsList.on('click', function () {
                if (typeof _this.queryData.near !== 'undefined' && _this.queryData.near) {
                    _this.querySearchGetData = true;
                }

                if ($(this).data('parent')) {
                    if ($(this).prop('checked')) {
                        $(this).parent().find('ul [id^=filter_itemList_]').prop('checked', true);
                    } else {
                        $(this).parent().find('ul [id^=filter_itemList_]').prop('checked', false);
                    }
                }

                var itemsList = [];
                filterItemsList.each(function (index, item) {
                    if ($(item).prop('checked') && !$(item).data('parent')) {
                        itemsList.push($(item).val());
                    }
                });

                _this.queryData.itemList = itemsList.join();

                // Get locations data from API
                _this._getAPILocationsData();
            });
        }
    }

    _initMarkersCluster() {
        if (this._infoWindow) {
            this._infoWindow.close();
        }

        if (this._mapLayer) {
            this._map.removeLayer(this._mapLayer);
        }

        if (this._markersGroup) {
            this._markersGroup.removeAll();
        }

        this.proccessedMarkers = [];
    }

    get _getLanguage() {
        var hereLanguage = 'en-US';

        if (this.settings.language && this.settings.language !== 'en') {
            hereLanguage = this.settings.language + '-' + this.settings.language.toUpperCase();
        }

        return hereLanguage;
    }

    get _getCoutry() {
        var hereCountry = 'MUL'; // Multiple Languages
        var allHereCountries = {'da':'DAN','nl':'DUT','en':'ENG','fi':'FIN','fr':'FRE','de':'GER','it':'ITA','no':'NOR','pl':'POL','pt':'POR','ru':'RUS','es':'SPA','sv':'SWE'};

        if (this.settings.language && typeof allHereCountries[this.settings.language] !== 'undefined') {
            hereCountry = allHereCountries[this.settings.language];
        }

        return hereCountry;
    }

    get _getCoutriesISO3() {
        var hereCountries = [];
        var languageCountriesISO3 = {'da':'DNK','nl':'BEL','en':'GBR','fi':'FIN','fr':'FRA','de':'DEU','it':'ITA','no':'NOR','pl':'POL','pt':'PRT','ru':'RUS','es':'ESP','sv':'SWE'};
        var countriesISO3 = {'DK':'DNK','BE':'BEL','GB':'GBR','FI':'FIN','FR':'FRA','DE':'DEU','IT':'ITA','NO':'NOR','PL':'POL','PT':'PRT','RU':'RUS','ES':'ESP','SE':'SWE'};

        if (this.settings.geoLocationCountry) {
            if (Array.isArray(this.settings.geoLocationCountry)) {
                this.settings.geoLocationCountry.map(function (countryCode) {
                    if (typeof countriesISO3[countryCode] !== 'undefined') {
                        hereCountries.push(countriesISO3[countryCode]);
                    }
                })
            } else {
                if (typeof countriesISO3[this.settings.geoLocationCountry] !== 'undefined') {
                    hereCountries.push(countriesISO3[this.settings.geoLocationCountry]);
                }
            }
        }

        if (!hereCountries.length && this.settings.language && typeof languageCountriesISO3[this.settings.language] !== 'undefined') {
            hereCountries.push(languageCountriesISO3[this.settings.language]);
        }

        return hereCountries;
    }
}
