export default class SearchNoMap {
    // Default Search with image options
    static get DEFAULT_SETTINGS() {
        return {
            query: {
                page: 0,
                size: 3,
                search: null,
            },
        }
    };

    /**
     * @param {Object} settings
     * @param {TemplateHelper} TemplateClass
     */
    constructor(settings, TemplateClass) {
        this.TemplateClass = TemplateClass;

        this._settings = {...SearchNoMap.DEFAULT_SETTINGS, ...settings};

        // Don't change query settings for this search
        this._settings.query = {
            page: 0,
            size: 3,
            search: null,
        };
    }

    searchActions(searchBlock) {
        // Autocomplete for search
        this._searchAutocomplete(searchBlock);

        // Show address results
        this._submitSearchAddress(searchBlock);
    }

    _searchAutocomplete(searchBlock) {
        var searchAutocompleteInput = searchBlock.find('#map_image_search_input');
        if (searchAutocompleteInput.length) {
            searchAutocompleteInput.autocomplete({
                minLength: 2,
                autoFocus: true,
                focus: function (event) {
                    event.preventDefault();
                },
                source: function (request, response) {
                    $.get(globalData.routes.apiLocalLocations, {
                        page: 0,
                        size: 10,
                        search: request.term,
                    }, function (data) {
                        if (data && data.length) {
                            response($.map(data, function (item) {
                                return {
                                    value: item.name + ', ' + item.address.locality,
                                    label: item.name + ', ' + item.address.locality,
                                };
                            }));
                        }
                    });
                }
            });
        }
    }

    _submitSearchAddress(searchBlock) {
        var _this = this;

        var searchAddressButton = searchBlock.find('#map_image_search_button'),
            searchAddressInput = searchBlock.find('#map_image_search_input');
        if (searchAddressButton.length && searchAddressInput.length) {
            searchAddressButton.on('click', function (e) {
                e.preventDefault();

                _this._settings.query.search = searchAddressInput.val();
                _this._getAPILocationsData(searchBlock);
            });
        }
    }

    _getAPILocationsData(searchBlock) {
        var _this = this;

        if (this._settings.query.search) {
            // Get Locations from API
            $.get(globalData.routes.apiLocalLocations, this._settings.query, function (data) {
                if (data && data.length) {
                    // Show shops on search list
                    _this._updateSearchList(searchBlock, data);
                } else {
                    // Init search list
                    _this._initSearchList(searchBlock);
                }
            });
        } else {
            // Init search list
            this._initSearchList(searchBlock);
        }
    }

    _updateSearchList(searchBlock, locations) {
        var searchResultsBlock = searchBlock.find('#search_results_block');
        if (searchResultsBlock.length) {
            // Init location search results
            searchResultsBlock.html('');

            var searchResultsTitle = searchBlock.find('#search_results_title');
            if (searchResultsTitle.length) {
                searchResultsTitle.text(searchResultsTitle.data('title') + ' ' + searchBlock.find('#map_image_search_input').val());
            }

            var template = this.TemplateClass.getLocationsTemplates(locations, searchResultsBlock.data('prototype'), false);

            searchResultsBlock.append(template);

            searchBlock.find('#map_image_search_results').show();

            // Location route
            searchResultsBlock.off('click', '.location-button')
                .on('click', '.location-button', function (e) {
                    e.preventDefault();

                    if (typeof globalData.useLocationRouteMap !== 'undefined' && globalData.useLocationRouteMap) {
                        window.open($(this).data('show-route'),'_blank');
                    } else {
                        window.open('https://www.google.com/maps/dir/?api=1&destination='
                            + $(this).data('latitude') + ',' + $(this).data('longitude'),'_blank');
                    }
                });
        }
    }

    _initSearchList(searchBlock) {
        searchBlock.find('#map_image_search_results').hide();

        searchBlock.find('#search_results_block').html('');
    }
}
