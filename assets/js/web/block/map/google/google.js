import MarkerClusterer from './../../../block/map/google/google-markerclusterer';
import StoreLocatorMap from '../StoreLocatorMap/StoreLocatorMap';
import GoogleMapsApi from 'load-google-maps-api';

export default class GoogleMap extends StoreLocatorMap {
    // Default Google Maps options
    static get DEFAULT_SETTINGS() {
        return {
            ...StoreLocatorMap.DEFAULT_SETTINGS,
        };
    };

    /**
     * Class constructor
     * @param {Object} settings
     * @param {TemplateHelper} TemplateClass
     */
    constructor(settings, TemplateClass) {
        super(settings);

        // GOOGLE API objects
        this._markersCluster = null;
        this._geocoder = null;

        this.settings = {...GoogleMap.DEFAULT_SETTINGS, ...settings,
            mapType: 'google',
        };

        this.TemplateClass = TemplateClass;
    }

    /**
     * Get map bounds
     */
    get getMapBounds() {
        var bounds = this._map.getBounds();

        return [
            bounds.getNorthEast().lat(), 
            bounds.getSouthWest().lng(), 
            bounds.getSouthWest().lat(), 
            bounds.getNorthEast().lng(),
        ];
    }

    /**
     * Getter map options
     */
    get _mapOptions() {
        var mapOptions = {
            zoom: this.settings.map.zoom,
            center: this.settings.map.center,
            mapTypeId: this._storeLocatorApi.MapTypeId.ROADMAP,
            disableDefaultUI: true,
        };

        if (this.settings.mapMouse === false) {
            mapOptions.gestureHandling = 'none';
        }

        if (this.settings.mapZoom) {
            mapOptions.zoomControl = true;

            var mapPosition = this._storeLocatorApi.ControlPosition.LEFT_BOTTOM;
            if (typeof this.settings.mapZoom.position !== 'undefined' && this.settings.mapZoom.position) {
                mapPosition = this._storeLocatorApi.ControlPosition[this.settings.mapZoom.position.replace('-', '_').toUpperCase()];
            }
            mapOptions.zoomControlOptions = {position: mapPosition};
        }

        if (this._isMobile && !this.settings.mapZoomMobile) {
            mapOptions.zoomControl = false;
        }

        if (this.settings.mapFullScreen) {
            mapOptions.fullscreenControl = this.settings.mapFullScreen;
        }

        if (typeof this.settings.restriction !== 'undefined' && this.settings.restriction) {
            mapOptions.restriction = this.settings.restriction;
        }

        if (typeof this.settings.styles !== 'undefined' && this.settings.styles) {
            mapOptions.styles = this.settings.styles;
        }

        return mapOptions;
    }

    /**
     * Getter cluser options
     */
    get _clusterOptions() {
        var clusterStyles = [];

        for (var index = 0; index < 5; index++) {
            var clusterStyle = {
                url: this.settings.marker_cluster.images + (index + 1) + '.png',
                width: this.settings.marker_cluster.sizes[index].w,
                height: this.settings.marker_cluster.sizes[index].h,
            };

            if (typeof this.settings.marker_cluster.textColor !== 'undefined' && this.settings.marker_cluster.textColor) {
                clusterStyle.textColor = this.settings.marker_cluster.textColor;
            }

            clusterStyles.push(clusterStyle);
        }

        return {
            imagePath: this.settings.marker_cluster.images,
            styles: clusterStyles,
            averageCenter: true,
            maxZoom: this.settings.maxClusterZoom,
        };
    }

    /**
     * Load map api
     */
    _loadMapApi() {
        return GoogleMapsApi(this._mapCredentials);
    }

    /**
     * Init map
     * @param {*} mapElement 
     */
    _initMap(mapElement) {
        this._map = new this._storeLocatorApi.Map(mapElement, this._mapOptions);
        this._geocoder = new this._storeLocatorApi.Geocoder();
    }

    /**
     * Attach map events  
     */
    _addMapEvents() {
        const _that = this;
        
        if (this._isMobile) {
            this._storeLocatorApi.event.addListener(this._map, 'idle', (event) => {
                if (_that.isFitBoundsCentred) {
                    _that.isFitBoundsCentred = false;
                    return;
                }

                if (_that.isFirstAPICall) {
                    if (_that.autocompleteInputValue) {
                        return _that._getPlace(_that.autocompleteInputValue);
                    } else {
                        return _that._addMapData();
                    }
                }

                _that._changeSearchType('WITHIN');

                if (_that.settings.refreshControlCheckbox) {
                    _that._addMapData();
                } else {
                    this.refreshControlEl.addClass('need-map-refresh');
                }
            });
        } else {
            // Init map on load
            this._storeLocatorApi.event.addListenerOnce(this._map, 'tilesloaded', () => {
                _that.showOnSearch = true;

                if (_that.isFirstAPICall) {
                    if (_that.autocompleteInputValue) {
                        return _that._getPlace(_that.autocompleteInputValue);
                    } else if (_that.settings.defaultGeolocationSearch) {
                        return super._getCurrentPosition(this._currentPositionSuccess);
                    } else {
                        return _that._addMapData(true);
                    }
                } else {
                    return _that._addMapData(true);
                }
            });

            // On drag map
            this._storeLocatorApi.event.addListener(this._map, 'dragend', () => {
                _that._changeSearchType('WITHIN');
                
                if (_that.settings.refreshControlCheckbox) {
                    _that._addMapData();
                } else {
                    this.refreshControlEl.addClass('need-map-refresh');
                }
            });

            // On change zoom
            this._storeLocatorApi.event.addListener(this._map, 'zoom_changed', () => {
                if (_that.zoomChanged && typeof _that.zoomChanged === 'function') {
                    _that.zoomChanged();
                }

                if (_that.isFitBoundsCentred) {
                    _that.isFitBoundsCentred = false;
                    return;
                }

                _that._changeSearchType('WITHIN');

                if (_that.settings.refreshControlCheckbox) {
                    _that._addMapData();
                } else {
                    _that.refreshControlEl.addClass('need-map-refresh');
                }
            });
        }

        super._addMapEvents();
    }

    /**
     * Center map depend of markers
     * @param list
     */
    _fitBounds(list = [], callback = () => {}, centerPosition = null) {
        const now = new Date();
        const markers = list.length ? list : this.proccessedMarkers;
        const bounds = new this._storeLocatorApi.LatLngBounds();
        let timeAnimation = now.getTime();

        if (!markers.length) {
            return;
        }

        markers.map((marker) => {
            bounds.extend(marker.getPosition());
            return marker;
        });

        if(centerPosition) {
            bounds.extend(centerPosition);
        }

        this.isFitBoundsCentred = true;

        if (this._isMobile) {
            return bounds;
        }
        
        this.zoomChanged = () => {
            const newdate = new Date();

            timeAnimation = newdate.getTime() - timeAnimation;

            if (!this._isMobile) {
                callback(timeAnimation);
            }

            this.zoomChanged = null;
        };

        this._map.fitBounds(bounds);

        if(this.settings.map.centerOffsetX *1> 0 || this.settings.map.centerOffsetY *1 > 0) {
            if(!this._isMobile) {
                this._mapCenterOffset(this._map.getCenter(), this.settings.map.centerOffsetX, this.settings.map.centerOffsetY);
            }
        }


    }

    /**
     * Add markers to map
     * @param {*} data 
     * @param animateMarkers
     */
    _addMarkersToMap(data, animateMarkers) {
        var _this = this;

        if (!this.markersLayer) {
            this.markersLayer = new this._storeLocatorApi.OverlayView();
            this.markersLayer.draw = function () {
                this.getPanes().markerLayer.id='markerLayer';
            };
            
            this.markersLayer.setMap(this._map);
        }

        this.proccessedMarkers = data.map(function (item, index) {

            var marker = new _this._storeLocatorApi.Marker(_this._getMarkerOptions(item, index));

            if (animateMarkers) {
                marker.setAnimation(_this._storeLocatorApi.Animation.DROP);
            }

            if (!_this._isMobile) {
                // Add info window for marker
                _this._addMarkerInfoWindow(marker, item);
            }


            return marker;
        });

        // Add a marker cluster to manage the markers.
        this._markersClusterActions();
    }

    /**
     * Detect if it's clouster or fit bounce and set map zoom
     */
    _markersClusterActions() {
        const clusteringMarkers = this.settings.maxMarkersSizeForClustering < this.proccessedMarkers.length;

        // Fit all marker bounds on map
        if (clusteringMarkers && this.settings.showMarkersClusterOnSearch || this.settings.searchType === 'FIT_ALL') {
            this.isFitBoundsCentred = true;
            this._markersCluster = null;

            if (this.settings.searchType === 'NEAR') {
                this._markersCluster = new MarkerClusterer(this._map, [], this._clusterOptions);
                let allMarkers = this.proccessedMarkers;
                const prevZoom = this._map.getZoom();

                if (this.centerMarker) {
                    allMarkers = allMarkers.concat([this.centerMarker]);
                }

                if (this._isMobile) {
                    this._markersCluster = new MarkerClusterer(this._map, [], this._clusterOptions);
                    this._markersCluster.addMarkers(this.proccessedMarkers, false);                    
                    // const bounds = this._fitBounds(this.proccessedMarkers);

                    // setTimeout(() => {
                    //     this._markersCluster.fitMapToMarkers();
                    // }, 4000)
                } else {
                    this._fitBounds(this.proccessedMarkers, (time, bounds) => {
                        const nextZoom = this._map.getZoom();
    
                        this._markersCluster = new MarkerClusterer(this._map, [], this._clusterOptions);
                        setTimeout(()=> {
                            this._markersCluster.addMarkers(this.proccessedMarkers, false);
                        }, (prevZoom !== nextZoom) ? (time * 100) + 250 : 0);
                    });
                }
            } else {
                this._markersCluster = new MarkerClusterer(this._map, this.proccessedMarkers, this._clusterOptions);
                if (this.settings.searchType === 'FIT_ALL') {
                    this._markersCluster.fitMapToMarkers();
                }

                this._addCenterMarker(this._map.getBounds().getCenter());
                this._centerMap();
            }
        } else if (!clusteringMarkers) {
            this._markersCluster = null;

            var centerPosition = this._addCenterMarker(this._map.getBounds().getCenter());
            if (this.settings.searchType !== 'WITHIN') {
                this._fitBounds(this.proccessedMarkers, () => {}, centerPosition);
            }
        }
    }

    /**
     * Center map
     */
    _addCenterMarker(centerPosition) {
        if (this.centerMarker) {
            this.centerMarker.setMap(null);
        }

        if (Boolean(this.queryData.within) || Boolean(this.queryData.near)) {
            if (!this.centerMarker) {
                this.centerMarker = new this._storeLocatorApi.Marker();

                if (Boolean(StoreLocatorMap.DEFAULT_SETTINGS.marker.icon_center)) {
                    this.centerMarker.setIcon({
                        url: StoreLocatorMap.DEFAULT_SETTINGS.marker.icon_center,
                        scaledSize: new this._storeLocatorApi.Size(
                            StoreLocatorMap.DEFAULT_SETTINGS.marker.icon_width_center, 
                            StoreLocatorMap.DEFAULT_SETTINGS.marker.icon_height_center,
                        ),
                        alt: "marker"
                    });
                }
            }

            var position;

            if (Boolean(this.queryData.near)) {
                var locations = this.queryData.near.split(',');

                position = new this._storeLocatorApi.LatLng(locations[0], locations[1])
            } else {
                position = this._map.getCenter();
            }

            this.centerMarker.setMap(this._map);
            this.centerMarker.setPosition(centerPosition || position);

            return centerPosition || position;

        }
    }

    /**
     * Get marker options
     * @param {*} item 
     */
    _getMarkerOptions(item, index) {
        var markerOptions = {
            map: this._map,
            position: new this._storeLocatorApi.LatLng(item.address.latitude, item.address.longitude),
            title: item.name,
            externalId: item.externalId,
            optimized: false,
        };

        if(typeof this.settings.marker.iconIndex !== 'undefined' && index <= 9) {

            var markerUrl = this.settings.marker.iconIndex;
            markerUrl = markerUrl.replace('%%index%%', (index+1));
            markerOptions.icon = {
                url: `${markerUrl}?markerID=${item.externalId}!`,
                scaledSize: new this._storeLocatorApi.Size(this.settings.marker.iconIndex_width, this.settings.marker.iconIndex_height),
            };

        }
        else {

            if (typeof this.settings.marker.icon !== 'undefined') {
                var markerUrl = this.settings.marker.icon,
                    markerName = 'default';

                if (typeof globalData.brandSlugs !== 'undefined') {
                    if (item.brandSlug && globalData.brandSlugs.includes(item.brandSlug)) {
                        markerName = item.brandSlug;
                    } else {
                        // Random marker icon
                        markerName = globalData.brandSlugs[Math.floor((Math.random() * globalData.brandSlugs.length))];
                    }
                }

                markerOptions.markerName = markerName;
                markerOptions.icon = {
                    url: `${markerUrl.replace('default', markerName)}?markerID=${item.externalId}!`,
                    scaledSize: new this._storeLocatorApi.Size(this.settings.marker.icon_width, this.settings.marker.icon_height),
                };
            }

        }


        return markerOptions;
    }

    /**
     * Change selected marker
     * @param {*} marker 
     */
    _changeSelectedMarker(marker) {
        if (typeof this.settings.marker.icon_selected !== 'undefined' && this.settings.marker.icon_selected) {
            if (this.selectedMarker) {
                this._addDefaultMarkerIcon(this.selectedMarker);
            }
            this.selectedMarker = marker;

            var markerUrl = this.settings.marker.icon_selected,
                markerName = 'default';

            if (typeof marker.markerName !== 'undefined' && marker.markerName && typeof globalData.brandSlugs !== 'undefined'
                && globalData.brandSlugs.includes(marker.markerName)
            ) {
                markerName = marker.markerName;
            }

            marker.setIcon({
                url: `${markerUrl.replace('default', markerName)}?markerID=${marker.externalId}!`,
                scaledSize: new this._storeLocatorApi.Size(this.settings.marker.icon_width_selected, this.settings.marker.icon_height_selected),
                alt: "marker"
            });
        }
    }

    /**
     * Add default marker icon
     * @param {*} marker 
     */
    _addDefaultMarkerIcon(marker) {
        if (typeof this.settings.marker.icon !== 'undefined') {
            var markerUrl = this.settings.marker.icon,
                markerName = 'default';

            if (typeof marker.markerName !== 'undefined' && marker.markerName && typeof globalData.brandSlugs !== 'undefined'
                && globalData.brandSlugs.includes(marker.markerName)
            ) {
                markerName = marker.markerName;
            }

            marker.setIcon({
                url: `${markerUrl.replace('default', markerName)}?markerID=${marker.externalId}!`,
                scaledSize: new this._storeLocatorApi.Size(this.settings.marker.icon_width, this.settings.marker.icon_height),
                alt: "marker"
            });
        }
    }

    /**
     * Add marker infow window
     * @param {*} marker 
     * @param {*} item 
     */
    _addMarkerInfoWindow(marker, item) {
        const _that = this;

        const infoWindowContent = this.TemplateClass.getLocationTemplate(item, $('#map_container').data('prototype'));
        const infoWindow = new this._storeLocatorApi.InfoWindow({
            content: infoWindowContent,
            externalId: item.externalId,
        });

        marker.addListener('click', () => _that._onMarkerClick(infoWindow, marker));
        infoWindow.addListener('closeclick', _that._onInfoWindowCloseClick.bind(this));

        this.infoWindows.push(infoWindow);
    }

    /**
     * Show info window
     * @param {*} infoWindow 
     * @param {*} marker 
     */
    _showInfoWindow(infoWindow, marker) {
        this._closeInfoWindows();
        
        this._map.setCenter(marker.getPosition());
        marker.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);

        // change marker to selected
        this._changeSelectedMarker(marker);

        infoWindow.open(this._map, marker);

        new this._storeLocatorApi.event.addListener(infoWindow, 'domready', function () {});
    }

    /**
     * On close all info windows
     */
    _closeInfoWindows() {
        for (var index = 0; index < this.infoWindows.length; index++) {
            this.infoWindows[index].close();
        }

        if (this.selectedMarker) {
            this._addDefaultMarkerIcon(this.selectedMarker);
            this.selectedMarker.setZIndex(1);
        }
    }

    /**
     * Zoom clouser markers
     * @param {*} callback 
     */
    _zoomCluster(proccessedMarker, callback = () => {}) {
        if (this._markersCluster) {
            this._markersCluster.clusters_.map((cluster) => {
                if (cluster) {
                    const markers = cluster.getMarkers() || [];

                    markers.map((marker) => {
                        if (marker.externalId === proccessedMarker.externalId) {
                            this.refreshControlEl.addClass('need-map-refresh');
                            this._map.setZoom(this.settings.maxClusterZoom+1);
                            this._centerMap(marker);
                        }

                        return marker;
                    });
                }

                return cluster;
            });
        }

        callback();
    }

    /**
     * Handle get current position success event
     * @param {*} position 
     */
    _currentPositionSuccess(position) {
        const _this = this;

        _this._geocoder.geocode({location: {
            lat: position.coords.latitude, 
            lng: position.coords.longitude
        }}, (results, status) => {
            if (_this._storeLocatorApi.GeocoderStatus.OK === status) {
                let settingsGeoLocationCountry = _this.settings.geoLocationCountry || [];
                var geolocationCountry = results[0].address_components.filter(
                    components => {return components.types.includes('country');}
                )[0].short_name || '';
                
                if (geolocationCountry) {
                    geolocationCountry = geolocationCountry.toUpperCase();
                }
                
                _this.settings.currentUserPosition = results[0];

                if (settingsGeoLocationCountry && settingsGeoLocationCountry.toString().toUpperCase().indexOf(geolocationCountry) > -1 ) {
                    _this._showLocationsNear(position.coords.latitude, position.coords.longitude, true, true);
                } else {
                    _this._showAllLocations();
                }
            } else {
                _this._showAllLocations();
            }
        });
    }

    /**
     * Get current position DONE
     */
    _currentPosition() {
        super._currentPosition(this._currentPositionSuccess.bind(this));
    }

    /**
     * Search locations DONE
     */
    _searchAutocomplete() {
        const searchAutocompleteInput = $('#map_search_autocomplete');
        const _this = this;

        if (searchAutocompleteInput.get(0)) {
            var autocomplete = new _this._storeLocatorApi.places.Autocomplete(searchAutocompleteInput.get(0));
            
            // Restrict autocomplete results to specific country/ies
            if (this.settings.geoLocationCountry) {
                autocomplete.setComponentRestrictions({'country': this.settings.geoLocationCountry});
            }

            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', this._map);

            // On select from autocomplete
            autocomplete.addListener('place_changed', () => {
                let place = autocomplete.getPlace() || {};

                if (place.geometry) {
                    super._searchAutocomplete(autocomplete.getPlace());
                } else {
                    _this._centerMapBySearch(place.name, true);
                }      
            });
        }
    }

    /**
     * Center map DONE
     * @param {*} address 
     */
    _centerMapBySearch(address, animateMarkers) {
        const geoLocationCountry = this.settings.geoLocationCountry || [];

        if(geoLocationCountry.length == 1) {
            
            this._geocoder.geocode({
                componentRestrictions: {
                    country: geoLocationCountry[0]
                },
                'address': address
            }, this._geocodeResult.bind(this, animateMarkers));
        }
        else {

            this._geocoder.geocode({
                'address': address
            }, this._geocodeResult.bind(this, animateMarkers));

        }

    }

    /**
     * Geocode results event DONE
     * @param animateMarkers
     * @param {*} results 
     * @param {*} status 
     */
    _geocodeResult(animateMarkers, results, status) {
        let _this = this;
       
        if (_this._storeLocatorApi.GeocoderStatus.OK === status) {
            var position = results[0].geometry.location;

            super._centerMapBySearch(position, animateMarkers);
        } if (_this._storeLocatorApi.GeocoderStatus.ZERO_RESULTS === status) {
            _this._addMapData(true);
        }
    }

    /**
     * should set the center with an offset
     * @param latlng
     * @param offsetx
     * @param offsety
     * @private
     */
    _mapCenterOffset(latlng, offsetx, offsety) {

        var point1 = this._map.getProjection().fromLatLngToPoint(
            (latlng instanceof google.maps.LatLng) ? latlng : this._map.getCenter()
        );
        var point2 = new google.maps.Point(
            ( (typeof(offsetx) == 'number' ? offsetx : 0) / Math.pow(2, this._map.getZoom()) ) || 0,
            ( (typeof(offsety) == 'number' ? offsety : 0) / Math.pow(2, this._map.getZoom()) ) || 0
        );
        this._map.setCenter(this._map.getProjection().fromPointToLatLng(new google.maps.Point(
            point1.x - point2.x,
            point1.y + point2.y
        )));
    }
}
