import 'bootstrap-datepicker';
import moment from 'moment';

export default class MapListFilters {
  /**
   * Class constructor
   * @param {*} props
   */
  constructor(props = {}) {
    this.openHoursOldValue = '';
    this.items = {
      openHours: $(`[data-radio="open_hours"]`),
      inputsCheckbox: $(`[data-checkbox]`),
      selectFilters: $(`[data-select]`),
      datepicker: $('[data-datepicker]'),
      toggleOpenFilter: $('.open-filters')
    };

    this.openHoursDate = moment().utc(true).toJSON();
    this.filtersOn = false;
    this.filters = {
      openHours: null,
      serviceKeys: {},
      orServiceKeys: {},
      brandSlugs: {},
      itemList: {},
      country: null,
    };

    this.getInitialData();
    this.events();
  }

  /**
   * Get default filters
   */
  getInitialData() {
    const {inputsCheckbox, selectFilters} = this.items;

    inputsCheckbox.each((index, item) => {
      if (item.checked) {
        this.filters[item.dataset.checkbox][item.name] = item.value;
      }
    });

    selectFilters.each((index, item) => {
      if (item.value) {
        this.filters[item.dataset.select] = item.value;
      }
    });
  }

  /**
   * Save current filtres
   */
  saveCurrentFiltres() {
    this.filtersOn = true;
  }

  /**
   * Clear filters
   */
  clearFilters() {
    const { items: {inputsCheckbox, openHours, datepicker}} = this;

    this.openHoursDate = moment().utc(true).toJSON();
    this.filtersOn = false;
    this.filters = {
      openHours: null,
      serviceKeys: {},
      brandSlugs: {},
      itemList: {},
    };

    datepicker.removeClass('show-date');
    datepicker.datepicker('hide');

    inputsCheckbox.each((index, item) => {
      $(item).prop('checked', false);
    });

    openHours.each((index, item) => {
      $(item).prop('checked', false);
    });
  }

  /**
   * On date change
   */
  onDateChange(event) {
    const {items: {datepicker}} = this;
    const date = moment(event.date);

    this.openHoursDate= date.utc(true).toJSON();

    datepicker.find('.calendar-selection').html(date.utc(true).format('dddd D MMMM'));
    datepicker.addClass('show-date');
    datepicker.datepicker('hide');

    if (this.filtersOn && this.settings.mapObj) {
      this.settings.mapObj._updateFiltres(this.formatData());
    }
  }

  /**
   * Add filters events
   */
  events() {
    const {items: {inputsCheckbox, openHours, datepicker, toggleOpenFilter, selectFilters}} = this;

    datepicker.datepicker({
      maxViewMode: 1,
      keyboardNavigation: false,
      forceParse: false,
      todayHighlight: true,
      weekStart: 1,
      startDate: new Date(),
    }).on('changeDate', this.onDateChange.bind(this));

    selectFilters.on('change', this.onSelectChange.bind(this));
    inputsCheckbox.on('change', this.onCheckboxChange.bind(this));
    openHours.on('click', this.onOpenHoursClick.bind(this));
    toggleOpenFilter.on('click', this.toggleOpenFilter.bind(this))
  }

  toggleOpenFilter() {
    const {toggleOpenFilter} = this.items;

    toggleOpenFilter.parent('.search-filters').toggleClass('closed-box');
  }

  /**
   * On select change
   * @param event
   */
  onSelectChange(event) {
    const $el = $(event.currentTarget);
    const type = $el.data('select');

    this.filters[type] = $el.val() || null;

    var  current_country = $(".current_country");
    if(current_country && current_country.length) {

      var id = ('#'+$el.prop('id'));
      current_country.html($(id+' option:selected').text());
    }

    if (this.settings.mapObj) {
      this.settings.mapObj._updateFiltres(this.formatData(), true);
    }
  }

  /**
   * On open hours event click
   * @param {*} event
   */
  onOpenHoursClick(event) {
    const {items: {datepicker}} = this;
    const $el = $(event.currentTarget);
    const checked = $el.prop('checked');

    if (this.filters.openHours === $el.val() && checked) {
      this.filters.openHours = null;
      $el.prop('checked', false);
    } else {
      this.filters.openHours = $el.val();
    }

    if ($el.val() === 'openOn') {
      datepicker.datepicker('show');
    }

    if (this.settings.mapObj) {
      this.settings.mapObj._updateFiltres(this.formatData(), true);
    }
  }

  /**
   * On checkbox change
   * @param {*} event
   */
  onCheckboxChange(event) {
    const $el = $(event.currentTarget);
    const checked = $el.prop('checked');
    const type = $el.data('checkbox');
    const name = $el.prop('name');

    if (checked) {
      this.filters[type][name] = $el.val();
    } else {
      delete this.filters[type][name];
    }

    if (this.settings.mapObj) {
      this.settings.mapObj._updateFiltres(this.formatData(), true);
    }
  }

  /**
   * Format data
   */
  formatData() {
    let results = {};

    for (let filterName in this.filters) {
      if (this.filters[filterName] && typeof this.filters[filterName] === 'object') {
        results[filterName] =  [];
        for (let prop in this.filters[filterName]) {
          results[filterName].push(this.filters[filterName][prop]);
        }
      } else if (this.filters[filterName] && typeof this.filters[filterName] === 'string') {
        const property = this.filters[filterName];

        if (property !== 'openOn') {
          results[property] = true;
        } else if (this.openHoursDate) {
          results[property] = this.openHoursDate;
        }

        if (filterName === 'country') {
          results[filterName] = this.filters[filterName];
        }
      }
    }

    return results;
  }

    /**
   * Add default filters
   * @param {*} filters
   */
  addDefaultFilters(filters = {}) {
    const checkboxFilters = $(`[data-checkbox]`);
    const radioFilters = $(`[data-radio]`);

    for (let prop in filters) {
      if (filters[prop] && Array.isArray(filters[prop])) {
        filters[prop].map((value) => {
            checkboxFilters.each((index, item) => {
                const el = $(item);

                if (el.val() === value || el.val().indexOf(value) > -1) {
                    $(item).prop('checked', true);
                    this.filters[prop][`${item.name}`] = value;

                    if (item.name === 'orServiceKeys[1]') {
                      this.filters[prop][`${item.name}`] = el.val();
                    }
                }
            });

            radioFilters.each((index, item) => {
              const el = $(item);

              if (el.val() === prop) {
                el.prop('checked', filters[prop]);
              }
            });
            return value;
        });
      }
    }

    this.filtersOn = true;
  }
}
