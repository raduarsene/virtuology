export default class BrowserDetect {
  /**
   * Class constructor
   * @param {*} props 
   */
  constructor() {
    this.dataBrowser = [
      {string: navigator.userAgent, subString: "Edge", identity: "MS Edge"},
      {string: navigator.userAgent, subString: "MSIE", identity: "Explorer"},
      {string: navigator.userAgent, subString: "Trident", identity: "Explorer"},
      {string: navigator.userAgent, subString: "Firefox", identity: "Firefox"},
      {string: navigator.userAgent, subString: "Opera", identity: "Opera"},  
      {string: navigator.userAgent, subString: "OPR", identity: "Opera"},  
      {string: navigator.userAgent, subString: "Chrome", identity: "Chrome"}, 
     ];

     this.os = ["Win", "Mac", "X11", "Linux"]
     this.init();
  }

    /**
   * Getter is mobile
   */
  get _isMobile() {
    return navigator && navigator.userAgent 
      && /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  }
  
  /**
   * Init
   */
  init() {
    this.browser = (this.searchString(this.dataBrowser) || "Other").toLowerCase();
    this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
    this.osType = this.detectOs();
    $('body').addClass(`${this.browser} ${this.osType}`)
  }

  /**
   * Detect os
   */
  detectOs() {
    let osName = '';

    this.os.map((item) => {
      if (navigator.appVersion.indexOf(item) > -1) {
        osName = item.toLowerCase();
      }

      return item;
    });

    return osName;
  }

  /**
   * Search in string
   * @param {*} data 
   */
  searchString(data) {
    for (let i = 0; i < data.length; i++) {
      let dataString = data[i].string;

      this.versionSearchString = data[i].subString;

      if (dataString.indexOf(data[i].subString) !== -1) {
        return data[i].identity;
      }
    }
  }

  /**
   * Search for browser version
   * @param {*} dataString 
   */
  searchVersion(dataString) {
    const index = dataString.indexOf(this.versionSearchString);
    const rv = dataString.indexOf("rv:");

    if (index === -1) {
      return;
    }

    if (this.versionSearchString === "Trident" && rv !== -1) {
      return parseFloat(dataString.substring(rv + 3));
    } else {
      return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
    }
  }
}
