import MapListFilters from './MapListFilters';

export default class MapListView extends MapListFilters {
  /**
   * Class constructor
   * @param {*} props
   */
  constructor(props = {}) {
    super(props);

    this.elements = {
      mapElement: $('#map_container #map_element'),
      searchResultsList: $('.search-results-list'),
      resultWrapper: $('.results-wrapper'),
      searchWrapper: $('.search-wrapper'),
      searchResults: $('.search-results'),
      mapContainer: $('#map_container'),
    };

    this.settings = {
      ...props,
      mapObj: null,
    };

    this.attachEvents();
  }

  /**
   * On loader list change
   * @param {*} action
   * @param {*} data
   */
  onLoaderListChange(action, data) {
    const { elements: {searchResults}} = this;

    if (action && searchResults.hasClass('no-results')) {
      searchResults.removeClass('no-results');
    } else if (!action && !data.length) {
      searchResults.addClass('no-results');
    }

    searchResults[action? 'addClass' : 'removeClass']('loading')
  }

  /**
   * Set map obj in list
   * @param {*} instance
   */
  setMapObj(instance) {
    this.settings.mapObj = instance;
  }

  /**
   * Attach list events
   */
  attachEvents() {
    const { elements } = this;

    // Handle screen change event
    $('[data-change-screen]').on('click', this.onHandleScreenChange.bind(this));

    $('#accordion [data-toggle="collapse"]').on('click', () => {
      $($(this).data('target')).collapse('toggle');
    });
  }

  /**
   * Submit filtres
   */
  submitFilters() {
    const {settings: {mapObj}} = this;
    const fields = this.formatData();

    if (mapObj && typeof mapObj._addMapData === 'function') {
      mapObj._addMapData(true, fields);
    }
  }

  /**
   * Activate screen name
   * @param {*} name
   */
  activateListScreen(name) {
    const { elements: { searchResults } } = this;
    const next = $(`[data-screen="${name}"]`);
    const current = $('[data-screen].show');

    current.removeClass('show');
    next.addClass('show');

    if (name === 'search' && searchResults.hasClass('no-results')) {
      searchResults.removeClass('no-results');
    }

    if (current.data('screen') === 'filters' && !this.filtersOn) {
      this.clearFilters();
    }
  }

  /**
   * On handle screen change
   */
  onHandleScreenChange() {
    const el = $(event.currentTarget);
    const screenName = el.attr('data-change-screen');

    this.activateListScreen(screenName);
  }

  /**
   * On marker click select item from list
   * @param {*} externalId
   */
  selectItemFromList({externalId, marker, needScroll, animateMarker, animationType = 1}) {
    const markerLayer = $('#markerLayer');
    const markerDom = markerLayer.find(`img[src*="?markerID=${externalId}!"]`).closest('div');
    const el = $(`[data-id="${externalId}"]`).parent('li');
    const { elements: {searchResultsList} } = this;
    const lastElement = searchResultsList.find('.selected');

    if (lastElement.get(0)) {
      lastElement.removeClass('selected active');
    }

    markerLayer.find('.animate-marker').removeClass('animate-marker');
    el.addClass('selected active');

    if (needScroll) {
      this.animateScrollOnSelectedElement(el);
    }

    if (animateMarker) {
      markerDom.addClass('animate-marker');
    }
  }

  /**
   * On info window close unselect item
   * @param {*} externalId
   */
  unselectItemFromList({externalId, marker}) {
    const el = $(`[data-id="${externalId}"]`).parent('li');
    const markerDom = $(`#markerLayer .animate-marker`);
    const { elements: {resultWrapper} } = this;

    resultWrapper.find('.selected').removeClass('selected active');
    markerDom.removeClass('animate-marker');
    el.removeClass('selected active');
  }

  /**
   * Calculate animation speed
   * @param {*} a
   * @param {*} b
   * @param minSeconds
   */
  calculateAnimationRatio(a, b, minSeconds = 500) {
    let max = Math.max(a,b);
    let min = Math.min(a,b);

    return ((1 - (min/max)) * 100) * minSeconds;
  }

  /**
   * Animate scroll on selected element
   * @param {*} el
   */
  animateScrollOnSelectedElement(el) {
    const { elements: {searchResultsList} } = this;

    if (!el.get(0)) {
      return;
    }

    let index = el.index();
    let positionTop = index * el.outerHeight(true);
    let speed = positionTop < 20 ? 0 : 500;

    if (speed => 500) {
      speed = this.calculateAnimationRatio(searchResultsList.scrollTop(), positionTop) + speed;

      if (speed > 800) {
        speed = 800;
      }
    }

    searchResultsList.animate({
      scrollTop: index * el.outerHeight(true),
    }, speed);
  }
}
