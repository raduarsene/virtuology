import {GtmEvents} from '../../../global/gtm-events';
import {GaEvents} from '../../../global/ga-events';
import MapListView from '../components/MapListView';
import Cookies from 'js-cookie';

export default class StoreLocatorMap extends MapListView {
  static get DEFAULT_SETTINGS() {
    return {
      maxMarkersSizeForClustering: 1,
      maxClusterZoom: 13,
      fitBounce: true,
      showMarkersClusterOnSearch: true,
      credentials: {key: null, key_premium: null},
      defaultGeolocationSearch: false,
      refreshControlCheckbox: false,
      refreshControlMobile: true,
      removeOnMobile: false,
      language: null,
      saveRemoteData: false,
      map: {
        center: {lat: null, lng: null},
        zoom: null,
        centerOffsetX: 0,
        centerOffsetY: 0,
      },
      searchType: 'FIT_ALL', // NEAR, FIT_ALL, WITHIN
      mapLibraries: ['geometry', 'places'],
      mapMouse: true, // Set mapMouse: false - if you want to not be able to drag and zoom the map with the mouse
      mapZoom: {position: null}, // Set mapZoom: false - if you want to hide zoom in/out,
      mapZoomMobile: true,
      mapFullScreen: true,
      restriction: null,
      marker: {
        icon: '/build/images/web/block/map/marker/icon.png',
        icon_width: 33,
        icon_height: 53,
        icon_center: '/build/images/web/block/map/marker/center.png',
        icon_width_center: 30,
        icon_height_center: 30,
        icon_selected: '/build/images/web/block/map/marker/icon.png',
        icon_width_selected: 53,
        icon_height_selected: 73,
      },
      marker_cluster: {
        images: '/build/images/web/block/map/marker/m',
        sizes: [{w: 53, h: 52}, {w: 56, h: 55}, {w: 66, h: 65}, {w: 78, h: 77}, {w: 90, h: 89}],
        textColor: '#ffffff',
      },
      autocompleteUrl: null,
      geoLocationCountry: [], // country name or a two letter ISO 3166-1 country code (it can be an array of countries up to 5)
      query: {
        page: null,
        size: null,
        near: null,
        radius: null,
        onEmptyRadius: null,
        onGeolocationRadius: null,
        within: null,

        serviceKeys: null, //array
        brandSlugs: null, //array,
        itemList: null,

        openOn: null, //string
        openNow: null, // boolean
        openOnSundays: null //boolean
      },

      // Events
      onAutocompletePlaceSelect: () => null,
      onCloseAllInfoWindows: () => null,
      onLoaderListChange: () => null,
      onStorageDataLoad: () => null,
      onInfoWindowClose: () => null,
      onShopSelected: () => null,
      onClearFilters: () => null,
      onMarkerClick: () => null,
      clearSearch: () => null,

      //gtm events
      searchGtmEvents: null,
      currentSearch: null,
      currentUserPosition: null,
      searchGaEvents: null,
    }
  }

  /**
   * Class constructor
   * @param {*} options
   */
  constructor(options={}) {
    super(options);

    this.searchAutocompleteInput = $('#map_search_autocomplete');
    this.searchButton = $('#map_search_address_button');
    this.refreshControlEl = $('.map-search-options');
    this.gecolocatorButton = $('#geolocation-btn');

    // Objects
    this._storeLocatorApi = null;
    this._map = null;

    // Var
    this.autocompleteInputValue = this.searchAutocompleteInput.val();
    this.proccessedMarkers = [];
    this.centerMarker = null;
    this.infoWindows = [];
    this.showOnSearch = false;
    this.selectedMarker = null;
    this.isFirstAPICall = true;
    this.queryData = {};

    this.searchGtmEvents = new GtmEvents();
    this.searchGaEvents = new GaEvents();
  }

  /**
   * Getter is mobile
   */
  get _isMobile() {
    return navigator && navigator.userAgent
      && /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  }

  /**
   * Getter map credentials
   */
  get _mapCredentials() {
    const {credentials} = this.settings;
    let options = {};

    if (this.settings.mapLibraries) {
      options.libraries = this.settings.mapLibraries;
    }

    if (this.settings.language) {
      options.language = this.settings.language;
    }

    if (typeof credentials.key_premium !== 'undefined' && credentials.key_premium) {
      options.client = credentials.key_premium;
    } else {
      options.key = credentials.key;
    }

    return options;
  }

  /**
   * Set refresh control checkbox
   * @param value
   */
  set refreshControlCheckbox(value) {
    this.settings.refreshControlCheckbox = value;
  }

  /**
   * Set queries from cookies
   * @param {*} lastSearchQuery
   */
  _setQueryesFromCookies(lastSearchQuery) {
    if (lastSearchQuery && lastSearchQuery.query) {
      this.settings.searchType = lastSearchQuery.searchType;
      this.queryData = lastSearchQuery.query;
    }
  }

  /**
   * Get cooikes last search
   */
  _getCookieSearch() {
    const lastSearchQuery = Cookies.get('last_search_query');
    const data = lastSearchQuery ? JSON.parse(lastSearchQuery) : null;

    return data ? {
      currentSerachAddress: data.currentSerachAddress ? data.currentSerachAddress : '',
      visible_markers: data ? data.visible_markers : this.settings.visible_markers,
      searchType: data.searchType,
      query: data.query,
    } : null;
  }

  /**
   * Serialize querry data
   * @param {*} queries
   */
  serialaizeQueryData(queries) {
    let results = {};

    for (let prop in queries) {
      if (Array.isArray(queries[prop])) {
        results[prop] = queries[prop];
      } else {
        if (queries[prop]) {
          results[prop] = queries[prop];
        }
      }
    }

    return results;
  }

  /**
   * Save last search data to cookies
   */
  _saveCookieSearch() {
    const now = new Date();

    now.setHours(now.getHours() + 1);

    Cookies.set('last_search_query', JSON.stringify({
      currentSerachAddress: this._currentSearchAddress ? this._currentSearchAddress : '',
      visible_markers: this.settings.visible_markers || 0,
      searchType: this.settings.searchType,
      query: this.queryData,
    }), {
      expires: now,
    });
  }

  /**
   * Remove last search data from cookies
   */
  _removeCookieSearch() {
    Cookies.remove('last_search_query');
  }

  /**
   * Add map events
   */
  _addMapEvents() {
    /**
     * Map refresh control events
     */
    var that = this;
    this.refreshControlInput = $('#map_refresh_on_drag');
    this.refreshControlInput.on('change', this._onRefreshControlInputChange.bind(this));
    this.refreshControlInput.prop('checked', this.settings.refreshControlCheckbox);
    this.searchButton.on('click', this._onSearchButtonClick.bind(this));
    this.searchAutocompleteInput.on('keyup', function (event) {

      if (event.keyCode === 13) {

        event.preventDefault();
        that._onSearchButtonClick();

      }

    });

    if (this._isMobile && !this.settings.refreshControlMobile) {
      this.refreshControlEl.css({display: 'none'});
    }

    $(document).on('click', '.need-map-refresh', this._onMapRefreshClick.bind(this));

    // Open hours expand in infoWindow
    $(document)
      .off('click', '.open-close-box .open-sentence')
      .on('click', '.open-close-box .open-sentence', function () {
        $(this).closest('.open-close-box').toggleClass('box-opened');
      });

    // Marker location route
    $(document)
      .off('click', '.info-window-block .location-button')
      .on('click', '.info-window-block .location-button', function (e) {
        e.preventDefault();

        if (typeof globalData.useLocationRouteMap !== 'undefined' && globalData.useLocationRouteMap) {
          window.open($(this).data('show-route'), '_blank');
        } else {
          window.open('https://www.google.com/maps/dir/?api=1&destination='
            + $(this).data('latitude') + ',' + $(this).data('longitude'), '_blank');
        }
    });

    // Map search events
    this._addSearchEvents();
  }

  /**
   * On search button click
   * @param {*} event
   */
  _onSearchButtonClick(event) {
    const value = this.searchAutocompleteInput.val();

    this._getPlace(value);
  }

  /**
   * On map refresh click
   * @param {*} event
   */
  _onMapRefreshClick(event) {
    this._changeSearchType('WITHIN', true);
    this.settings.clearSearch();
    this.isMapRefresh = true;
    this._addMapData();
  }

  /**
   * Can do action after
   * @returns {*|boolean}
   * @private
   */
  _canDoAction() {
    return !this.settings.removeOnMobile && this._isMobile || !this._isMobile;
  }

  /**
   * Init marker cluster
   */
  _initMarkersCluster() {
    if (this._canDoAction()) {
      if (this.settings.mapType === 'google') {
        this.proccessedMarkers.map((marker) => {
          marker.setMap(null);

          return marker;
        });
      } else {
        if (this._mapLayer) {
          this._map.removeLayer(this._mapLayer);
        }

        if (this._markersCluster) {
          this._markersCluster.removeAll();
        }
      }

      if (this._markersCluster) {
        this._markersCluster[this.settings.mapType === 'google' ? 'clearMarkers' : 'removeAll']();
      }

      this.proccessedMarkers = [];
      this.infoWindows = [];
    }
  }

  /**
   * Create map
   * @param {*} mapElement
   */
  createMap(mapElement) {
    var _this = this;

    if (!this._canDoAction()) {
      $(mapElement).closest('.map-container').css({'display':'none'});
      $('body').addClass('map-not-visible');
    }

    // Load Map API
    this._loadMapApi().then(function (MapApi) {
      _this._storeLocatorApi = MapApi;

      // Initialize the map
      _this._initMap(mapElement);
      // Add map events
      _this._addMapEvents();
    });
  }

  /**
   * Get place
   * @param {*} address
   */
  _getPlace(address) {
    this._centerMapBySearch(address, true);
  }

  /**
   * Center marker
   */
  _centerMap(item) {
    const marker = item ? item : this.centerMarker;

    if(marker) {
      this._map.setCenter(marker.getPosition());
    }
  }

  /**
   * Show locations
   */
  _showAllLocations() {
    this.settings.searchType = 'FIT_ALL';
    this._initMarkersCluster();
    this._addMapData(true);
  }

  /**
   * Center map by search
   * @param {*} position
   * @param {*} animateMarkers
   */
  _centerMapBySearch(position, animateMarkers) {
    this._showLocationsNear(position.lat(), position.lng(), animateMarkers)
  }

  /**
   * On autocomplete focus
   * @param {*} event
   */
  _onAutocompleteInputFocus(event) {
    this._closeInfoWindows();
  }

  /**
   * Search autocomplete
   * @param {*} place
   */
  _searchAutocomplete(place) {
    let searchResultsLocation = $(".search-results-location");

    if(typeof  searchResultsLocation !=="undefined" && searchResultsLocation.length) {
      searchResultsLocation.html(place.formatted_address);
    }

    this.settings.searchType = 'NEAR';
    this.rejectChangeType = true;
    this._clearFilters();
    this._initMarkersCluster();
    this._clearSearchList();

    const position = this.settings.mapType === 'here' ? place.position : place.geometry.location;
    const lat = typeof position.lat === 'function' ? position.lat() : position.lat;
    const lng = typeof position.lng === 'function' ? position.lng() : position.lng;

    this._showLocationsNear(lat, lng, true);
    this.settings.onAutocompletePlaceSelect(place);
    this.settings.currentSearch = place.formatted_address;
  }

  /**
   * Get api locations
   * @param animateMarkers
   * @param queries
   */
  _getAPILocationsData(animateMarkers, queries = {}) {
    const _this = this;

    this._getFiltersForSearch(queries);

    if (this.getLocationsRequest && typeof this.getLocationsRequest.abort === 'function') {
      this.getLocationsRequest.abort();
    }

    this.isFitBoundsCentred = false;
    this._toggleResultsLoader(true);
    this._clearSearchList();

    const lastSearchQuery = this._getCookieSearch();

    if (this.isFirstAPICall && this.settings.saveRemoteData) {
      this._addLocalStorgeMarkers(lastSearchQuery);
    }

    // Get Locations from API
    this.getLocationsRequest = $.get(globalData.routes.apiLocations, this.queryData, function (data) {
      // Init markers on map
      _this.getLocationsRequest = null;
      _this._initMarkersCluster();

      if (data && data.length) {
        // Add markers to map
        if (_this._canDoAction()) {
          _this._addMarkersToMap(data, animateMarkers);
        }
      } else {
        if (_this.wasAddedLocalStorgeMarkers) {
          _this._initMarkersCluster();
          _this._clearSearchList();
        }

        if (_this._canDoAction()) {
          _this._addCenterMarker();
        }
      }


      if (_this.settings.searchType !== 'FIT_ALL' && _this.settings.saveRemoteData) {
        _this._setLocalStorageData(data);
      }

      _this.wasAddedLocalStorgeMarkers = false;
      _this._updateSearchList(data);
      _this._toggleResultsLoader(false, data);
      _this.rejectChangeType = false;
      _this.isFirstAPICall = false;
      _this.isMapRefresh = false;

      if(_this.settings.searchType == 'NEAR') {
        if(_this.settings.currentSearch) {
          _this.searchGtmEvents.gtmSearchInputEvent(_this.settings.currentSearch, data.length);
          _this.searchGaEvents.gaSearchInputEvent(_this.settings.currentSearch, data.length);
          _this.settings.currentSearch = null;
        }
      }

      if(_this.settings.currentUserPosition) {
        _this.searchGtmEvents.gtmSearchGeoloc(_this.settings.currentUserPosition, data.length);
        _this.searchGaEvents.gaSearchGeoloc(_this.settings.currentUserPosition, data.length);
        _this.settings.currentUserPosition = null;
      }

      _this.searchGtmEvents.gtmSearchFilter(_this.queryData, data.length);
      _this.searchGaEvents.gaSearchFilter(_this.queryData, data.length);
    });
  }

  /**
   * Add local storge markers
   * @param {*} query
   */
  _addLocalStorgeMarkers(query) {
    if (window.localStorage) {
      const lastSearch = window.localStorage.getItem('last_search');
      const data = window.localStorage.getItem('last_search') ? JSON.parse(lastSearch) : { items: [] };

      if (Array.isArray(data.items) && data.items.length && (query || data.search_type === 'FIT_ALL')) {
        this.wasAddedLocalStorgeMarkers = true;
        this.showOnSearch = true;

        this._initMarkersCluster();

        if (this._canDoAction()) {
          this._addMarkersToMap(data.items, false);
        }

        this._updateSearchList(data.items);
        this._toggleResultsLoader(false, data.items);
        this.settings.onStorageDataLoad({
          searchType: this.settings.searchType,
          ui: {
            item: {
              value: query ? query.currentSerachAddress : '',
            },
          },
          items: data.items,
          query,
        });
      }
    }
  }

  /**
   * Set localstorge data
   * @param {*} data
   */
  _setLocalStorageData(data = []) {
    if (window.localStorage) {
      if (data.length) {
        window.localStorage.setItem('last_search', JSON.stringify({
          search_type: this.settings.searchType,
          items: data,
        }));
      } else {
        window.localStorage.removeItem('last_search');
      }
    }
  }

  /**
   * Clear search list
   */
  _clearSearchList() {
    var searchResultsList = $('.search-results ul');

    searchResultsList.html('');
  }

  /**
   * Extend map settings
   * @param {*} settings
   */
  extendSettings(settings = {}) {
    this.settings = {
      ...this.settings,
      ...settings
    };
  }

  /**
   * Change search type
   * @param {*} type
   * @param canChange
   */
  _changeSearchType(type, action) {
    const canChangeType = this.settings.refreshControlCheckbox || action;

    if (
      !this.rejectChangeType && canChangeType ||
      !this.rejectChangeType && type !== 'WITHIN'
    ) {
      this.settings.searchType = type;
    }

    this.rejectChangeType = false;
  }

  /**
   * Add map data
   * @param animateMarkers
   * @param queries
   */
  _addMapData(animateMarkers, queries={}) {
    const formData = {
      ...this.formatData(),
    };

    const queryResults = {
      ...queries,
      ...formData,
    };

    this._getAPILocationsData(animateMarkers, queryResults);
  }

  /**
   * Generate filters for search
   * @param {*} queries
   */
  _getFiltersForSearch(queries = {}) {
    if (this.settings.searchType === 'FIT_ALL') {
      this.queryData = {
        fitAll: true,
      };

      if(typeof this.settings.query.near !=="undefined" && this.settings.query.near) {
        this.queryData.near = this.settings.query.near;
      }

      for (let prop in queries) {
        this.queryData[prop] = queries[prop];
      }

      this.showOnSearch = true;
    } else {
      delete this.queryData.fitAll;
      this.queryData.page = this.settings.query.page;
      this.queryData.size = this.settings.query.size;

      if (this.settings.searchType === 'NEAR') {
        const center = this._map.getCenter();
        const position = {...center};

        delete this.queryData.within;

        if (typeof position.lat !== 'function') {
          position.lat = () => center.lat;
        }

        if (typeof position.lng !== 'function') {
          position.lng = () => center.lng;
        }

        if (!this.queryData.near) {
          this.queryData.near = [position.lat(), position.lng()].join();
        }

        // this.queryData.radius = this.settings.query.radius;
        // this.queryData.onEmptyRadius = this.settings.query.onEmptyRadius;



      } else {
        delete this.queryData.near;
        delete this.queryData.radius;
        delete this.queryData.onEmptyRadius;

        // Set query bounds
        this.queryData.within = this.getMapBounds.join();
      }

      this.queryData = $.extend(this.queryData, {...queries});
    }

    const lastSearchQuery = this._getCookieSearch();

    if (this.isFirstAPICall && this.settings.saveRemoteData) {
      this._setQueryesFromCookies(lastSearchQuery);
      this.queryData = this.serialaizeQueryData(this.queryData);
      this.addDefaultFilters(this.queryData);
      return;
    }

    this.queryData = this.serialaizeQueryData(this.queryData);

    if (this.settings.searchType !== 'FIT_ALL' && this.settings.saveRemoteData) {
      this._saveCookieSearch();
    }
  }

  /**
   * Update filtres
   * @param {*} queries
   */
  _updateFiltres(queries={}, updateMap) {
    this._getFiltersForSearch(queries);

    if (!this.settings.refreshControlCheckbox && !updateMap) {
      this.refreshControlEl.addClass('need-map-refresh');
    } else {
      this._addMapData();
    }
  }

  /**
   * On refresh control input change
   * @param {*} event
   */
  _onRefreshControlInputChange(event) {
    const checked = event.currentTarget.checked;

    this.refreshControlCheckbox = checked;
  }

  /**
   * Toggle show result laoder
   * @param {*} el
   * @param {*} show
   */
  _toggleResultsLoader(show, data) {
    this.onLoaderListChange(show, data);
    this.refreshControlEl[ show ? 'addClass' : 'removeClass']('loading');
    this.settings.onLoaderListChange(show, data);

    if (!show && this.refreshControlEl.hasClass('need-map-refresh')) {
      this.refreshControlEl.removeClass('need-map-refresh');
    }
  }

  /**
   * Show near location
   * @param {*} latitude
   * @param {*} longitude
   */
  _showLocationsNear(latitude, longitude, animateMarkers, useGeolocationRadius) {
    this.queryData.near = [latitude, longitude].join();
    this.queryData.radius = this.settings.query.radius;
    if(typeof useGeolocationRadius !== "undefined" && useGeolocationRadius && this.settings.query.onGeolocationRadius) {
      this.queryData.radius = this.settings.query.onGeolocationRadius;
    }
    this.queryData.onEmptyRadius = this.settings.query.onEmptyRadius;
    this.settings.searchType = 'NEAR';
    this.showOnSearch = true;
    this.rejectChangeType = true;
    this.queryData.fitAll = false;

    if (this._canDoAction()) {
      this._addCenterMarker(this.currentSearchPosition);
      this._centerMap(this.centerMarker);
      this._map.setZoom(10);
    }

    this._addMapData(animateMarkers);
  }

  /**
   * Clear search filters
   */
  _clearFilters() {
    delete this.queryData.openOnSundays;
    delete this.queryData.serviceKeys;
    delete this.queryData.brandSlugs;
    delete this.queryData.itemList;
    delete this.queryData.openNow;
    delete this.queryData.openOn;
    this.settings.onClearFilters();
  }

  /**
   * Add search events
   */
  _addSearchEvents() {
    this._currentPosition();
    this._searchAutocomplete();
  }

  /**
   * Get current position
   * @param {*} success
   * @param {*} error
   */
  _getCurrentPosition(success = () => {}, error = () => {}) {
    const _this = this;

    if (typeof navigator !== 'undefined' && typeof navigator.geolocation !== 'undefined' && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        if (typeof success === 'function') {
          success(position);
        }
      }, (err) => {
        _this._showAllLocations();
        if (typeof error === 'function') {
          error(err);
        }
      });
    } else {
      _this._showAllLocations();
      if (typeof error === 'function') {
        error()
      }
    }
  }

  /**
   * Get current position
   */
  _currentPosition(success = () => {}, error = () => {}) {
    const currentPositionButton = $('#map_current_position, #geolocation-btn');
    const _this = this;

    if (currentPositionButton.length) {
      currentPositionButton.on('click',(e) => {
        e.preventDefault();

        _this._getCurrentPosition(success, error);
      })
    }
  }

  /**
   * On marker click
   * @param {*} infoWindow
   * @param {*} marker
   */
  _onMarkerClick(infoWindow, marker) {
    this._showInfoWindow(infoWindow, marker);
    this.settings.onMarkerClick(infoWindow, marker);
    this.selectItemFromList({
      externalId: marker.externalId,
      needScroll: true,
      marker,
    });
  }

  /**
   * On info window close click
   */
  _onInfoWindowCloseClick() {
    if (this.selectedMarker) {
      this.settings.onInfoWindowClose(this.selectedMarker);
      this._addDefaultMarkerIcon(this.selectedMarker);
      this.unselectItemFromList({
        externalId: this.selectedMarker.externalId,
        marker: this.selectedMarker,
      });

      this.selectedMarker = null;
    }
  }

  /**
   * Stop all animations on markers
   */
  _stopAnimationOnMarkers() {
    this.proccessedMarkers.map((marker) => {
      marker.setAnimation(null);
      return marker;
    });
  }

  /**
   * Update search list
   * @param {*} data
   */
  _updateSearchList(data) {
    let searchResultsBlock = $('#block_map .search-results');

    //update current total number of locations
    var _resultsNoContainer = $('.search-results-no');
    if(_resultsNoContainer.length) {
      $('.search-results-no').html(data.length);
      $('.search-results-no-loading').css("display", "none");
      $('.search-results-no').css("display", "inline");
    }

    if (searchResultsBlock.length && (this.showOnSearch
      || typeof this.queryData.near !== 'undefined' && this.queryData.near
      || typeof this.queryData.within !== 'undefined' && this.queryData.within)
    ) {
      this.showOnSearch = false;

      // Init location search results
      var searchResultsList = searchResultsBlock.find('.search-results-list ul');
      searchResultsList.html('');

      var resultsBlock = this.TemplateClass.getLocationsTemplates(data, searchResultsBlock.data('prototype'));
      searchResultsList.append(resultsBlock);

      if (!this._isMobile) {
        searchResultsList.find('.show-open-window').on('click', this._onShopListClick.bind(this));
      }
    }
  }

  /**
   * Get marker external id
   * @param {*} marker
   */
  _getMarkerExternalId(marker) {
    const {mapType} = this.settings;
    let markerExternalId = null;

    if (mapType === 'google') {
      markerExternalId = marker.externalId.toString();
    } else if (mapType === 'here') {
      markerExternalId = marker.getData().externalId.toString();
    }

    return markerExternalId;
  }

    /**
   * On shop list click
   */
  _onShopListClick(e) {
    const externalId = $(e.currentTarget).data('id').toString();
    const {mapType} = this.settings;

    if ($(e.currentTarget).is('a') || $(e.currentTarget).parent().is('a')) {
      return;
    }

    e.preventDefault();

    if (this.proccessedMarkers.length) {
      $.each(this.proccessedMarkers, (index, proccessedMarker) => {
        let markerExternalId = this._getMarkerExternalId(proccessedMarker);

        if (markerExternalId === externalId) {
          if (mapType === 'google') {
            $.each(this.infoWindows, (key, infoWindow) => {
              if (infoWindow.externalId.toString() === externalId) {
                this._stopAnimationOnMarkers();
                this._zoomCluster(proccessedMarker, () => {
                  this.settings.onShopSelected($(this), infoWindow, proccessedMarker, externalId);
                  this._showInfoWindow(infoWindow, proccessedMarker);
                });

                return false;
              }
            });
          } else if (mapType === 'here') {
            this.settings.onShopSelected($(this), this._infoWindow, proccessedMarker, externalId);
            this._createHereMapInfoWindow(proccessedMarker);
            this._map.setZoom(this.settings.maxClusterZoom+2);
            this._showInfoWindow(this._infoWindow, proccessedMarker);
          }

          return false;
        }
      });
    }
  }
}
