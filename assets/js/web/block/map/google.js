import GoogleMapsApi from 'load-google-maps-api';
import MarkerClusterer from './google/google-markerclusterer';

export default class GoogleMap {
    // Default Google Maps options
    static get DEFAULT_SETTINGS() {
        return {
            credentials: {key: null, key_premium: null},
            language: null,
            map: {
                center: {lat: null, lng: null},
                zoom: null,
                fitAll: false,
            },
            mapLibraries: ['geometry', 'places'],
            mapMouse: true, // Set mapMouse: false - if you want to not be able to drag and zoom the map with the mouse
            mapZoom: {position: 'left-bottom'}, // Set mapZoom: false - if you want to hide zoom in/out
            mapFullScreen: true,
            marker: {
                icon: '/build/images/web/block/map/marker/icon.png',
                icon_width: 33,
                icon_height: 53,
            },
            marker_cluster: {
                images: '/build/images/web/block/map/marker/m',
                textColor: '#ffffff',
            },
            geoLocationCountry: null, // country name or a two letter ISO 3166-1 country code (it can be an array of countries up to 5)
            query: {
                page: null,
                size: null,
                near: null,
                radius: null,
                within: null,
                itemList: null,
                brandSlugs: null,
                serviceKeys: null,
            },
        }
    };

    /**
     * @param {Object} settings
     * @param {TemplateHelper} TemplateClass
     */
    constructor(settings, TemplateClass) {
        // Classes
        this.TemplateClass = TemplateClass;

        // Objects
        this._googleMapApi = null;
        this._markersCluster = null;
        this._geocoder = null;
        this._map = null;

        // Var
        this.proccessedMarkers = [];
        this.infoWindows = [];
        this.showOnSearch = false;
        this.querySearch = false;
        this.querySearchGetData = false;
        this.queryData = {};

        this.settings = {...GoogleMap.DEFAULT_SETTINGS, ...settings};
    }

    createMap(mapElement) {
        var _this = this;

        // Load Google Map API
        this._loadMapApi().then(function (MapApi) {
            // Initialize the map
            _this._initMap(MapApi, mapElement);

            // Add map events
            _this._addMapEvents();
        });
    }

    _loadMapApi() {
        return GoogleMapsApi(this._mapCredentials);
    }

    _initMap(MapApi, mapElement) {
        this._googleMapApi = MapApi;

        this._map = new this._googleMapApi.Map(mapElement, this._mapOptions);

        this._geocoder = new this._googleMapApi.Geocoder();
    }

    _addMapEvents() {
        var _this = this;

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            this._googleMapApi.event.addListener(this._map, 'idle', function () {
                _this._addMapData();
            });
        } else {
            // Init map on load
            this._googleMapApi.event.addListenerOnce(this._map, 'tilesloaded', function () {
                if (typeof _this.settings.defaultLocation !== "undefined" && _this.settings.defaultLocation) {
                    $("#map_search_address_button").trigger('click');
                } else {
                    _this._addMapData();
                }
            });

            // On drag map
            this._googleMapApi.event.addListener(this._map, 'dragend', function () {
                _this._addMapData();
            });

            // On change zoom
            this._googleMapApi.event.addListener(this._map, 'zoom_changed', function () {
                _this._addMapData();
            });
        }

        // Map search events
        this._addSearchEvents();
    }

    _addMapData() {
        if (typeof this.settings.map.fitAll !== 'undefined' && this.settings.map.fitAll && this._markersCluster) {
            this.settings.map.fitAll = false;
        } else {
            if (this.querySearch && !this.querySearchGetData) {
                this.querySearch = false;
            } else {
                // Get locations data from API
                this._getAPILocationsData();
            }
        }
    }

    _getAPILocationsData() {
        var _this = this;

        if (typeof this.settings.map.fitAll !== 'undefined' && this.settings.map.fitAll) {
            this.queryData = {};
        } else {
            this.queryData.page = this.settings.query.page;

            if (this.querySearchGetData && typeof this.queryData.near !== 'undefined' && this.queryData.near) {
                this.queryData.within = null;

                this.querySearchGetData = false;
            } else {
                this.queryData.near = null;
                this.queryData.radius = null;
                if (this.showOnSearch) {
                    this.queryData.itemList = null;
                }

                // Set query bounds
                var bounds = this._map.getBounds();
                this.queryData.within = [bounds.getNorthEast().lat(), bounds.getSouthWest().lng(), bounds.getSouthWest().lat(), bounds.getNorthEast().lng()].join();
            }
        }

        // set default or active filters to query
        this._setFiltersQuery();

        if (this.getDataRequest && typeof this.getDataRequest.abort === 'function') {
            this.getDataRequest.abort();
        }

        // Get Locations from API
        this.getDataRequest = $.get(globalData.routes.apiLocations, this.queryData, function (data) {
            // Init markers on map
            _this._initMarkersCluster();
            this.getDataRequest = null;

            if (data && data.length) {
                // Add markers to map
                _this._addMarkersToMap(data);

                // Show shops on search list
                _this._updateSearchList(data);
            } else {
                // Init search list
                _this._updateSearchList([]);
            }
        });
    }

    _addMarkersToMap(data) {
        var _this = this;

        this.proccessedMarkers = data.map(function (item) {
            var markerOptions = {
                map: _this._map,
                position: new _this._googleMapApi.LatLng(item.address.latitude, item.address.longitude),
                title: item.name,
                externalId: item.externalId,
            };

            if (_this.settings.marker.icon) {


                var markerName = "default",
                    markerUrl = _this.settings.marker.icon;

                if (typeof globalData.brandSlugs !== 'undefined') {
                    if (item.brandSlug && globalData.brandSlugs.includes(item.brandSlug)) {
                        markerName = item.brandSlug;
                    }
                }

                markerOptions.icon = {
                    url: `${markerUrl.replace('default', markerName)}`,
                    scaledSize: new _this._googleMapApi.Size(_this.settings.marker.icon_width, _this.settings.marker.icon_height),
                };

            }

            var marker = new _this._googleMapApi.Marker(markerOptions);

            marker.set("id", item.externalId);

            _this._addMarkerInfoWindow(marker, item);

            // if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            //     // Add info window for marker
            //     // _this._addMarkerInfoWindow(marker, item);
            // }

            return marker;
        });

        // Add a marker clusterer to manage the markers.
        this._markersClustererActions();
    }

    _markersClustererActions() {
        // Set marker clusterer
        this._markersCluster = new MarkerClusterer(this._map, this.proccessedMarkers, this._clusterOptions);

        // Fit all marker bounds on map
        if (typeof this.settings.map.fitAll !== 'undefined' && this.settings.map.fitAll || this.querySearch) {
            this._markersCluster.fitMapToMarkers();
        }
    }

    _initMarkersCluster() {
        this.proccessedMarkers = [];

        if (this._markersCluster) {
            this._markersCluster.clearMarkers();
        }
    }

    _addMarkerInfoWindow(marker, item) {
        var _this = this;

        var infoWindowContent = this.TemplateClass.getLocationTemplate(item, $('#map_container').data('prototype'));
        var infoWindow = new this._googleMapApi.InfoWindow({
            content: infoWindowContent,
            externalId: item.externalId,
        });

        marker.addListener('click', function () {
            _this._showInfoWindow(infoWindow, marker);
        });

        this.infoWindows.push(infoWindow);
    }

    _showInfoWindow(infoWindow, marker) {
        this._closeInfoWindows();

        this._map.setCenter(marker.getPosition());

        infoWindow.open(this._map, marker);

        new this._googleMapApi.event.addListener(infoWindow, 'domready', function () {
            // Open hours expand in infoWindow
            $(document).off('click', '.info-window-block .open-close-box .open-sentence')
                .on('click', '.info-window-block .open-close-box .open-sentence', function () {
                    $(this).closest(".open-close-box").toggleClass('box-opened');
                });

            // Marker location route
            $(document).off('click', '.info-window-block .location-button')
                .on('click', '.info-window-block .location-button', function (e) {
                    e.preventDefault();

                    if (typeof globalData.useLocationRouteMap !== 'undefined' && globalData.useLocationRouteMap) {
                        window.open($(this).data('show-route'), '_blank');
                    } else {
                        window.open('https://www.google.com/maps/dir/?api=1&destination='
                            + $(this).data('latitude') + ',' + $(this).data('longitude'), '_blank');
                    }
                });
        });
    }

    _closeInfoWindows() {
        for (var index = 0; index < this.infoWindows.length; index++) {
            this.infoWindows[index].close();
        }
    }

    _updateSearchList(data) {
        var _this = this;

        //update current total number of locations
        var _resultsNoContainer = $('.search-results-no');
        if(_resultsNoContainer.length) {
            $('.search-results-no').html(data.length);
            $('.search-results-no-loading').css("display", "none");
            $('.search-results-no').css("display", "inline");
        }

        var searchResultsBlock = $('.map-search-box .search-results');
        if (searchResultsBlock.length) {
            this.showOnSearch = searchResultsBlock.data('search');
            var searchFilters = $('.map-search-box #map_search_filters');

            var searchResultBlockLoading = searchResultsBlock.find('.search-results-list-loading');
            if(typeof searchResultBlockLoading!=="undefined" && searchResultBlockLoading.length) {
                searchResultBlockLoading.css('display', 'none');
            }


            // Init location search results
            var searchResultsList = searchResultsBlock.find('.search-results-list ul');
            searchResultsList.html('');

            if (!this.showOnSearch || typeof this.queryData.near !== 'undefined' && this.queryData.near) {
                var resultsBlock = this.TemplateClass.getLocationsTemplates(data, searchResultsBlock.data('prototype'));
                searchResultsList.append(resultsBlock);

                if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    resultsBlock.on('click', '.show-open-window', function (e) {
                        e.preventDefault();

                        var externalId = $(this).data('id');
                        if (_this.proccessedMarkers.length) {
                            $.each(_this.proccessedMarkers, function (index, marker) {
                                var markerExternalId = parseInt(marker.externalId);
                                if (markerExternalId === externalId) {
                                    // Show marker info window
                                    $.each(_this.infoWindows, function (key, infoWindow) {
                                        if (parseInt(infoWindow.externalId) === externalId) {
                                            _this._showInfoWindow(infoWindow, marker);

                                            return false;
                                        }
                                    });

                                    return false;
                                }
                            })
                        }
                    });
                }

                if (this.showOnSearch) {
                    searchResultsBlock.show();

                    if (searchFilters.length) {
                        searchFilters.show();
                    }
                }
            } else {
                if (this.showOnSearch) {
                    searchResultsBlock.hide();

                    if (searchFilters.length) {
                        searchFilters.hide();

                        searchFilters.find('[id^=map_filters_]').prop('checked', false);
                    }
                }
            }
        }
    }

    _addSearchEvents() {
        // Go to current position event
        this._currentPosition();

        // Input search with autocomplete
        this._searchAutocomplete();

        // Go to autocomplete address on map
        this._submitSearchAddress();

        this._filtersActions();
    }

    _currentPosition() {
        var _this = this;

        var currentPositionButton = $('#map_current_position');
        if (currentPositionButton.length) {
            currentPositionButton.on('click', function (e) {
                e.preventDefault();

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        _this._map.setCenter({lat: position.coords.latitude, lng: position.coords.longitude});

                        _this._addMapData();
                    });
                }
            })
        }
    }

    _searchAutocomplete() {
        var _this = this;

        var searchAutocompleteInput = $('#map_search_autocomplete');
        if (searchAutocompleteInput.length) {
            var autocomplete = new google.maps.places.Autocomplete(searchAutocompleteInput.get(0));

            // Restrict autocomplete results to specific country/ies
            if (this.settings.geoLocationCountry) {
                autocomplete.setComponentRestrictions({'country': this.settings.geoLocationCountry});
            }

            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', this._map);

            // On select from autocomplete
            autocomplete.addListener('place_changed', function () {
                var place = autocomplete.getPlace();

                //add current search
                //@todo here maps should also support this
                if(place.formatted_address) {
                    var _currentSearchContainer = $(".search-results-location");
                    if(_currentSearchContainer.length) {
                        _currentSearchContainer.html(place.formatted_address)
                    }
                }

                //add loading for default search
                //@todo this should be also available for here maps
                var _resultsNoContainer = $(".search-results-no");
                if(_resultsNoContainer.length) {
                    _resultsNoContainer.css('display', 'none');
                    $(".search-results-no-loading").css('display', 'inline');
                }
                var searchResultsBlock = $('.map-search-box .search-results');
                if(searchResultsBlock.length) {
                    var searchResultBlockLoading = searchResultsBlock.find('.search-results-list-loading');
                    if(typeof searchResultBlockLoading!=="undefined" && searchResultBlockLoading.length) {
                        searchResultBlockLoading.css('display', 'block');
                    }
                }

                if (place.geometry) {
                    var position = place.geometry.location;

                    _this._centerMapBySearch(position);
                }


            });
        }
    }

    _submitSearchAddress() {
        var _this = this;

        var searchAddressButton = $('#map_search_address_button'),
            searchAutocompleteInput = $('#map_search_autocomplete');
        if (searchAddressButton.length && searchAutocompleteInput.length) {
            searchAddressButton.on('click', function (e) {
                e.preventDefault();

                var address = searchAutocompleteInput.val();
                if (address && address.length >= 2) {
                    _this._geocoder.geocode({'address': address}, function (results, status) {
                        if (status === _this._googleMapApi.GeocoderStatus.OK) {
                            var position = results[0].geometry.location;

                            _this._centerMapBySearch(position);
                        }
                    });
                }
            });
        }
    }

    _centerMapBySearch(position) {
        this.querySearch = true;
        this.querySearchGetData = true;
        this.queryData.near = [position.lat(), position.lng()].join();
        this.queryData.radius = this.settings.query.radius;

        // Fix marker clusterer on big zoom to fit on map
        if (this._map.getZoom() < this.settings.map.zoom) {
            this._map.setZoom(this.settings.map.zoom);
        }

        this._map.setCenter(position);

        this._addMapData();
    }

    _filtersActions() {
        var _this = this;

        // Map search filters
        var mapSearchFilters = $('#map_search_filters .search-filters');
        if (mapSearchFilters.length) {
            mapSearchFilters.on('click', '.open-filters', function () {
                $(this).parent('.search-filters').toggleClass('closed-box');
            });

            var mapFiltersInput = mapSearchFilters.find('[id^=map_filters_]');
            mapFiltersInput.on('click', function () {
                if (typeof _this.queryData.near !== 'undefined' && _this.queryData.near) {
                    _this.querySearchGetData = true;
                }

                if ($(this).data('parent')) {
                    if ($(this).prop('checked')) {
                        $(this).parent().find('ul [id^=map_filters_]').prop('checked', true);
                    } else {
                        $(this).parent().find('ul [id^=map_filters_]').prop('checked', false);
                    }
                }

                // Get locations data from API
                _this._getAPILocationsData();
            });
        }
    }

    _setFiltersQuery() {
        var _this = this;

        var mapSearchFilters = $('#map_search_filters .search-filters');
        if (mapSearchFilters.length) {
            this.queryData.itemList = null;
            this.queryData.serviceKeys = null;
            this.queryData.brandSlugs = null;

            var mapFiltersInput = mapSearchFilters.find('[id^=map_filters_]');

            var filters = {};
            mapFiltersInput.each(function (index, item) {
                if ($(item).prop('checked') && !$(item).data('parent')) {
                    var filterName = $(item).attr('name');

                    if (filterName) {
                        var filterKey = null;
                        if (filterName.indexOf('[') !== -1) {
                            var filterKeys = filterName.split('[');
                            filterName = filterKeys[0];

                            if (filterKeys[1] !== ']') {
                                filterKey = filterKeys[1].replace(']', '');
                            }
                        }

                        if (typeof filters[filterName] === 'undefined') {
                            filters[filterName] = [];
                        }

                        if (filterKey) {
                            filters[filterName][filterKey] = $(item).val();
                        } else {
                            filters[filterName].push($(item).val());
                        }
                    }
                }
            });

            if (Object.keys(filters).length) {
                $.each(filters, function (filterName, filterValues) {
                    _this.queryData[filterName] = filterValues;
                });
            }
        }
    }

    get _mapCredentials() {
        var options = {};

        // Google map libraries
        if (this.settings.mapLibraries) {
            options.libraries = this.settings.mapLibraries;
        }

        // Google map language
        if (this.settings.language) {
            options.language = this.settings.language;
        }

        // Google map credentials
        var credentials = this.settings.credentials;
        if (typeof credentials.key_premium !== 'undefined' && credentials.key_premium) {
            options.client = credentials.key_premium;
        } else {
            options.key = credentials.key;
        }

        return options;
    }

    get _mapOptions() {
        var mapOptions = {
            zoom: this.settings.map.zoom,
            center: this.settings.map.center,
            mapTypeId: this._googleMapApi.MapTypeId.ROADMAP,
            disableDefaultUI: true,
        };

        if (this.settings.mapMouse === false) {
            mapOptions.gestureHandling = 'none';
        }

        if (this.settings.mapZoom) {
            mapOptions.zoomControl = true;

            var mapPosition = this._googleMapApi.ControlPosition.LEFT_BOTTOM;
            if (typeof this.settings.mapZoom.position !== 'undefined' && this.settings.mapZoom.position) {
                mapPosition = this._googleMapApi.ControlPosition[this.settings.mapZoom.position.replace('-', '_').toUpperCase()];
            }
            mapOptions.zoomControlOptions = {position: mapPosition};
        }

        if (this.settings.mapFullScreen) {
            mapOptions.fullscreenControl = this.settings.mapFullScreen;
        }

        return mapOptions;
    }

    get _clusterOptions() {
        var clusterStyles = [],
            clusterSizes = [53, 56, 66, 78, 90];
        for (var index = 0; index < 5; index++) {
            var clusterStyle = {
                url: this.settings.marker_cluster.images + (index + 1) + '.png',
                width: clusterSizes[index],
                height: clusterSizes[index] - 1,
            };

            if (typeof this.settings.marker_cluster.textColor !== 'undefined' && this.settings.marker_cluster.textColor) {
                clusterStyle.textColor = this.settings.marker_cluster.textColor;
            }

            clusterStyles.push(clusterStyle);
        }

        return {
            imagePath: this.settings.marker_cluster.images,
            gridSize: 50,
            maxZoom: 15,
            averageCenter: true,
            styles: clusterStyles
        };
    }
}
