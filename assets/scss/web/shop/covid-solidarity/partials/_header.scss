.app-header {
  padding-top: 9px;
  padding-bottom: 9px;

  .container {
    width: 100%;
    margin: 0 auto;
    padding-right: 15px;
    padding-left: 15px;

    @media (min-width: 576px) {
      max-width: 540px;
    }

    @media (min-width: 768px) {
      max-width: 720px;
    }

    @media (min-width: 992px) {
      max-width: 960px;
    }

    @media (min-width: 1200px) {
      max-width: 1140px;
    }
  }

  .navbar {
    padding: 8px 16px;
    display: flex;
    align-items: flex-start;
    justify-content: space-between;

    @media (max-width: 767px) {
      flex-direction: column;
      padding: 0;
    }

    a {
      display: block;
    }

    .navbar-brand {
      margin-top: 7px;
    }
  }

  .navbar-nav {
    display: flex;
    align-items: center;

    @media (max-width: 767px) {
      min-height: 68px;
      width: 100%;
    }

    .nav-item {

      &.nav-item-share {
        margin-left: 20px;
        margin-right: 20px;

        @media (max-width: 767px) {
          margin-left: 10px;
          margin-right: 10px;
        }
      }

      span,
      a {
        display: block;
      }

      &.nav-item-phone {
        a {
          width: 36px;
          height: 36px;
          border-radius: 50%;
          background-color: $green_28;
          color: $white;
          display: flex;
          align-items: center;
          justify-content: center;

          svg {
            width: 20px;
            height: 20px;
            color: $white;

            path {
              fill: $white;
            }
          }
        }
      }

      &.active {
        .nav-link {
          color: $black;

          &:hover {
            text-decoration: underline;
          }
        }
      }

      .nav-link {
        padding: 8px 9px;
        font-size: 16px;
        line-height: 24px;
        color: $green_44;
        transition: color ease 0.2s;

        &:hover {
          text-decoration: none;
        }
      }
    }
  }
}