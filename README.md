Virtuology
==========

A website portal.


Docker
------

Go to project directory and execute:

  * `docker-compose build`
  * `docker-compose up -d`
  * get container IP `docker network inspect bridge | grep Gateway` and add to **/etc/hosts** , ex: `171.17.0.1 vir.loc`
  * `docker-compose exec db mysql -uvir -p"vir"` docker mysql
  * `docker-compose exec php sh` project docker container


Installation
------------

  * `composer install`
  * `composer dump-env dev` and change *DATABASE_URL* credentials in new created file [.env.local.php](.env.local.php)
    with `mysql://vir:vir@db/vir`
  * `php bin/console doctrine:database:create`
  * `php bin/console doctrine:migrations:migrate`
  * `php bin/console doctrine:fixtures:load`
  * `php bin/console assets:install`


Working with [WebPack][1]
-------------------------

Make sure that you have [yarn][2] installed.
Then install packages:

    yarn install

build the assets:

    yarn encore production


Admin Translations [Lexik][3]
-----------------------------

To import translations files to database use _-p [path_to_project]/translations_, ex:

    php bin/console lexik:translations:import -p /var/www/vir/translations

Export translations from database to files:

    php bin/console lexik:translations:export

----------------------------------------------------------------------------

Go to http://vir.loc:88/admin/login (if you are using docker),
you can login using email **_admin@vir.loc_** and password **_admin_**


Add locations for homepage letter display
-----------------------------------------
Use this command for full locations

    php bin/console app:add-shop-location --projection=LocationFull


[1]: https://symfony.com/doc/current/frontend.html
[2]: https://yarnpkg.com/lang/en/docs/install/
[3]: https://github.com/lexik/LexikTranslationBundle/blob/master/Resources/doc/index.md