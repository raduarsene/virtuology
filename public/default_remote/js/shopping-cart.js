"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var ShellAroundApp =
/*#__PURE__*/
function () {
  function ShellAroundApp(callback) {
    _classCallCheck(this, ShellAroundApp);

    this.eventBus = null;
    this.initialized = false;
    this.state = "";
  } // Initiate the module and its


  _createClass(ShellAroundApp, [{
    key: "init",
    value: function init() {
      var _this = this;

      document.addEventListener('crf_module_init', function (result) {
        var register = result.detail;
        var moduleData = {
          name: 'AddToBasketWebmodule',
          // Set the name of the module to be registered
          version: '0.0.1',
          // Set the registration version of the module, all versions of all modules must match
          eventTypes: ['core/state/running', 'core/api/onToken', 'core/api/request', 'app/ready', 'authentication/isAuthenticated', 'authentication/isAuthenticatedDone', 'store/singleProductByBiggestStore', 'store/singleProductByBiggestStoreDone', 'store/getDriveStore', 'store/getDefaultTimeslot', 'store/getDefaultTimeslotDone', 'analytics/trackView', 'store/searchIngredient'],
          isInitialModule: false,
          // Only 1 module can be the Initial Module
          isAuthenticationModule: false // Only 1 module can be the authenticationModule

        }; // Call registration function with the moduleData variable to register it

        register(moduleData, function (eventbus) {
          _this.eventBus = eventbus;

          _this.eventBus.registerHandler("app/ready", function (eventType) {
            _this.registerEvents();

            if (_this.getCookie('_locale') != '') {
              _this.eventBus.throwEvent("app/changeLanguage", _this.getCookie('_locale'));
            }

            _this.eventBus.throwEvent('authentication/isAuthenticated');

            _this.callback(_this.e, _this.mockEvent);
          });
        });
      });
    } // Method to register all initial events

  }, {
    key: "registerEvents",
    value: function registerEvents() {
      var _this2 = this;

      var that = this;
      this.eventBus.registerHandler("core/closeApp", function (eventType, basket) {
        if (document.body.classList.contains('app-open')) {
          document.body.classList.remove('app-open');
          document.getElementById('header').classList.remove('app-open');
        }
      });
      this.eventBus.registerHandler("basket/getDone", function (eventType, basket) {
        document.body.classList.add("basket-known");

        if (basket) {
          var productsCount = document.querySelectorAll(".items-in-basket");
          productsCount.forEach(function (item, index) {
            item.innerHTML = basket.productCount;
          });
          document.cookie = "basketItems=" + basket.productCount + ";path=/";
        }

        if (!_this2.initialized) {
          _this2.initialized = true;

          if (localStorage.getItem('carrefour.lastEvent')) {
            var lastEvent = JSON.parse(localStorage.getItem('carrefour.lastEvent'));

            _this2.detectEvent(lastEvent.action, lastEvent.params);
          }
        }
      });
      this.eventBus.registerHandler("store/getDriveStore", function (eventType, store) {
        if (store) {
          Helper.showToaster("Drive store added.");
        }
      });
      this.eventBus.registerHandler('store/setDriveStore', function (eventType, store) {
        if (store != null) {
          var str = "shopId:" + store.ref;
          document.cookie = "Carrefour=\"" + str + "\";domain=.carrefour.eu;path=/"; // if($('.nameOfStore').length > 0){
          //     $('.nameOfStore span').html('('+store.name+')');
          // 	if(localStorage.getItem("sku") != null){
          // 		console.log("ready");
          // 		var data= "data-action-params=" + localStorage.getItem("sku");  
          // 		console.log(data);
          // 		$("["+data+"]")[0].click();
          // 		localStorage.removeItem("sku");
          // 	}
          // }

          if ($('#m-UpdateDeliveryPickup').length > 0 && $('#d-UpdateDeliveryPickup').length > 0) {
            $('.storepicker').addClass('store-added');
            updateStoreDetails(store.name);

            if (localStorage.getItem("sku") != null) {
              console.log("ready");
              var data = "data-action-params=" + localStorage.getItem("sku");
              console.log(data);
              $("[" + data + "]")[0].click();
              localStorage.removeItem("sku");
            }
          } //that.searchStore();

        }
      });
      this.eventBus.registerHandler("app/getLanguageDone", function (eventType, language) {//console.log("language", language);
      });
      this.eventBus.registerHandler("authentication/isAuthenticatedDone", function (eventType, isAuthenticated) {
        if (!isAuthenticated) {
          Helper.showToaster("Not logged in. Please login first.");
        }

        if (isAuthenticated) {
          that.initialized = true;
        }

        document.body.classList.add("basket-known");
      });
      this.eventBus.registerHandler("core/stateTransition", function (eventType, data) {
        _this2.state = data;
      }); //console.log("Shopping cart auth: " + this.eventBus.throwEvent("authentication/isAuthenticated"));
      //console.log("Shopping cart language: " + this.eventBus.throwEvent("app/getLanguage"));
    }
  }, {
    key: "detectEvent",
    value: function detectEvent(action, skuval) {
      // =========================== //
      // User clicked on show basket //
      // =========================== //
      if (action && action == "show-basket") {
        if (!document.body.classList.contains('app-open')) {
          document.body.classList.add('app-open');
          document.getElementById("header").classList.add('app-open');

          if (!Helper.checkDriveStore()) {
            if (this.state !== "FavStorePage") {
              this.searchStore();
            }
          } else {
            this.showBasket();
          }
        } else {
          document.body.classList.remove('app-open');
          document.getElementById("header").classList.remove('app-open');
        }
      } // ============================ //
      // User clicked on show product //
      // ============================ //


      if (action && action == "show-productdetail") {
        if (!document.body.classList.contains('app-open')) {
          document.body.classList.add('app-open');
          document.getElementById("header").classList.add('app-open');
        }

        if (!Helper.checkDriveStore()) {
          if (this.state !== "FavStorePage") {
            this.searchStore();
          }
        } else {
          this.showProductDetail(arguments.length <= 1 ? undefined : arguments[1]);
        }
      } // ===================================== //
      // User clicked on add product to basket //
      // ===================================== //


      if (action && action == "add-product-to-basket") {
        if (!Helper.checkDriveStore()) {
          localStorage.setItem('sku', skuval);

          if (!document.body.classList.contains('app-open')) {
            document.body.classList.add('app-open');
            document.getElementById("header").classList.add('app-open');
          }

          if (this.state !== "FavStorePage") {
            this.searchStore();
          }
        } else {
          if (document.body.classList.contains('app-open')) {
            document.body.classList.remove('app-open');
            document.getElementById("header").classList.remove('app-open');
          }

          this.addProductToBasket(arguments.length <= 1 ? undefined : arguments[1]);
        }
      } // ================================= //
      // User clicked on search ingredient //
      // ================================= //


      if (action && action == "search-ingredient") {
        if (!document.body.classList.contains('app-open')) {
          document.body.classList.add('app-open');
          document.getElementById("header").classList.add('app-open');
        }

        if (!Helper.checkDriveStore()) {
          if (this.state !== "FavStorePage") {
            this.searchStore();
          }
        } else {
          this.searchIngredient(arguments.length <= 1 ? undefined : arguments[1]);
        }
      } // ============================ //
      // User clicked on select store //
      // ============================ //


      if (action && action == "show-store") {
        if (!document.body.classList.contains('app-open')) {
          document.body.classList.add('app-open');
          document.getElementById("header").classList.add('app-open');
        }

        if (this.state !== "FavStorePage") {
          this.searchStore();
        }
      } // ============================ //
      // User clicked on select store list //
      // ============================ //


      if (action && action == "show-store-list") {
        if (!document.body.classList.contains('app-open')) {
          document.body.classList.add('app-open');
          document.getElementById("header").classList.add('app-open');
        }

        this.searchStore();
      }
    }
  }, {
    key: "showBasket",
    value: function showBasket() {
      //console.log("SHOW BASKET");
      // Throw navigation event to basketPage
      this.eventBus.throwEvent('app/navigate', {
        key: 'drive/basketPage',
        options: {
          root: true,
          animate: false
        }
      });
    }
  }, {
    key: "searchStore",
    value: function searchStore() {
      //console.log("SEARCH STORE");
      // Throw navigation event to favstorePage
      this.eventBus.throwEvent('app/navigate', {
        key: 'store/chooseDrive',
        params: {
          shouldSetDriveStore: true,
          closeComponentsAfterSelecting: true
        }
      });
    }
  }, {
    key: "showProductDetail",
    value: function showProductDetail(productId) {
      var _this3 = this;

      //console.log("SHOW PRODUCT DETAIL:", productId);
      var singleProductCallback = function singleProductCallback(eventType, product) {
        if (product) {
          _this3.eventBus.throwEvent('app/navigate', {
            key: 'drive/productInfoPage',
            params: {
              product: product,
              disableBackButton: true
            },
            options: {
              animate: false,
              root: true
            }
          });
        }

        _this3.eventBus.unregisterHandlerByFunction(singleProductCallback);
      };

      var timeslotCallback = function timeslotCallback(eventType, time) {
        _this3.eventBus.throwEvent("store/singleProductByBiggestStore", {
          product: productId
        });

        _this3.eventBus.unregisterHandlerByFunction(timeslotCallback);
      };

      this.eventBus.registerHandler("store/singleProductByBiggestStoreDone", singleProductCallback);
      this.eventBus.registerHandler("store/getDefaultTimeslotDone", timeslotCallback); // Throw event to get the default timeslot, tis is mandatory to get a product

      this.eventBus.throwEvent("store/getDefaultTimeslot");
    }
  }, {
    key: "addProductToBasket",
    value: function addProductToBasket(productId) {
      var _this4 = this;

      //console.log("ADD PRODUCT TO BASKET:", productId);
      var cbUpdate = function cbUpdate(eventType, basket) {
        var myProduct = null;
        basket.products.forEach(function (product) {
          if (product.ref == productId) {
            myProduct = product;
            product.addAmount(product.stepToAddOrRemove); //Helper.showToaster("Je voegde " + product.shortDesc + "(" + product.price.stdPrice + "€) toe.")

            _this4.eventBus.throwEvent("basket/update", product);

            _this4.eventBus.registerHandler('basket/getDone', cbUpdateSingle);
          }
        });

        if (myProduct) {
          if (_this4.state !== "BasketPage") {
            _this4.eventBus.throwEvent('app/navigate', {
              key: 'drive/basketPage',
              options: {
                root: true,
                animate: false
              }
            });
          }
        } else {
          _this4.eventBus.registerHandler("store/singleProductByBiggestStoreDone", singleProductCallback);

          _this4.eventBus.registerHandler("store/getDefaultTimeslotDone", timeslotCallback); // Throw event to get the default timeslot, tis is mandatory to get a product


          _this4.eventBus.throwEvent("store/getDefaultTimeslot");
        }

        _this4.eventBus.unregisterHandlerByFunction(cbUpdate);
      };

      var cbUpdateSingle = function cbUpdateSingle(eventType, basket) {
        basket.products.forEach(function (product) {
          if (product.ref == productId) {
            //Helper.showToaster('Added ' + product.shortDesc + '(' + product.price.stdPrice + '€) to basket.')
            Helper.showProductToaster(product);
          }
        });

        if (_this4.state !== 'BasketPage') {
          _this4.eventBus.throwEvent('app/navigate', {
            key: 'drive/basketPage'
          });
        }

        _this4.eventBus.unregisterHandlerByFunction(cbUpdateSingle);
      };

      var singleProductCallback = function singleProductCallback(eventType, product) {
        if (product) {
          product.addAmount(product.stepToAddOrRemove); //Helper.showToaster("Added " + product.shortDesc + " (" + product.price.stdPrice + "€) to basket.")

          Helper.showProductToaster(product);

          _this4.eventBus.registerHandler("basket/getDone", cbUpdateSingle);
          /*if(product.stockAvailability != null && product.stockAvailability > 0){
              this.eventBus.throwEvent("basket/update", product);
          }*/


          _this4.eventBus.throwEvent("basket/update", product);
        }

        _this4.eventBus.unregisterHandlerByFunction(singleProductCallback);
      };

      var timeslotCallback = function timeslotCallback(eventType, time) {
        _this4.eventBus.throwEvent("store/singleProductByBiggestStore", {
          product: productId
        });

        _this4.eventBus.unregisterHandlerByFunction(timeslotCallback);
      };

      this.eventBus.registerHandler("basket/getDone", cbUpdate);
      this.eventBus.throwEvent("basket/get");
    }
  }, {
    key: "searchIngredient",
    value: function searchIngredient(ingredient) {
      //console.log("SEARCH INGREDIENT", ingredient);
      //console.log(this.state);
      // Check if you are on the categoriespage first...go to it when you are not on it to see the results.
      if (this.state !== "CategoriesPage") {
        this.eventBus.throwEvent('app/navigate', {
          key: 'drive'
        });
      } // throw custom event


      this.eventBus.throwEvent("store/searchIngredient", ingredient);
    }
  }, {
    key: "showStoreList",
    value: function showStoreList() {
      //console.log("SHOW STORE LIST");
      this.eventBus.throwEvent('app/navigate', {
        key: 'store/chooseDrive',
        params: {
          shouldSetDriveStore: true,
          closeComponentsAfterSelecting: true
        }
      });
    }
  }, {
    key: "getCookie",
    value: function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');

      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }

        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }

      return "";
    }
  }]);

  return ShellAroundApp;
}();

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');

  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];

    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }

    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }

  return "";
} // ============ //
// HELPER CLASS //
// ============ //


var Helper =
/*#__PURE__*/
function () {
  function Helper() {
    _classCallCheck(this, Helper);
  }

  _createClass(Helper, null, [{
    key: "showToaster",
    value: function showToaster(text) {
      var toaster = document.querySelectorAll('.toaster');
      var cartIcon = document.querySelectorAll('.nav-cart');
      var toasterMessage = document.querySelectorAll('.toaster__message');
      toaster.forEach(function (item, index) {
        item.classList.add("toaster--shown");
      });
      cartIcon.forEach(function (item, index) {
        item.classList.add("toaster--shown");
      });
      toasterMessage.forEach(function (item, index) {
        item.innerHTML = text;
      });
      var timer = setTimeout(function () {
        toaster.forEach(function (item, index) {
          item.classList.remove("toaster--shown");
        });
        cartIcon.forEach(function (item, index) {
          item.classList.remove("toaster--shown");
        });
        clearTimeout(timer);
      }, 5000);
    }
  }, {
    key: "showProductToaster",
    value: function showProductToaster(product) {
      var toaster = document.querySelectorAll('.toaster');
      var cartIcon = document.querySelectorAll('.nav-cart');
      var toasterMessage = document.querySelectorAll('.toaster__message');
      toaster.forEach(function (item, index) {
        item.classList.add("toaster--shown");
        var toasterStatus = $(item).find('.status');
        var toasterError = $(item).find('.error');
        var notFoundDiv = $(item).find('.not-found');
        var status = "+" + product.increment + " " + product.unitOfMeasure;
        toasterStatus.html(status);

        if (product.mmContents != null) {
          var images = product.mmContents;
          $.each(images, function (i, e) {
            if (e.contentType == 'DETAILPICTURE') {
              $(item).find('img').attr('src', "https://drive.carrefour.eu/" + e.uri);
            }
          });
        }

        toasterStatus.css('display', 'block');
        toasterError.css('display', 'none');
        notFoundDiv.css('display', 'none');
        item.classList.add("toaster--shown");
      });
      toasterMessage.forEach(function (item, index) {
        item.innerHTML = product.shortDesc;
      });
      /*if(product.stockAvailability != null && product.stockAvailability > 0){
          toasterStatus.css('display','block');
          toasterError.css('display','none');
          notFoundDiv.css('display','none');
      }else{
          toasterStatus.css('display','none');
          toasterError.css('display','block');
          notFoundDiv.css('display','block');
      }*/

      cartIcon.forEach(function (item, index) {
        item.classList.add("toaster--shown");
      });
      var timer = setTimeout(function () {
        toaster.forEach(function (item, index) {
          item.classList.remove("toaster--shown");
        });
        cartIcon.forEach(function (item, index) {
          item.classList.remove("toaster--shown");
        });
        clearTimeout(timer);
      }, 5000);
    }
  }, {
    key: "checkDriveStore",
    value: function checkDriveStore() {
      if (localStorage.getItem("store.drive") === null) {
        return false;
      }

      return true;
    }
  }, {
    key: "loadJS",
    value: function loadJS(url, implementationCode, location) {
      var scriptTag = document.createElement('script');
      scriptTag.src = url;
      scriptTag.onload = implementationCode;
      scriptTag.onreadystatechange = implementationCode;
      location.appendChild(scriptTag);
    }
  }]);

  return Helper;
}(); // All click events on .link and .button


function clickButton(e, mockEvent) {
  if (shell.callback == null) {
    e.preventDefault();
  }

  localStorage.removeItem('carrefour.lastEvent'); // Make an instance of shellaroundapp

  if (!shell.initialized && shell.callback == null) {
    shell.init();
    shell.callback = clickButton;
    shell.e = e;
    shell.mockEvent = jQuery.extend(true, {}, e);
    Helper.loadJS('/etc/clientlibs/carrefour/shoppingcart/main.js', function () {}, document.body);
    var eventData = {
      action: e.currentTarget.getAttribute('data-action'),
      params: e.currentTarget.getAttribute('data-action-params'),
      fallback: e.currentTarget.getAttribute('data-action-fallback')
    };
    localStorage.setItem('carrefour.lastEvent', JSON.stringify(eventData));
  } else {
    if (shell.initialized) {
      e.preventDefault();

      if (mockEvent != null) {
        shell.detectEvent(mockEvent.currentTarget.getAttribute('data-action'), mockEvent.currentTarget.getAttribute('data-action-params'));
      } else {
        shell.detectEvent(e.currentTarget.getAttribute('data-action'), e.currentTarget.getAttribute('data-action-params'));
      }
    } else {
      var eventData = {};

      if (mockEvent != null) {
        eventData = {
          action: mockEvent.currentTarget.getAttribute('data-action'),
          params: mockEvent.currentTarget.getAttribute('data-action-params'),
          fallback: mockEvent.currentTarget.getAttribute('data-action-fallback')
        };
        localStorage.setItem('carrefour.lastEvent', JSON.stringify(eventData));

        if (!mockEvent.currentTarget.hasAttribute('href')) {
          localStorage.setItem('sku', mockEvent.currentTarget.getAttribute('data-action-params'));
          window.location.href = mockEvent.currentTarget.getAttribute('data-action-fallback');
        }
      } else {
        eventData = {
          action: e.currentTarget.getAttribute('data-action'),
          params: e.currentTarget.getAttribute('data-action-params'),
          fallback: e.currentTarget.getAttribute('data-action-fallback')
        };
        localStorage.setItem('carrefour.lastEvent', JSON.stringify(eventData));

        if (!e.currentTarget.hasAttribute('href')) {
          localStorage.setItem('sku', e.currentTarget.getAttribute('data-action-params'));
          window.location.href = e.currentTarget.getAttribute('data-action-fallback');
        }
      }
    }
  }
} // Attach eventlisteners


var buttons = document.querySelectorAll('[data-action]');
buttons.forEach(function (button) {
  // button.addEventListener("click", clickButton, false);
  button.addEventListener("click", clickButton, true);
});
var shell = new ShellAroundApp(); // add classes to body to make the layout look ok

document.body.classList.add("not-logged-in");
document.body.classList.add("no-store");

if ($('#m-UpdateDeliveryPickup').length > 0 && localStorage.getItem("store.drive") != null) {
  var store = JSON.parse(localStorage.getItem('store.drive'));
  updateStoreDetails(store.name);
}

if ($('#d-UpdateDeliveryPickup').length > 0 && localStorage.getItem("store.drive") != null) {
  var store = JSON.parse(localStorage.getItem('store.drive'));
  updateStoreDetails(store.name);
}

if (getCookie('basketItems') != '') {
  var productsCount = document.querySelectorAll(".items-in-basket");
  productsCount.forEach(function (item, index) {
    item.innerHTML = getCookie('basketItems');
  });
}

function updateStoreDetails(storename) {
  $('.storepicker').addClass('store-added');
  $('.choose-delivery').removeClass('d-flex').addClass('d-none');
  $('.update-delivery-pickup').removeClass('d-none').addClass('d-flex');
  $('#m-UpdateDeliveryPickup span.store-info').html(storename);
  $('#d-UpdateDeliveryPickup span.store-info').html(storename);
  $('.store-nav-title #storename').html(storename);
  var storedata = $('#d-UpdateDeliveryPickup').html();
  $('.storename').html(storedata);
}