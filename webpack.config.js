var Encore = require('@symfony/webpack-encore');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables @babel/preset-env polyfills
    .configureBabel(() => {}, {
        useBuiltIns: 'usage',
        corejs: 3
    })

    /** Javascripts **/
    // Admin
    .addEntry('js/admin/app', './assets/js/admin/app.js')
    // Web
    .addEntry('js/web/home', './assets/js/web/home.js')
    .addEntry('js/web/show', './assets/js/web/show.js')
    .addEntry('js/web/global/gtm-events', './assets/js/web/global/gtm-events.js')
    .addEntry('js/web/global/ga-events', './assets/js/web/global/ga-events.js')
    .addEntry('js/web/global/cookies', './assets/js/web/global/cookies.js')
    // Blocks
    .addEntry('js/web/block/map', './assets/js/web/block/map.js')
    .addEntry('js/web/block/locations', './assets/js/web/block/locations.js')
    .addEntry('js/web/block/location_details', './assets/js/web/block/location_details.js')
    .addEntry('js/web/block/location_banner', './assets/js/web/block/location_banner.js')
    .addEntry('js/web/block/location_description', './assets/js/web/block/location_description.js')
    .addEntry('js/web/block/location_promotions', './assets/js/web/block/location_promotions.js')
    .addEntry('js/web/block/location_events', './assets/js/web/block/location_events.js')
    .addEntry('js/web/block/location_services', './assets/js/web/block/location_services.js')
    .addEntry('js/web/block/near_location', './assets/js/web/block/near_location.js')
    .addEntry('js/web/block/location_route', './assets/js/web/block/location_route.js')

    .addEntry('js/web/block/all_blocks', './assets/js/web/block/all_blocks.js')

    // Shop Blocks
    .addEntry('js/web/shop/voo-be/home', './assets/js/web/shop/voo-be/home.js')
    .addEntry('js/web/shop/voo-be/show', './assets/js/web/shop/voo-be/show.js')
    .addEntry('js/web/shop/voo-be/smartphones', './assets/js/web/shop/voo-be/smartphones.js')

    .addEntry('js/web/shop/tomandco/home', './assets/js/web/shop/tomandco/home.js')

    .addEntry('js/web/shop/proximus-be/base', './assets/js/web/shop/proximus-be/base.js')
    .addEntry('js/web/shop/mobilosoft-test/base', './assets/js/web/shop/mobilosoft-test/base.js')
    .addEntry('js/web/shop/carrefour-be/home', './assets/js/web/shop/carrefour-be/home.js')
    .addEntry('js/web/shop/carrefour-be/show', './assets/js/web/shop/carrefour-be/show.js')
    .addEntry('js/web/shop/medimarket-be/home', './assets/js/web/shop/medimarket-be/home.js')
    .addEntry('js/web/shop/medimarket-be/show', './assets/js/web/shop/medimarket-be/show.js')
    .addEntry('js/web/shop/leondebruxelles-fr/app', './assets/js/web/shop/leondebruxelles-fr/app.js')
    .addEntry('js/web/shop/leondebruxelles-fr/home', './assets/js/web/shop/leondebruxelles-fr/home.js')
    .addEntry('js/web/shop/lloydspharma-be/app', './assets/js/web/shop/lloydspharma-be/app.js')
    .addEntry('js/web/shop/lloydspharma-be/home', './assets/js/web/shop/lloydspharma-be/home.js')
    .addEntry('js/web/shop/lolaliza-be/app', './assets/js/web/shop/lolaliza-be/app.js')
    .addEntry('js/web/shop/refinerylab-us/app', './assets/js/web/shop/refinerylab-us/app.js')
    .addEntry('js/web/shop/refinerylab-us/home', './assets/js/web/shop/refinerylab-us/home.js')
    .addEntry('js/web/shop/refinerylab-us/show', './assets/js/web/shop/refinerylab-us/show.js')
    .addEntry('js/web/shop/louisdelhaize-be/app', './assets/js/web/shop/louisdelhaize-be/app.js')
    .addEntry('js/web/shop/les3brasseurs-fr/app', './assets/js/web/shop/les3brasseurs-fr/app.js')
    .addEntry('js/web/shop/les3brasseurs-fr/home', './assets/js/web/shop/les3brasseurs-fr/home.js')
    .addEntry('js/web/shop/pv-be/app', './assets/js/web/shop/pv-be/app.js')
    .addEntry('js/web/shop/workforcesolutions-us/app', './assets/js/web/shop/workforcesolutions-us/app.js')
    .addEntry('js/web/shop/workforcesolutions-us/home', './assets/js/web/shop/workforcesolutions-us/home.js')
    .addEntry('js/web/shop/workforcesolutions-us/show', './assets/js/web/shop/workforcesolutions-us/show.js')
    .addEntry('js/web/shop/laposte-fr/app', './assets/js/web/shop/laposte-fr/app.js')
    .addEntry('js/web/shop/manpower-be/app', './assets/js/web/shop/manpower-be/app.js')
    .addEntry('js/web/shop/manpower-be/home', './assets/js/web/shop/manpower-be/home.js')
    .addEntry('js/web/shop/farm-be/app', './assets/js/web/shop/farm-be/app.js')
    .addEntry('js/web/shop/pickles-au/app', './assets/js/web/shop/pickles-au/app.js')
    .addEntry('js/web/shop/multipharma/app', './assets/js/web/shop/multipharma/app.js')
    .addEntry('js/web/shop/brocom-be/app', './assets/js/web/shop/brocom-be/app.js')

    .addEntry('js/web/shop/wasteconnections-us/home', './assets/js/web/shop/wasteconnections-us/home.js')
    .addEntry('js/web/shop/wasteconnections-us/show', './assets/js/web/shop/wasteconnections-us/show.js')

    /** Css/Scss styles **/
    .enableSassLoader()
    // Admin
    .addStyleEntry('css/admin/app', './assets/scss/admin/app.scss')
    // Web
    .addStyleEntry('css/web/home', './assets/scss/web/home.scss')
    .addStyleEntry('css/web/show', './assets/scss/web/show.scss')

    // Components
    .addStyleEntry('css/web/components/cookie-consent', './assets/scss/web/components/cookie-consent.scss')

    // Blocks
    .addStyleEntry('css/web/block/map', './assets/scss/web/block/map.scss')
    .addStyleEntry('css/web/block/locations', './assets/scss/web/block/locations.scss')
    .addStyleEntry('css/web/block/location_details', './assets/scss/web/block/location_details.scss')
    .addStyleEntry('css/web/block/location_banner', './assets/scss/web/block/location_banner.scss')
    .addStyleEntry('css/web/block/breadcrumb', './assets/scss/web/block/breadcrumb.scss')
    .addStyleEntry('css/web/block/location_description', './assets/scss/web/block/location_description.scss')
    .addStyleEntry('css/web/block/location_promotions', './assets/scss/web/block/location_promotions.scss')
    .addStyleEntry('css/web/block/location_events', './assets/scss/web/block/location_events.scss')
    .addStyleEntry('css/web/block/location_services', './assets/scss/web/block/location_services.scss')
    .addStyleEntry('css/web/block/near_location', './assets/scss/web/block/near_location.scss')
    .addStyleEntry('css/web/block/location_route', './assets/scss/web/block/location_route.scss')

    // Shop Blocks
    .addStyleEntry('css/web/shop/voo-be/home', './assets/scss/web/shop/voo-be/home.scss')
    .addStyleEntry('css/web/shop/voo-be/show', './assets/scss/web/shop/voo-be/show.scss')
    .addStyleEntry('css/web/shop/voo-be/smartphones', './assets/scss/web/shop/voo-be/smartphones.scss')
    .addStyleEntry('css/web/shop/tomandco/home', './assets/scss/web/shop/tomandco/home.scss')
    .addStyleEntry('css/web/shop/proximus-be/base', './assets/scss/web/shop/proximus-be/base.scss')
    .addStyleEntry('css/web/shop/mobilosoft-test/base', './assets/scss/web/shop/mobilosoft-test/base.scss')
    .addStyleEntry('css/web/shop/carrefour-be/home', './assets/scss/web/shop/carrefour-be/home.scss')
    .addStyleEntry('css/web/shop/carrefour-be/show', './assets/scss/web/shop/carrefour-be/show.scss')
    .addStyleEntry('css/web/shop/medimarket-be/home', './assets/scss/web/shop/medimarket-be/home.scss')
    .addStyleEntry('css/web/shop/medimarket-be/show', './assets/scss/web/shop/medimarket-be/show.scss')
    .addStyleEntry('css/web/shop/leondebruxelles-fr/app', './assets/scss/web/shop/leondebruxelles-fr/app.scss')
    .addStyleEntry('css/web/shop/lloydspharma-be/app', './assets/scss/web/shop/lloydspharma-be/app.scss')
    .addStyleEntry('css/web/shop/lolaliza-be/home', './assets/scss/web/shop/lolaliza-be/home.scss')
    .addStyleEntry('css/web/shop/lolaliza-be/show', './assets/scss/web/shop/lolaliza-be/show.scss')
    .addStyleEntry('css/web/shop/louisdelhaize-be/home', './assets/scss/web/shop/louisdelhaize-be/home.scss')
    .addStyleEntry('css/web/shop/louisdelhaize-be/show', './assets/scss/web/shop/louisdelhaize-be/show.scss')
    .addStyleEntry('css/web/shop/covid-solidarity/home', './assets/scss/web/shop/covid-solidarity/home.scss')
    .addStyleEntry('css/web/shop/covid-solidarity/show', './assets/scss/web/shop/covid-solidarity/show.scss')
    .addStyleEntry('css/web/shop/refinerylab-us/home', './assets/scss/web/shop/refinerylab-us/home.scss')
    .addStyleEntry('css/web/shop/refinerylab-us/show', './assets/scss/web/shop/refinerylab-us/show.scss')
    .addStyleEntry('css/web/shop/pepejeans-int/home', './assets/scss/web/shop/pepejeans-int/home.scss')
    .addStyleEntry('css/web/shop/pepejeans-int/show', './assets/scss/web/shop/pepejeans-int/show.scss')
    .addStyleEntry('css/web/shop/farm-be/home', './assets/scss/web/shop/farm-be/home.scss')
    .addStyleEntry('css/web/shop/farm-be/show', './assets/scss/web/shop/farm-be/show.scss')
    .addStyleEntry('css/web/shop/sequoia-be/home', './assets/scss/web/shop/sequoia-be/home.scss')
    .addStyleEntry('css/web/shop/sequoia-be/show', './assets/scss/web/shop/sequoia-be/show.scss')
    .addStyleEntry('css/web/shop/workforcesolutions-us/home', './assets/scss/web/shop/workforcesolutions-us/home.scss')
    .addStyleEntry('css/web/shop/workforcesolutions-us/show', './assets/scss/web/shop/workforcesolutions-us/show.scss')
    .addStyleEntry('css/web/shop/laposte-fr/home', './assets/scss/web/shop/laposte-fr/home.scss')
    .addStyleEntry('css/web/shop/laposte-fr/show', './assets/scss/web/shop/laposte-fr/show.scss')
    .addStyleEntry('css/web/shop/hackett-int/home', './assets/scss/web/shop/hackett-int/home.scss')
    .addStyleEntry('css/web/shop/hackett-int/show', './assets/scss/web/shop/hackett-int/show.scss')

    .addStyleEntry('css/web/shop/les3brasseurs-fr/home', './assets/scss/web/shop/les3brasseurs-fr/home.scss')
    .addStyleEntry('css/web/shop/les3brasseurs-fr/show', './assets/scss/web/shop/les3brasseurs-fr/show.scss')

    .addStyleEntry('css/web/shop/pv-be/home', './assets/scss/web/shop/pv-be/home.scss')
    .addStyleEntry('css/web/shop/pv-be/show', './assets/scss/web/shop/pv-be/show.scss')

    .addStyleEntry('css/web/shop/manpower-be/home', './assets/scss/web/shop/manpower-be/home.scss')
    .addStyleEntry('css/web/shop/manpower-be/show', './assets/scss/web/shop/manpower-be/show.scss')

    .addStyleEntry('css/web/shop/pickles-au/home', './assets/scss/web/shop/pickles-au/home.scss')
    .addStyleEntry('css/web/shop/pickles-au/show', './assets/scss/web/shop/pickles-au/show.scss')

    .addStyleEntry('css/web/shop/multipharma/home', './assets/scss/web/shop/multipharma/home.scss')
    .addStyleEntry('css/web/shop/multipharma/show', './assets/scss/web/shop/multipharma/show.scss')

    .addStyleEntry('css/web/shop/brocom-be/home', './assets/scss/web/shop/brocom-be/home.scss')
    .addStyleEntry('css/web/shop/brocom-be/show', './assets/scss/web/shop/brocom-be/show.scss')

    .addStyleEntry('css/web/shop/vival-fr/home', './assets/scss/web/shop/vival-fr/home.scss')
    .addStyleEntry('css/web/shop/vival-fr/show', './assets/scss/web/shop/vival-fr/show.scss')

    .addStyleEntry('css/web/shop/spar-fr/home', './assets/scss/web/shop/spar-fr/home.scss')
    .addStyleEntry('css/web/shop/spar-fr/show', './assets/scss/web/shop/spar-fr/show.scss')

    .addStyleEntry('css/web/shop/wasteconnections-us/home', './assets/scss/web/shop/wasteconnections-us/home.scss')
    .addStyleEntry('css/web/shop/wasteconnections-us/show', './assets/scss/web/shop/wasteconnections-us/show.scss')

    .addStyleEntry('css/web/shop/lepetitcasino-fr/home', './assets/scss/web/shop/lepetitcasino-fr/home.scss')
    .addStyleEntry('css/web/shop/lepetitcasino-fr/show', './assets/scss/web/shop/lepetitcasino-fr/show.scss')

    .addStyleEntry('css/web/shop/lapataterie-fr/home', './assets/scss/web/shop/lapataterie-fr/home.scss')
    .addStyleEntry('css/web/shop/lapataterie-fr/show', './assets/scss/web/shop/lapataterie-fr/show.scss')

    // Fonts
    .addStyleEntry('css/web/shop/farm-be/fonts', './assets/scss/web/shop/farm-be/base/_fonts.scss')

    // load images
    .copyFiles({
        from: './assets/images',
        to: 'images/[path][name].[ext]',
    })
;

module.exports = Encore.getWebpackConfig();
