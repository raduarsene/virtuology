<?php

declare(strict_types=1);

namespace App\Helper;

class DateTimeFormat
{
    const FORMAT_DEFAULT = 'd/m/Y H:i';

    const FORMAT_YMD_SLASH = 'Y/m/d H:i';
    const FORMAT_YMD_DASH = 'Y-m-d H:i';
    const FORMAT_YMD_DOT = 'Y.m.d H:i';
    const FORMAT_DMY_SLASH = 'd/m/Y H:i';
    const FORMAT_DMY_DASH = 'd-m-Y H:i';
    const FORMAT_DMY_DOT = 'd.m.Y H:i';
    const FORMAT_YMD_12HOUR = 'Y/m/d g:i a';
    const FORMAT_DMY_12HOUR = 'd/m/Y g:i a';
    const FORMAT_MDY_12HOUR = 'm/d/Y g:i a';

    const FORMATS = [
        self::FORMAT_YMD_SLASH  => [
            'date'       => 'Y/m/d',
            'momentDate' => 'YYYY/MM/DD',
            'time'       => 'H:i',
            'momentTime' => 'HH:mm',
        ],
        self::FORMAT_YMD_DASH   => [
            'date'       => 'Y-m-d',
            'momentDate' => 'YYYY-MM-DD',
            'time'       => 'H:i',
            'momentTime' => 'HH:mm',
        ],
        self::FORMAT_YMD_DOT    => [
            'date'       => 'Y.m.d',
            'momentDate' => 'YYYY.MM.DD',
            'time'       => 'H:i',
            'momentTime' => 'HH:mm',
        ],
        self::FORMAT_DMY_SLASH  => [
            'date'       => 'd/m/Y',
            'momentDate' => 'DD/MM/YYYY',
            'time'       => 'H:i',
            'momentTime' => 'HH:mm',
        ],
        self::FORMAT_DMY_DASH   => [
            'date'       => 'd-m-Y',
            'momentDate' => 'DD-MM-YYYY',
            'time'       => 'H:i',
            'momentTime' => 'HH:mm',
        ],
        self::FORMAT_DMY_DOT    => [
            'date'       => 'd.m.Y',
            'momentDate' => 'DD.MM.YYYY',
            'time'       => 'H:i',
            'momentTime' => 'HH:mm',
        ],
        self::FORMAT_YMD_12HOUR => [
            'date'       => 'Y/m/d',
            'momentDate' => 'YYYY/MM/DD',
            'time'       => 'g:i a',
            'momentTime' => 'h:mm a',
        ],
        self::FORMAT_DMY_12HOUR => [
            'date'       => 'd/m/Y',
            'momentDate' => 'DD/MM/YYYY',
            'time'       => 'g:i a',
            'momentTime' => 'h:mm a',
        ],
        self::FORMAT_MDY_12HOUR => [
            'date'       => 'm/d/Y',
            'momentDate' => 'MM/DD/YYYY',
            'time'       => 'g:i a',
            'momentTime' => 'h:mm a',
        ],
    ];

    public static function getDateFormat(?string $format): string
    {
        if ($format && !empty(self::FORMATS[$format])) {
            $dateFormat = self::FORMATS[$format]['date'];
        } else {
            $dateFormat = self::FORMATS[self::FORMAT_DEFAULT]['date'];
        }

        return $dateFormat;
    }

    public static function getMomentDateFormat(?string $format): string
    {
        if ($format && !empty(self::FORMATS[$format])) {
            $dateFormat = self::FORMATS[$format]['momentDate'];
        } else {
            $dateFormat = self::FORMATS[self::FORMAT_DEFAULT]['momentDate'];
        }

        return $dateFormat;
    }

    public static function getTimeFormat(?string $format): string
    {
        if ($format && !empty(self::FORMATS[$format])) {
            $timeFormat = self::FORMATS[$format]['time'];
        } else {
            $timeFormat = self::FORMATS[self::FORMAT_DEFAULT]['time'];
        }

        return $timeFormat;
    }

    public static function getMomentTimeFormat(?string $format): string
    {
        if ($format && !empty(self::FORMATS[$format])) {
            $timeFormat = self::FORMATS[$format]['momentTime'];
        } else {
            $timeFormat = self::FORMATS[self::FORMAT_DEFAULT]['momentTime'];
        }

        return $timeFormat;
    }

    public static function getDefaultFormattedDateTime(): string
    {
        return (new \DateTime())->format(self::FORMAT_DEFAULT);
    }

    public static function getChoicesSelect(): array
    {
        $dateTime = new \DateTime();

        return [
            $dateTime->format(self::FORMAT_YMD_SLASH)  => self::FORMAT_YMD_SLASH,
            $dateTime->format(self::FORMAT_YMD_DASH)   => self::FORMAT_YMD_DASH,
            $dateTime->format(self::FORMAT_YMD_DOT)    => self::FORMAT_YMD_DOT,
            $dateTime->format(self::FORMAT_DMY_SLASH)  => self::FORMAT_DMY_SLASH,
            $dateTime->format(self::FORMAT_DMY_DASH)   => self::FORMAT_DMY_DASH,
            $dateTime->format(self::FORMAT_DMY_DOT)    => self::FORMAT_DMY_DOT,
            $dateTime->format(self::FORMAT_YMD_12HOUR) => self::FORMAT_YMD_12HOUR,
            $dateTime->format(self::FORMAT_DMY_12HOUR) => self::FORMAT_DMY_12HOUR,
            $dateTime->format(self::FORMAT_MDY_12HOUR) => self::FORMAT_MDY_12HOUR,
        ];
    }
}
