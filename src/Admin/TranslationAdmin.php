<?php

namespace App\Admin;

use App\Entity\Language;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\TranslationBundle\Entity\File;
use Lexik\Bundle\TranslationBundle\Manager\FileManager;
use Lexik\Bundle\TranslationBundle\Manager\TransUnitManagerInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;
use Sonata\DoctrineORMAdminBundle\Filter\ChoiceFilter;
use Sonata\DoctrineORMAdminBundle\Filter\StringFilter;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class TranslationAdmin extends AbstractAdmin
{
    const DEFAULT_DOMAIN = 'messages';

    /** @var FileManager */
    private $fileManager;

    /** @var EntityManagerInterface */
    private $em;

    /** @var array */
    private $appLocales;

    /** @var TransUnitManagerInterface */
    private $transUnitManager;

    /** @var array */
    private $filterLocales = array();

    /** @var array */
    private $managedLocales = array();
    
    
    /**
     * TranslationAdmin constructor.
     *
     * @param                           $code
     * @param                           $class
     * @param                           $baseControllerName
     * @param TransUnitManagerInterface $transUnitManager
     * @param FileManager               $fileManager
     * @param EntityManagerInterface    $em
     * @param                           $appLocales
     */
    public function __construct(
        $code,
        $class,
        $baseControllerName,
        TransUnitManagerInterface $transUnitManager,
        FileManager $fileManager,
        EntityManagerInterface $em,
        $appLocales
    ) {
        $this->fileManager = $fileManager;
        $this->em = $em;
        $this->appLocales = $appLocales;
        $this->transUnitManager = $transUnitManager;
        $this->managedLocales = $this->getAppLocales();
        

        $this->setTranslationDomain(self::DEFAULT_DOMAIN);

        parent::__construct($code, $class, $baseControllerName);
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $domains = [];
        $domainsQueryResult = $this->em->createQueryBuilder()
            ->select('DISTINCT t.domain')
            ->from(File::class, 't')
            ->getQuery()
            ->getArrayResult();

        array_walk_recursive(
            $domainsQueryResult,
            function ($domain) use (&$domains) {
                $domains[$domain] = $domain;
            }
        );
        ksort($domains);

        $filter
            ->add('locale', CallbackFilter::class, [
                'callback'      => function ($queryBuilder, $alias, $field, $options) {
                    if (!isset($options['value']) || empty($options['value'])) {
                        return;
                    }

                    // use on to filter locales
                    $this->joinTranslations($queryBuilder, $alias, $options['value']);
                },
                'field_options' => [
                    'choices'  => $this->formatLocales($this->managedLocales),
                    'required' => false,
                    'multiple' => true,
                    'expanded' => false,
                ],
                'field_type'    => ChoiceType::class,
            ])
            ->add('show_non_translated_only', CallbackFilter::class, [
                'callback'      => function ($queryBuilder, $alias, $field, $options) {
                    if (!isset($options['value']) || empty($options['value']) || false === $options['value']) {
                        return;
                    }

                    $this->joinTranslations($queryBuilder, $alias);

                    $emptyFieldPrefixes = ['__', 'new_', ''];
                    foreach ($emptyFieldPrefixes as $prefix) {
                        if (empty($prefix)) {
                            $queryBuilder->orWhere('translations.content IS NULL');
                        } else {
                            $queryBuilder->orWhere('translations.content LIKE :content')
                                ->setParameter('content', $prefix.'%');
                        }

                    }
                },
                'field_options' => [
                    'required' => true,
                    'value'    => false,
                ],
                'field_type'    => CheckboxType::class,
                'label'         => 'Shown non translated only',
            ])
            ->add('key', StringFilter::class)
            ->add('domain', ChoiceFilter::class, [
                'field_options' => array(
                    'choices'    => $domains,
                    'required'   => true,
                    'multiple'   => false,
                    'expanded'   => false,
                    'empty_data' => 'all',
                ),
                'field_type'    => ChoiceType::class,
            ])
            ->add('content', CallbackFilter::class, [
                'callback'   => function ($queryBuilder, $alias, $field, $options) {
                    if (!isset($options['value']) || empty($options['value'])) {
                        return;
                    }

                    $this->joinTranslations($queryBuilder, $alias);
                    $queryBuilder->andWhere('translations.content LIKE :content')
                        ->setParameter('content', '%'.$options['value'].'%');
                },
                'field_type' => TextType::class,
                'label'      => 'content',
            ]);
    }

    /**
     * @param ListMapper $list
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('id', 'integer')
            ->add('key', 'string')
            ->add('domain', 'string');

        $localesToShow = count($this->filterLocales) > 0 ? $this->filterLocales : $this->managedLocales;

        foreach ($localesToShow as $locale) {
            $fieldDescription = $this->modelManager->getNewFieldDescriptionInstance($this->getClass(), $locale);
            $fieldDescription->setTemplate('admin/translation/base_inline_translation_field.html.twig');
            $fieldDescription->setOption('locale', $locale);
            $list->add($fieldDescription);
        }
    }

    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form)
    {
        $subject = $this->getSubject();

        if (null === $subject->getId()) {
            $subject->setDomain(self::DEFAULT_DOMAIN);
        }

        $form
            ->add('key', TextType::class)
            ->add('domain', TextType::class);
    }

    /**
     * @param string $name
     * @return string|null
     */
    public function getTemplate($name)
    {
        if ($name === 'list') {
            return 'admin/translation/list.html.twig';
        }

        return parent::getTemplateRegistry()->getTemplate($name);
    }

    /**
     * @param string $name
     * @return string
     */
    public function getOriginalTemplate($name)
    {
        return $this->getTemplateRegistry()->getTemplate($name);
    }

    /**
     * Get languages from database as locales
     *
     * @return array
     */
    public function getAppLocales()
    {
        $locales = $this->appLocales;

        $languages = $this->em->getRepository(Language::class)->findAll();
        foreach ($languages as $language) {
            if (!in_array($language->getCode(), $locales)) {
                $locales[] = $language->getCode();
            }
        }

        return $locales;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * @return TransUnitManagerInterface
     */
    public function getTransUnitManager()
    {
        return $this->transUnitManager;
    }

    /**
     * @return FileManager
     */
    public function getFileManager()
    {
        return $this->fileManager;
    }

    /**
     * @return array
     */
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(
            array(
                'domain' => array(
                    'value' => self::DEFAULT_DOMAIN,
                ),
            ),
            $this->datagridValues

        );

        return parent::getFilterParameters();
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('clear_cache')
            ->add('create_trans_unit')
            ->add('sync_translations')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatagrid()
    {
        if ($this->datagrid) {
            return;
        }

        $filterParameters = $this->getFilterParameters();

        // transform _sort_by from a string to a FieldDescriptionInterface for the datagrid.
        if (isset($filterParameters['locale']) && is_array($filterParameters['locale'])) {
            $this->filterLocales = array_key_exists('value', $filterParameters['locale']) ? $filterParameters['locale']['value'] : $this->managedLocales;
        }

        parent::buildDatagrid();
    }

    /**
     * {@inheritdoc}
     */
    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        $actions['download'] = array(
            'label'            => 'Download',
            'ask_confirmation' => false,
        );

        return $actions;
    }

    /**
     * @param ProxyQuery $queryBuilder
     * @param String     $alias
     * @param array|null $locales
     */
    private function joinTranslations($queryBuilder, $alias, array $locales = null)
    {
        $alreadyJoined = false;
        $joins = $queryBuilder->getDQLPart('join');
        if (array_key_exists($alias, $joins)) {
            $joins = $joins[$alias];
            foreach ($joins as $join) {
                if (strpos($join->__toString(), "$alias.translations ")) {
                    $alreadyJoined = true;
                }
            }
        }

        if (!$alreadyJoined) {
            /** @var QueryBuilder $queryBuilder */
            if ($locales) {
                $queryBuilder->leftJoin(
                    sprintf('%s.translations', $alias),
                    'translations',
                    'WITH',
                    'translations.locale = :locales'
                );
                $queryBuilder->setParameter('locales', $locales);
            } else {
                $queryBuilder->leftJoin(sprintf('%s.translations', $alias), 'translations');
            }
        }
    }

    /**
     * @param array $locales
     * @return array
     */
    private function formatLocales(array $locales)
    {
        $formattedLocales = array();
        array_walk_recursive(
            $locales,
            function ($language) use (&$formattedLocales) {
                $formattedLocales[$language] = $language;
            }
        );

        return $formattedLocales;
    }
    
    
}
