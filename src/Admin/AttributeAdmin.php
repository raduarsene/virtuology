<?php

namespace App\Admin;

use App\Entity\Attribute;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

final class AttributeAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Homepage'  => Attribute::TYPE_PAGE_HOME,
                    'Show page' => Attribute::TYPE_PAGE_SHOW,
                    'Global'    => Attribute::TYPE_GLOBAL,
                ],
            ])
            ->add('block', null, [
                'help' => 'Name of the directory and block of this attribute',
            ])
            ->add('description')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('type')
            ->add('block')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('name')
            ->add('type')
            ->add('block')
            ->add('_action', null, [
                'actions' => [
                    'edit'   => [],
                    'delete' => [],
                ]
            ])
        ;
    }
}
