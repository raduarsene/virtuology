<?php

namespace App\Admin;

use App\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

final class UserAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('email')
            ->add('plainPassword', PasswordType::class, [
                'required' => false,
                'help'     => 'Type a new password if you want to change.',
            ])
            ->add('roles', ChoiceType::class, [
                'choices'  => [User::ROLE_USER => User::ROLE_USER, User::ROLE_ADMIN => User::ROLE_ADMIN, User::ROLE_SUPER_ADMIN => User::ROLE_SUPER_ADMIN],
                'multiple' => true,
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('email')
            ->add('roles', null, [], ChoiceType::class, [
                'choices' => [User::ROLE_USER => User::ROLE_USER, User::ROLE_ADMIN => User::ROLE_ADMIN, User::ROLE_SUPER_ADMIN => User::ROLE_SUPER_ADMIN],
            ])
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('name')
            ->add('email')
            ->add('createdAt')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ]
            ])
        ;
    }
}
