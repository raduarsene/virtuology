<?php

namespace App\Admin;

use App\Entity\Attribute;
use App\Entity\AttributeValue;
use App\Entity\ShopTemplate;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class ShopTemplateAdmin extends AbstractAdmin
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var UrlGeneratorInterface */
    private $router;

    /** @var array */
    private $usedShopAttributes = array();

    /**
     * ShopTemplateAdmin constructor.
     *
     * @param string                 $code
     * @param string                 $class
     * @param string                 $baseControllerName
     * @param EntityManagerInterface $em
     * @param UrlGeneratorInterface  $router
     */
    public function __construct(
        $code,
        $class,
        $baseControllerName,
        EntityManagerInterface $em,
        UrlGeneratorInterface $router
    ) {
        $this->em = $em;
        $this->router = $router;

        parent::__construct($code, $class, $baseControllerName);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('position');

        /** @var ShopTemplate $shopTemplate */
        $shopTemplate = $this->getSubject();

        $attribute = $shopTemplate ? $shopTemplate->getAttribute() : null;
        $attributeValue = $shopTemplate ? $shopTemplate->getAttributeValue() : null;

        $this->addElements($formMapper, $attribute, $attributeValue, true);

        $builder = $formMapper->getFormBuilder();

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($formMapper) {
            /** @var ShopTemplate $shopTemplate */
            $shopTemplate = $event->getData();

            $attribute = $shopTemplate ? $shopTemplate->getAttribute() : null;
            $attributeValue = $shopTemplate ? $shopTemplate->getAttributeValue() : null;

            $this->addElements($formMapper, $attribute, $attributeValue);
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($formMapper) {
            $data = $event->getData();

            $attribute = $this->em->getRepository(Attribute::class)->find($data['attribute']);
            $attributeValue = $this->em->getRepository(AttributeValue::class)->findOneBy([
                'id'        => $data['attributeValue'],
                'attribute' => $attribute,
            ]);

            $this->addElements($formMapper, $attribute, $attributeValue);
        });
    }

    /**
     * Create form fields with options for shop template
     *
     * @param FormMapper          $formMapper
     * @param Attribute|null      $attribute
     * @param AttributeValue|null $attributeValue
     * @param bool                $showAll Show all templates options in select on page load
     */
    private function addElements(FormMapper $formMapper, Attribute $attribute = null, AttributeValue $attributeValue = null, $showAll = false) {
        $container = $this->getConfigurationPool()->getContainer();
        $request = $container->get('request_stack')->getCurrentRequest();

        if ($showAll) {
            $attributes = $this->em->getRepository(Attribute::class)->findAll();
        } else {
            $usedShopAttributes = $attribute ? array_diff(array_values($this->usedShopAttributes), [$attribute->getId()]) : array_values($this->usedShopAttributes);

            $attributes = $this->em->getRepository(Attribute::class)->findNotUsedAttributes($usedShopAttributes);
        }

        if (!$showAll && $attribute) {
            $this->usedShopAttributes[$attribute->getId()] = $attribute->getId();

            $attributeValues = $this->em->getRepository(AttributeValue::class)->findBy([
                'attribute' => $attribute->getId()
            ]);
        } else {
            $attributeValues = $this->em->getRepository(AttributeValue::class)->findAll();
        }

        // Show AttributeValue image if any
        $helpImage = '';
        $imagePath = $container->getParameter('app.attribute_value.image.path');
        if ($attributeValue && ($imageName = $attributeValue->getImageName())) {
            $imageUrl = $request->getUriForPath('/'.$imagePath.$imageName);

            // add a 'help' option containing the preview's img tag
            $helpImage = '<img src="'.$imageUrl.'" alt="'.$imageName.'" class="attribute-image-select"/>';
        }

        $formMapper
            ->add('attribute', EntityType::class, array(
                'required'    => true,
                'class'       => Attribute::class,
                'choices'     => $attributes,
                'data'        => $attribute,
                'attr'        => ['class' => 'admin-shop-attribute-select'],
                'choice_attr' => function ($attribute) {
                    return ['data-url' => $this->router->generate('app_admin_values_by_attribute', ['id' => $attribute->getId()])];
                },
                'placeholder' => 'Select an Attribute ...',
            ))
            ->add('attributeValue', EntityType::class, array(
                'required'    => true,
                'class'       => AttributeValue::class,
                'choices'     => $attributeValues,
                'data'        => $attributeValue,
                'attr'        => ['class' => 'admin-shop-attribute-value-select'],
                'choice_attr' => function ($attributeValue) use ($request, $imagePath) {
                    $imageName = $imageUrl = '';
                    if ($attributeValue->getImageName()) {
                        $imageName = $attributeValue->getImageName();
                        $imageUrl = $request->getUriForPath('/'.$imagePath.$imageName);
                    }

                    return [
                        'data-image' => $imageName,
                        'data-url'   => $imageUrl,
                    ];
                },
                'sonata_help' => $helpImage,
            ))
        ;
    }
}
