<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

final class ApiErrorAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_sort_order' => 'DESC',
        '_sort_by'    => 'createdAt',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('shop')
            ->add('errorCode')
            ->add('apiUrl', UrlType::class)
            ->add('apiQueryString', null, [
                'help' => "Format <b>json</b>. Ex. <i>{\"key\":\"value\"}</i>",
            ])
            ->add('errorMessage')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('shop')
            ->add('errorCode')
            ->add('apiUrl')
            ->add('apiQueryString')
            ->add('errorMessage')
            ->add('createdAt')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('shop', null, [
                'show_filter' => true,
            ])
            ->add('errorCode')
            ->add('apiUrl')
            ->add('apiQueryString')
            ->add('errorMessage')
            ->add('createdAt')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('shop')
            ->add('errorCode')
            ->add('apiUrl')
            ->add('errorMessage')
            ->add('createdAt')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                ],
            ])
        ;
    }
}
