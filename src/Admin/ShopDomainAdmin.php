<?php

namespace App\Admin;

use App\Helper\DateTimeFormat;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

final class ShopDomainAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('language')
            ->add('availableLanguages')
            ->add('gClientCode', null, [
                'label' => 'Google Client Code',
            ])
            ->add('gMobiCode', null, [
                'label' => 'Google Mobilosoft Code',
            ])
            ->add('gtmCode', null, [
                'label' => 'Google Tag Manager Code',
            ])
            ->add('gMetaTag', null, [
                'label' => 'Google Meta Tag',
            ])
            ->add('datetimeFormat', ChoiceType::class, [
                'required' => false,
                'choices'  => DateTimeFormat::getChoicesSelect(),
                'label'    => 'Datetime Format',
                'help'     => "Default format:<br>".DateTimeFormat::getDefaultFormattedDateTime(),
            ])
            ->add('defaultDomain', null, [
                'label' => 'Default',
            ])
            ->add('hasGdpr', null, [
                'label' => 'Gdpr',
            ])
        ;
    }
}
