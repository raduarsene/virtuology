<?php

namespace App\Admin;

use App\Entity\AttributeValue;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;

final class AttributeValueAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var AttributeValue $attributeValue */
        $attributeValue = $this->getSubject();

        $imageFieldOptions = ['required' => false];
        if ($attributeValue && ($imageName = $attributeValue->getImageName())) {
            $container = $this->getConfigurationPool()->getContainer();
            $imageUrl = $container->get('request_stack')->getCurrentRequest()->getUriForPath('/'.$container->getParameter('app.attribute_value.image.path').$imageName);

            // add a 'help' option containing the preview's img tag
            $imageFieldOptions['help'] = 'Uploaded image:<br><img src="'.$imageUrl.'" alt="'.$imageName.'" class="attribute-image-preview"/>';
        }

        $formMapper
            ->add('attribute')
            ->add('name')
            ->add('value')
            ->add('description')
            ->add('isDefault')
            ->add('image', FileType::class, $imageFieldOptions)
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('attribute')
            ->add('name')
            ->add('value')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('attribute')
            ->add('name')
            ->add('value')
            ->add('_action', null, [
                'actions' => [
                    'edit'   => [],
                    'delete' => [],
                ]
            ])
        ;
    }
}
