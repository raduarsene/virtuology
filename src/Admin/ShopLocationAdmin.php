<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

final class ShopLocationAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_sort_order' => 'DESC',
        '_sort_by'    => 'createdAt',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('shop')
            ->add('name')
            ->add('brand')
            ->add('city')
            ->add('lat')
            ->add('lng')
            ->add('details', null, [
                'help' => "Format <b>json</b>. Ex. <i>{\"key\":\"value\"}</i>",
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('shop', null, [
                'show_filter' => true,
            ])
            ->add('name')
            ->add('brand')
            ->add('city')
            ->add('lat')
            ->add('lng')
            ->add('details')
            ->add('createdAt')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('shop')
            ->add('name')
            ->add('brand')
            ->add('city')
            ->add('lat')
            ->add('lng')
            ->add('_action', null, [
                'actions' => [
                    'edit'   => [],
                    'delete' => [],
                ],
            ])
        ;
    }
}
