<?php

namespace App\Admin;

use App\Entity\AttributeValue;
use App\Entity\Shop;
use App\Entity\ShopTemplate;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

final class ShopAdmin extends AbstractAdmin
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * ShopAdmin constructor.
     *
     * @param string                 $code
     * @param string                 $class
     * @param string                 $baseControllerName
     * @param EntityManagerInterface $em
     */
    public function __construct(
        $code,
        $class,
        $baseControllerName,
        EntityManagerInterface $em
    ) {
        $this->em = $em;

        parent::__construct($code, $class, $baseControllerName);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $this->checkShopTemplate();

        $formMapper
            ->tab('Main')
                ->with('Title', ['class' => 'col-md-6'])
                    ->add('name')
                    ->add('code', null, ['help' => 'This is a unique code that identifies this shop'])
                ->end()

                ->with('Map', ['class' => 'col-md-6'])
                    ->add('mapType', ChoiceType::class, [
                        'choices' => ['Google Map' => Shop::MAP_TYPE_GOOGLE, 'Here Map' => Shop::MAP_TYPE_HERE],
                    ])
                    ->add('mapCredentials', null, [
                        'help' => "Json format credentials for map.<br>For <b><i>Google Map</i></b> use keys <i>\"key\"</i> or <i>\"key_premium\"</i> , ex: <b>{\"key\":\"VqDQvB0z39n\",\"key_premium\":\"C5guaEFmDw\"}</b> <br>For <b><i>Here Map</i></b> use keys <i>\"id\"</i> and <i>\"code\"</i> , ex: <b>{\"id\":\"5QvB0z39p3\",\"code\":\"qguaEFh3a1\"}</b>",
                    ])
                ->end()

                ->with('API', ['class' => 'col-md-6'])
                    ->add('apiVersion', ChoiceType::class, [
                        'choices' => [Shop::API_VERSION_2 => Shop::API_VERSION_2, Shop::API_VERSION_3 => Shop::API_VERSION_3, Shop::API_VERSION_LOCAL.' - (local)' => Shop::API_VERSION_LOCAL],
                    ])
                    ->add('apiSiteId', null, [
                        'help' => 'Site Id used for API V2 in my.mobilosoft.com',
                    ])
                    ->add('apiToken', null, [
                        'help' => 'Token used for API v3',
                    ])
                    ->add('apiKey', null, [
                        'help' => 'Key param used for Cloudinary API images',
                    ])
                    ->add('apiUrl', null, [
                        'help' => 'API Version V3 Url: staging or live',
                    ])
                ->end()

                ->with('Robots', ['class' => 'col-md-6'])
                    ->add('allowRobots', null, [
                        'help' => 'Allow crawlers in robots.txt',
                    ])
                ->end()

                ->with('Demo', ['class' => 'col-md-6'])
                    ->add('demoShop', null, [
                        'help' => 'Check this if you want a demo shop',
                    ])
                ->end()

                ->with('Maintenance', ['class' => 'col-md-6'])
                    ->add('maintenance', null, [
                        'help' => 'Put the shop under maintenance mode',
                    ])
                ->end()
    
                ->with('TrustPilot', ['class' => 'col-md-6'])
                    ->add('trustPilotConf', null, [
                        'help' => '{"businessUnitId":"", "apiKey":"", "apiSecretKey":""}',
                    ])
                ->end()

                ->with('Shop Domains', ['class' => 'col-md-12'])
                    ->add('domains', CollectionType::class, [
                        'by_reference' => false,
                    ], [
                        'edit'   => 'inline',
                        'inline' => 'table',
                    ])
                ->end()
    
                ->with('Local Business Type', ['class' => 'col-md-12'])
                    ->add('localBusinessTypes', CollectionType::class, [
                        'by_reference' => false,
                    ], [
                        'edit'   => 'inline',
                        'inline' => 'table',
                    ])
                ->end()

                ->with('Shop Templates', ['class' => 'col-md-12'])
                    ->add('templates', CollectionType::class, [
                        'by_reference' => false,
                    ], [
                        'edit'   => 'inline',
                        'inline' => 'table',
                    ])
                ->end()
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('code')
            ->add('mapType', null, [], ChoiceType::class, [
                'choices' => ['Google Map' => Shop::MAP_TYPE_GOOGLE, 'Here Map' => Shop::MAP_TYPE_HERE],
            ])
            ->add('apiVersion', null, [], ChoiceType::class, [
                'choices' => [Shop::API_VERSION_2 => Shop::API_VERSION_2, Shop::API_VERSION_3 => Shop::API_VERSION_3, Shop::API_VERSION_LOCAL.' - (local)' => Shop::API_VERSION_LOCAL],
            ])
            ->add('apiSiteId')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('name')
            ->add('code')
            ->add('mapType')
            ->add('apiVersion')
            ->add('apiSiteId')
            ->add('_action', null, [
                'actions' => [
                    'edit'   => [],
                    'delete' => [],
                ]
            ])
        ;
    }

    /**
     * Set default template on shop create
     */
    private function checkShopTemplate()
    {
        /** @var Shop $shop */
        $shop = $this->getSubject();
        if (!$shop->getId() && !count($shop->getTemplates())) {
            $attributeValues = $this->em->getRepository(AttributeValue::class)->findBy(['isDefault' => true]);

            if ($attributeValues) {
                foreach ($attributeValues as $attributeValue) {
                    $shopTemplate = new ShopTemplate();
                    $shopTemplate->setAttributeValue($attributeValue);

                    $shop->addTemplate($shopTemplate);
                }
            }
        }
    }
}
