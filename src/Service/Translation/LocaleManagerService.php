<?php

namespace App\Service\Translation;

use App\Entity\Language;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\TranslationBundle\Manager\LocaleManager;

class LocaleManagerService extends LocaleManager
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * LocaleManagerService constructor.
     *
     * @param EntityManagerInterface $em
     * @param array                  $appLocales
     */
    public function __construct(EntityManagerInterface $em, $appLocales)
    {
        $this->em = $em;

        parent::__construct($this->getAppLocales($appLocales));
    }

    /**
     * @param array $appLocales
     * @return array
     */
    private function getAppLocales($appLocales)
    {
        $locales = $appLocales;

        $languages = $this->em->getRepository(Language::class)->findAll();
        foreach ($languages as $language) {
            if (!in_array($language->getCode(), $locales)) {
                $locales[] = $language->getCode();
            }
        }

        return $locales;
    }
}
