<?php

namespace App\Service\Translation;


use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\TranslationBundle\Entity\File;
use Lexik\Bundle\TranslationBundle\Entity\Translation;
use Lexik\Bundle\TranslationBundle\Entity\TransUnit;
use Doctrine\Common\Collections\ArrayCollection;

class SyncManagerService
{
    /** @var EntityManagerInterface */
    private $default;
    
    /** @var EntityManagerInterface */
    private $translations;
    
    const BATCH_PERSIST = 30;
    
    /**
     * SyncManagerService constructor.
     *
     * @param EntityManagerInterface      $default
     * @param EntityManagerInterface $translations
     */
    public function __construct(EntityManagerInterface $default, EntityManagerInterface $translations)
    {
        $this->default = $default;
        $this->translations = $translations;
        
    }
    
    
    public function syncTranslationDomain(string $domain)
    {
        
        /** @var Connection $translationConnection */
        $translationConnection = $this->translations->getConnection();
        
        
        
        //1. remove actual translations
        $translationConnection->query("DELETE t FROM lexik_trans_unit AS u LEFT JOIN lexik_trans_unit_translations AS t ON u.id = t.trans_unit_id LEFT JOIN `lexik_translation_file` as f ON t.file_id = f.id WHERE u.domain = '".$domain."' AND f.domain = '".$domain."'");
        
        $translationConnection->query("DELETE u FROM  lexik_trans_unit AS u WHERE  u.domain = '".$domain."'");
    
        
        $currentFiles = [];
        
        $translationUnitArray = $this->default->getRepository(TransUnit::class)->findBy(['domain'=>$domain]);
        /** @var TransUnit $translationUnit */
        foreach ($translationUnitArray as $translationUnit) {
    
            $syncUnit = new TransUnit();
            $syncUnit->setKey($translationUnit->getKey());
            $syncUnit->setDomain($translationUnit->getDomain());
            $syncUnit->setTranslations(new ArrayCollection());
            
            $dateNow = new \DateTime();
            $translationConnection->insert('lexik_trans_unit', ['key_name'=>$syncUnit->getKey(), 'domain'=>$syncUnit->getDomain(), 'created_at'=>$dateNow->format('Y-m-d H:i:s'), 'updated_at'=>$dateNow->format('Y-m-d H:i:s')]);
            
            $syncUnitId = $translationConnection->lastInsertId();
            
            
            
            $currentTranslations = $this->default->getRepository(Translation::class)->findBy(['transUnit' =>$translationUnit]);
            /** @var Translation $translationObject */
            $i = 0;
            foreach ($currentTranslations as $translationObject) {
    
    
                /** @var File $file */
                $file = $translationObject->getFile();
                
    
                //check File
                if(!isset($currentFiles[$file->getLocale()]))
                {
    
                    $syncFile = $translationConnection->fetchAssoc("SELECT * FROM `lexik_translation_file` WHERE `domain`='".$domain."' AND `locale`='".$file->getLocale()."'");
                    
                    if(empty($syncFile)) {
                
                        $translationConnection->insert('lexik_translation_file', ['hash'=>$file->getHash(), 'domain'=>$domain, 'locale'=>$file->getLocale(), 'path'=>$file->getPath()]);
                        $syncFileId = $translationConnection->lastInsertId();
                
                    }
                    else {
                        $syncFileId = $syncFile['id'];
                        $translationConnection->update('lexik_translation_file', ['hash'=>$file->getHash()], ['id'=>$syncFileId]);
                    }
                    
                    $currentFiles[$file->getLocale()] = $syncFileId;
        
                }
                else {
                    $syncFileId = $currentFiles[$file->getLocale()];
                }
    
                $quotedContent = $translationConnection->quote($translationObject->getContent());
                $quotedContent = $translationObject->getContent();
                
                $translationConnection->insert('lexik_trans_unit_translations', ['file_id'=>$syncFileId, 'trans_unit_id'=>$syncUnitId, 'locale'=>$translationObject->getLocale(), 'content'=>$quotedContent, 'created_at'=>$dateNow->format('Y-m-d H:i:s'), 'updated_at'=>$dateNow->format('Y-m-d H:i:s'), 'modified_manually'=>'0']);
                
                
                
                
                
            }
            
        
        }
        
    
    }
    
}
