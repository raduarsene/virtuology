<?php

namespace App\Service\Api;

use App\Entity\Shop;
use App\Service\Api\Request\RoadRequest;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Response;

class PicklesAuctionService extends RequestService
{
    
    
    /** @var string */
    private $picklesAuctionUrl;
    
    
    public function __construct(int $apiAuctionsRequestTimeout, string $picklesAuctionUrl)
    {
        $this->picklesAuctionUrl = $picklesAuctionUrl;
        
        
        parent::__construct($apiAuctionsRequestTimeout);
    }

    
    public function getAuctions()
    {
        
        $response = [];
        
        
        try {
            
            
           
            $response = $this->getRequest($this->picklesAuctionUrl, [], []);
        
            $statusCode = $response->getStatusCode();
        
            $response = $response->getBody()->getContents();
            
    
            $bodyObject = simplexml_load_string($response);
            
           
            
            $response = [];
    
    
            $i = 0;
            foreach($bodyObject->vehicle as $vehicle)
            {
        
                $vehicleJson = json_decode(json_encode($vehicle, true), true);
        
                $nvicCode = !empty($vehicleJson["nvic_code"])&&is_string($vehicleJson["nvic_code"])?$vehicleJson["nvic_code"]:"";
                $lotId = !empty($vehicleJson["lot_id"])&&is_string($vehicleJson["lot_id"])?$vehicleJson["lot_id"]:"";
        
                $organisation = isset($vehicleJson["organisation"])?$vehicleJson["organisation"]:"";
                
        
                $organisationCode = $this->slugify($organisation);
                $organisationName = $organisation;
        
                $response[] = [
                    'organizationName' => $organisationName,
                    'organizationCode' => $organisationCode,
                    'lotId'            => $lotId,
                    'nvicCode'         => $nvicCode,
                    'vehicleDetails'   => $vehicleJson,
                ];
                
                $i++;
            }
            
            
        
        } catch (GuzzleException $e) {
            
            dump($e->getMessage());
            $response = $e->getResponse();
            $statusCode = $response ? $response->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
        
        }
        
        
        return [$response, $statusCode];
        
    }
    
    /**
     * @param $text
     *
     * @return mixed|string
     */
    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '_', $text);
        
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        
        // remove unwanted characters
        $text = preg_replace('~[^_\w]+~', '', $text);
        
        // trim
        $text = trim($text, '_');
        
        // remove duplicate -
        $text = preg_replace('~_+~', '_', $text);
        
        // lowercase
        $text = strtolower($text);
        
        if (empty($text)) {
            return 'n-a';
        }
        
        return $text;
    }
    
    
    
    
}
