<?php

namespace App\Service\Api\Request;

use App\Entity\ShopDomain;

class FeedDataRequest extends AbstractRequest
{
    const DEFAULT_PAGE = 0;
    const DEFAULT_PAGE_SIZE = 100;

    /**
     * Page to retrieve, 0 indexed
     *
     * @var int
     */
    private $page = self::DEFAULT_PAGE;

    /**
     * Size of the page to retrieve
     *
     * @var int
     */
    private $size = self::DEFAULT_PAGE_SIZE;

    /**
     * @var int
     */
    private $shopId;

    /**
     * The type of the feed data to get from shop feed
     *
     * @var string
     */
    private $type;

    /**
     * If entity is visible
     *
     * @var bool
     */
    private $isVisible;

    /**
     * If entity is in stock
     *
     * @var bool
     */
    private $inStock;

    /**
     * FeedDataRequest constructor.
     *
     * @param array           $data
     * @param ShopDomain|null $shopDomain
     */
    public function __construct(array $data = array(), ShopDomain $shopDomain = null)
    {
        $this->initialize($data, $shopDomain);
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        if ((int)$page > 0) {
            $this->page = (int)$page;
        } elseif ($page === null) {
            $this->page = $page;
        }
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        if ((int)$size > 0) {
            $this->size = (int)$size;
        } elseif ($size === null) {
            $this->size = $size;
        }
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $shopId
     */
    public function setShopId($shopId)
    {
        if (!empty($shopId)) {
            $this->shopId = $shopId;
        } elseif ($this->_shop) {
            $this->shopId = $this->_shop->getId();
        }
    }

    /**
     * @return int
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        if (!empty($type)) {
            $this->type = $type;
        }
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param bool $isVisible
     */
    public function setIsVisible($isVisible)
    {
        if ($isVisible !== null && $isVisible !== '') {
            $this->isVisible = (bool)$isVisible;
        }
    }

    /**
     * @return bool|null
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }

    /**
     * @param bool $inStock
     */
    public function setInStock($inStock)
    {
        if ($inStock !== null && $inStock !== '') {
            $this->inStock = (bool)$inStock;
        }
    }

    /**
     * @return bool|null
     */
    public function getInStock()
    {
        return $this->inStock;
    }
}
