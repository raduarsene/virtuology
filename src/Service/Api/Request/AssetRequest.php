<?php

namespace App\Service\Api\Request;

use App\Entity\ShopDomain;

class AssetRequest extends AbstractRequest
{
    /**
     * @var string
     */
    private $locationId;

    /**
     * @var string
     */
    private $siteId;

    /**
     * AssetRequest constructor.
     *
     * @param array           $data
     * @param ShopDomain|null $shopDomain
     */
    public function __construct(array $data = array(), ShopDomain $shopDomain = null)
    {
        $this->initialize($data, $shopDomain);
    }

    /**
     * @param string $locationId
     */
    public function setLocationId($locationId)
    {
        if (!empty($locationId)) {
            $this->locationId = $locationId;
        }
    }

    /**
     * @return string
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * @param string $siteId
     */
    public function setSiteId($siteId)
    {
        if (!empty($siteId)) {
            $this->siteId = $siteId;
        } elseif ($this->_shop) {
            $this->siteId = $this->_shop->getApiSiteId();
        }
    }

    /**
     * @return string
     */
    public function getSiteId()
    {
        return $this->siteId;
    }
}
