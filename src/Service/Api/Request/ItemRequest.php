<?php

namespace App\Service\Api\Request;

use App\Entity\ShopDomain;

class ItemRequest extends AbstractRequest
{
    const DEFAULT_PAGE = 0;
    const DEFAULT_PAGE_SIZE = 1000;
    const DEFAULT_STATUS_LIST = 'PUBLISHED';
    const PROJECTION_FULL = "ItemFull";

    /**
     * Page to retrieve, 0 indexed
     *
     * @var int
     */
    private $page = self::DEFAULT_PAGE;

    /**
     * Size of the page to retrieve
     *
     * @var int
     */
    private $size = self::DEFAULT_PAGE_SIZE;

    /**
     * @var string
     */
    private $siteId;

    /**
     * ID/GUID of the location
     *
     * @var string
     */
    private $locationId;

    /**
     * The language of the item
     *
     * @var string
     */
    private $language;

    /**
     * @var string
     */
    private $statusList = self::DEFAULT_STATUS_LIST;

    /**
     * @var string
     */
    private $projection = self::PROJECTION_FULL;

    /**
     * ItemRequest constructor.
     *
     * @param array           $data
     * @param ShopDomain|null $shopDomain Used in setters
     */
    public function __construct(array $data = array(), ShopDomain $shopDomain = null)
    {
        $this->initialize($data, $shopDomain);
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        if ((int)$page > 0) {
            $this->page = (int)$page;
        }
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        if ((int)$size > 0) {
            $this->size = (int)$size;
        }
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param string $siteId
     */
    public function setSiteId($siteId)
    {
        if (!empty($siteId)) {
            $this->siteId = $siteId;
        } elseif ($this->_shop) {
            $this->siteId = $this->_shop->getApiSiteId();
        }
    }

    /**
     * @return string
     */
    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * @param string $locationId
     */
    public function setLocationId($locationId)
    {
        if (!empty($locationId)) {
            $this->locationId = $locationId;
        }
    }

    /**
     * @return string
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        if (!empty($language)) {
            $this->language = $language;
        } elseif ($this->_shopDomain) {
            $this->language = $this->_shopDomain->getLanguage()->getCode();
        }
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $statusList
     */
    public function setStatusList($statusList)
    {
        if (!empty($statusList)) {
            $this->statusList = $statusList;
        }
    }

    /**
     * @return string
     */
    public function getStatusList()
    {
        return $this->statusList;
    }

    /**
     * @return string
     */
    public function getProjection(): string
    {
        return $this->projection;
    }

    /**
     * @param string $projection
     */
    public function setProjection(string $projection): void
    {
        $this->projection = $projection;
    }
}
