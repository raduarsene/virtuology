<?php

namespace App\Service\Api\Request;

use App\Entity\ShopDomain;

class FilterRequest extends AbstractRequest
{
    /**
     * @var string
     */
    private $siteId;
    

    /**
     * The language of the review
     *
     * @var string|null
     */
    private $language;

    /**
     * ItemRequest constructor.
     *
     * @param array           $data
     * @param ShopDomain|null $shopDomain Used in setters
     */
    public function __construct(array $data = array(), ShopDomain $shopDomain = null)
    {
        $this->initialize($data, $shopDomain);
    }

    /**
     * @param string $siteId
     */
    public function setSiteId(string $siteId)
    {
        if (!empty($siteId)) {
            $this->siteId = $siteId;
        } elseif ($this->_shop) {
            $this->siteId = $this->_shop->getApiSiteId();
        }
    }

    /**
     * @return string
     */
    public function getSiteId(): string
    {
        return $this->siteId;
    }

    
    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        if(strlen($language) > 2) {
            $language = substr($language, 0, 2);
        }
        
        if (!$language && $this->_shopDomain) {
            $language = $this->_shopDomain->getLanguage()->getCode();
        }

        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
}
