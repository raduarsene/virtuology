<?php

namespace App\Service\Api\Request;

use App\Entity\Shop;
use App\Entity\ShopDomain;
use App\Service\Api\Request\Location\LocalRequest;
use Zend\Code\Reflection\PropertyReflection;

abstract class AbstractRequest
{
    /** @var Shop|null */
    protected $_shop;

    /** @var ShopDomain|null */
    protected $_shopDomain;

    /** @var LocalRequest|null */
    protected $_localRequest;

    /**
     * @param array             $data
     * @param ShopDomain|null   $shopDomain
     * @param LocalRequest|null $localRequest
     */
    public function initialize(array $data = array(), ShopDomain $shopDomain = null, LocalRequest $localRequest = null)
    {
        $this->_shopDomain = $shopDomain;
        $this->_shop = $shopDomain ? $shopDomain->getShop() : null;
        $this->_localRequest = $localRequest;

        foreach ($data as $field => $value) {
            $method = 'set'.ucfirst($field);

            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $reflection = new \ReflectionClass(static::class);

        $array = [];
        /** @var \ReflectionProperty $property */
        foreach ($reflection->getProperties(PropertyReflection::IS_PRIVATE) as $property) {
            $field = $property->getName();
            $method = 'get'.ucfirst($field);

            if (method_exists($this, $method) && null !== $value = $this->$method()) {
                $array[$field] = $value;
            }
        }

        return $array;
    }
}
