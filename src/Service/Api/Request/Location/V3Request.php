<?php

namespace App\Service\Api\Request\Location;

use App\Service\Api\Request\AbstractRequest;

class V3Request extends AbstractRequest
{
    const DEFAULT_PAGE = 0;
    const DEFAULT_SIZE_PER_PAGE = 100;
    const DEFAULT_RADIUS_ON_NEAR = 50; // in km
    const OPEN_DATETIME_FORMAT = 'Y-m-d\TH:i:s\Z';
    const LIMIT_EMPTY_RADIUS = 2;

    const LANGUAGE_ADAPTER = ['nl' => 'nl', 'fr' => 'fr', 'en' => 'en'];

    /**
     * Page to retrieve, 0 indexed
     *
     * @var int
     */
    private $page = self::DEFAULT_PAGE;

    /**
     * Size of the page to retrieve
     *
     * @var int
     */
    private $size;

    /**
     * @var string
     */
    private $siteId;

    /**
     * @var float
     */
    private $latitude;

    /**
     * @var float
     */
    private $longitude;

    /**
     * @var float
     */
    private $bottomLatitude;

    /**
     * @var float
     */
    private $bottomLongitude;

    /**
     * Radius to search in km from center point
     *
     * @var int
     */
    private $radius;

    /**
     * Radius to search in metters from center point if no locations found with default radius
     *
     * @var int
     */
    protected $onEmptyRadius;
    
    /**
     * @var int
     */
    protected $onEmptyRadiusLimit = self::LIMIT_EMPTY_RADIUS;

    /**
     * @var string
     */
    private $language;

    /**
     * @var string
     */
    private $openNow;

    /**
     * @var string
     */
    private $openOn;

    /**
     * @var boolean
     */
    private $openOnSundays;

    /**
     * @var array
     */
    private $serviceKeys;
    
    /**
     * @var array
     */
    private $orServiceKeys;
    

    /**
     * @var array
     */
    private $brandSlugs;

    /**
     * Slug of location
     *
     * @var string
     */
    private $slug;
    
    
    
    /**
     * External id of location
     *
     * @var string|null
     */
    private $externalId;

    /**
     * Get location services/items
     *
     * @var bool
     */
    private $services = false;
    
    
    /** @var null|string */
    private $country;
   
    
    /**
     * a fitAll request can be with lat / long
     * @var bool
     */
    private $fitAll = false;

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        if (null !== $page) {
            $this->page = (int)$page;
        }

        $this->_localRequest && $this->_localRequest->setPage($this->page);
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        if (null !== $size) {
            $this->size = (int)$size;
        }

        $this->_localRequest && $this->_localRequest->setSize($this->size);
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param string $siteId
     */
    public function setSiteId($siteId)
    {
        if ($siteId) {
            $this->siteId = $siteId;
        } elseif ($this->_shop) {
            $this->siteId = $this->_shop->getApiSiteId();
        }

        $this->_localRequest && $this->_localRequest->setSiteId($this->siteId);
    }

    /**
     * @return string
     */
    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * @param string $within
     */
    public function setWithin($within)
    {
        if (!empty($within) && count(explode(',', $within)) === 4) {
            list($this->latitude, $this->longitude, $this->bottomLatitude, $this->bottomLongitude) = explode(',', $within);

            $this->_localRequest && $this->_localRequest->setWithin($within);
        }
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @return float
     */
    public function getBottomLatitude()
    {
        return $this->bottomLatitude;
    }

    /**
     * @return float
     */
    public function getBottomLongitude()
    {
        return $this->bottomLongitude;
    }

    /**
     * @param string $near
     */
    public function setNear($near)
    {
        if (!empty($near) && count(explode(',', $near)) === 2) {
            list($this->latitude, $this->longitude) = explode(',', $near);

            $this->_localRequest && $this->_localRequest->setNear($near);
        }
    }

    /**
     * @param int $radius
     */
    public function setRadius($radius)
    {
        if($this->isFitAll()) {
            $this->radius = null;
        }
        
        if (!empty($radius)) {
            
            $this->radius = (int)round($radius / 1000); // set radius in km

            $this->_localRequest && $this->_localRequest->setRadius($radius);
        } elseif ($this->latitude && !$this->bottomLatitude) {
            $this->radius = self::DEFAULT_RADIUS_ON_NEAR;
        }
    }

    /**
     * @return int|null
     */
    public function getRadius()
    {
        if($this->isFitAll()) {
            return null;
        }
        return $this->radius;
    }

    /**
     * @param int $onEmptyRadius
     */
    public function setOnEmptyRadius($onEmptyRadius)
    {
        if (!empty($onEmptyRadius)) {
            $this->onEmptyRadius = (int)$onEmptyRadius; // set radius in metters

            $this->_localRequest && $this->_localRequest->setRadius($onEmptyRadius);
        }
    }

    /**
     * @return int|null
     */
    public function getOnEmptyRadius()
    {
        return $this->onEmptyRadius;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language): void
    {
        if (!$language && $this->_shopDomain) {
            $language = $this->_shopDomain->getLanguage()->getCode();
        }

        $this->language = $language;
    }

    /**
     * @return string|null
     */
    public function getLanguage(): ?string
    {
        if(strlen($this->language) > 2) {
            return substr($this->language, 0, 2);
        }
        return $this->language;
    }

    /**
     * @return string|null
     */
    public function getLanguageAdapter(): ?string
    {
        return isset(self::LANGUAGE_ADAPTER[$this->language]) ? self::LANGUAGE_ADAPTER[$this->language] : $this->language;
    }

    /**
     * @param string|null $openNow
     */
    public function setOpenNow($openNow): void
    {
        if ($openNow) {
            $openNow = new \DateTime('now', new \DateTimeZone('UTC'));

            $this->openNow = $openNow->format(self::OPEN_DATETIME_FORMAT);

            $this->_localRequest && $this->_localRequest->setOpenNow($openNow);
        }
    }

    /**
     * @return string|null
     */
    public function getOpenNow(): ?string
    {
        return $this->openNow;
    }

    /**
     * @param string $openOn
     */
    public function setOpenOn($openOn): void
    {
        if ($openOn) {
            $openOn = new \DateTime($openOn, new \DateTimeZone('UTC'));

            $this->openOn = $openOn->format(self::OPEN_DATETIME_FORMAT);

            $this->_localRequest && $this->_localRequest->setOpenOn($openOn);
        }
    }

    /**
     * @return string
     */
    public function getOpenOn(): ?string
    {
        return $this->openOn;
    }

    /**
     * @param bool $openOnSundays
     */
    public function setOpenOnSundays($openOnSundays): void
    {
        if ($openOnSundays) {
            $this->openOnSundays = (bool)$openOnSundays;

            $this->_localRequest && $this->_localRequest->setOpenOnSundays($openOnSundays);
        }
    }

    /**
     * @return bool
     */
    public function getOpenOnSundays(): ?bool
    {
        return $this->openOnSundays;
    }

    /**
     * @param array $serviceKeys
     */
    public function setServiceKeys($serviceKeys): void
    {
        if ($serviceKeys) {
            $this->serviceKeys = $serviceKeys;

            $this->_localRequest && $this->_localRequest->setServiceKeys($serviceKeys);
        }
    }

    /**
     * @return array
     */
    public function getServiceKeys(): ?array
    {
        return $this->serviceKeys;
    }

    /**
     * @param array $brandSlugs
     */
    public function setBrandSlugs($brandSlugs): void
    {
        if ($brandSlugs) {
            $this->brandSlugs = $brandSlugs;

            $this->_localRequest && $this->_localRequest->setBrandSlugs($brandSlugs);
        }
    }

    /**
     * @return array
     */
    public function getBrandSlugs(): ?array
    {
        return $this->brandSlugs;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        if ($slug) {
            $this->slug = $slug;

            $this->_localRequest && $this->_localRequest->setSlug($slug);
        }
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param bool $services
     */
    public function setServices($services)
    {
        $this->services = (bool)$services;
    }

    /**
     * @return bool
     */
    public function getServices()
    {
        return $this->services;
    }
    
    /**
     * @return int
     */
    public function getOnEmptyRadiusLimit(): int
    {
        return $this->onEmptyRadiusLimit;
    }
    
    /**
     * @param int $onEmptyRadiusLimit
     */
    public function setOnEmptyRadiusLimit(int $onEmptyRadiusLimit): void
    {
        $this->onEmptyRadiusLimit = $onEmptyRadiusLimit;
    }
    
    /**
     * @return bool
     */
    public function isAllLocationsRequest()
    {
        
        $allLocationsRequest = true;
    
        if($this->getLatitude() && $this->getLongitude()) {
    
            $allLocationsRequest = false;
        }
        
        if(!empty($this->brandSlugs) || !empty($this->serviceKeys) || !empty($this->orServiceKeys)) {
            $allLocationsRequest = false;
        }
        return $allLocationsRequest;
    }
    
    /**
     * @return bool
     */
    public function isFitAll(): bool
    {
        return $this->fitAll;
    }
    
    /**
     * @param bool $fitAll
     */
    public function setFitAll(bool $fitAll): void
    {
        
        $this->fitAll = $fitAll;
        if($this->fitAll) {
            $this->setRadius(null);
        }
    }
    
    /**
     * @return array
     */
    public function getOrServiceKeys(): ?array
    {
        return $this->orServiceKeys;
    }
    
    /**
     * @param array $orServiceKeys
     */
    public function setOrServiceKeys(?array $orServiceKeys): void
    {
        $this->orServiceKeys = $orServiceKeys;
    }
    
    /**
     * @return string
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    
    /**
     * @param string $externalId
     */
    public function setExternalId(?string $externalId): void
    {
        $this->externalId = $externalId;
    }
    
    /**
     * @return string
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }
    
    /**
     * @param string $country
     */
    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }
    
    
}
