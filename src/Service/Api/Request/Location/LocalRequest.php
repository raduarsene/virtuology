<?php

namespace App\Service\Api\Request\Location;

use App\Service\Api\Request\AbstractRequest;

class LocalRequest extends AbstractRequest
{
    const DEFAULT_RADIUS_ON_NEAR = 15000; // metters

    /**
     * Page to retrieve, 0 indexed
     *
     * @var int
     */
    private $page;

    /**
     * Size of the page to retrieve
     *
     * @var int
     */
    private $size;

    /**
     * @var string
     */
    private $siteId;

    /**
     * Map bondaries, ex: "50.8187,4.0474,50.4266,6.6841"
     *
     * @var string
     */
    private $within;

    /**
     * Map center point, ex: "50.8187,4.0474"
     *
     * @var string
     */
    private $near;

    /**
     * Radius to search in metters from center point
     *
     * @var int
     */
    private $radius;

    /**
     * Search location by name, brand or city (used only on local API)
     *
     * @var string
     */
    private $search;

    /**
     * Slug of location
     *
     * @var string
     */
    private $slug;

    /**
     * Search with filters
     *
     * @var array
     */
    private $filter;

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param string $siteId
     */
    public function setSiteId($siteId)
    {
        if (!empty($siteId)) {
            $this->siteId = $siteId;
        } elseif ($this->_shop) {
            $this->siteId = $this->_shop->getApiSiteId();
        }
    }

    /**
     * @return string
     */
    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * @param string $within
     */
    public function setWithin($within)
    {
        if (!empty($within)) {
            $this->within = $within;
        }
    }

    /**
     * @return string
     */
    public function getWithin()
    {
        return $this->within;
    }

    /**
     * @param string $near
     */
    public function setNear($near)
    {
        if (!empty($near)) {
            $this->near = $near;
        }
    }

    /**
     * @return string
     */
    public function getNear()
    {
        return $this->near;
    }

    /**
     * @param int $radius
     */
    public function setRadius($radius)
    {
        if (!empty($radius)) {
            $this->radius = (int)$radius;
        }
    }

    /**
     * @return int
     */
    public function getRadius()
    {
        return $this->radius;
    }

    /**
     * @param string $search
     */
    public function setSearch($search)
    {
        if (!empty($search)) {
            $this->search = $search;
        }
    }

    /**
     * @return string
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        if (!empty($slug)) {
            $this->slug = $slug;
        }
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param array $filter
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;
    }

    /**
     * @param array $brandSlugs
     */
    public function setBrandSlugs($brandSlugs)
    {
        if ($brandSlugs) {
            $this->filter['brandSlugs'] = $brandSlugs;
        }
    }

    /**
     * @param string|\DateTime|null $openNow
     */
    public function setOpenNow($openNow)
    {
        if ($openNow) {
            $this->filter['openNow'] = $openNow instanceof \DateTime ? $openNow->format('Y-m-d H:i:s') : $openNow;
        }
    }

    /**
     * @param string|\DateTime|null $openOn
     */
    public function setOpenOn($openOn)
    {
        if ($openOn) {
            $this->filter['openOn'] = $openOn instanceof \DateTime ? $openOn->format('Y-m-d H:i:s') : $openOn;
        }
    }

    /**
     * @param bool|null $openOnSundays
     */
    public function setOpenOnSundays($openOnSundays)
    {
        if ($openOnSundays) {
            $this->filter['openOnSundays'] = (bool)$openOnSundays;
        }
    }

    /**
     * @param array|null $serviceKeys
     */
    public function setServiceKeys($serviceKeys)
    {
        if ($serviceKeys) {
            $this->filter['serviceKeys'] = $serviceKeys;
        }
    }

    /**
     * @return array|null
     */
    public function getFilter()
    {
        return $this->filter;
    }
}
