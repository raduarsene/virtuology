<?php

namespace App\Service\Api\Request\Location;

use App\Service\Api\Request\AbstractRequest;

class V2Request extends AbstractRequest
{
    const DEFAULT_PAGE = 0;
    const DEFAULT_PAGE_SIZE = 50;
    const DEFAULT_SIZE_PER_PAGE = 100;
    const DEFAULT_PROJECTION = 'StoreLocation';
    const DEFAULT_PROJECTION_FULL = 'LocationFull';
    const DEFAULT_RADIUS_ON_NEAR = 15000; // metters

    /**
     * Page to retrieve, 0 indexed
     *
     * @var int
     */
    private $page = self::DEFAULT_PAGE;

    /**
     * Size of the page to retrieve
     *
     * @var int
     */
    private $size;

    /**
     * @var string
     */
    private $siteId;

    /**
     * Type of extra fields to retrieve
     *
     * @var string
     */
    private $projection = self::DEFAULT_PROJECTION;

    /**
     * Map bondaries, ex: "50.8187,4.0474,50.4266,6.6841"
     *
     * @var string
     */
    private $within;

    /**
     * Map center point, ex: "50.8187,4.0474"
     *
     * @var string
     */
    private $near;

    /**
     * Radius to search in metters from center point
     *
     * @var int
     */
    private $radius;

    /**
     * Attributes/items attached to a location
     *
     * @var array
     */
    private $itemList;

    /**
     * Slug of location
     *
     * @var string
     */
    private $slug;

    /**
     * Language of location
     *
     * @var string|null
     */
    private $language;

    /**
     * Get location services/items
     *
     * @var bool
     */
    protected $services = false;

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        if ((int)$page > 0) {
            $this->page = (int)$page;
        }

        $this->_localRequest && $this->_localRequest->setPage($this->page);
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        if ((int)$size > 0) {
            $this->size = (int)$size;
        }

        $this->_localRequest && $this->_localRequest->setSize($this->size);
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param string $siteId
     */
    public function setSiteId($siteId)
    {
        if (!empty($siteId)) {
            $this->siteId = $siteId;
        } elseif ($this->_shop) {
            $this->siteId = $this->_shop->getApiSiteId();
        }

        $this->_localRequest && $this->_localRequest->setSiteId($this->siteId);
    }

    /**
     * @return string
     */
    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * @param string $projection
     */
    public function setProjection($projection)
    {
        if ($projection) {
            $this->projection = $projection;
        }
    }

    /**
     * @return string
     */
    public function getProjection()
    {
        return $this->projection;
    }

    /**
     * @param string $within
     */
    public function setWithin($within)
    {
        if (!empty($within)) {
            $this->within = $within;

            $this->_localRequest && $this->_localRequest->setWithin($this->within);
        }
    }

    /**
     * @return string
     */
    public function getWithin()
    {
        return $this->within;
    }

    /**
     * @param string $near
     */
    public function setNear($near)
    {
        if (!empty($near)) {
            $this->near = $near;

            $this->_localRequest && $this->_localRequest->setNear($this->near);
        }
    }

    /**
     * @return string
     */
    public function getNear()
    {
        return $this->near;
    }

    /**
     * @param int $radius
     */
    public function setRadius($radius)
    {
        if (!empty($radius)) {
            $this->radius = (int)$radius;

            $this->_localRequest && $this->_localRequest->setRadius($this->radius);
        }
    }

    /**
     * @return int|null
     */
    public function getRadius()
    {
        return $this->radius;
    }

    /**
     * @param array $itemList
     */
    public function setItemList($itemList)
    {
        if (!empty($itemList)) {
            $this->itemList = $itemList;
        }
    }

    /**
     * @return array
     */
    public function getItemList()
    {
        return $this->itemList;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        if (!empty($slug)) {
            $this->slug = $slug;

            $this->_localRequest && $this->_localRequest->setSlug($this->slug);
        }
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        if ($language) {
            $this->language = $language;
        }
    }

    /**
     * @return string|null
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param bool $services
     */
    public function setServices($services)
    {
        $this->services = (bool)$services;
    }

    /**
     * @return bool
     */
    public function getServices()
    {
        return $this->services;
    }
}
