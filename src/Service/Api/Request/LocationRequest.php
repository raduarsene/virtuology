<?php

namespace App\Service\Api\Request;

use App\Entity\ShopDomain;
use App\Service\Api\Request\Location\LocalRequest;
use App\Service\Api\Request\Location\V2Request;
use App\Service\Api\Request\Location\V3Request;

class LocationRequest
{
    /** @var LocalRequest */
    public $local;

    /** @var V2Request */
    public $v2;

    /** @var V3Request */
    public $v3;

    public function __construct()
    {
        $this->local = new LocalRequest();
        $this->v2 = new V2Request();
        $this->v3 = new V3Request();
    }

    /**
     * @param array           $data
     * @param ShopDomain|null $shopDomain
     */
    public function initialize(array $data, ShopDomain $shopDomain = null)
    {
        $this->initLocal($data, $shopDomain);

        $this->initV2($data, $shopDomain);

        $this->initV3($data, $shopDomain);
    }

    /**
     * @param array           $data
     * @param ShopDomain|null $shopDomain
     */
    public function initLocal(array $data, ShopDomain $shopDomain = null)
    {
        $this->local->initialize($data, $shopDomain);
    }

    /**
     * @param array           $data
     * @param ShopDomain|null $shopDomain
     * @param bool            $initLocal
     */
    public function initV2(array $data, ShopDomain $shopDomain = null, bool $initLocal = false)
    {
        $this->v2->initialize($data, $shopDomain, $initLocal ? $this->local : null);
    }

    /**
     * @param array           $data
     * @param ShopDomain|null $shopDomain
     * @param bool            $initLocal
     */
    public function initV3(array $data, ShopDomain $shopDomain = null, bool $initLocal = false)
    {
        $this->v3->initialize($data, $shopDomain, $initLocal ? $this->local : null);
    }

    /**
     * @param array           $data
     * @param ShopDomain|null $shopDomain
     * @return $this
     */
    public static function createRequest(array $data, ShopDomain $shopDomain = null)
    {
        $request = new static();

        $request->initialize($data, $shopDomain);

        return $request;
    }

    /**
     * @param array           $data
     * @param ShopDomain|null $shopDomain
     * @return $this
     */
    public static function createLocalRequest(array $data, ShopDomain $shopDomain = null)
    {
        $request = new static();

        $request->initLocal($data, $shopDomain);

        return $request;
    }

    /**
     * @param array           $data
     * @param ShopDomain|null $shopDomain
     * @param bool            $initLocal
     * @return $this
     */
    public static function createV2Request(array $data, ShopDomain $shopDomain = null, bool $initLocal = true)
    {
        $request = new static();

        $request->initV2($data, $shopDomain, $initLocal);

        return $request;
    }

    /**
     * @param array           $data
     * @param ShopDomain|null $shopDomain
     * @param bool            $initLocal
     * @return $this
     */
    public static function createV3Request(array $data, ShopDomain $shopDomain = null, bool $initLocal = true)
    {
        $request = new static();

        $request->initV3($data, $shopDomain, $initLocal);

        return $request;
    }
}
