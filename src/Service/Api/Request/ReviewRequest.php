<?php

namespace App\Service\Api\Request;

use App\Entity\ShopDomain;

class ReviewRequest extends AbstractRequest
{
    /**
     * @var string
     */
    private $siteId;

    /**
     * ID/GUID of the location
     *
     * @var string
     */
    private $locationId;

    /**
     * The language of the review
     *
     * @var string|null
     */
    private $language;

    /**
     * ItemRequest constructor.
     *
     * @param array           $data
     * @param ShopDomain|null $shopDomain Used in setters
     */
    public function __construct(array $data = array(), ShopDomain $shopDomain = null)
    {
        $this->initialize($data, $shopDomain);
    }

    /**
     * @param string $siteId
     */
    public function setSiteId(string $siteId)
    {
        if (!empty($siteId)) {
            $this->siteId = $siteId;
        } elseif ($this->_shop) {
            $this->siteId = $this->_shop->getApiSiteId();
        }
    }

    /**
     * @return string
     */
    public function getSiteId(): string
    {
        return $this->siteId;
    }

    /**
     * @param string $locationId
     */
    public function setLocationId(string $locationId)
    {
        $this->locationId = $locationId;
    }

    /**
     * @return string
     */
    public function getLocationId(): string
    {
        return $this->locationId;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        if (!$language && $this->_shopDomain) {
            $language = $this->_shopDomain->getLanguage()->getCode();
        }

        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
}
