<?php

namespace App\Service\Api\Request;

class RoadRequest extends AbstractRequest
{
    const DEFAULT_LANGUAGE = 'F';
    const LANGUAGE_ADAPTER = ['nl' => 'N', 'fr' => 'F'];

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $country = 'B';

    /**
     * @var string
     */
    private $language;

    /**
     * @var string
     */
    private $service = 'road65_quickzip';

    /**
     * @var string
     */
    private $zipcity;

    private $module = 'interactiv';

    /**
     * RoadRequest constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = array())
    {
        $this->initialize($data);
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        if (isset(self::LANGUAGE_ADAPTER[$this->language])) {
            return self::LANGUAGE_ADAPTER[$this->language];
        }

        return self::DEFAULT_LANGUAGE;
    }

    /**
     * @param string $language
     */
    public function setLanguage(string $language): void
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getService(): string
    {
        return $this->service;
    }

    /**
     * @param string $service
     */
    public function setService(string $service): void
    {
        $this->service = $service;
    }

    /**
     * @return string
     */
    public function getZipcity(): string
    {
        return $this->zipcity;
    }

    /**
     * @param string $zipcity
     */
    public function setZipcity(string $zipcity): void
    {
        $this->zipcity = $zipcity;
    }

    /**
     * @return string
     */
    public function getModule(): string
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule(string $module): void
    {
        $this->module = $module;
    }
}
