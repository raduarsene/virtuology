<?php

namespace App\Service\Api;

use App\Entity\Shop;
use App\Entity\ShopDomain;
use App\Service\Api\Version\ApiLocalService;
use App\Service\Api\Version\ApiV2Service;
use App\Service\Api\Version\ApiV3Service;
use App\Service\Api\Version\ApiVersionInterface;

class MainService implements ApiInterface
{
    /** @var string */
    protected $apiVersion;

    /** @var ApiVersionInterface[] Array with API services having key API version */
    protected $apiServices;

    /**
     * MainService constructor.
     *
     * @param ApiV2Service    $apiV2
     * @param ApiV3Service    $apiV3
     * @param ApiLocalService $apiLocal
     */
    public function __construct(ApiV2Service $apiV2, ApiV3Service $apiV3, ApiLocalService $apiLocal)
    {
        // Set Default API version v2
        $this->apiVersion = Shop::API_VERSION_2;

        // Set implemented API services
        $this->apiServices = [
            $apiV2->getVersion()    => $apiV2,
            $apiV3->getVersion()    => $apiV3,
            $apiLocal->getVersion() => $apiLocal,
        ];
    }

    /**
     * @inheritDoc
     */
    public function init(string $version, ShopDomain $shopDomain = null)
    {
        $this->apiVersion = $version;
        $this->getApiService()->setShopDomain($shopDomain);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getLocations(array $parameters = array(), bool $getLocalOnError = true)
    {
        return $this->getApiService()->searchLocationsByCriteria($parameters, $getLocalOnError);
    }

    /**
     * @inheritDoc
     */
    public function getLocationsLetterGrouped(array $parameters = array())
    {
        return $this->getApiService()->searchLocationsLetterGrouped($parameters);
    }

    /**
     * @inheritDoc
     */
    public function getNearLocationsBySlug(array $parameters = array(), bool $getLocalOnError = true)
    {
        return $this->getApiService()->searchNearLocationsBySlug($parameters, $getLocalOnError);
    }

    /**
     * @inheritDoc
     */
    public function getLocationAssets(array $parameters = array())
    {
        return $this->getApiService()->getLocationAssets($parameters);
    }

    /**
     * @inheritDoc
     */
    public function getLocationBySlug(array $parameters = array(), bool $getLocalOnError = true)
    {
        return $this->getApiService()->searchLocationBySlug($parameters, $getLocalOnError);
    }

    /**
     * @inheritDoc
     */
    public function getLocationItems(array $parameters = array())
    {
        return $this->getApiService()->searchItemsByCriteria($parameters);
    }

    /**
     * @inheritDoc
     */
    public function getFeedData(array $parameters = array(), bool $getLocalOnError = true)
    {
        return $this->getApiService()->searchFeedData($parameters, $getLocalOnError);
    }

    /**
     * @return ApiVersionInterface
     */
    protected function getApiService()
    {
        return $this->apiServices[$this->apiVersion];
    }
    
    /**
     * @inheritDoc
     */
    public function getLocationReviews(array $parameters = array())
    {
        return $this->getApiService()->searchReviewsByCriteria($parameters);
    }
    
    /**
     * @inheritDoc
     */
    public function getFiltersByCriteria(array $parameters = array())
    {
        return $this->getApiService()->getFiltersByCriteria($parameters);
    }
}
