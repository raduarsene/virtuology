<?php

namespace App\Service\Api;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Extend this service if you need external API data
 *
 * @package App\Service\Api
 */
class RequestService
{
    /** @var Client */
    private $client;
    
    /** @var int */
    private $apiRequestTimeout;
    
    /**
     * RequestService constructor.
     *
     * @param int $apiRequestTimeout
     */
    public function __construct(int $apiRequestTimeout)
    {
        $this->client = new Client();
        
        $this->apiRequestTimeout = $apiRequestTimeout;
    }

    /**
     * @param string $url
     * @param array  $query
     * @param array  $headers
     * @return mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function getRequest(string $url, array $query = array(), array $headers = array())
    {
        return $this->client->request(Request::METHOD_GET, $url, [
            RequestOptions::QUERY   => $query,
            RequestOptions::HEADERS => $headers,
            RequestOptions::CONNECT_TIMEOUT => 2,
            RequestOptions::TIMEOUT => $this->apiRequestTimeout - 2,
//            'debug' => true,

        ]);
    }

    /**
     * @param string $url
     * @param array  $body
     * @param array  $headers
     * @return mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function postRequest(string $url, array $body = array(), array $headers = array())
    {
        return $this->client->request(Request::METHOD_POST, $url, [
            RequestOptions::JSON    => $body,
            RequestOptions::HEADERS => $headers,
            RequestOptions::CONNECT_TIMEOUT => 2,
            RequestOptions::TIMEOUT => $this->apiRequestTimeout - 2

        ]);
    }
    
    public function postRequestForm(string $url, array $body = array(), array $headers = array())
    {
        return $this->client->request(Request::METHOD_POST, $url, [
            RequestOptions::FORM_PARAMS => $body,
            RequestOptions::HEADERS => $headers,
            RequestOptions::CONNECT_TIMEOUT => 2,
            RequestOptions::TIMEOUT => $this->apiRequestTimeout - 2,
//            'debug' => true,
    
        ]);
    }
}
