<?php

namespace App\Service\Api;

use App\Service\Api\Request\RoadRequest;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Response;

class RoadService extends RequestService
{
    const DEFAULT_COUNTRY_NAME = "Belgium";

    /** @var string */
    private $roadApiUser;

    /** @var string */
    private $roadApiPassword;

    /** @var string */
    private $roadApiUrl;
    
    /**
     * RoadService constructor.
     *
     * @param string $roadApiUser
     * @param string $roadApiPassword
     * @param string $roadApiUrl
     * @param int    $apiRequestTimeout
     */
    public function __construct(string $roadApiUser, string $roadApiPassword, string $roadApiUrl, int $apiRequestTimeout)
    {
        $this->roadApiPassword = $roadApiPassword;
        $this->roadApiUser = $roadApiUser;
        $this->roadApiUrl = $roadApiUrl;

        parent::__construct($apiRequestTimeout);
    }

    /**
     * @param array $parameters
     * @return array
     */
    public function searchLocationsByCriteria(array $parameters = array())
    {
        $request = new RoadRequest($parameters);

        //do not expose this in the url
        $request->setLogin($this->roadApiUser);
        $request->setPassword($this->roadApiPassword);

        $requestArray = $request->toArray();
        unset($requestArray['language']);

        try {
            $response = $this->getRequest($this->roadApiUrl, $requestArray, ['Content-Type' => 'application/json']);

            $statusCode = $response->getStatusCode();

            $locations = $this->processLocationsResponse(
                json_decode($response->getBody()->getContents(), true),
                $request->toArray()
            );
        } catch (GuzzleException $e) {
            $response = $e->getResponse();

            $locations = [];
            $statusCode = $response ? $response->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return [$locations, $statusCode];
    }

    /**
     * @param $data
     * @param $requestArray
     * @return array
     */
    private function processLocationsResponse($data, $requestArray)
    {
        $resultArray = [];
    
        $isZip = is_numeric(substr($requestArray["zipcity"], 0, 1)) ? true : false;
        
        if (isset($data["road65_quickzip"]["row"])) {
            if (isset($data["road65_quickzip"]["row"]["@id"])) {
                $details = $data["road65_quickzip"]["row"];

                $data["road65_quickzip"]["row"] = [];
                $data["road65_quickzip"]["row"][] = $details;
            }

            foreach ($data["road65_quickzip"]["row"] as $details) {
                
                if (isset($resultArray[$details["@city_id"]][$details["@zipcode"]]) && isset($details["@language"]) && $requestArray["language"] !== $details["@language"]) {
                    continue;
                }
                
                $cityName = ucwords(strtolower($details["@cityname"]));
                if(strpos($details["@cityname"], "-")!==false) {
                    $comp = explode("-", $details["@cityname"]);
                    
                    foreach ($comp as $key=>$value) {
                        $comp[$key] = ucwords(strtolower($value));
                    }
                    $cityName = implode("-", $comp);
                }

                $resultArray[$details["@city_id"]][$details["@zipcode"]] = [
                    
                    "label" => $details["@zipcode"].", ".$cityName,
                    "value" => $details["@zipcode"].", ".$details["@cityname"].", ".self::DEFAULT_COUNTRY_NAME,
                    "city" => $details["@cityname"],
                    "zipcode" => $details["@zipcode"],
                ];
            }
        }
        
        $autocomplete = [];
        foreach ($resultArray as $cityId => $zipCodeValues) {
            $autocomplete = array_merge_recursive($autocomplete, array_values($zipCodeValues));
        }
        
        return $autocomplete;
        
        
        
//        $resultSort = [];
//        foreach ($autocomplete as $details) {
//            $resultSort['suggestions'][] = $details;
//            $resultSort['city'][] = $details['city'];
//            $resultSort['zipcode'][] = $details['zipcode'];
//        }
//
//        if($isZip) {
//
//            array_multisort($resultSort['zipcode'], SORT_ASC, SORT_NUMERIC, $resultSort['suggestions'], $resultSort['city']);
//        }
//        else {
//
//            array_multisort($resultSort['city'], SORT_ASC, SORT_STRING, $resultSort['suggestions'], $resultSort['zipcode']);
//        }
//
//
//        return $resultSort['suggestions'];
    
    }
    
}
