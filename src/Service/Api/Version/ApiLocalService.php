<?php

namespace App\Service\Api\Version;

use App\Entity\Shop;
use App\Entity\ShopFeedData;
use App\Entity\ShopLocation;
use App\Helper\Formatting;
use App\Service\Api\Entity\LocationEntity;
use App\Service\Api\Entity\SmartphoneEntity;
use App\Service\Api\Request\FeedDataRequest;
use App\Service\Api\Request\LocationRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class ApiLocalService extends AbstractApiVersion
{
    /**
     * ApiLocalService constructor.
     *
     * @param EntityManagerInterface $em
     * @param int                    $apiRequestTimeout
     */
    public function __construct(EntityManagerInterface $em, int $apiRequestTimeout)
    {
        parent::__construct($em, null, null, $apiRequestTimeout);
    }

    /**
     * @inheritDoc
     */
    public function getVersion()
    {
        return Shop::API_VERSION_LOCAL;
    }

    /**
     * @inheritDoc
     */
    public function searchLocationsByCriteria(array $parameters = array(), bool $getLocalOnError = true)
    {
        $locations = [];

        $request = LocationRequest::createLocalRequest($parameters, $this->shopDomain);

        $shop = $this->em->getRepository(Shop::class)->findOneBy(['apiSiteId' => $request->local->getSiteId()]);

        if ($shop) {
            $shopLocations = $this->em->getRepository(ShopLocation::class)->searchLocationsByCriteria($shop, $request->local->toArray());

            foreach ($shopLocations as $shopLocation) {
                $location = new LocationEntity(
                    $shopLocation->getDetailsArray(),
                    array_merge($parameters, ['datetimeFormat' => $this->shopDomain ? $this->shopDomain->getDatetimeFormat() : null])
                );

                $this->setLocationTimezone($location);

                $locations[] = $location->toArray();
            }

            $statusCode = Response::HTTP_OK;
        } else {
            $statusCode = Response::HTTP_NOT_FOUND;
        }

        return [$locations, $statusCode];
    }

    /**
     * @inheritDoc
     */
    public function searchLocationsLetterGrouped(array $parameters = array())
    {
        $locations = [];

        if ($this->shop) {
            $shopLocations = $this->em->getRepository(ShopLocation::class)->searchLocationsByShop($this->shop, $parameters);
            /** @var ShopLocation $shopLocation */
            foreach ($shopLocations as $shopLocation) {
                if ($shopLocation->getName()) {
                    $locationEntity = new LocationEntity(
                        $shopLocation->getDetailsArray(),
                        ['datetimeFormat' => $this->shopDomain ? $this->shopDomain->getDatetimeFormat() : null]
                    );

                    $this->setLocationTimezone($locationEntity);

                    if ($letter = mb_substr($shopLocation->getCity(), 0, 1)) {
                        $letter = mb_strtoupper(Formatting::removeAccents($letter));

                        $locations[$letter][] = $locationEntity->toArray();
                    }
                }
            }

            $statusCode = Response::HTTP_OK;
        } else {
            $statusCode = Response::HTTP_NOT_FOUND;
        }

        return [$locations, $statusCode];
    }

    /**
     * @inheritDoc
     */
    public function searchNearLocationsBySlug(array $parameters = array(), bool $getLocalOnError = true)
    {
        $locations = [];

        $request = LocationRequest::createLocalRequest($parameters, $this->shopDomain);

        $shop = $this->em->getRepository(Shop::class)->findOneBy(['apiSiteId' => $request->local->getSiteId()]);
        
        if ($shop) {
            $data = $this->em->getRepository(ShopLocation::class)->searchNearLocations($shop, $request->local->toArray());

            foreach ($data as $location) {
                $locationEntity = new LocationEntity(
                    $location->getDetailsArray(),
                    array_merge(
                        $request->local->toArray(),
                        ['datetimeFormat' => $this->shopDomain ? $this->shopDomain->getDatetimeFormat() : null]
                    )
                );

                $this->setLocationTimezone($locationEntity);

                $locations[] = $locationEntity->toArray();
            }

            $statusCode = Response::HTTP_OK;
        } else {
            $statusCode = Response::HTTP_NOT_FOUND;
        }

        return [$locations, $statusCode];
    }

    /**
     * @inheritDoc
     */
    public function searchLocationBySlug(array $parameters = array(), bool $getLocalOnError = true)
    {
        $location = [];

        $request = LocationRequest::createLocalRequest($parameters, $this->shopDomain);

        $shop = $this->em->getRepository(Shop::class)->findOneBy(['apiSiteId' => $request->local->getSiteId()]);

        if ($shop) {
            $shopLocation = $this->em->getRepository(ShopLocation::class)->getLocationBySlug($shop, $request->local->getSlug());

            if ($shopLocation) {
                $locationEntity = new LocationEntity(
                    $shopLocation->getDetailsArray(),
                    array_merge(
                        $parameters,
                        ['datetimeFormat' => $this->shopDomain ? $this->shopDomain->getDatetimeFormat() : null]
                    )
                );

                $this->setLocationTimezone($locationEntity);

                $location = $locationEntity->toArray();
            }

            $statusCode = Response::HTTP_OK;
        } else {
            $statusCode = Response::HTTP_NOT_FOUND;
        }

        return [$location, $statusCode];
    }

    /**
     * @inheritDoc
     */
    public function searchFeedData(array $parameters = array(), bool $getLocalOnError = true)
    {
        $feedData = [];

        $request = new FeedDataRequest($parameters, $this->shopDomain);

        if ($request->getShopId()) {
            $shopFeedData = $this->em->getRepository(ShopFeedData::class)->getFeedByParameters($request->toArray());

            if ($shopFeedData) {
                $feedData = $this->getFeedDataByType($shopFeedData, $request->getType());
            }

            $statusCode = Response::HTTP_OK;
        } else {
            $statusCode = Response::HTTP_NOT_FOUND;
        }

        return [$feedData, $statusCode];
    }

    /**
     * @param ShopFeedData[] $shopFeed
     * @param string         $type
     * @return array
     */
    private function getFeedDataByType($shopFeed, $type)
    {
        $feedData = [];

        switch ($type) {
            case ShopFeedData::TYPE_SMARTPHONE:
            default:
                foreach ($shopFeed as $feed) {
                    $smartphoneEntity = new SmartphoneEntity($feed->getData());

                    $feedData[] = $smartphoneEntity->toArray();
                }
        }

        return $feedData;
    }
}
