<?php

namespace App\Service\Api\Version;

use App\Entity\ApiError;
use App\Entity\Shop;
use App\Entity\ShopDomain;
use App\Entity\ShopFeedData;
use App\Entity\Zone;
use App\Service\Api\Entity\LocationEntity;
use App\Service\Api\Entity\SmartphoneEntity;
use App\Service\Api\Request\FeedDataRequest;
use App\Service\Api\RequestService;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractApiVersion extends RequestService implements ApiVersionInterface
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @var Shop|null */
    protected $shop;

    /** @var ShopDomain|null */
    protected $shopDomain;

    /** @var ApiLocalService|null */
    private $apiLocal;

    /** @var string|null */
    private $apiSmartphoneUrl;

    /** @var array */
    private $countryTimezone = array();
    
    /** @var int */
    private $apiRequestTimeout;
    
    /**
     * AbstractApiVersion constructor.
     *
     * @param EntityManagerInterface $em
     * @param ApiLocalService|null   $apiLocal
     * @param string|null            $apiSmartphoneUrl
     * @param                        $apiRequestTimeout
     */
    public function __construct(EntityManagerInterface $em, ApiLocalService $apiLocal = null, string $apiSmartphoneUrl = null, int $apiRequestTimeout)
    {
        $this->em = $em;
        $this->apiLocal = $apiLocal;
        $this->apiSmartphoneUrl = $apiSmartphoneUrl;

        parent::__construct($apiRequestTimeout);
    }

    /**
     * @inheritDoc
     */
    public function setShopDomain(ShopDomain $shopDomain = null)
    {
        $this->shopDomain = $shopDomain;
        $this->shop = $shopDomain ? $shopDomain->getShop() : null;
    }

    /**
     * @inheritDoc
     */
    public function searchLocationsByCriteria(array $parameters = array(), bool $getLocalOnError = true)
    {
        return [[], Response::HTTP_OK];
    }

    /**
     * @inheritDoc
     */
    public function searchLocationsLetterGrouped(array $parameters = array())
    {
        return [[], Response::HTTP_OK];
    }

    /**
     * @inheritDoc
     */
    public function searchNearLocationsBySlug(array $parameters = array(), bool $getLocalOnError = true)
    {
        return [[], Response::HTTP_OK];
    }

    /**
     * @inheritDoc
     */
    public function searchLocationBySlug(array $parameters = array(), bool $getLocalOnError = true)
    {
        return [[], Response::HTTP_OK];
    }

    /**
     * @inheritDoc
     */
    public function getLocationAssets(array $parameters = array())
    {
        return [[], Response::HTTP_OK];
    }

    /**
     * @inheritDoc
     */
    public function searchItemsByCriteria(array $parameters = array())
    {
        return [[], Response::HTTP_OK];
    }

    /**
     * @inheritDoc
     */
    public function searchReviewsByCriteria(array $parameters = array())
    {
        return [[], Response::HTTP_OK];
    }
    
    /**
     * @inheritDoc
     */
    public function getFiltersByCriteria(array $parameters = array())
    {
        return [[], Response::HTTP_OK];
    }

    /**
     * @inheritDoc
     */
    public function searchFeedData(array $parameters = array(), bool $getLocalOnError = true)
    {
        $request = new FeedDataRequest($parameters, $this->shopDomain);

        try {
            list($feedData, $statusCode) = $this->getFeedDataByType($request);
        } catch (GuzzleException $e) {
            // Save API request error in database
            $this->createApiError($this->apiSmartphoneUrl, $request->toArray(), $e);

            $response = $e->getResponse();
            // Get locations from local database
            if ($getLocalOnError) {
                list($feedData, $statusCode) = $this->apiLocal->searchFeedData($parameters);

                if (empty($feedData)) {
                    $statusCode = $response ? $response->getStatusCode() : Response::HTTP_NOT_FOUND;
                }
            } else {
                $feedData = [];
                $statusCode = $response ? $response->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
            }
        }

        return [$feedData, $statusCode];
    }

    /**
     * @param LocationEntity $location
     */
    protected function setLocationTimezone($location)
    {
        $address = $location->getAddress();

        if ($address && empty($address['timezone'])) {
            if (!empty($address['country'])) {
                if (empty($this->countryTimezone[$address['country']])) {
                    $zone = $this->em->getRepository(Zone::class)->findOneBy([
                        'countryCode' => $address['country'],
                        'main'        => true,
                    ]);

                    if ($zone) {
                        $this->countryTimezone[$address['country']] = $zone->getZoneName();
                    }
                }

                if (!empty($this->countryTimezone[$address['country']])) {
                    $address['timezone'] = $this->countryTimezone[$address['country']];
                }
            }

            if (empty($address['timezone'])) {
                $address['timezone'] = 'UTC';
            }

            $location->setAddress($address);
        }
    }

    /**
     * @param string          $url
     * @param array           $parameters
     * @param GuzzleException $e
     */
    protected function createApiError(string $url, array $parameters, GuzzleException $e)
    {
        $response = $e->getResponse();

        if ($this->shop) {
            $apiError = new ApiError();
            $apiError->setShop($this->shop)
                ->setApiUrl($url)
                ->setApiQueryString($parameters)
                ->setErrorCode($response ? $response->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR)
                ->setErrorMessage($e->getMessage());

            $this->em->persist($apiError);
            $this->em->flush();
        }
    }

    /**
     * @param FeedDataRequest $request
     * @return array
     * @throws GuzzleException
     */
    private function getFeedDataByType(FeedDataRequest $request)
    {
        switch ($request->getType()) {
            case ShopFeedData::TYPE_SMARTPHONE:
            default:
                $response = $this->getRequest($this->apiSmartphoneUrl);

                $statusCode = $response->getStatusCode();
                $feedData = $this->processSmartphonesResponse(
                    json_decode($response->getBody()->getContents(), true),
                    $request
                );
        }

        return [$feedData, $statusCode];
    }

    /**
     * @param array           $responseData
     * @param FeedDataRequest $request
     * @return array
     */
    private function processSmartphonesResponse($responseData, FeedDataRequest $request)
    {
        $smartphones = [];

        if ($responseData && !empty($responseData)) {
            foreach ($responseData as $data) {
                $smartphone = new SmartphoneEntity($data);

                if ($request->getIsVisible() !== null && $request->getIsVisible() !== $smartphone->getVisibilityWeb()) {
                    continue;
                }

                if ($request->getInStock() !== null && $request->getInStock() !== $smartphone->getInStock()) {
                    continue;
                }

                $smartphones[] = $smartphone->toArray();
            }

            if ($request->getSize()) {
                $smartphones = array_slice($smartphones, $request->getPage(), $request->getSize());
            }
        }

        return $smartphones;
    }
}
