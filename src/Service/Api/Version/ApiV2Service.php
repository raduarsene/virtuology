<?php

namespace App\Service\Api\Version;

use App\Entity\Shop;
use App\Service\Api\Entity\ItemEntity;
use App\Service\Api\Entity\Location\AssetEntity;
use App\Service\Api\Entity\LocationEntity;
use App\Service\Api\Request\AssetRequest;
use App\Service\Api\Request\ItemRequest;
use App\Service\Api\Request\Location\V2Request;
use App\Service\Api\Request\LocationRequest;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Response;

class ApiV2Service extends AbstractApiVersion
{
    const MOBILOSOFT_LOCATIONS_URI = '/data/locations/search/findByCriteria';
    const MOBILOSOFT_LOCATION_ASSETS_URI = '/data/locations/{location-id}/assets';
    const MOBILOSOFT_ITEMS_URI = '/data/items/search/findByCriteria';

    /** @var ApiLocalService */
    private $apiLocal;

    /** @var string */
    private $apiMobilosoftUrl;

    /** @var string */
    private $apiMobilosoftKey;

    /**
     * ApiV2Service constructor.
     *
     * @param EntityManagerInterface $em
     * @param ApiLocalService        $apiLocal
     * @param string                 $apiMobilosoftUrl
     * @param string                 $apiMobilosoftKey
     * @param string                 $apiSmartphoneUrl
     * @param int                    $apiRequestTimeout
     */
    public function __construct(
        EntityManagerInterface $em,
        ApiLocalService $apiLocal,
        string $apiMobilosoftUrl,
        string $apiMobilosoftKey,
        string $apiSmartphoneUrl,
        int $apiRequestTimeout
    ) {
        $this->apiLocal = $apiLocal;

        $this->apiMobilosoftUrl = $apiMobilosoftUrl;
        $this->apiMobilosoftKey = $apiMobilosoftKey;
        

        parent::__construct($em, $apiLocal, $apiSmartphoneUrl, $apiRequestTimeout);
    }

    /**
     * @inheritDoc
     */
    public function getVersion()
    {
        return Shop::API_VERSION_2;
    }

    /**
     * @inheritDoc
     */
    public function searchLocationsByCriteria(array $parameters = array(), bool $getLocalOnError = true)
    {
        $request = LocationRequest::createV2Request($parameters, $this->shopDomain);

        $url = $this->apiMobilosoftUrl.self::MOBILOSOFT_LOCATIONS_URI;

        try {
            list($locations, $statusCode) = $this->getLocationsByCriteria($request, $url);
        } catch (GuzzleException $e) {
            // Save API request error in database
            $this->createApiError($url, $request->v2->toArray(), $e);

            $response = $e->getResponse();
            // Get locations from local database
            if ($getLocalOnError) {
                list($locations, $statusCode) = $this->apiLocal->searchLocationsByCriteria($request->local->toArray());

                if (empty($locations)) {
                    $statusCode = $response ? $response->getStatusCode() : Response::HTTP_NOT_FOUND;
                }
            } else {
                $locations = [];
                $statusCode = $response ? $response->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
            }
        }
        
        if(count($locations)) {
            
            $locationsOrdered = [];
            foreach ($locations as $location) {
                $locationsOrdered["locations"][] = $location;
                $locationsOrdered["distance"][] = $location["address"]["distance"];
            }
    
            array_multisort($locationsOrdered['distance'], SORT_ASC, SORT_NUMERIC, $locationsOrdered["locations"]);
            $locations = $locationsOrdered["locations"];
        }

        return [$locations, $statusCode];
    }

    /**
     * @inheritDoc
     */
    public function searchNearLocationsBySlug(array $parameters = array(), bool $getLocalOnError = true)
    {
        $request = LocationRequest::createV2Request($parameters, $this->shopDomain);

        return $this->apiLocal->searchNearLocationsBySlug($request->local->toArray());
    }

    /**
     * @inheritDoc
     */
    public function searchLocationBySlug(array $parameters = array(), bool $getLocalOnError = true)
    {
        $request = LocationRequest::createV2Request($parameters, $this->shopDomain);

        $url = $this->apiMobilosoftUrl.self::MOBILOSOFT_LOCATIONS_URI;

        try {
            $response = $this->getRequest($url, $request->v2->toArray(), $this->getRequestHeaders());

            $statusCode = $response->getStatusCode();
            $location = $this->processLocationResponse(json_decode($response->getBody()->getContents(), true), $request);
        } catch (GuzzleException $e) {
            // Save API request error in database
            $this->createApiError($url, $request->v2->toArray(), $e);

            $response = $e->getResponse();
            // Get locations from local database
            if ($getLocalOnError) {
                list($location, $statusCode) = $this->apiLocal->searchLocationBySlug($request->local->toArray());

                if (empty($location)) {
                    $statusCode = $response ? $response->getStatusCode() : Response::HTTP_NOT_FOUND;
                }
            } else {
                $location = [];
                $statusCode = $response ? $response->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
            }
        }

        return [$location, $statusCode];
    }

    /**
     * @inheritDoc
     */
    public function getLocationAssets(array $parameters = array())
    {
        $request = new AssetRequest($parameters, $this->shopDomain);

        $url = $this->apiMobilosoftUrl.str_replace('{location-id}', $request->getLocationId(), self::MOBILOSOFT_LOCATION_ASSETS_URI);

        try {
            $response = $this->getRequest($url, $request->toArray(), $this->getRequestHeaders());

            $statusCode = $response->getStatusCode();
            $locationAssets = $this->processAssetsResponse(json_decode($response->getBody()->getContents(), true));
        } catch (GuzzleException $e) {
            // Save API request error in database
            $this->createApiError($url, $request->toArray(), $e);

            $statusCode = $e->getResponse()->getStatusCode();
            $locationAssets = [];
        }

        return [$locationAssets, $statusCode];
    }

    /**
     * @inheritDoc
     */
    public function searchItemsByCriteria(array $parameters = array())
    {
        $request = new ItemRequest($parameters, $this->shopDomain);

        $url = $this->apiMobilosoftUrl.self::MOBILOSOFT_ITEMS_URI;

        try {
            $response = $this->getRequest($url, $request->toArray(), $this->getRequestHeaders());

            $statusCode = $response->getStatusCode();
            $items = $this->processItemsResponse(json_decode($response->getBody()->getContents(), true));
        } catch (GuzzleException $e) {
            // Save API request error in database
            $this->createApiError($url, $request->toArray(), $e);

            $items = [];
            $statusCode = $e->getResponse()->getStatusCode();
        }

        return [$items, $statusCode];
    }

    /**
     * @param LocationRequest $request
     * @param string          $url
     * @return array
     * @throws GuzzleException
     */
    private function getLocationsByCriteria(LocationRequest $request, string $url)
    {
        if ($request->v2->getSize()) {
            list($locations, $statusCode) = $this->getLocationsResponse($request, $url);
        } else {
            $locations = [];

            $page = 0;
            $request->v2->setSize(V2Request::DEFAULT_SIZE_PER_PAGE);

            do {
                $request->v2->setPage($page++);

                list($locationsPage, $statusCode) = $this->getLocationsResponse($request, $url);

                $locations = array_merge($locations, $locationsPage);
            } while (Response::HTTP_OK === $statusCode && count($locationsPage) === V2Request::DEFAULT_SIZE_PER_PAGE);
        }

        return [$locations, $statusCode];
    }

    /**
     * @param LocationRequest $request
     * @param string          $url
     * @return array
     * @throws GuzzleException
     */
    private function getLocationsResponse(LocationRequest $request, string $url)
    {
        $response = $this->getRequest($url, $request->v2->toArray(), $this->getRequestHeaders());

        $statusCode = $response->getStatusCode();
        $locations = $this->processLocationsResponse(json_decode($response->getBody()->getContents(), true), $request->v2->toArray());

        return [$locations, $statusCode];
    }

    /**
     * @param array $responseData
     * @param array $requestData
     * @return array
     */
    private function processLocationsResponse($responseData, $requestData)
    {
        $processedData = [];

        if ($responseData && !empty($responseData['_embedded']['locations'])) {
            foreach ($responseData['_embedded']['locations'] as $data) {
                if (!empty($data['address']['latitude']) && !empty($data['address']['longitude'])) {
                    $location = new LocationEntity(
                        $data,
                        array_merge(
                            $requestData,
                            ['datetimeFormat' => $this->shopDomain ? $this->shopDomain->getDatetimeFormat() : null]
                        )
                    );
                    $this->setLocationTimezone($location);

                    $processedData[] = $location->toArray();
                }
            }
        }

        return $processedData;
    }

    /**
     * @param array           $responseData
     * @param LocationRequest $request
     * @return array
     */
    private function processLocationResponse($responseData, $request)
    {
        $processedData = [];

        if ($responseData && !empty($responseData['_embedded']['locations'])) {
            $location = new LocationEntity(
                $responseData['_embedded']['locations'][0],
                array_merge(
                    $request->v2->toArray(),
                    ['datetimeFormat' => $this->shopDomain ? $this->shopDomain->getDatetimeFormat() : null]
                )
            );

            $this->setLocationTimezone($location);
            $this->setLocationServices($location, $request);

            $processedData = $location->toArray();
        }

        return $processedData;
    }

    /**
     * @param array $responseData
     * @return array
     */
    private function processAssetsResponse($responseData)
    {
        $processedData = [];

        if ($responseData && is_array($responseData)) {
            foreach ($responseData as $data) {
                
                if( isset($data['externalId']) && strpos($data['externalId'], 'location_logo') !== false ) {
                    continue;
                }

                if( isset($data['externalId']) && strpos($data['externalId'], 'location_profile') !== false ) {
                    continue;
                }
                
    
                $item = new AssetEntity($data);
                $processedData[] = $item->toArray();
                
            }
        }

        return $processedData;
    }

    /**
     * @param array $responseData
     * @return array
     */
    private function processItemsResponse($responseData)
    {
        $processedData = [];

        if ($responseData && !empty($responseData['_embedded']['items'])) {
            foreach ($responseData['_embedded']['items'] as $data) {
                $item = new ItemEntity($data);

                if (!isset($processedData[$item->getItemTypeExternalId()][$item->getOrderIndex()])) {
                    $processedData[$item->getItemTypeExternalId()][$item->getOrderIndex()] = $item->toArray();
                } else {
                    $processedData[$item->getItemTypeExternalId()][] = $item->toArray();
                }
            }
        }

        return $processedData;
    }

    /**
     * @param LocationEntity  $location
     * @param LocationRequest $request
     */
    private function setLocationServices($location, $request)
    {
        // Get Location services/items
        if ($request->v2->getServices()) {
            $services = [];

            if ($location->getGuid()) {
                $locationServices =  $this->searchItemsByCriteria([
                    'siteId'     => $request->v2->getSiteId(),
                    'locationId' => $location->getGuid(),
                    'language'   => $request->v2->getLanguage(),
                ]);

                $services = current($locationServices);
            }

            $location->setServices($services);
        }
    }
    
    /**
     * @return array
     */
    private function getRequestHeaders()
    {
        $headers = ['Content-Type' => 'application/json', 'X-Api-Key' => $this->apiMobilosoftKey];
        
        return $headers;
    }
}
