<?php

namespace App\Service\Api\Version;

use App\Entity\Shop;
use App\Service\Api\Entity\Item\AssetEntity;
use App\Service\Api\Entity\ItemEntity;
use App\Service\Api\Entity\LocationEntity;
use App\Service\Api\Entity\ReviewEntity;
use App\Service\Api\Request\Location\V3Request;
use App\Service\Api\Request\LocationRequest;
use App\Service\Api\Request\ReviewRequest;
use App\Service\Api\Request\ItemRequest;
use App\Service\Api\Request\FilterRequest;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Response;

class ApiV3Service extends AbstractApiVersion
{
    const MOBILOSOFT_LOCATIONS_URI = '/api/locations/{siteId}/search/paginated';
    const MOBILOSOFT_ALL_LOCATIONS_URI = '/api/locations/{siteId}/list';
    const MOBILOSOFT_LOCATION_DETAILS = '/api/locations/{siteId}/{locale}/{slug}';
    const MOBILOSOFT_LOCATION_DETAILS_EXTERNAL_ID = '/api/locations/{siteId}/{externalId}';
    const MOBILOSOFT_LOCATION_REVIEWS = '/api/locations/{siteId}/{locationId}/reviews';
    const MOBILOSOFT_ITEMS_URI = '/api/{siteId}/locations/{locationId}/campaigns';
    const MOBILOSOFT_FILTERS = '/api/sites/{siteId}/filters/{locale}';

    /** @var ApiLocalService */
    private $apiLocal;

    /** @var string */
    private $apiV3MobilosoftUrl;

    public function __construct(
        EntityManagerInterface $em,
        ApiLocalService $apiLocal,
        string $apiV3MobilosoftUrl,
        string $apiSmartphoneUrl,
        int $apiRequestTimeout
    ) {
        $this->apiLocal = $apiLocal;
        $this->apiV3MobilosoftUrl = $apiV3MobilosoftUrl;

        parent::__construct($em, $apiLocal, $apiSmartphoneUrl, $apiRequestTimeout);
    }

    /**
     * @inheritDoc
     */
    public function getVersion()
    {
        return Shop::API_VERSION_3;
    }

    /**
     * @inheritDoc
     */
    public function searchLocationsByCriteria(array $parameters = array(), bool $getLocalOnError = true)
    {
        $request = LocationRequest::createV3Request($parameters, $this->shopDomain);

        if (!$this->isAllLocationsRequest($request)) {
            // Use this url when there is 'near' or 'within' in request
            $url = $this->getApiUrl().str_replace('{siteId}', $request->v3->getSiteId(), self::MOBILOSOFT_LOCATIONS_URI);
        } else {
            $url = $this->getApiUrl().str_replace('{siteId}', $request->v3->getSiteId(), self::MOBILOSOFT_ALL_LOCATIONS_URI);
        }

        try {
            list($locations, $statusCode) = $this->getLocationsByCriteria($request, $url, $this->getRequestHeaders());

            // If no locations found with default radius, search again with new radius parameter
            if (empty($locations) && $request->v3->getRadius() && $request->v3->getOnEmptyRadius()) {
                $request->v3->setRadius($request->v3->getOnEmptyRadius());
                $request->v3->setSize($request->v3->getOnEmptyRadiusLimit());

                list($locations, $statusCode) = $this->getLocationsByCriteria($request, $url, $this->getRequestHeaders());
            }
        } catch (GuzzleException $e) {
            
            list($locations, $statusCode) = $this->getLocalLocationsByCriteria($e, $request, $url, $getLocalOnError);
        }

        return [$locations, $statusCode];
    }

    /**
     * @inheritDoc
     */
    public function searchLocationBySlug(array $parameters = array(), bool $getLocalOnError = true)
    {
        $request = LocationRequest::createV3Request($parameters, $this->shopDomain);

        if ($this->shop->getCode() == Shop::CODE_CARREFOUR) {
            $url = $this->getApiUrl().str_replace(
                    ['{siteId}', '{externalId}', '{locale}'],
                    [$this->shop->getApiSiteId(), $request->v3->getExternalId(), $request->v3->getLanguageAdapter()],
                    self::MOBILOSOFT_LOCATION_DETAILS_EXTERNAL_ID
                );
        } else {
            $url = $this->getApiUrl().str_replace(
                    ['{siteId}', '{slug}', '{locale}'],
                    [$this->shop->getApiSiteId(), $request->v3->getSlug(), $request->v3->getLanguageAdapter()],
                    self::MOBILOSOFT_LOCATION_DETAILS
                );
        }
        
        
        try {
            $response = $this->getRequest($url, [], $this->getRequestHeaders());

            $statusCode = $response->getStatusCode();
            $location = $this->processLocationResponse(json_decode($response->getBody()->getContents(), true), $request);
            
        } catch (GuzzleException $e) {
            // Save API request error in database
            $this->createApiError($url, $request->v3->toArray(), $e);

            $response = $e->getResponse();
            // Get locations from local database
            if ($getLocalOnError) {
                list($location, $statusCode) = $this->apiLocal->searchLocationBySlug($request->local->toArray());

                if (empty($location)) {
                    $statusCode = $response ? $response->getStatusCode() : Response::HTTP_NOT_FOUND;
                }
            } else {
                $location = [];
                $statusCode = $response ? $response->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
            }
        }

        return [$location, $statusCode];
    }

    /**
     * @inheritDoc
     */
    public function searchNearLocationsBySlug(array $parameters = array(), bool $getLocalOnError = true)
    {
        $request = LocationRequest::createV3Request($parameters, $this->shopDomain);
        $url = $this->getApiUrl().str_replace('{siteId}', $request->v3->getSiteId(), self::MOBILOSOFT_LOCATIONS_URI)
            .'?page='.$request->v3->getPage().'&size='.$request->v3->getSize();

        try {
            $response = $this->postRequest($url, $request->v3->toArray(), $this->getRequestHeaders());

            $statusCode = $response->getStatusCode();
            $location = $this->processNearLocationsResponse(json_decode($response->getBody()->getContents(), true), $request);
        } catch (GuzzleException $e) {
            // Save API request error in database
            $this->createApiError($url, $request->v3->toArray(), $e);

            $response = $e->getResponse();
            // Get locations from local database
            if ($getLocalOnError) {
                list($location, $statusCode) = $this->apiLocal->searchNearLocationsBySlug($request->local->toArray());

                if (empty($location)) {
                    $statusCode = $response ? $response->getStatusCode() : Response::HTTP_NOT_FOUND;
                }
            } else {
                $location = [];
                $statusCode = $response ? $response->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
            }
        }

        return [$location, $statusCode];
    }

    /**
     * @inheritDoc
     */
    public function searchItemsByCriteria(array $parameters = array())
    {
        $request = new ItemRequest($parameters, $this->shopDomain);
        $url = $this->getApiUrl().str_replace(['{siteId}', '{locationId}'], [$request->getSiteId(), $request->getLocationId()], self::MOBILOSOFT_ITEMS_URI);

        try {
            $response = $this->getRequest($url, $request->toArray(), $this->getRequestHeaders());

            $statusCode = $response->getStatusCode();
            $items = $this->processItemsResponse(json_decode($response->getBody()->getContents(), true), $request);
            
        } catch (GuzzleException $e) {
            // Save API request error in database
            $this->createApiError($url, $request->toArray(), $e);

            $items = [];
    
            $response = $e->getResponse();
            $statusCode = $response ? $response->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
            
        }

        return [$items, $statusCode];
    }

    /**
     * @inheritDoc
     */
    public function searchReviewsByCriteria(array $parameters = array())
    {
        $request = new ReviewRequest($parameters, $this->shopDomain);
        $url = $this->getApiUrl().str_replace(
            ['{siteId}', '{locationId}'],
            [$request->getSiteId(), $request->getLocationId()],
            self::MOBILOSOFT_LOCATION_REVIEWS
        );

        try {
            $response = $this->getRequest($url, $request->toArray(), $this->getRequestHeaders());

            $statusCode = $response->getStatusCode();
            $reviews = $this->processReviewsResponse(json_decode($response->getBody()->getContents(), true));
        } catch (GuzzleException $e) {
            // Save API request error in database
            $this->createApiError($url, $request->toArray(), $e);

            $reviews = [];
    
            $response = $e->getResponse();
            $statusCode = $response ? $response->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return [$reviews, $statusCode];
    }
    
    
    /**
     * @param array $parameters
     *
     * @return array
     */
    public function getFiltersByCriteria(array $parameters = [])
    {
        $request = new FilterRequest($parameters, $this->shopDomain);
    
        $url = $this->getApiUrl().str_replace(
                ['{siteId}', '{locale}'],
                [$request->getSiteId(), $request->getLanguage()],
                self::MOBILOSOFT_FILTERS
            );
    
        try {
            $response = $this->getRequest($url, $request->toArray(), $this->getRequestHeaders());
        
            $statusCode = $response->getStatusCode();
            $filters = $this->processFiltersResponse(json_decode($response->getBody()->getContents(), true));
            
            
        } catch (GuzzleException $e) {
            // Save API request error in database
            $this->createApiError($url, $request->toArray(), $e);
        
            $filters = [];
    
            $response = $e->getResponse();
            $statusCode = $response ? $response->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
        }
    
        return [$filters, $statusCode];
    }
    
    /**
     * @param $filtersData
     *
     * @return array
     */
    private function processFiltersResponse($filtersData)
    {
        $filters = [];
        
        
        $filters = $filtersData;
        
        return $filters;
    }

    /**
     * @param LocationRequest $request
     * @param string          $url
     * @param array           $headers
     * @return array
     * @throws GuzzleException
     */
    private function getLocationsByCriteria(LocationRequest $request, string $url, array $headers)
    {
        if ($request->v3->getSize()) {
            list($locations, $statusCode) = $this->getLocationsResponse($request, $url, $headers);
        } else {
            $locations = [];

            $page = 0;
            $request->v3->setSize(V3Request::DEFAULT_SIZE_PER_PAGE);

            do {
                $request->v3->setPage($page);

                list($locationsPage, $statusCode, $totalPages) = $this->getLocationsResponse($request, $url, $headers);

                $locations = array_merge($locations, $locationsPage);
            } while (Response::HTTP_OK === $statusCode && ++$page < $totalPages);
        }

        
        
        
        //special ordering case for voo - only after we get all the locations
        switch($this->shop->getCode()) {
            case Shop::CODE_VOO:
                if (!$this->isAllLocationsRequest($request)) {
                    $locations = $this->processVooLocationsResponse($locations);
                }
                break;
            case Shop::CODE_COVID:
                $locations = $this->processCovidLocationsResponse($locations);
                break;
                
            case Shop::CODE_HACKETT:
            case Shop::CODE_FACONNABLE:
                $country = $request->v3->getCountry();
                if(!empty($country)) {
                    $locations = $this->processCountryLocationsResponse($locations, $country);
                }
                break;
                
            default:
                break;
        }

        return [$locations, $statusCode];
    }

    /**
     * @param LocationRequest $request
     * @param string          $url
     * @param array           $headers
     * @return array
     * @throws GuzzleException
     */
    private function getLocationsResponse(LocationRequest $request, string $url, array $headers)
    {
        $response = $this->postRequest($url.'?page='.$request->v3->getPage().'&size='.$request->v3->getSize(), $request->v3->toArray(), $headers);

        $statusCode = $response->getStatusCode();
        $responseContent = json_decode($response->getBody()->getContents(), true);

        $totalPages = isset($responseContent['totalPages']) ? $responseContent['totalPages'] : 0;
    
        $locations = $this->processLocationsResponse($responseContent, $request);

        return [$locations, $statusCode, $totalPages];
    }

    /**
     * @param GuzzleException $e
     * @param LocationRequest $request
     * @param string          $url
     * @param bool            $getLocalOnError
     *
     * @return array
     */
    private function getLocalLocationsByCriteria(GuzzleException $e, LocationRequest $request, string $url, bool $getLocalOnError)
    {
        // Save API request error in database
        $this->createApiError($url, $request->v3->toArray(), $e);

        $response = $e->getResponse();

        // Get locations from local database
        if ($getLocalOnError) {
            $request->local->setPage(0);
            $request->local->setSize(0);

            list($locations, $statusCode) = $this->apiLocal->searchLocationsByCriteria($request->local->toArray());

            if (empty($locations)) {
                $statusCode = $response ? $response->getStatusCode() : Response::HTTP_NOT_FOUND;
            }
        } else {
            $locations = [];
            $statusCode = $response ? $response->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return [$locations, $statusCode];
    }

    /**
     * @param array           $responseData
     * @param LocationRequest $request
     * @return array
     */
    private function processLocationsResponse($responseData, $request)
    {
        $processedData = [];

        if (!empty($responseData) && isset($responseData['content'])) {
            foreach ($responseData['content'] as $data) {
                if (!empty($data['latitude']) && !empty($data['longitude'])
                    && (empty($data['status']) || strtolower($data['status']) !== 'closed')
                    && ($this->shop->getCode() !== Shop::CODE_CARREFOUR || !empty($data['brandSlug']))
                ) {
                    $location = new LocationEntity(
                        $this->locationResponseAdapter($data, $request),
                        array_merge(
                            $request->v3->toArray(),
                            ['datetimeFormat' => $this->shopDomain ? $this->shopDomain->getDatetimeFormat() : null]
                        )
                    );

                    $this->setLocationTimezone($location);

                    $processedData[] = $location->toArray();
                }
            }
        }

        return $processedData;
    }

    /**
     * @param array $responseData
     * @return array
     * special case for locations on Voo
     */
    private function processVooLocationsResponse($responseData)
    {
        $processedData = [];

        foreach ($responseData as $location) {
            $distance = $location['address']['distance'];
            switch ($location['brandSlug']) {
                case LocationEntity::VOO_BRAND_SLUG_BOUTIQUE:
                    if ($distance <= 40) {
                        $weight = $distance;
                    } else {
                        $weight = 50 + $distance / 200;
                    }
                    break;
                case LocationEntity::VOO_BRAND_SLUG_REVENDEUR:
                    if ($distance <= 40) {
                        $weight = 41 + $distance / 40;
                    } else {
                        $weight = 52 + $distance / 200;
                    }
                    break;
                case LocationEntity::VOO_BRAND_SLUG_POS:
                    if ($distance <= 40) {
                        $weight = 43 + $distance / 40;
                    } else {
                        $weight = 53 + $distance / 200;
                    }
                    break;
                default:
                    $weight = null;
            }

            if ($weight) {
                $processedData['details'][] = $location;
                $processedData['weight'][] = $weight;
            }
        }

        if (!empty($processedData)) {
            array_multisort($processedData['weight'], SORT_ASC, SORT_NUMERIC, $processedData['details']);

            return array_values($processedData['details']);
        }

        return $responseData;
    }

    private function processCovidLocationsResponse($responseData)
    {
        $processedData = [];

        foreach ($responseData as $location) {
            if (strtoupper($location["address"]["country"]) != "BE") {
                continue;
            }

            if ($location["status"] != "OPEN") {
                continue;
            }

            $processedData[] = $location;
        }

        return $processedData;
    }
    
    private function processCountryLocationsResponse($responseData, $country)
    {
        $processedData = [];
        
        foreach ($responseData as $location) {
            if (strtoupper($location["address"]["country"]) != strtoupper($country)) {
                continue;
            }
            
            if ($location["status"] != "OPEN") {
                continue;
            }
            
            $processedData[] = $location;
        }
        
        return $processedData;
    }

    /**
     * @param array           $responseData
     * @param LocationRequest $request
     * @return array
     */
    private function processLocationResponse($responseData, $request)
    {
        $processedData = [];

        if ($responseData) {
            $location = new LocationEntity(
                $this->locationResponseAdapter($responseData, $request),
                array_merge(
                    $request->v3->toArray(),
                    ['datetimeFormat' => $this->shopDomain ? $this->shopDomain->getDatetimeFormat() : null]
                )
            );

            $this->setLocationTimezone($location);
            $this->setLocationItems($location, $request);

            $processedData = $location->toArray();
        }

        return $processedData;
    }

    /**
     * @param array           $responseData
     * @param LocationRequest $request
     * @return array
     */
    private function processNearLocationsResponse($responseData, $request)
    {
        $processedData = [];

        if (!empty($responseData) && isset($responseData['content'])) {
            foreach ($responseData['content'] as $data) {
                if (!empty($data['latitude']) && !empty($data['longitude']) && !empty($data['distance'])) {
                    $location = new LocationEntity(
                        $this->locationResponseAdapter($data, $request),
                        array_merge(
                            $request->v3->toArray(),
                            ['datetimeFormat' => $this->shopDomain ? $this->shopDomain->getDatetimeFormat() : null]
                        )
                    );

                    $this->setLocationTimezone($location);

                    $processedData[] = $location->toArray();
                }
            }
        }

        return $processedData;
    }

    /**
     * @param array       $responseData
     * @param ItemRequest $request
     * @return array
     */
    private function processItemsResponse($responseData, $request)
    {
        
        
        $items = [];

        foreach ($responseData as $data) {
           
            if(!empty($data['publicationEndTime'])) {
                $dateNow = new \DateTime();
                $dateNowFormatted = $dateNow->format('Y-m-d H:i:s');
                
                
                if(strtotime($dateNowFormatted) > strtotime($data['publicationEndTime'])) {
                    continue;
                }
            }
            
            
            
            $itemLanguage = null;
            if( isset($data['language']) && !empty($data['language']) ) {
                
                if(isset(ItemEntity::ITEM_LANGUAGES[$data['language']])) {
                    
                    $itemLanguage = ItemEntity::ITEM_LANGUAGES[$data['language']];
                    if($itemLanguage!=$request->getLanguage()) {
                        continue;
                    }
                    
                }
            
            }
            
            
            $validFrom = isset($data['eventStartDate']) ? $data['eventStartDate'] : null;
            if(empty($validFrom)) {
                
                $validFrom = $data['publicationTime'];
            }
            
            $itemData = [
                'id'                    => isset($data['id']) ? $data['id'] : null,
                'externalId'            => isset($data['externalId']) ? $data['externalId'] : null,
                'itemTypeExternalId'    => $data['type'],
                'itemTypeCategoryLabel' => !empty($data['nationalCampaign']) ? ItemEntity::TYPE_NATIONAL : $data['type'],
                'language'              => $itemLanguage?$itemLanguage : $request->getLanguage(),
                'contentFacet'          => [
                    'title'       => isset($data['title']) ? $data['title'] : null,
                    'longContent' => isset($data['content']) ? $data['content'] : null,
                ],
                'validityFacet'         => [
                    'validFrom'  => $validFrom,
                    'validUntil' => isset($data['eventEndDate']) ? $data['eventEndDate'] : null,
                ],
                'callToActionFacet'     => [
                    'ctaType' => isset($data['ctaType']) ? $data['ctaType'] : null,
                    'ctaUrl'  => isset($data['ctaUrl']) ? $data['ctaUrl'] : null,
                ],
            ];

            $itemData['assets'] = [];
            if (!empty($data['images'])) {
                foreach ($data['images'] as $image) {
                    $itemData['assets'][] = [
                        'fileName'  => isset($image['fileName']) ? $image['fileName'] : null,
                        'publicId'  => isset($image['cloudinaryPublicId']) ? $image['cloudinaryPublicId'] : null,
                        'cloudName' => isset($image['cloudinaryCloudName']) ? $image['cloudinaryCloudName'] : null,
                        'order'     => isset($image['order']) ? $image['order'] : null,
                        'type'      => AssetEntity::TYPE_IMAGE
                    ];
                }
            }
    
            //check if we have a video as asset
            if (!empty($data['video'])) {
                $image = $data['video'];
                $itemData['assets'][] = [
                    'fileName'  => isset($image['fileName']) ? $image['fileName'] : null,
                    'publicId'  => isset($image['cloudinaryPublicId']) ? $image['cloudinaryPublicId'] : null,
                    'cloudName' => isset($image['cloudinaryCloudName']) ? $image['cloudinaryCloudName'] : null,
                    'order'     => isset($image['order']) ? $image['order'] : null,
                    'type'      => AssetEntity::TYPE_VIDEO
                ];
            }

            $item = new ItemEntity($itemData);

            $items[$item->getItemTypeCategoryLabel()][] = $item->toArray();
        }
        
        return $items;
    }

    /**
     * @param array $responseData
     * @return array
     */
    private function processReviewsResponse($responseData)
    {
        $reviews = [];
        $totalRating = 0;

        if (!empty($responseData)) {
            foreach ($responseData as $data) {
                $review = new ReviewEntity($data);

                $totalRating += $review->getRating();
                $reviews[] = $review->toArray();
            }

            $totalRating = round($totalRating / count($reviews), 1);
        }

        return ['reviews' => $reviews, 'rating' => $totalRating];
    }

    /**
     * @param array           $data
     * @param LocationRequest $request
     * @return array
     */
    private function locationResponseAdapter($data, $request)
    {
        $name = isset($data['name']) ? $data['name'] : null;
        $slug = isset($data['slug']) ? $data['slug'] : null;
        $brand = isset($data['brand']) ? $data['brand'] : null;

        if (!empty($data['translatedName'])) {
            foreach ($data['translatedName'] as $values) {
                if ($values['language'] === $request->v3->getLanguage() && !empty($values['value'])) {
                    $name = $values['value'];
                    break;
                }
            }
        }

        if (!empty($data['translatedSlug'])) {
            foreach ($data['translatedSlug'] as $values) {
                if ($values['language'] === $request->v3->getLanguage() && !empty($values['value'])) {
                    $slug = $values['value'];
                    break;
                }
            }
        }

        if (!empty($data['translatedBrand'])) {
            foreach ($data['translatedBrand'] as $values) {
                if ($values['language'] === $request->v3->getLanguage() && !empty($values['value'])) {
                    $brand = $values['value'];
                    break;
                }
            }
        }

        //process extra data
        $extraData = isset($data['extraData']) ? $data['extraData'] : [];
        foreach ($extraData as $key => $extraDataDetails) {
            if (!empty($extraDataDetails['translatedValues'])) {
                foreach ($extraDataDetails['translatedValues'] as $langDetails) {
                    if ($langDetails['language'] == $request->v3->getLanguage() && !empty($langDetails['value'])) {
                        $extraData[$key]['value'] = $langDetails['value'];
                    }
                }
            }
        }
        
        //@todo remove this when we will have the api working
        if($data['id'] == "be6c9372-0272-4870-bc68-60cb3ed10aad" && $data['status'] == "OPEN_SOON" && empty($data['openingDate'])) {
            $data['openingDate'] = "2020-11-19";
        }

        $locationData = [
            'guid'              => isset($data['id']) ? $data['id'] : null,
            'siteId'            => isset($data['siteId']) ? $data['siteId'] : null,
            'externalId'        => isset($data['externalId']) ? $data['externalId'] : null,
            'name'              => $name,
            'status'            => isset($data['status']) ? $data['status'] : null,
            'slug'              => $slug,
            'brand'             => $brand,
            'brandSlug'         => isset($data['brandSlug']) ? $data['brandSlug'] : null,
            'openingDate'       => isset($data['openingDate']) ? $data['openingDate'] : null,
            'closingDate'       => isset($data['closingDate']) ? $data['closingDate'] : null,
            'language'          => isset($data['language']) ? $data['language'] : null,
            'description'       => $this->locationDescriptionAdapter($data, $request),
            'businessHours'     => isset($data['businessHours']) ? $data['businessHours'] : [],
            'specialHours'      => isset($data['specialHours']) ? $data['specialHours'] : [],
            'locationExtraData' => $extraData,
            'images'            => isset($data['images']) ? $data['images'] : [],
            'address'           => $this->locationAddressAdapter($data),
            'contact'           => $this->locationContactAdapter($data),
            'locationItemIds'   => $this->locationItemIdsAdapter($data),
            'services'          => $this->locationServicesAdapter($data, $request),
        ];

        return $locationData;
    }

    /**
     * @param array $data
     * @return array
     */
    private function locationAddressAdapter($data)
    {
        return [
            'street'    => isset($data['address']) ? $data['address'] : null,
            'locality'  => isset($data['city']) ? $data['city'] : null,
            'zipCode'   => isset($data['zipCode']) ? $data['zipCode'] : null,
            'country'   => isset($data['country']) ? $data['country'] : null,
            'latitude'  => isset($data['latitude']) ? $data['latitude'] : 0,
            'longitude' => isset($data['longitude']) ? $data['longitude'] : 0,
            'distance'  => isset($data['distance']) ? $data['distance'] : 0,
            'extraInfo' => isset($data['adressExtraInfo']) ? $data['adressExtraInfo'] : null,
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    private function locationContactAdapter($data)
    {
        $contact = [];

        if (!empty($data['contact']) && $dataContact = $data['contact']) {
            $contact = [
                'type'              => isset($dataContact['type']) ? $dataContact['type'] : null,
                'name'              => isset($dataContact['name']) ? $dataContact['name'] : null,
                'phone'             => isset($dataContact['phone']) ? $dataContact['phone'] : null,
                'phone2'            => isset($dataContact['phone2']) ? $dataContact['phone2'] : null,
                'email'             => isset($dataContact['email']) ? $dataContact['email'] : null,
                'url'               => isset($dataContact['url']) ? $dataContact['url'] : null,
                'profilePictureUrl' => isset($dataContact['profilePictureUrl']) ? $dataContact['profilePictureUrl'] : null,
            ];
        }

        return $contact;
    }

    /**
     * @param array           $data
     * @param LocationRequest $request
     * @return array
     */
    private function locationDescriptionAdapter($data, $request)
    {
        $description = null;

        if (!empty($data['translatedDescriptions'])) {
            foreach ($data['translatedDescriptions'] as $translatedDescription) {
                if (!empty($translatedDescription['value']) && $translatedDescription['language'] === $request->v3->getLanguage()) {
                    $description = $translatedDescription['value'];
                    break;
                }
            }
        }
        
        if(empty($description) && isset($data['description'])) {
            $description = $data['description'];
        }

        return $description;
    }

    /**
     * @param $data
     * @return array
     */
    private function locationItemIdsAdapter($data)
    {
        $itemIds = [];

        if (!empty($data['propertyItems'])) {
            foreach ($data['propertyItems'] as $propertyItem) {
                if (!empty($propertyItem['key'])) {
                    $itemIds[] = $propertyItem['key'];
                }
            }
        }

        return $itemIds;
    }

    /**
     * @param array           $data
     * @param LocationRequest $request
     * @return array
     */
    private function locationServicesAdapter($data, $request)
    {
        $services = [];

        if (!empty($data['propertyItems'])) {
            
            uasort($data['propertyItems'], function ($a, $b) {
                if ($a["order"] == $b["order"]) {
                    return 0;
                }
                return ($a["order"] < $b["order"]) ? -1 : 1;
            });
            
            foreach ($data['propertyItems'] as $propertyItem) {
                $itemData = [
                    'externalId'            => isset($propertyItem['externalId']) ? $propertyItem['externalId'] : null,
                    'itemTypeExternalId'    => isset($propertyItem['key']) ? $propertyItem['key'] : null,
                    'itemTypeCategoryLabel' => $propertyItem['type'],
                    'contentFacet'          => ['title' => ''],
                ];

                if (!empty($propertyItem['groups'])) {
                    foreach ($propertyItem['groups'] as $group) {
                        if (!empty($group['translatedLabels'])) {
                            foreach ($group['translatedLabels'] as $translatedLabel) {
                                if (!empty($translatedLabel['value']) && $translatedLabel['language'] === $request->v3->getLanguage()) {
                                    $itemData['itemTypeCategoryLabel'] = $translatedLabel['value'];
                                    break;
                                }
                            }
                        } else {
                            $itemData['itemTypeCategoryLabel'] = $group['externalId'];
                        }
                    }
                }
                
                //fallback
                if(empty($itemData['itemTypeCategoryLabel'])) {
                    $itemData['itemTypeCategoryLabel'] = 'SERVICE';
                }

                if (!empty($propertyItem['translatedLabels'])) {
                    foreach ($propertyItem['translatedLabels'] as $translatedLabel) {
                        if (!empty($translatedLabel['value']) && $translatedLabel['language'] === $request->v3->getLanguage()) {
                            $itemData['contentFacet']['title'] = $translatedLabel['value'];
                            break;
                        }
                    }
                }
                
                //fallback
                if(empty($itemData['contentFacet']['title']) && !empty($propertyItem['label'])) {
                    $itemData['contentFacet']['title'] = $propertyItem['label'];
                }

                if (!empty($propertyItem['translatedDescriptions'])) {
                    foreach ($propertyItem['translatedDescriptions'] as $translatedDescription) {
                        if (!empty($translatedDescription['value']) && $translatedDescription['language'] === $request->v3->getLanguage()) {
                            $itemData['contentFacet']['longContent'] = $translatedDescription['value'];
                            break;
                        }
                    }
                }
                
                if(empty($itemData['contentFacet']['longContent']) && !empty($propertyItem["description"])) {
                    $itemData['contentFacet']['longContent'] = $propertyItem["description"];
                }

                if (!empty($propertyItem['translatedIcons'])) {
                    foreach ($propertyItem['translatedIcons'] as $translatedIcon) {
                        if (!empty($translatedIcon['value']) && $translatedIcon['language'] === $request->v3->getLanguage()) {
                            $itemData['icon'] = $translatedIcon['value'];
                            break;
                        }
                    }
                }
                

                if (!empty($propertyItem['mongoImage'])) {
                    $itemData['assets'][] = $propertyItem['mongoImage'];
                }

                $item = new ItemEntity($itemData);

                $services[$item->getItemTypeCategoryLabel()][] = $item->toArray();
            }
        }

        return $services;
    }

    /**
     * @param LocationEntity  $location
     * @param LocationRequest $request
     */
    private function setLocationItems($location, $request)
    {
        // Get Location services/items
        if ($request->v3->getServices()) {
            $items = [];

            if ($location->getGuid()) {
                $locationItems =  $this->searchItemsByCriteria([
                    'siteId'     => $request->v3->getSiteId(),
                    'locationId' => $location->getGuid(),
                    'language'   => $request->v3->getLanguage(),
                ]);

                $items = current($locationItems);
            }

            $location->setItems($items);
        }
    }
    
    /**
     * @return array
     */
    private function getRequestHeaders()
    {
        $headers = ['Content-Type' => 'application/json', 'X-Api-Token' => $this->shop ? $this->shop->getApiToken() : ''];
        
        return $headers;
    }
    
    /**
     * @param $request
     *
     * @return bool
     */
    private function isAllLocationsRequest($request)
    {
        switch ($this->shop->getCode()) {
//            case Shop::CODE_COVID:
            case Shop::CODE_MEDIMARKET:
                $allLocationRequest = false;
                break;
            case Shop::CODE_LOLALIZA:
                $allLocationRequest = false;
                break;
            default:
                $allLocationRequest = $request->v3->isAllLocationsRequest();
                break;
        }
        
        
        return $allLocationRequest;
    }

    /**
     * @return string|null
     */
    private function getApiUrl()
    {
        if ($shopApiUrl = $this->shop->getApiUrl()) {
            return trim($shopApiUrl, '/');
        }

        return $this->apiV3MobilosoftUrl;
    }
}
