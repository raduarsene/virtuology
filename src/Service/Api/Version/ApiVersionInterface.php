<?php

namespace App\Service\Api\Version;

use App\Entity\ShopDomain;

interface ApiVersionInterface
{
    /**
     * Set shop domain
     *
     * @param ShopDomain|null $shopDomain
     */
    public function setShopDomain(ShopDomain $shopDomain = null);

    /**
     * Return API version implemented in service
     *
     * @return string
     */
    public function getVersion();

    /**
     * Find and return locations searched by criteria
     *
     * @param array $parameters
     * @param bool  $getLocalOnError Get local data from database on external request error
     * @return array First item: data from response; And the second: status code
     */
    public function searchLocationsByCriteria(array $parameters = array(), bool $getLocalOnError = true);

    /**
     * Find and return shop locations grouped by city letter
     *
     * @param array $parameters
     * @return array First item: data from response; And the second: status code
     */
    public function searchLocationsLetterGrouped(array $parameters = array());

    /**
     * Find and return near locations searched by location slug
     *
     * @param array $parameters
     * @param bool  $getLocalOnError Get local data from database on external request error
     * @return array First item: data from response; And the second: status code
     */
    public function searchNearLocationsBySlug(array $parameters = array(), bool $getLocalOnError = true);

    /**
     * Find and return location searched by slug
     *
     * @param array $parameters
     * @param bool  $getLocalOnError Get local data from database on external request error
     * @return array First item: data from response; And the second: status code
     */
    public function searchLocationBySlug(array $parameters = array(), bool $getLocalOnError = true);

    /**
     * Find and return location assets
     *
     * @param array $parameters
     * @return array First item: data from response; And the second: status code
     */
    public function getLocationAssets(array $parameters = array());

    /**
     * Find and return items searched by criteria
     *
     * @param array $parameters
     * @return array First item: data from response; And the second: status code
     */
    public function searchItemsByCriteria(array $parameters = array());

    /**
     * Find and return reviews searched by criteria
     *
     * @param array $parameters
     * @return array First item: data from response; And the second: status code
     */
    public function searchReviewsByCriteria(array $parameters = array());

    /**
     * Find and return feed data of the shop
     *
     * @param array $parameters
     * @param bool  $getLocalOnError Get local data from database on external request error
     * @return array First item: data from response; And the second: status code
     */
    public function searchFeedData(array $parameters = array(), bool $getLocalOnError = true);
}
