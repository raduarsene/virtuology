<?php

namespace App\Service\Api;

use App\Entity\ShopDomain;

interface ApiInterface
{
    /**
     * Init API service
     *
     * @param string          $version
     * @param ShopDomain|null $shopDomain
     * @return ApiInterface
     */
    public function init(string $version, ShopDomain $shopDomain = null);

    /**
     * Get shop locations
     *
     * @param array $parameters
     * @param bool  $getLocalOnError Get local data from database on external request error
     * @return array
     */
    public function getLocations(array $parameters = array(), bool $getLocalOnError = true);

    /**
     * Get shop locations grouped by city letter
     *
     * @param array $parameters
     * @return array
     */
    public function getLocationsLetterGrouped(array $parameters = array());

    /**
     * Get near locations by location slug
     *
     * @param array $parameters
     * @param bool  $getLocalOnError Get local data from database on external request error
     * @return array
     */
    public function getNearLocationsBySlug(array $parameters = array(), bool $getLocalOnError = true);

    /**
     * Get location by slug
     *
     * @param array $parameters
     * @param bool  $getLocalOnError Get local data from database on external request error
     * @return array
     */
    public function getLocationBySlug(array $parameters = array(), bool $getLocalOnError = true);

    /**
     * Get location assets
     *
     * @param array $parameters
     * @return array
     */
    public function getLocationAssets(array $parameters = array());

    /**
     * Get location items
     *
     * @param array $parameters
     * @return array
     */
    public function getLocationItems(array $parameters = array());

    /**
     * Get shop feed data
     *
     * @param array $parameters
     * @param bool  $getLocalOnError Get local data from database on external request error
     * @return array
     */
    public function getFeedData(array $parameters = array(), bool $getLocalOnError = true);
    
    /**
     * Get location reviews
     *
     * @param array $parameters
     * @return array
     */
    public function getLocationReviews(array $parameters = array());
}
