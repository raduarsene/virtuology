<?php

namespace App\Service\Api\Entity;

class ReviewEntity extends AbstractEntity
{
    const DEFAULT_DATE_FORMAT = 'Y-m-d H:i:s';

    /** @var string|null */
    protected $language;

    /** @var string|null */
    protected $channelReviewId;

    /** @var string|null */
    protected $content;

    /** @var float */
    protected $rating;

    /** @var string|null */
    protected $status;

    /** @var string */
    protected $channel;

    /** @var string|null */
    protected $authorId;

    /** @var string|null */
    protected $authorName;

    /** @var string|null */
    protected $channelCreationDate;

    /** @var string|null */
    protected $channelModifiedDate;

    /** @var array|null */
    protected $extraData;

    /**
     * @param string|null $language
     */
    public function setLanguage(?string $language)
    {
        $this->language = $language;
    }

    /**
     * @return string|null
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @param string|null $channelReviewId
     */
    public function setChannelReviewId(?string $channelReviewId)
    {
        $this->channelReviewId = $channelReviewId;
    }

    /**
     * @return string|null
     */
    public function getChannelReviewId(): ?string
    {
        return $this->channelReviewId;
    }

    /**
     * @param string|null $content
     */
    public function setContent(?string $content)
    {
        $this->content = $content;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param float $rating
     */
    public function setRating(float $rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return float
     */
    public function getRating(): float
    {
        return $this->rating;
    }

    /**
     * @param string|null $status
     */
    public function setStatus(?string $status)
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $channel
     */
    public function setChannel(string $channel)
    {
        $this->channel = $channel;
    }

    /**
     * @return string
     */
    public function getChannel(): string
    {
        return $this->channel;
    }

    /**
     * @param string|null $authorId
     */
    public function setAuthorId(?string $authorId)
    {
        $this->authorId = $authorId;
    }

    /**
     * @return string|null
     */
    public function getAuthorId(): ?string
    {
        return $this->authorId;
    }

    /**
     * @param string|null $authorName
     */
    public function setAuthorName(?string $authorName)
    {
        $this->authorName = $authorName;
    }

    /**
     * @return string|null
     */
    public function getAuthorName(): ?string
    {
        return $this->authorName;
    }

    /**
     * @param string|null $channelCreationDate
     */
    public function setChannelCreationDate($channelCreationDate)
    {
        if ($channelCreationDate && is_string($channelCreationDate)) {
            try {
                $channelCreationDate = new \DateTime($channelCreationDate);
            } catch (\Exception $e) {
                $channelCreationDate = null;
            }
        }

        $this->channelCreationDate = $channelCreationDate;
    }

    /**
     * @return string|null
     */
    public function getChannelCreationDate(): ?string
    {
        return $this->channelCreationDate instanceof \DateTime
            ? $this->channelCreationDate->format(self::DEFAULT_DATE_FORMAT)
            : $this->channelCreationDate;
    }

    /**
     * @param string|null $channelModifiedDate
     */
    public function setChannelModifiedDate($channelModifiedDate)
    {
        if ($channelModifiedDate && is_string($channelModifiedDate)) {
            try {
                $channelModifiedDate = new \DateTime($channelModifiedDate);
            } catch (\Exception $e) {
                $channelModifiedDate = null;
            }
        }

        $this->channelModifiedDate = $channelModifiedDate;
    }

    /**
     * @return string|null
     */
    public function getChannelModifiedDate(): ?string
    {
        return $this->channelModifiedDate instanceof \DateTime
            ? $this->channelModifiedDate->format(self::DEFAULT_DATE_FORMAT)
            : $this->channelModifiedDate;
    }

    /**
     * @param array|null $extraData
     */
    public function setExtraData(?array $extraData)
    {
        $this->extraData = $extraData;
    }

    /**
     * @return array|null
     */
    public function getExtraData(): ?array
    {
        return $this->extraData;
    }
}
