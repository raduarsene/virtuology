<?php

namespace App\Service\Api\Entity\Location;

use App\Service\Api\Entity\AbstractEntity;

class AddressEntity extends AbstractEntity
{
    /**
     * @var string
     */
    protected $street;

    /**
     * @var string
     */
    protected $number;

    /**
     * @var string|null
     */
    protected $extraInfo;

    /**
     * @var string
     */
    protected $locality;

    /**
     * @var string
     */
    protected $zipCode;

    /**
     * @var string
     */
    protected $country;

    /**
     * @var string
     */
    protected $fullAddress;

    /**
     * @var string|null
     */
    protected $timezone;

    /**
     * @var float
     */
    protected $latitude;

    /**
     * @var float
     */
    protected $longitude;

    /**
     * Distance from center point in km.
     *
     * @var float|null
     */
    protected $distance;

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return preg_replace("/,+/", ",", $this->street);
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string|null $extraInfo
     */
    public function setExtraInfo($extraInfo)
    {
        $this->extraInfo = $extraInfo;
    }

    /**
     * @return string|null
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }

    /**
     * @param string $locality
     */
    public function setLocality($locality)
    {
        $this->locality = $locality;
    }

    /**
     * @return string
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function getFullAddress()
    {
        $fullAddress = $this->street;

        if (!empty($this->number)) {
            $fullAddress .= ','.$this->number;
        }

        if (!empty($this->zipCode)) {
            $fullAddress .= ','.$this->zipCode;
        }

        $fullAddress .= ','.$this->locality.','.$this->country;

        return $fullAddress;
    }

    /**
     * @param string|null $timezone
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    }

    /**
     * @return string|null
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param float $latitude
     * @param array $parameters
     */
    public function setLatitude($latitude, array $parameters = array())
    {
        $this->latitude = (float)$latitude;

        // Set Distance if there is latitude and longitude
        if ($this->latitude && $this->longitude && empty($this->distance)) {
            $this->calculateDistance($this->latitude, $this->longitude, $parameters);
        }
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param float $longitude
     * @param array $parameters
     */
    public function setLongitude($longitude, array $parameters = array())
    {
        $this->longitude = (float)$longitude;

        // Set Distance if there is latitude and longitude
        if ($this->latitude && $this->longitude && empty($this->distance)) {
            $this->calculateDistance($this->latitude, $this->longitude, $parameters);
        }
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param float $distance
     */
    public function setDistance($distance)
    {
        $this->distance = round($distance, 1);
    }

    /**
     * @return float|null
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @param float $latFrom
     * @param float $longFrom
     * @param array $parameters
     */
    public function calculateDistance($latFrom, $longFrom, array $parameters = array())
    {
        $distance = 0;
        $earthRadius = 6371000; // in metters

        if (!empty($parameters['near'])) {
            $near = explode(',', $parameters['near']);

            $latTo = (float)$near[0];
            $longTo = (float)$near[1];
        } elseif (!empty($parameters['within'])) {
            $within = explode(',', $parameters['within']);

            $latTo = ($within[0] + $within[2]) / 2;
            $longTo = ($within[1] + $within[3]) / 2;
        }

        if (!empty($latTo) && !empty($longTo)) {
            // convert from degrees to radians
            $latFrom = deg2rad($latFrom);
            $lonFrom = deg2rad($longFrom);
            $latTo = deg2rad($latTo);
            $longTo = deg2rad($longTo);

            $latDelta = $latTo - $latFrom;
            $longDelta = $longTo - $lonFrom;

            $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($longDelta / 2), 2)));

            $distance = ($angle * $earthRadius) / 1000; // get distance in km (/ 1000)
        }

        $this->setDistance($distance);
    }
}
