<?php

namespace App\Service\Api\Entity\Location;

use App\Service\Api\Entity\AbstractEntity;

class ExtraDataEntity extends AbstractEntity
{
    const DESCRIPTION_KEY = 'description.data.STORE_LOCATOR';

    /**
     * @var string
     */
    protected $language;

    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $value;

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
}
