<?php

namespace App\Service\Api\Entity\Location;

use App\Service\Api\Entity\AbstractEntity;

class ImageEntity extends AbstractEntity
{
    /**
     * @var string
     */
    protected $cloudName;

    /**
     * @var string
     */
    protected $publicId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $type;

    /**
     * @param string $cloudName
     */
    public function setCloudName($cloudName)
    {
        $this->cloudName = $cloudName;
    }

    /**
     * @return string
     */
    public function getCloudName()
    {
        return $this->cloudName;
    }

    /**
     * @param string $publicId
     */
    public function setPublicId($publicId)
    {
        $this->publicId = $publicId;
    }

    /**
     * @return string
     */
    public function getPublicId()
    {
        return $this->publicId;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
