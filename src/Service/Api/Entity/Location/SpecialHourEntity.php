<?php

namespace App\Service\Api\Entity\Location;

use App\Helper\DateTimeFormat;
use App\Service\Api\Entity\AbstractEntity;
use App\Service\Api\Entity\LocationEntity;

class SpecialHourEntity extends AbstractEntity
{
    /**
     * @var \DateTime|null
     */
    protected $startDate;

    /**
     * @var \DateTime|null
     */
    protected $endDate;

    /**
     * @var string|null
     */
    protected $openTime;

    /**
     * @var string|null
     */
    protected $openTimeFormat;

    /**
     * @var string|null
     */
    protected $closeTime;

    /**
     * @var string|null
     */
    protected $closeTimeFormat;

    /**
     * @var string|null
     */
    protected $additionalOpenTime;

    /**
     * @var string|null
     */
    protected $additionalOpenTimeFormat;

    /**
     * @var string|null
     */
    protected $additionalCloseTime;

    /**
     * @var string|null
     */
    protected $additionalCloseTimeFormat;

    /**
     * @var bool
     */
    protected $closed;

    /**
     * @var string|null
     */
    protected $type;

    /**
     * @param \DateTime|string|null $startDate
     */
    public function setStartDate($startDate)
    {
        if ($startDate && is_string($startDate)) {
            try {
                $startDate = new \DateTime($startDate);
            } catch (\Exception $e) {
                $startDate = null;
            }
        }

        $this->startDate = $startDate;
    }

    /**
     * @return string|null
     */
    public function getStartDate()
    {
        return $this->startDate instanceof \DateTime ? $this->startDate->format(LocationEntity::DEFAULT_DATE_FORMAT) : $this->startDate;
    }

    /**
     * @param \DateTime|string|null $endDate
     */
    public function setEndDate($endDate)
    {
        if ($endDate && is_string($endDate)) {
            try {
                $endDate = new \DateTime($endDate);
            } catch (\Exception $e) {
                $endDate = null;
            }
        }

        $this->endDate = $endDate;
    }

    /**
     * @return string|null
     */
    public function getEndDate()
    {
        return $this->endDate instanceof \DateTime ? $this->endDate->format(LocationEntity::DEFAULT_DATE_FORMAT) : $this->endDate;
    }

    /**
     * @param string $openTime
     * @param array  $parameters
     */
    public function setOpenTime($openTime, array $parameters = array())
    {
        $this->openTime = $openTime;

        if ($openTime) {
            $timeFormat = DateTimeFormat::getTimeFormat($parameters['datetimeFormat'] ?? DateTimeFormat::FORMAT_DEFAULT);
            $dateTime = \DateTime::createFromFormat(LocationEntity::DEFAULT_HOUR_FORMAT, $openTime);

            if($dateTime) {
                $this->openTimeFormat = $dateTime->format($timeFormat);
            }
            
        }
    }

    /**
     * @return string
     */
    public function getOpenTime()
    {
        return $this->openTime;
    }

    /**
     * @return string
     */
    public function getOpenTimeFormat()
    {
        return $this->openTimeFormat;
    }

    /**
     * @param string $closeTime
     * @param array  $parameters
     */
    public function setCloseTime($closeTime, array $parameters = array())
    {
        $this->closeTime = $closeTime;

        if ($closeTime) {
            $timeFormat = DateTimeFormat::getTimeFormat($parameters['datetimeFormat'] ?? DateTimeFormat::FORMAT_DEFAULT);
            $dateTime = \DateTime::createFromFormat(LocationEntity::DEFAULT_HOUR_FORMAT, $closeTime);

            if($dateTime) {
                $this->closeTimeFormat = $dateTime->format($timeFormat);
            }
            
        }
    }

    /**
     * @return string
     */
    public function getCloseTime()
    {
        return $this->closeTime;
    }

    /**
     * @return string
     */
    public function getCloseTimeFormat()
    {
        return $this->closeTimeFormat;
    }

    /**
     * @param string $additionalOpenTime
     * @param array  $parameters
     */
    public function setAdditionalOpenTime($additionalOpenTime, array $parameters = array())
    {
        $this->additionalOpenTime = $additionalOpenTime;

        if ($additionalOpenTime && !empty($additionalOpenTime) && is_string($additionalOpenTime)) {
            $timeFormat = DateTimeFormat::getTimeFormat($parameters['datetimeFormat'] ?? DateTimeFormat::FORMAT_DEFAULT);
            $dateTime = \DateTime::createFromFormat(LocationEntity::DEFAULT_HOUR_FORMAT, $additionalOpenTime);

            if($dateTime) {
                $this->additionalOpenTimeFormat = $dateTime->format($timeFormat);
            }
            
        }
    }

    /**
     * @return string
     */
    public function getAdditionalOpenTime()
    {
        return $this->additionalOpenTime;
    }

    /**
     * @return string
     */
    public function getAdditionalOpenTimeFormat()
    {
        return $this->additionalOpenTimeFormat;
    }

    /**
     * @param string $additionalCloseTime
     * @param array  $parameters
     */
    public function setAdditionalCloseTime($additionalCloseTime, array $parameters = array())
    {
        $this->additionalCloseTime = $additionalCloseTime;

        if ($additionalCloseTime && !empty($additionalCloseTime) && is_string($additionalCloseTime)) {
            
            $timeFormat = DateTimeFormat::getTimeFormat($parameters['datetimeFormat'] ?? DateTimeFormat::FORMAT_DEFAULT);
            
            
            $dateTime = \DateTime::createFromFormat(LocationEntity::DEFAULT_HOUR_FORMAT, $additionalCloseTime);
            

            if($dateTime) {
                $this->additionalCloseTimeFormat = $dateTime->format($timeFormat);
            }
            
        }
    }

    /**
     * @return string
     */
    public function getAdditionalCloseTime()
    {
        return $this->additionalCloseTime;
    }

    /**
     * @return string
     */
    public function getAdditionalCloseTimeFormat()
    {
        return $this->additionalCloseTimeFormat;
    }

    /**
     * @param bool $closed
     */
    public function setClosed($closed)
    {
        $this->closed = (bool)$closed;
    }

    /**
     * @return bool
     */
    public function getClosed()
    {
        return $this->closed;
    }

    /**
     * @param string|null $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
}
