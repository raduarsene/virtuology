<?php

namespace App\Service\Api\Entity\Location;

use App\Helper\DateTimeFormat;
use App\Service\Api\Entity\AbstractEntity;
use App\Service\Api\Entity\LocationEntity;

class BusinessHourEntity extends AbstractEntity
{
    /**
     * @var int
     */
    protected $startDay;

    /**
     * @var int
     */
    protected $endDay;

    /**
     * @var string
     */
    protected $openTime;

    /**
     * @var string
     */
    protected $openTimeFormat;

    /**
     * @var string
     */
    protected $closeTime;

    /**
     * @var string
     */
    protected $closeTimeFormat;

    /**
     * @param int $startDay
     */
    public function setStartDay($startDay)
    {
        $this->startDay = (int)$startDay;
    }

    /**
     * @return int
     */
    public function getStartDay()
    {
        return $this->startDay;
    }

    /**
     * @param int $endDay
     */
    public function setEndDay($endDay)
    {
        $this->endDay = (int)$endDay;
    }

    /**
     * @return int
     */
    public function getEndDay()
    {
        return $this->endDay;
    }

    /**
     * @param string $openTime
     * @param array  $parameters
     */
    public function setOpenTime($openTime, array $parameters = array())
    {
        $this->openTime = $openTime;

        if ($openTime) {
            $timeFormat = DateTimeFormat::getTimeFormat($parameters['datetimeFormat'] ?? DateTimeFormat::FORMAT_DEFAULT);
            $dateTime = \DateTime::createFromFormat(LocationEntity::DEFAULT_HOUR_FORMAT, $openTime);

            $this->openTimeFormat = $dateTime->format($timeFormat);
        }
    }

    /**
     * @return string
     */
    public function getOpenTime()
    {
        return $this->openTime;
    }

    /**
     * @return string
     */
    public function getOpenTimeFormat()
    {
        return $this->openTimeFormat;
    }

    /**
     * @param string $closeTime
     * @param array  $parameters
     */
    public function setCloseTime($closeTime, array $parameters = array())
    {
        $this->closeTime = $closeTime;

        if ($closeTime) {
            $timeFormat = DateTimeFormat::getTimeFormat($parameters['datetimeFormat'] ?? DateTimeFormat::FORMAT_DEFAULT);
            $dateTime = \DateTime::createFromFormat(LocationEntity::DEFAULT_HOUR_FORMAT, $closeTime);

            $this->closeTimeFormat = $dateTime->format($timeFormat);
        }
    }

    /**
     * @return string
     */
    public function getCloseTime()
    {
        return $this->closeTime;
    }

    /**
     * @return string
     */
    public function getCloseTimeFormat()
    {
        return $this->closeTimeFormat;
    }
}
