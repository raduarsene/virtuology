<?php

namespace App\Service\Api\Entity\Item;

use App\Service\Api\Entity\AbstractEntity;

class PriceFacetEntity extends AbstractEntity
{
    /**
     * The price of the promotion.
     *
     * @var string|null
     */
    protected $price;

    /**
     * The old price of the promotion.
     *
     * @var string|null
     */
    protected $priceOld;

    /**
     * @param string|null $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string|null $priceOld
     */
    public function setPriceOld($priceOld)
    {
        $this->priceOld = $priceOld;
    }

    /**
     * @return string|null
     */
    public function getPriceOld()
    {
        return $this->priceOld;
    }
}
