<?php

namespace App\Service\Api\Entity\Item;

use App\Service\Api\Entity\AbstractEntity;

class AssetEntity extends AbstractEntity
{
    
    const TYPE_IMAGE = 'image';
    const TYPE_VIDEO = 'video';
    
    /**
     * @var string|null
     */
    protected $fileName;

    /**
     * @var string|null
     */
    protected $publicId;

    /**
     * @var string|null
     */
    protected $cloudName;

    /**
     * @var string|null
     */
    protected $url;

    /**
     * @var int|null
     */
    protected $order;
    
    
    /** @var null|string */
    protected $type = self::TYPE_IMAGE;
    

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string|null
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $publicId
     */
    public function setPublicId($publicId)
    {
        $this->publicId = $publicId;
    }

    /**
     * @return string|null
     */
    public function getPublicId()
    {
        return $this->publicId;
    }

    /**
     * @param string $cloudName
     */
    public function setCloudName($cloudName)
    {
        $this->cloudName = $cloudName;
    }

    /**
     * @return string|null
     */
    public function getCloudName()
    {
        return $this->cloudName;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return int|null
     */
    public function getOrder()
    {
        return $this->order;
    }
    
    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    
    /**
     * @param null|string $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }
    
    
}
