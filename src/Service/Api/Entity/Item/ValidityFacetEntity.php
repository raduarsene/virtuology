<?php

namespace App\Service\Api\Entity\Item;

use App\Service\Api\Entity\AbstractEntity;
use App\Service\Api\Entity\ItemEntity;

class ValidityFacetEntity extends AbstractEntity
{
    /**
     * The date this post is valid from.
     *
     * @var \DateTime|null
     */
    protected $validFrom;

    /**
     * The date until this post is valid.
     *
     * @var \DateTime|null
     */
    protected $validUntil;

    /**
     * The date from which this post is visible.
     *
     * @var \DateTime|null
     */
    protected $visibleFrom;

    /**
     * @param \DateTime|string|null $validFrom
     */
    public function setValidFrom($validFrom)
    {
        if ($validFrom && is_string($validFrom)) {
            try {
                $validFrom = new \DateTime($validFrom);
            } catch (\Exception $e) {
                $validFrom = null;
            }
        }

        $this->validFrom = $validFrom;
    }

    /**
     * @return string|null
     */
    public function getValidFrom()
    {
        return $this->validFrom instanceof \DateTime ? $this->validFrom->format(ItemEntity::DEFAULT_DATE_FORMAT) : $this->validFrom;
    }

    /**
     * @param \DateTime|string|null $validUntil
     */
    public function setValidUntil($validUntil)
    {
        if ($validUntil && is_string($validUntil)) {
            try {
                $validUntil = new \DateTime($validUntil);
            } catch (\Exception $e) {
                $validUntil = null;
            }
        }

        $this->validUntil = $validUntil;
    }

    /**
     * @return string|null
     */
    public function getValidUntil()
    {
        return $this->validUntil instanceof \DateTime ? $this->validUntil->format(ItemEntity::DEFAULT_DATE_FORMAT) : $this->validUntil;
    }

    /**
     * @param \DateTime|string|null $visibleFrom
     */
    public function setVisibleFrom($visibleFrom)
    {
        if ($visibleFrom && is_string($visibleFrom)) {
            try {
                $visibleFrom = new \DateTime($visibleFrom);
            } catch (\Exception $e) {
                $visibleFrom = null;
            }
        }

        $this->visibleFrom = $visibleFrom;
    }

    /**
     * @return string|null
     */
    public function getVisibleFrom()
    {
        return $this->visibleFrom instanceof \DateTime ? $this->visibleFrom->format(ItemEntity::DEFAULT_DATE_FORMAT) : $this->visibleFrom;
    }
}
