<?php

namespace App\Service\Api\Entity\Item;

use App\Service\Api\Entity\AbstractEntity;

class ContentFacetEntity extends AbstractEntity
{
    /**
     * The title of the post.
     *
     * @var string
     */
    protected $title;

    /**
     * The short text of the post.
     *
     * @var string|null
     */
    protected $shortContent;

    /**
     * The long text of the post.
     *
     * @var string|null
     */
    protected $longContent;

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string|null $shortContent
     */
    public function setShortContent($shortContent)
    {
        $this->shortContent = $shortContent;
    }

    /**
     * @return string|null
     */
    public function getShortContent()
    {
        return $this->shortContent;
    }

    /**
     * @param string|null $longContent
     */
    public function setLongContent($longContent)
    {
        $this->longContent = $longContent;
    }

    /**
     * @return string|null
     */
    public function getLongContent()
    {
        return $this->longContent;
    }
}
