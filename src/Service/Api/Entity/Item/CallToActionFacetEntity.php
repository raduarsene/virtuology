<?php

namespace App\Service\Api\Entity\Item;

use App\Service\Api\Entity\AbstractEntity;

class CallToActionFacetEntity extends AbstractEntity
{
    /**
     * @var string|null
     */
    protected $ctaType;

    /**
     * @var string|null
     */
    protected $ctaLabel;

    /**
     * @var string|null
     */
    protected $ctaUrl;

    /**
     * @param string|null $ctaType
     */
    public function setCtaType($ctaType)
    {
        $this->ctaType = $ctaType;
    }

    /**
     * @return string|null
     */
    public function getCtaType()
    {
        return $this->ctaType;
    }

    /**
     * @param string|null $ctaLabel
     */
    public function setCtaLabel($ctaLabel)
    {
        $this->ctaLabel = $ctaLabel;
    }

    /**
     * @return string|null
     */
    public function getCtaLabel()
    {
        return $this->ctaLabel;
    }

    /**
     * @param string|null $ctaUrl
     */
    public function setCtaUrl($ctaUrl)
    {
        $this->ctaUrl = $ctaUrl;
    }

    /**
     * @return string|null
     */
    public function getCtaUrl()
    {
        return $this->ctaUrl;
    }
}
