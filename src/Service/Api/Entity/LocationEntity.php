<?php

namespace App\Service\Api\Entity;

use App\Service\Api\Entity\Location\AddressEntity;
use App\Service\Api\Entity\Location\BusinessHourEntity;
use App\Service\Api\Entity\Location\ContactEntity;
use App\Service\Api\Entity\Location\ExtraDataEntity;
use App\Service\Api\Entity\Location\ImageEntity;
use App\Service\Api\Entity\Location\SpecialHourEntity;

class LocationEntity extends AbstractEntity
{
    const DEFAULT_DATE_FORMAT = 'Y-m-d H:i:s';
    const DEFAULT_HOUR_FORMAT = 'Hi';

    const VOO_BRAND_SLUG_BOUTIQUE = 'boutique';
    const VOO_BRAND_SLUG_REVENDEUR = 'revendeur-exclusif';
    const VOO_BRAND_SLUG_POS = 'pos';

    const MEDIMARKET_BRAND_SLUG_MEDI = 'medi-market';
    const MEDIMARKET_BRAND_SLUG_INSTITUT = 'institut-by-medi-market';
    const MEDIMARKET_BRAND_SLUG_PHARMACY = 'pharmacy-by-medi-market-group';

    /**
     * The ID of the location
     *
     * @var string
     */
    protected $guid;

    /**
     * The ID of the site the location belongs to
     *
     * @var string
     */
    protected $siteId;

    /**
     * ID used by the customer
     *
     * @var string
     */
    protected $externalId;

    /**
     * Name of the location
     *
     * @var string
     */
    protected $name;

    /**
     * Location status
     *
     * @var string
     */
    protected $status;

    /**
     * Slug of the location. Clean URL version of the name
     *
     * @var string
     */
    protected $slug;

    /**
     * Brand name of this location
     *
     * @var string|null
     */
    protected $brand;

    /**
     * Brand name of this location
     *
     * @var string|null
     */
    protected $brandSlug;

    /**
     * Date the location is opening (or has opened)
     *
     * @var \DateTime|null
     */
    protected $openingDate;

    /**
     * Date the location is closing (or has closed)
     *
     * @var \DateTime|null
     */
    protected $closingDate;

    /**
     * Language of this contact. Must match the regular expression [a-z]{2}
     *
     * @var string
     */
    protected $language;

    /**
     * Description of the location
     *
     * @var string|null
     */
    protected $description;

    /**
     * Multiple tags as array with key 0 indexed
     *
     * @var array
     */
    protected $tags;

    /**
     * Location Item Ids as array with key 0 indexed
     *
     * @var array
     */
    protected $locationItemIds;

    /**
     * Location Contact
     *
     * @var ContactEntity
     */
    protected $contact;

    /**
     * Location Address
     *
     * @var AddressEntity
     */
    protected $address;

    /**
     * Location Business Hours
     *
     * @var BusinessHourEntity[]
     */
    protected $businessHours;

    /**
     * Location Special Hours
     *
     * @var SpecialHourEntity[]
     */
    protected $specialHours;

    /**
     * Location Extra Data
     *
     * @var ExtraDataEntity[]
     */
    protected $locationExtraData;

    /**
     * Location images
     *
     * @var ImageEntity[]
     */
    protected $images;

    /**
     * Location items
     *
     * @var array|null
     */
    protected $items;

    /**
     * Location services
     *
     * @var array|null
     */
    protected $services;
    
    
    /**
     * @var array | null
     */
    protected $translatedSlug;

    /**
     * @param string $guid
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    }

    /**
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param string $siteId
     */
    public function setSiteId($siteId)
    {
        $this->siteId = $siteId;
    }

    /**
     * @return string
     */
    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @return string|null
     */
    public function getBrandSlug(): ?string
    {
        return $this->brandSlug;
    }

    /**
     * @param string|null $brandSlug
     */
    public function setBrandSlug(?string $brandSlug): void
    {
        $this->brandSlug = $brandSlug;
    }

    /**
     * @param \DateTime|string|null $openingDate
     */
    public function setOpeningDate($openingDate)
    {
        if ($openingDate && is_string($openingDate)) {
            try {
                $openingDate = new \DateTime($openingDate);
            } catch (\Exception $e) {
                $openingDate = null;
            }
        }

        $this->openingDate = $openingDate;
    }

    /**
     * @return string|null
     */
    public function getOpeningDate()
    {
        return $this->openingDate instanceof \DateTime ? $this->openingDate->format(LocationEntity::DEFAULT_DATE_FORMAT) : $this->openingDate;
    }

    /**
     * @param \DateTime|string|null $closingDate
     */
    public function setClosingDate($closingDate)
    {
        if ($closingDate && is_string($closingDate)) {
            try {
                $closingDate = new \DateTime($closingDate);
            } catch (\Exception $e) {
                $closingDate = null;
            }
        }

        $this->closingDate = $closingDate;
    }

    /**
     * @return string|null
     */
    public function getClosingDate()
    {
        return $this->closingDate instanceof \DateTime ? $this->closingDate->format(LocationEntity::DEFAULT_DATE_FORMAT) : $this->closingDate;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string|null $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param array $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param array $locationItemIds
     */
    public function setLocationItemIds($locationItemIds)
    {
        $this->locationItemIds = $locationItemIds;
    }

    /**
     * @return array
     */
    public function getLocationItemIds()
    {
        return $this->locationItemIds;
    }

    /**
     * @param array $contact
     */
    public function setContact($contact)
    {
        $this->contact = new ContactEntity($contact ?: array());
    }

    /**
     * @return array
     */
    public function getContact()
    {
        return $this->contact ? $this->contact->toArray() : array();
    }

    /**
     * @param array $address
     * @param array $parameters
     */
    public function setAddress($address, array $parameters = array())
    {
        $this->address = new AddressEntity($address ?: array(), $parameters);
    }

    /**
     * @return array
     */
    public function getAddress()
    {
        return $this->address ? $this->address->toArray() : array();
    }

    /**
     * @param array $businessHours
     * @param array $parameters
     */
    public function setBusinessHours($businessHours, array $parameters = array())
    {
        $this->businessHours = [];

        if ($businessHours && is_array($businessHours)) {
            foreach ($businessHours as $businessHour) {
                $this->businessHours[] = new BusinessHourEntity($businessHour, $parameters);
            }
        }
    }

    /**
     * @return array
     */
    public function getBusinessHours()
    {
        $businessHours = [];

        if ($this->businessHours) {
            foreach ($this->businessHours as $businessHour) {
                $businessHours[] = $businessHour->toArray();
            }
        }

        return $businessHours;
    }

    /**
     * @param array $specialHours
     * @param array $parameters
     */
    public function setSpecialHours($specialHours, array $parameters = array())
    {
        $this->specialHours = [];

        if ($specialHours && is_array($specialHours)) {
            $data = [];

            foreach ($specialHours as $specialHour) {
                $specialHourEntity = new SpecialHourEntity($specialHour, $parameters);

                $data[$specialHourEntity->getStartDate()] = $specialHourEntity;
            }

            ksort($data);
            $this->specialHours = array_values($data);
        }
    }

    /**
     * @return array
     */
    public function getSpecialHours()
    {
        $specialHours = [];

        if ($this->specialHours) {
            foreach ($this->specialHours as $specialHour) {
                $specialHours[] = $specialHour->toArray();
            }
        }

        return $specialHours;
    }

    /**
     * @param array $locationExtraData
     */
    public function setLocationExtraData($locationExtraData)
    {
        $this->locationExtraData = [];

        if ($locationExtraData && is_array($locationExtraData)) {
            foreach ($locationExtraData as $extraData) {
                $this->locationExtraData[] = new ExtraDataEntity($extraData);
            }
        }
    }

    /**
     * @return array
     */
    public function getLocationExtraData()
    {
        $locationExtraData = [];

        if ($this->locationExtraData) {
            foreach ($this->locationExtraData as $extraData) {
                $locationExtraData[] = $extraData->toArray();
            }
        }

        return $locationExtraData;
    }

    /**
     * @param array $images
     */
    public function setImages($images)
    {
        $this->images = [];

        if ($images && is_array($images)) {
            foreach ($images as $image) {
                $this->images[] = new ImageEntity($image);
            }
        }
    }

    /**
     * @return array
     */
    public function getImages()
    {
        $images = [];

        if ($this->images) {
            foreach ($this->images as $image) {
                $images[] = $image->toArray();
            }
        }

        return $images;
    }

    /**
     * @param array $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @return array|null
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param array $services
     */
    public function setServices($services)
    {
        $this->services = $services;
    }

    /**
     * @return array|null
     */
    public function getServices()
    {
        return $this->services;
    }
    
    /**
     * @return array|null
     */
    public function getTranslatedSlug(): ?array
    {
        return $this->translatedSlug;
    }
    
    /**
     * @param array|null $translatedSlug
     */
    public function setTranslatedSlug(?array $translatedSlug): void
    {
        $this->translatedSlug = $translatedSlug;
    }
    
    
}
