<?php

namespace App\Service\Api\Entity;

use Zend\Code\Reflection\PropertyReflection;

abstract class AbstractEntity
{
    /**
     * AbstractEntity constructor.
     *
     * @param array $data
     * @param array $parameters
     */
    public function __construct(array $data = array(), array $parameters = array())
    {
        foreach ($data as $field => $value) {
            $method = 'set'.strtr(ucwords(strtr($field, ['_' => ' ', '.' => ' ', '\\' => ' '])), [' ' => '']);

            if (method_exists($this, $method)) {
                $this->$method($value, $parameters);
            }
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $reflection = new \ReflectionClass(static::class);

        $array = [];
        /** @var \ReflectionProperty $property */
        foreach ($reflection->getProperties(PropertyReflection::IS_PROTECTED) as $property) {
            $field = $property->getName();
            $method = 'get'.ucfirst($property->getName());

            if (method_exists($this, $method)) {
                $array[$field] = $this->$method();
            }
        }

        return $array;
    }
}
