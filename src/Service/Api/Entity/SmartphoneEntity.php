<?php

namespace App\Service\Api\Entity;

class SmartphoneEntity extends AbstractEntity
{
    /**
     * @var int|null
     */
    protected $sku;

    /**
     * @var string
     */
    protected $image;

    /**
     * @var string|null
     */
    protected $imageLowResolution;

    /**
     * @var string|null
     */
    protected $brand;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string|null
     */
    protected $model;

    /**
     * @var string|null
     */
    protected $internalStorage;

    /**
     * @var string|null
     */
    protected $color;

    /**
     * @var string|null
     */
    protected $screenSize;

    /**
     * @var string|null
     */
    protected $operatingSystem;

    /**
     * @var string|null
     */
    protected $operatingSystemVersion;

    /**
     * @var float|null
     */
    protected $camera;

    /**
     * @var float|null
     */
    protected $rearCamera;

    /**
     * @var float|null
     */
    protected $frontCamera;

    /**
     * @var string|null
     */
    protected $simType;

    /**
     * @var int
     */
    protected $price;

    /**
     * @var int|null
     */
    protected $bundlePrice;

    /**
     * @var int|null
     */
    protected $optionSmartphonePrice;

    /**
     * @var bool|null
     */
    protected $visibilityWeb;

    /**
     * @var string|null
     */
    protected $deviceRange;

    /**
     * @var int|null
     */
    protected $contractDuration;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var bool|array|null
     */
    protected $cashback;

    /**
     * @var bool|null
     */
    protected $isStar;

    /**
     * @var int|null
     */
    protected $qty;

    /**
     * @var bool|null
     */
    protected $inStock;

    /**
     * @return int|null
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param int|null $sku
     */
    public function setSku(?int $sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image)
    {
        $this->image = $image;
    }

    /**
     * @return string|null
     */
    public function getImageLowResolution()
    {
        return $this->imageLowResolution;
    }

    /**
     * @param string|null $imageLowResolution
     */
    public function setImageLowResolution(?string $imageLowResolution)
    {
        $this->imageLowResolution = $imageLowResolution;
    }

    /**
     * @return string|null
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param string|null $brand
     */
    public function setBrand(?string $brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param string|null $model
     */
    public function setModel(?string $model)
    {
        $this->model = $model;
    }

    /**
     * @return string|null
     */
    public function getInternalStorage()
    {
        return $this->internalStorage;
    }

    /**
     * @param string|null $internalStorage
     */
    public function setInternalStorage(?string $internalStorage)
    {
        $this->internalStorage = $internalStorage;
    }

    /**
     * @return string|null
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string|null $color
     */
    public function setColor(?string $color)
    {
        $this->color = $color;
    }

    /**
     * @return string|null
     */
    public function getScreenSize()
    {
        return $this->screenSize;
    }

    /**
     * @param string|null $screenSize
     */
    public function setScreenSize(?string $screenSize)
    {
        $this->screenSize = $screenSize;
    }

    /**
     * @return string|null
     */
    public function getOperatingSystem()
    {
        return $this->operatingSystem;
    }

    /**
     * @param string|null $operatingSystem
     */
    public function setOperatingSystem(?string $operatingSystem)
    {
        $this->operatingSystem = $operatingSystem;
    }

    /**
     * @return string|null
     */
    public function getOperatingSystemVersion()
    {
        return $this->operatingSystemVersion;
    }

    /**
     * @param string|null $operatingSystemVersion
     */
    public function setOperatingSystemVersion(?string $operatingSystemVersion)
    {
        $this->operatingSystemVersion = $operatingSystemVersion;
    }

    /**
     * @return float|null
     */
    public function getCamera()
    {
        return $this->camera;
    }

    /**
     * @param float|null $camera
     */
    public function setCamera(?float $camera)
    {
        $this->camera = $camera;
    }

    /**
     * @return float|null
     */
    public function getRearCamera()
    {
        return $this->rearCamera;
    }

    /**
     * @param float|null $rearCamera
     */
    public function setRearCamera(?float $rearCamera)
    {
        $this->rearCamera = $rearCamera;
    }

    /**
     * @return float|null
     */
    public function getFrontCamera()
    {
        return $this->frontCamera;
    }

    /**
     * @param float|null $frontCamera
     */
    public function setFrontCamera(?float $frontCamera)
    {
        $this->frontCamera = $frontCamera;
    }

    /**
     * @return string|null
     */
    public function getSimType()
    {
        return $this->simType;
    }

    /**
     * @param string|null $simType
     */
    public function setSimType(?string $simType)
    {
        $this->simType = $simType;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price)
    {
        $this->price = $price;
    }

    /**
     * @return int|null
     */
    public function getBundlePrice()
    {
        return $this->bundlePrice;
    }

    /**
     * @param int|null $bundlePrice
     */
    public function setBundlePrice(?int $bundlePrice)
    {
        $this->bundlePrice = $bundlePrice;
    }

    /**
     * @return int|null
     */
    public function getOptionSmartphonePrice()
    {
        return $this->optionSmartphonePrice;
    }

    /**
     * @param int|null $optionSmartphonePrice
     */
    public function setOptionSmartphonePrice(?int $optionSmartphonePrice)
    {
        $this->optionSmartphonePrice = $optionSmartphonePrice;
    }

    /**
     * @return bool|null
     */
    public function getVisibilityWeb()
    {
        return $this->visibilityWeb;
    }

    /**
     * @param bool|null $visibilityWeb
     */
    public function setVisibilityWeb($visibilityWeb)
    {
        $this->visibilityWeb = $visibilityWeb;
    }

    /**
     * @return string|null
     */
    public function getDeviceRange()
    {
        return $this->deviceRange;
    }

    /**
     * @param string|null $deviceRange
     */
    public function setDeviceRange(?string $deviceRange)
    {
        $this->deviceRange = $deviceRange;
    }

    /**
     * @return int|null
     */
    public function getContractDuration()
    {
        return $this->contractDuration;
    }

    /**
     * @param int|null $contractDuration
     */
    public function setContractDuration(?int $contractDuration)
    {
        $this->contractDuration = $contractDuration;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return bool|array|null
     */
    public function getCashback()
    {
        return $this->cashback;
    }

    /**
     * @param bool|array|null $cashback
     */
    public function setCashback($cashback)
    {
        $this->cashback = $cashback;
    }

    /**
     * @return bool|null
     */
    public function getIsStar()
    {
        return $this->isStar;
    }

    /**
     * @param bool|null $isStar
     */
    public function setIsStar($isStar)
    {
        $this->isStar = $isStar;
    }

    /**
     * @return int|null
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param int|null $qty
     */
    public function setQty(?int $qty)
    {
        $this->qty = $qty;
    }

    /**
     * @return bool|null
     */
    public function getInStock()
    {
        return $this->inStock;
    }

    /**
     * @param bool|null $inStock
     */
    public function setInStock($inStock)
    {
        $this->inStock = $inStock;
    }
}
