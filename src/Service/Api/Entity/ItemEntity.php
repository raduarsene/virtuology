<?php

namespace App\Service\Api\Entity;

use App\Service\Api\Entity\Item\AssetEntity;
use App\Service\Api\Entity\Item\CallToActionFacetEntity;
use App\Service\Api\Entity\Item\ContentFacetEntity;
use App\Service\Api\Entity\Item\PriceFacetEntity;
use App\Service\Api\Entity\Item\ValidityFacetEntity;

class ItemEntity extends AbstractEntity
{
    const TYPE_POST = 'POST';
    const TYPE_BANNER = 'BANNER';
    const TYPE_FOLDER = 'FOLDER';
    const TYPE_NATIONAL = 'NATIONAL';

    const DEFAULT_DATE_FORMAT = 'Y-m-d H:i:s';
    
    const ITEM_LANGUAGES = ['FRENCH' => 'fr', 'DUTCH' => 'nl', 'ENGLISH'=>'en', 'GERMAN'=>'de', 'ITALIAN'=>'it', 'SPANISH'=>'es', 'PORTUGUESE'=>'pt', 'POLISH'=>'pl'];

    /**
     * The ID of the item
     *
     * @var string
     */
    protected $id;

    /**
     * ID used by the customer
     *
     * @var string
     */
    protected $externalId;

    /**
     * The item type of this item
     *
     * @var string
     */
    protected $itemTypeExternalId;

    /**
     * Multiple tags as array with key 0 indexed
     *
     * @var array|null
     */
    protected $tags;

    /**
     * The language of the item. Must match the regular expression [a-z]{2}.
     *
     * @var string
     */
    protected $language;

    /**
     * Item status
     *
     * @var string
     */
    protected $status;

    /**
     * Slug of the item.
     *
     * @var string|null
     */
    protected $slug;

    /**
     * @var array|null
     */
    protected $extraData;

    /**
     * The channels on which this post can be posted as array with key 0 indexed.
     *
     * @var array|null
     */
    protected $channels;

    /**
     * The validity facet for this post.
     *
     * @var ContentFacetEntity|null
     */
    protected $contentFacet;

    /**
     * The validity facet for this post.
     *
     * @var ValidityFacetEntity|null
     */
    protected $validityFacet;

    /**
     * The price facet for this post.
     *
     * @var PriceFacetEntity|null
     */
    protected $priceFacet;

    /**
     * The call to action facet for this post.
     *
     * @var CallToActionFacetEntity|null
     */
    protected $callToActionFacet;

    /**
     * @var string|null
     */
    protected $itemTypeCategoryLabel;

    /**
     * The icon of the post.
     *
     * @var string|null
     */
    protected $icon;

    /**
     * @var AssetEntity|null
     */
    protected $assets;

    /**
     * Order of this item in the list of assets.
     *
     * @var int|null
     */
    protected $orderIndex;

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $itemTypeExternalId
     */
    public function setItemTypeExternalId($itemTypeExternalId)
    {
        $this->itemTypeExternalId = $itemTypeExternalId;
    }

    /**
     * @return string
     */
    public function getItemTypeExternalId()
    {
        return $this->itemTypeExternalId;
    }

    /**
     * @param array|null $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return array|null
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string|null $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string|null
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param array|null $extraData
     */
    public function setExtraData($extraData)
    {
        $this->extraData = $extraData;
    }

    /**
     * @return array|null
     */
    public function getExtraData()
    {
        return $this->extraData;
    }

    /**
     * @param array|null $channels
     */
    public function setChannels($channels)
    {
        $this->channels = $channels;
    }

    /**
     * @return array|null
     */
    public function getChannels()
    {
        return $this->channels;
    }

    /**
     * @param array|null $contentFacet
     */
    public function setContentFacet($contentFacet)
    {
        $this->contentFacet = new ContentFacetEntity($contentFacet ?: array());
    }

    /**
     * @return array
     */
    public function getContentFacet()
    {
        return $this->contentFacet ? $this->contentFacet->toArray() : [];
    }

    /**
     * @param array|null $validityFacet
     */
    public function setValidityFacet($validityFacet)
    {
        $this->validityFacet = new ValidityFacetEntity($validityFacet ?: array());
    }

    /**
     * @return array
     */
    public function getValidityFacet()
    {
        return $this->validityFacet ? $this->validityFacet->toArray() : [];
    }

    /**
     * @param array|null $priceFacet
     */
    public function setPriceFacet($priceFacet)
    {
        $this->priceFacet = new PriceFacetEntity($priceFacet ?: array());
    }

    /**
     * @return array
     */
    public function getPriceFacet()
    {
        return $this->priceFacet ? $this->priceFacet->toArray() : [];
    }

    /**
     * @param array|null $callToActionFacet
     */
    public function setCallToActionFacet($callToActionFacet)
    {
        $this->callToActionFacet = new CallToActionFacetEntity($callToActionFacet ?: array());
    }

    /**
     * @return array
     */
    public function getCallToActionFacet()
    {
        return $this->callToActionFacet ? $this->callToActionFacet->toArray() : [];
    }

    /**
     * @param string|null $itemTypeCategoryLabel
     */
    public function setItemTypeCategoryLabel($itemTypeCategoryLabel)
    {
        $this->itemTypeCategoryLabel = $itemTypeCategoryLabel;
    }

    /**
     * @return string|null
     */
    public function getItemTypeCategoryLabel()
    {
        return $this->itemTypeCategoryLabel;
    }

    /**
     * @param string $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @return string|null
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param array $assets
     */
    public function setAssets($assets)
    {
        $this->assets = [];

        if ($assets && is_array($assets)) {
            foreach ($assets as $asset) {
                $this->assets[] = new AssetEntity($asset);
            }
        }
    }

    /**
     * @return array
     */
    public function getAssets()
    {
        $assets = [];

        if ($this->assets) {
            foreach ($this->assets as $asset) {
                $assets[] = $asset->toArray();
            }
        }

        return $assets;
    }

    /**
     * @param int|null $orderIndex
     */
    public function setOrderIndex($orderIndex)
    {
        $this->orderIndex = $orderIndex;
    }

    /**
     * @return int|null
     */
    public function getOrderIndex()
    {
        return $this->orderIndex;
    }
}
