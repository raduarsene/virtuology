<?php

namespace App\Service\Api;

use App\Entity\Shop;
use App\Service\Api\Request\RoadRequest;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Response;

class TrustpilotService extends RequestService
{
    
    const API_URL = "https://api.trustpilot.com";
    const API_VERSION = "v1";
    
    /** @var string */
    private $tokenUrl;
    
    /** @var string */
    private $locationsUrl;
    
    /** @var string */
    private $reviewsUrl;
    
    /** @var string */
    private $trustPilotUser;
    
    /** @var string */
    private $trustPilotPass;
    
    public function __construct(int $apiRequestTimeout, string $trustPilotUser, string $trustPilotPass)
    {
        $this->trustPilotUser = $trustPilotUser;
        $this->trustPilotPass = $trustPilotPass;
        
        $this->tokenUrl = self::API_URL."/".self::API_VERSION."/"."oauth/oauth-business-users-for-applications/accesstoken";
        
        $this->locationsUrl = self::API_URL."/".self::API_VERSION."/"."private/business-units/{businessUnitId}/locations";
        
        $this->reviewsUrl = self::API_URL."/".self::API_VERSION."/"."business-units/{businessUnitId}/reviews";
        
        
        parent::__construct($apiRequestTimeout);
    }

    
    private function getAccessToken(Shop $shop)
    {
        $trustPilotDetails = $shop->getTrustPilotConf();
        $trustPilotDetails = json_decode($trustPilotDetails, true);
        
        $parameters = ["grant_type" =>"password",
                       "username" => $this->trustPilotUser,
                       "password" => $this->trustPilotPass,
                       "scope" => "password"
                       ];
        
        try {
            $response = $this->postRequestForm($this->tokenUrl,  $parameters, ['Content-Type' => 'application/x-www-form-urlencoded', 'Authorization'=>'Basic '.base64_encode($trustPilotDetails["apiKey"].":".$trustPilotDetails["apiSecretKey"])]);
        
            $statusCode = $response->getStatusCode();
        
            $response = $response->getBody()->getContents();
            $response = \GuzzleHttp\json_decode($response, true);
            $response = isset($response['access_token'])?$response['access_token']:'';
            
            
        } catch (GuzzleException $e) {
            $response = $e->getResponse();
            
            $statusCode = $response ? $response->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
        }
        
        return [$response, $statusCode];
    }
    
    /**
     * @param Shop $shop
     */
    public function getBusinessUnitLocations(Shop $shop )
    {
        $trustPilotDetails = $shop->getTrustPilotConf();
        $trustPilotDetails = json_decode($trustPilotDetails, true);
        $businessUnitId = $trustPilotDetails['businessUnitId'];
        $requestUrl = str_replace('{businessUnitId}', $businessUnitId, $this->locationsUrl);
    
    
        //1. get access token
        list($token, $statusCode) = $this->getAccessToken($shop);
    
    
        if($statusCode == Response::HTTP_OK && $token) {
            try {
                $response = $this->getRequest($requestUrl, [], ['Content-Type' => 'application/json', 'Authorization'=>'Bearer '.$token]);
        
                $statusCode = $response->getStatusCode();
        
                $response = $response->getBody()->getContents();
                $response = json_decode($response, true);
                
                //process locations here
        
            } catch (GuzzleException $e) {
                $response = $e->getResponse();
                $statusCode = $response ? $response->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
        
            }
        }
        
        return [$response, $statusCode];
        
    }
    
    public function getShopReviews(Shop $shop)
    {
    
        $trustPilotDetails = $shop->getTrustPilotConf();
        $trustPilotDetails = json_decode($trustPilotDetails, true);
        $businessUnitId = $trustPilotDetails['businessUnitId'];
        $requestUrl = str_replace('{businessUnitId}', $businessUnitId, $this->reviewsUrl);
    
    
       
        $reviews = [];
        $parameters = ['apikey'=>$trustPilotDetails['apiKey'], 'perPage'=>'100'];
        
        $this->requestForShopReviews($requestUrl, $parameters, $reviews);
        
        return $reviews;
        
        
    }
    
    private function requestForShopReviews($requestUrl, $parameters, &$reviews)
    {
        
        $nextRequest = false;
        
        try {
            
            $response = $this->getRequest($requestUrl, $parameters, ['Content-Type' => 'application/json']);
        
            $statusCode = $response->getStatusCode();
        
            $response = $response->getBody()->getContents();
            $response = json_decode($response, true);
            
            
        
            foreach ($response['reviews'] as $reviewDetails) {
            
                if(isset($reviews[$reviewDetails['location']['id']])) {
                    $reviews[$reviewDetails['location']['id']]['reviewsNo'] ++;
                    $reviews[$reviewDetails['location']['id']]['reviewsScore'] += $reviewDetails['stars'];
                    
                }
                else {
                    $reviews[$reviewDetails['location']['id']]['reviewsNo'] = 1;
                    $reviews[$reviewDetails['location']['id']]['reviewsScore'] = $reviewDetails['stars'];
                }
            }
            
            foreach ($response['links'] as $link) {
                if($link['rel'] == 'next-page') {
                    $nextPage = $link['href'];
                    $parts = parse_url($nextPage);
                    parse_str($parts['query'], $query);
                    if(!isset($parameters['page']) || $parameters['page'] < $query['page']) {
                        $parameters = array_merge($parameters, $query);
                        $nextRequest = true;
                    }
                    
                    break;
                }
            }
            
            if($nextRequest) {
                $this->requestForShopReviews($requestUrl, $parameters, $reviews);
            }
            
            
            //process locations here
        
        } catch (GuzzleException $e) {
            $response = $e->getResponse();
            $statusCode = $response ? $response->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
        
        }
        
    }
    
    
    
}
