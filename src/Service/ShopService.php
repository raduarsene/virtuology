<?php

namespace App\Service;

use App\Entity\Attribute;
use App\Entity\Shop;
use App\Entity\ShopDomain;
use App\Entity\ShopLocation;
use App\Entity\ShopLocationSlugHistory;
use App\Entity\ShopTemplate;
use App\Entity\Zone;
use App\Service\Api\ApiInterface;
use App\Service\Api\Entity\LocationEntity;
use App\Service\Api\Request\Location\V2Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ShopService
 *
 * @package App\Service
 */
class ShopService
{
    const NUMBER_OF_LOCATIONS_FOR_STRUCTURE = 50;

    /** @var EntityManagerInterface */
    private $em;

    /** @var ApiInterface */
    private $apiService;

    /** @var array */
    private $countryTimezone = array();

    /**
     * ShopService constructor.
     *
     * @param EntityManagerInterface $em
     * @param ApiInterface           $apiService
     */
    public function __construct(EntityManagerInterface $em, ApiInterface $apiService)
    {
        $this->em = $em;
        $this->apiService = $apiService;
    }

    /**
     * @param Shop           $shop
     * @param ShopDomain     $shopDomain
     * @param ShopTemplate[] $templates
     * @return array
     */
    public function getHomeData(Shop $shop, ShopDomain $shopDomain, array $templates): array
    {
        $data = [];

        $getLocations = false;
        foreach ($templates as $template) {
            if (in_array($template->getAttribute()->getBlock(), [Attribute::BLOCK_MAP, Attribute::BLOCK_LOCATIONS])) {
                $getLocations = true;
                break;
            }
        }

        if (!$getLocations) {
            return $data;
        }

        $shopLocations = $this->em->getRepository(ShopLocation::class)->findBy(['shop' => $shop], [], self::NUMBER_OF_LOCATIONS_FOR_STRUCTURE);
        /** @var ShopLocation $shopLocation */
        foreach ($shopLocations as $shopLocation) {
            if ($shopLocation->getName()) {
                
                $locationEntity = new LocationEntity($shopLocation->getDetailsArray(), ['datetimeFormat' => $shopDomain->getDatetimeFormat()]);

                $this->setLocationTimezone($locationEntity);
                
                //@todo check this
                //complete tp reviews here?
                $locationDetails = $locationEntity->toArray();
                $locationDetails['tpScore'] = $shopLocation->getTpScore();
                $locationDetails['tpReviewsNo'] = $shopLocation->getTpReviewsNo();

                $data['locations'][] = $locationDetails;
            }
        }

        return $data;
    }

    /**
     * @param ShopDomain    $shopDomain
     * @param string        $slug
     * @param string        $language
     * @param string|null   $externalId
     *
     * @return mixed
     */
    public function getShowData(ShopDomain $shopDomain, $slug, $language, $externalId = null)
    {
        $shop = $shopDomain->getShop();

        $locationData = $this->apiService->init($shop->getApiVersion(), $shopDomain)->getLocationBySlug([
            'size'       => 1,
            'siteId'     => $shop->getApiSiteId(),
            'slug'       => $slug,
            'externalId' => $externalId,
            'language'   => $language,
            'projection' => V2Request::DEFAULT_PROJECTION_FULL,
            'services'   => true,
        ]);

        $location = current($locationData);

        if (empty($location)) {
            throw new NotFoundHttpException("Location with slug: '$slug', not found!");
        }

        //@todo - check this
        //complete here trust pilot reviews
        $shopLocation = $this->em->getRepository(ShopLocation::class)->findBy(['shop'=>$shop, 'externalId'=>$location['externalId']]);
        if($shopLocation) {
            $shopLocation = $shopLocation[0];
            $location['tpScore'] = $shopLocation->getTpScore();
            $location['tpReviewsNo'] = $shopLocation->getTpReviewsNo();
        }
        
        return $location;
    }

    /**
     * @param ShopDomain $shopDomain
     * @param            $language
     *
     * @return mixed
     */
    public function getFiltersData(ShopDomain $shopDomain, $language)
    {
        $shop = $shopDomain->getShop();
        $filtersData =  $this->apiService->init($shop->getApiVersion(), $shopDomain)->getFiltersByCriteria([
            'siteId'     => $shop->getApiSiteId(),
            'language'   => $language,
        ]);

        $filters = current($filtersData);

        return $filters;
    }

    /**
     * @param Shop   $shop
     * @param string|null $slug
     * @param string|null $externalId
     * @return string|null
     */
    public function getShopLocationSlugFromHistory(Shop $shop, ?string $slug = null, ?string $externalId = null)
    {
        $shopLocationSlug = null;
        $shopLocationSlugHistory = null;

        
        if(!empty($slug)) {
            $shopLocationSlugHistory = $this->em->getRepository(ShopLocationSlugHistory::class)->findOneBy([
                'shop' => $shop,
                'slug' => $slug
            ]);
        }
        
        if (!$shopLocationSlugHistory && !empty($externalId)) {
            $shopLocationSlugHistory = $this->em->getRepository(ShopLocationSlugHistory::class)->findOneBy([
                'shop' => $shop,
                'externalId' => $externalId
            ]);
        }

        if ($shopLocationSlugHistory) {
            
            //try to find the new slug
            $shopLocation = $this->em->getRepository(ShopLocation::class)->findOneBy([
                'shop' => $shop,
                'externalId' => $shopLocationSlugHistory->getExternalId()
            ]);

            if ($shopLocation) {
                $shopLocationSlug = $shopLocation->getSlug();
            }
        }

        return $shopLocationSlug;
    }

    /**
     * @param LocationEntity $location
     */
    private function setLocationTimezone($location)
    {
        $address = $location->getAddress();

        if ($address && empty($address['timezone'])) {
            if (!empty($address['country'])) {
                if (empty($this->countryTimezone[$address['country']])) {
                    $zone = $this->em->getRepository(Zone::class)->findOneBy([
                        'countryCode' => $address['country'],
                        'main'        => true,
                    ]);

                    if ($zone) {
                        $this->countryTimezone[$address['country']] = $zone->getZoneName();
                    }
                }

                if (!empty($this->countryTimezone[$address['country']])) {
                    $address['timezone'] = $this->countryTimezone[$address['country']];
                }
            }

            if (empty($address['timezone'])) {
                $address['timezone'] = 'UTC';
            }

            $location->setAddress($address);
        }
    }
}
