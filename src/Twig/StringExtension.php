<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Twig\TwigFilter;

class StringExtension extends AbstractExtension
{
    
    /**
     * Returns a list of functions to add to the existing list.
     *
     * @return array An array of functions
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('remove_digits', [$this, 'removeDigits']),
        ];
    }
    
    /**
     * @return array|TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('phone_format', [$this, 'phoneNumberFormat']),
            new TwigFilter('phone_format_us', [$this, 'formatPhoneNumberUs']),
            new TwigFilter('street_number_us_format', [$this, 'formatStreetNumberUs']),
            new TwigFilter('street_number_fr_format', [$this, 'formatStreetNumberFr']),
            new TwigFilter('check_stripos', [$this, 'checkStripos']),
        ];
    }
    
    /**
     * @param string $name
     *
     * @return string
     */
    public function removeDigits(string $name): string
    {
        return preg_replace('/[^a-z\s]/i', '', $name);
    
    }
    
    /**
     * @param string $phone
     *
     * @return string
     */
    public function phoneNumberFormat(string $phone): string
    {
        $phoneFormat = str_replace('+33', '', $phone);
        $phoneFormat = "0".$phoneFormat;
        $phoneComp = str_split($phoneFormat, 2);
        
        $phoneFormat = implode(" ", $phoneComp);
        
        return $phoneFormat;
        
    }
    
    public function formatStreetNumberUs(string $streetName)
    {
        $comp = explode(",", $streetName);
        return trim($comp[1])." ".trim($comp[0]);
    }
    
    public function formatStreetNumberFr(string $streetName)
    {
        $streetName = preg_replace("/,+/", ",", $streetName);
        $comp = explode(",", $streetName);
        if(isset($comp[1])) {
            return trim($comp[1]).", ".trim($comp[0]);
        }
        else {
            return $streetName;
        }
        
    }
    
    public function formatPhoneNumberUs(string $phone)
    {
        
        if(strpos($phone, ")") !== false) {
            
            return $phone;
        }
        
        $phone = str_replace(' ', '', trim($phone));
        return "(".substr($phone, 0, 3).") ".substr($phone, 3, 3)."-".substr($phone, 6, 4);
    }
    
    
    public function checkStripos(string $name, string $brand)
    {
        
        if(mb_stripos($name, $brand)!==false) {
            return true;
        }
        
        return false;
    }
    
}
