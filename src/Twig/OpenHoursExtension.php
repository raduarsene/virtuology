<?php

namespace App\Twig;

use App\Entity\Shop;
use App\Entity\ShopDomain;
use App\Helper\DateTimeFormat;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class OpenHoursExtension extends AbstractExtension
{
    const TYPE_WEEK = 'week';
    const TYPE_EXCEPTION = 'exception';

    /** @var TranslatorInterface */
    protected $translator;

    /** Local proprieties */
    /** @var array */
    protected $_actions;
    /** @var array */
    protected $_location;
    /** @var ShopDomain */
    protected $_shopDomain;
    /** @var array */
    protected $_openHours;
    /** @var string */
    protected $_dateFormat;
    /** @var string */
    protected $_timezone;
    /** @var \DateTime */
    protected $_currentDate;
    /** @var int */
    protected $_currentWeekDay;
    /** @var string */
    protected $_currentHour;
    /** @var array */
    protected $_thisWeekDays;
    /** @var array */
    protected $_nextWeekDays;
    /** @var array */
    protected $_weekDays;
    /** @var array */
    protected $_allSpecialDays;

    /**
     * OpenHoursExtension constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Returns a list of functions to add to the existing list.
     *
     * @return array An array of functions
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('open_hours', [$this, 'getOpenHours']),
        ];
    }

    /**
     * @param array      $location
     * @param ShopDomain $shopDomain
     * @param string     $type
     * @return array
     */
    public function getOpenHours(array $location, ShopDomain $shopDomain, string $type = self::TYPE_WEEK)
    {
        $this->setActions();

        switch ($type) {
            case self::TYPE_EXCEPTION:
                $this->setOpenHoursException($location, $shopDomain);
                break;
            case Shop::CODE_CARREFOUR:
                $this->setOpenHoursCarrefour($location, $shopDomain);
                break;
            case self::TYPE_WEEK:
            default:
                $this->setOpenHours($location, $shopDomain);
        }

        return $this->_openHours;
    }

    protected function setActions()
    {
        $this->_actions = [
            'init'          => 'initAction',
            'weekDays'      => 'weekDaysAction',
            'specialHour'   => 'specialHourAction',
            'specialHours'  => 'specialHoursAction',
            'setOpensData'  => 'setOpensDataAction',
            'todayOpenHour' => 'todayOpenHourAction',
            'businessHour'  => 'businessHourAction',
            'businessHours' => 'businessHoursAction',
        ];
    }

    protected function setOpenHoursException(array $location, ShopDomain $shopDomain)
    {
        $this->_actions['specialHour'] = 'specialHourExceptionAction';

        $this->setOpenHours($location, $shopDomain);
    }

    protected function setOpenHoursCarrefour(array $location, ShopDomain $shopDomain)
    {
        $this->_actions['setOpensData'] = 'setCarrefourOpensDataAction';

        $this->setOpenHours($location, $shopDomain);
    }

    protected function setOpenHours(array $location, ShopDomain $shopDomain)
    {
        $this->{$this->_actions['init']}($location, $shopDomain);

        $this->{$this->_actions['weekDays']}();

        $this->{$this->_actions['specialHours']}();

        $this->{$this->_actions['businessHours']}();
    }

    protected function initAction(array $location, ShopDomain $shopDomain)
    {
        $this->_location = $location;
        $this->_shopDomain = $shopDomain;

        $this->_dateFormat = DateTimeFormat::getDateFormat($shopDomain->getDatetimeFormat());
        $this->_timezone = !empty($location['address']['timezone']) ? $location['address']['timezone'] : 'UTC';
        $this->_currentDate = new \DateTime('now', new \DateTimeZone($this->_timezone));
        $this->_currentWeekDay = (int)$this->_currentDate->format('N');
        $this->_currentHour = $this->_currentDate->format('Hi');

        $this->_allSpecialDays = [];
        $this->_thisWeekDays = $this->_nextWeekDays = [];

        $this->_openHours = [
            'opens'            => [
                'isOpenNow' => false,
                'sentence'  => $this->translator->trans('app.web.global.open_hours.shop_close'),
                'openTime'  => '',
                'closeTime' => '',
            ],
            'currentWeekDay'   => $this->_currentWeekDay,
            'weekOpenHours'    => [],
            'weekSpecialHours' => [],
            'specialHours'     => ['open' => [], 'closed' => []],
        ];
    }

    protected function weekDaysAction()
    {
        $weekDate = new \DateTime('now', new \DateTimeZone($this->_timezone));

        $this->_weekDays = [];
        // Init the week special hours with date of the next 7 days
        for ($dayNumber = 1; $dayNumber <= 7; $dayNumber++) {
            $this->_weekDays[$weekDate->format('Y-m-d')] = (int)$weekDate->format('N');

            $weekDate->modify('+1 day');
        }
    }

    protected function specialHoursAction()
    {
        if (!empty($this->_location['specialHours'])) {
            foreach ($this->_location['specialHours'] as $specialHour) {
                $specialDate = new \DateTime($specialHour['startDate'], new \DateTimeZone($this->_timezone));
                $specialEndDate = new \DateTime($specialHour['endDate'], new \DateTimeZone($this->_timezone));

                for (; $specialDate->format('Y-m-d') <= $specialEndDate->format('Y-m-d'); $specialDate->modify('+1 day')) {
                    $this->{$this->_actions['specialHour']}($specialHour, $specialDate);
                }
            }
        }
    }

    protected function specialHourAction(array $specialHour, \DateTime $specialDate)
    {
        $specialDateString = $specialDate->format('Y-m-d');
        if ($specialDateString >= $this->_currentDate->format('Y-m-d')) {
            $this->_allSpecialDays[$specialDateString] = $specialHour;

            if (isset($this->_weekDays[$specialDateString])) {
                $this->_openHours['weekSpecialHours'][$this->_weekDays[$specialDateString]] = $specialHour;

                if (!$specialHour['closed']) {
                    if ($this->_weekDays[$specialDateString] > $this->_currentWeekDay) {
                        $this->_thisWeekDays[] = $this->_weekDays[$specialDateString];
                    } elseif ($this->_weekDays[$specialDateString] < $this->_currentWeekDay) {
                        $this->_nextWeekDays[] = $this->_weekDays[$specialDateString];
                    }
                }
            }
        }
    }

    protected function specialHourExceptionAction(array $specialHour, \DateTime $specialDate)
    {
        if ($this->_currentDate->format('Y-m-d') <= $specialDate->format('Y-m-d')) {
            if ($specialHour['closed']) {
                $this->_openHours['specialHours']['closed'][] = $this->translator->trans('app.web.global.open_hours.days.'
                    .$specialDate->format('N')).' '.$specialDate->format($this->_dateFormat);
            } else {
                $specialOpenHours = $specialHour['openTimeFormat'].' - '.$specialHour['closeTimeFormat'];

                // Check additional special hours
                if (!empty($specialHour['additionalOpenTime']) && !empty($specialHour['additionalCloseTime'])) {
                    $specialOpenHours .= ' | '.$specialHour['additionalOpenTimeFormat']
                        .' - '.$specialHour['additionalCloseTimeFormat'];
                }

                $this->_openHours['specialHours']['open'][] = $this->translator->trans('app.web.global.open_hours.days.'
                    .$specialDate->format('N')).' '.$specialDate->format($this->_dateFormat)
                    .' ('.$specialOpenHours.')';
            }
        }

        $this->specialHourAction($specialHour, $specialDate);
    }

    protected function businessHoursAction()
    {
        if (!empty($this->_location['businessHours'])) {
            // Get working days business hours
            foreach ($this->_location['businessHours'] as $businessHour) {
                $this->{$this->_actions['businessHour']}($businessHour);
            }

            $this->{$this->_actions['todayOpenHour']}();
        }
    }

    protected function businessHourAction(array $businessHour)
    {
        if (!isset($this->_openHours['weekOpenHours'][$businessHour['startDay']][$businessHour['openTime'].'_'.$businessHour['closeTime']])) {
            $this->_openHours['weekOpenHours'][$businessHour['startDay']][$businessHour['openTime'].'_'.$businessHour['closeTime']] = $businessHour;

            if (!isset($this->_openHours['weekSpecialHours'][$businessHour['startDay']])) {
                if ($businessHour['startDay'] > $this->_currentWeekDay) {
                    $this->_thisWeekDays[] = $businessHour['startDay'];
                } elseif ($businessHour['startDay'] < $this->_currentWeekDay) {
                    $this->_nextWeekDays[] = $businessHour['startDay'];
                }
            }
        }
    }

    protected function todayOpenHourAction()
    {
        $todayOpenTime = $todayOpenTimeFormat = $todayCloseTime = $todayCloseTimeFormat = '';
        if (isset($this->_openHours['weekSpecialHours'][$this->_currentWeekDay])) {
            // Check if today is a special open day
            if (!$this->_openHours['weekSpecialHours'][$this->_currentWeekDay]['closed']) {
                $todayOpenTime = $this->_openHours['weekSpecialHours'][$this->_currentWeekDay]['openTime'];
                $todayOpenTimeFormat = $this->_openHours['weekSpecialHours'][$this->_currentWeekDay]['openTimeFormat'];
                $todayCloseTime = $this->_openHours['weekSpecialHours'][$this->_currentWeekDay]['closeTime'];
                $todayCloseTimeFormat = $this->_openHours['weekSpecialHours'][$this->_currentWeekDay]['closeTimeFormat'];

                // Check additional special hours
                if (!empty($this->_openHours['weekSpecialHours'][$this->_currentWeekDay]['additionalOpenTime'])
                    && $this->_openHours['weekSpecialHours'][$this->_currentWeekDay]['additionalOpenTime'] < $todayOpenTime
                ) {
                    $todayOpenTime = $this->_openHours['weekSpecialHours'][$this->_currentWeekDay]['additionalOpenTime'];
                    $todayOpenTimeFormat = $this->_openHours['weekSpecialHours'][$this->_currentWeekDay]['additionalOpenTimeFormat'];
                }

                if (!empty($this->_openHours['weekSpecialHours'][$this->_currentWeekDay]['additionalCloseTime'])
                    && $this->_openHours['weekSpecialHours'][$this->_currentWeekDay]['additionalCloseTime'] > $todayCloseTime
                ) {
                    $todayCloseTime = $this->_openHours['weekSpecialHours'][$this->_currentWeekDay]['additionalCloseTime'];
                    $todayCloseTimeFormat = $this->_openHours['weekSpecialHours'][$this->_currentWeekDay]['additionalCloseTimeFormat'];
                }
            }
        } elseif (isset($this->_openHours['weekOpenHours'][$this->_currentWeekDay])) {
            // Check today usual open hours
            foreach ($this->_openHours['weekOpenHours'][$this->_currentWeekDay] as $openHour) {
                if (!$todayOpenTime || $openHour['openTime'] < $todayOpenTime) {
                    $todayOpenTime = $openHour['openTime'];
                    $todayOpenTimeFormat = $openHour['openTimeFormat'];
                }

                if (!$todayCloseTime || $openHour['closeTime'] > $todayCloseTime) {
                    $todayCloseTime = $openHour['closeTime'];
                    $todayCloseTimeFormat = $openHour['closeTimeFormat'];
                }
            }
        }

        // Check next day when will be opened
        $nextOpenDate = $this->_currentDate;
        $nextOpenDay = $this->_currentWeekDay;
        if ($this->_thisWeekDays || $this->_nextWeekDays) {
            // check this week open date
            if ($this->_thisWeekDays) {
                $nextOpenDay = min($this->_thisWeekDays);
            } elseif ($this->_nextWeekDays) {
                $nextOpenDay = min($this->_nextWeekDays);
            }

            $nextOpenDate = new \DateTime(array_search($nextOpenDay, $this->_weekDays), new \DateTimeZone($this->_timezone));
        } else {
            // check future weeks open date
            $possibleOpenDate = new \DateTime('+7 days', new \DateTimeZone($this->_timezone));
            $endOpenDate = new \DateTime('+1 year', new \DateTimeZone($this->_timezone));
            for (; $possibleOpenDate->format('Y-m-d') <= $endOpenDate->format('Y-m-d'); $possibleOpenDate->modify('+1 day')) {
                $openDateString = $possibleOpenDate->format('Y-m-d');
                $openDateDay = (int)$possibleOpenDate->format('N');
                if (isset($this->_openHours['weekOpenHours'][$openDateDay]) && !isset($this->_allSpecialDays[$openDateString])
                    || isset($this->_allSpecialDays[$openDateString]) && !$this->_allSpecialDays[$openDateString]['closed']
                ) {
                    $nextOpenDate = $possibleOpenDate;
                    $nextOpenDay = $openDateDay;
                    break;
                }
            }
        }

        $nextOpenTime = $nextOpenTimeFormat = $nextCloseTime = $nextCloseTimeFormat = '';
        $nextOpenDateString = $nextOpenDate->format('Y-m-d');
        if (isset($this->_allSpecialDays[$nextOpenDateString])
            && !$this->_allSpecialDays[$nextOpenDateString]['closed']
        ) {
            $nextOpenTime = $this->_allSpecialDays[$nextOpenDateString]['openTime'];
            $nextOpenTimeFormat = $this->_allSpecialDays[$nextOpenDateString]['openTimeFormat'];
            $nextCloseTime = $this->_allSpecialDays[$nextOpenDateString]['closeTime'];
            $nextCloseTimeFormat = $this->_allSpecialDays[$nextOpenDateString]['closeTimeFormat'];

            // Check additional special hours
            if (!empty($this->_allSpecialDays[$nextOpenDateString]['additionalOpenTime'])
                && $this->_allSpecialDays[$nextOpenDateString]['additionalOpenTime'] < $nextOpenTime
            ) {
                $nextOpenTime = $this->_allSpecialDays[$nextOpenDateString]['additionalOpenTime'];
                $nextOpenTimeFormat = $this->_allSpecialDays[$nextOpenDateString]['additionalOpenTimeFormat'];
            }

            if (!empty($this->_allSpecialDays[$nextOpenDateString]['additionalCloseTime'])
                && $this->_allSpecialDays[$nextOpenDateString]['additionalCloseTime'] > $nextCloseTime
            ) {
                $nextCloseTime = $this->_allSpecialDays[$nextOpenDateString]['additionalCloseTime'];
                $nextCloseTimeFormat = $this->_allSpecialDays[$nextOpenDateString]['additionalCloseTimeFormat'];
            }
        } elseif (isset($this->_openHours['weekOpenHours'][$nextOpenDay])) {
            foreach ($this->_openHours['weekOpenHours'][$nextOpenDay] as $openHour) {
                if (!$nextOpenTime || $openHour['openTime'] < $nextOpenTime) {
                    $nextOpenTime = $openHour['openTime'];
                    $nextOpenTimeFormat = $openHour['openTimeFormat'];
                }

                if (!$nextCloseTime || $openHour['closeTime'] > $nextCloseTime) {
                    $nextCloseTime = $openHour['closeTime'];
                    $nextCloseTimeFormat = $openHour['closeTimeFormat'];
                }
            }
        }

        $this->{$this->_actions['setOpensData']}([
            'todayOpenTime' => $todayOpenTime,
            'todayOpenTimeFormat' => $todayOpenTimeFormat,
            'todayCloseTime' => $todayCloseTime,
            'todayCloseTimeFormat' => $todayCloseTimeFormat,
            'nextOpenDate' => $nextOpenDate,
            'nextOpenTime' => $nextOpenTime,
            'nextOpenTimeFormat' => $nextOpenTimeFormat,
            'nextCloseTime' => $nextCloseTime,
            'nextCloseTimeFormat' => $nextCloseTimeFormat,
        ]);
    }

    protected function setOpensDataAction($data)
    {
        if ($data['todayOpenTime'] && $this->_currentHour < $data['todayOpenTime']) {
            $this->_openHours['opens'] = [
                'isOpenNow' => false,
                'sentence'  => $this->translator->trans('app.web.global.open_hours.shop_close_sentence').' '
                    .$data['todayOpenTimeFormat'],
                'openTime'  => $data['todayOpenTimeFormat'],
                'closeTime' => $data['todayCloseTimeFormat'],
            ];
        } elseif ($data['todayCloseTime'] && $this->_currentHour <= $data['todayCloseTime']) {
            $this->_openHours['opens'] = [
                'isOpenNow' => true,
                'sentence'  => $this->translator->trans('app.web.global.open_hours.shop_open_sentence')
                    .' '.$data['todayCloseTimeFormat'],
                'openTime'  => $data['todayOpenTimeFormat'],
                'closeTime' => $data['todayCloseTimeFormat'],
            ];
        } else {
            $this->_openHours['opens'] = [
                'isOpenNow' => false,
                'sentence'  => $this->translator->trans('app.web.global.open_hours.shop_close_sentence').' '
                    .$data['nextOpenTimeFormat'].' '
                    .$this->translator->trans('app.web.global.open_hours.days_short.'.$data['nextOpenDate']->format('N')),
                'openTime'  => $data['nextOpenTimeFormat'],
                'closeTime' => $data['nextCloseTimeFormat'],
            ];

            // if doesn't open this week
            if (!isset($this->_weekDays[$data['nextOpenDate']->format('Y-m-d')])) {
                $this->_openHours['opens']['sentence'] .= ' '.$data['nextOpenDate']->format($this->_dateFormat);
            }
        }
    }

    protected function setCarrefourOpensDataAction($data)
    {
        if ($data['todayOpenTime'] && $this->_currentHour < $data['todayOpenTime']) {
            $this->_openHours['opens'] = [
                'isOpenNow' => false,
                'sentence'  => $this->translator->trans('app.web.show.sidebar.now_closed', [], Shop::CODE_CARREFOUR)
                    .' '.$this->translator->trans('app.web.show.sidebar.today', [], Shop::CODE_CARREFOUR)
                    .' '.$this->translator->trans('app.web.show.sidebar.from', [], Shop::CODE_CARREFOUR),
                'openTime'  => $data['todayOpenTimeFormat'],
                'closeTime' => $data['todayCloseTimeFormat'],
            ];
        } elseif ($data['todayCloseTime'] && $this->_currentHour <= $data['todayCloseTime']) {
            $this->_openHours['opens'] = [
                'isOpenNow' => true,
                'sentence'  => $this->translator->trans('app.web.show.sidebar.now_open', [], Shop::CODE_CARREFOUR),
                'openTime'  => $data['todayOpenTimeFormat'],
                'closeTime' => $data['todayCloseTimeFormat'],
            ];
        } else {
            $this->_openHours['opens'] = [
                'isOpenNow' => false,
                'sentence'  => $this->translator->trans('app.web.show.sidebar.now_closed', [], Shop::CODE_CARREFOUR),
                'openTime'  => $data['nextOpenTimeFormat'],
                'closeTime' => $data['nextCloseTimeFormat'],
            ];

            if ((int)$data['nextOpenDate']->format('N') === $this->_currentWeekDay + 1) {
                $this->_openHours['opens']['sentence'] .= ' '.$this->translator->trans('app.web.show.sidebar.tomorrow', [], Shop::CODE_CARREFOUR)
                    .' '.$this->translator->trans('app.web.show.sidebar.from', [], Shop::CODE_CARREFOUR);
            } else {
                $this->_openHours['opens']['sentence'] .= ' '.$this->translator->trans('app.web.global.open_hours.days.'.$data['nextOpenDate']->format('N'))
                    .' '.$this->translator->trans('app.web.show.sidebar.from', [], Shop::CODE_CARREFOUR);

                // if doesn't open this week
                if (!isset($this->_weekDays[$data['nextOpenDate']->format('Y-m-d')])) {
                    $this->_openHours['opens']['sentence'] .= ' '.$data['nextOpenDate']->format($this->_dateFormat);
                }
            }
        }
    }
}
