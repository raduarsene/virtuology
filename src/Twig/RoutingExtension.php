<?php

namespace App\Twig;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class RoutingExtension extends AbstractExtension
{
    const QUERY_NO_HEADER = 'noHeader';
    const QUERY_NO_FOOTER = 'noFooter';

    private $generator;
    private $requestStack;

    public function __construct(UrlGeneratorInterface $generator, RequestStack $requestStack)
    {
        $this->generator = $generator;
        $this->requestStack = $requestStack;
    }

    /**
     * Returns a list of functions to add to the existing list.
     *
     * @return array An array of functions
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('path_query', [$this, 'getPathQuery']),
            new TwigFunction('url_https', [$this, 'getUrlHttps']),
        ];
    }

    /**
     * @param string $name
     * @param array  $parameters
     * @param bool   $relative
     * @return string
     */
    public function getPathQuery(string $name, array $parameters = [], bool $relative = false): string
    {
        $request = $this->requestStack->getCurrentRequest();

        if ($request->query->has(self::QUERY_NO_HEADER)) {
            $parameters[self::QUERY_NO_HEADER] = $request->query->get(self::QUERY_NO_HEADER);
        }

        if ($request->query->has(self::QUERY_NO_FOOTER)) {
            $parameters[self::QUERY_NO_FOOTER] = $request->query->get(self::QUERY_NO_FOOTER);
        }

        return $this->generator->generate($name, $parameters, $relative ? UrlGeneratorInterface::RELATIVE_PATH : UrlGeneratorInterface::ABSOLUTE_PATH);
    }

    /**
     * @param string $name
     * @param array  $parameters
     * @return string
     */
    public function getUrlHttps(string $name, array $parameters = []): string
    {
        return 'https:'.$this->generator->generate($name, $parameters, UrlGeneratorInterface::NETWORK_PATH);
    }
}
