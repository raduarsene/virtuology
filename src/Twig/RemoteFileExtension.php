<?php

namespace App\Twig;

use App\Entity\Language;
use App\Entity\Shop;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\RequestOptions;

class RemoteFileExtension extends AbstractExtension
{
    const REMOTE_DEFAULT = 'default_remote';
    const REMOTE_TEMPORARY = 'temp_remote';

    private $requestStack;
    private $router;
    private $projectDir;

    public function __construct(RequestStack $requestStack, UrlGeneratorInterface $router, string $projectDir)
    {
        $this->requestStack = $requestStack;
        $this->router = $router;
        $this->projectDir = $projectDir;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('remote_content', [$this, 'getRemoteContent']),
            new TwigFunction('remote_assets', [$this, 'getRemoteAssets']),
        ];
    }

    /**
     * @param Shop   $shop
     * @param string $url
     * @param string $type
     * @return string
     * @throws \Exception
     */
    public function getRemoteContent(Shop $shop, string $url, string $type): string
    {
        try {
            $remoteFileContent = $this->getExternalFileContent($shop, $url, $type);

            return $this->getContentByShop($shop, $remoteFileContent, $url, $type);
        } catch (\Exception $e) {
            return $e->getMessage();
            
        }
    }

    /**
     * @param Shop   $shop
     * @param string $url
     * @param string $type
     * @return array
     * @throws \Exception
     */
    public function getRemoteAssets(Shop $shop, string $url, string $type): array
    {
        $assets = [];
        
        try {
            $remoteFileContent = $this->getExternalFileContent($shop, $url, $type);
    
            if (!empty($remoteFileContent)) {
                try {
                    $matches = [];
                    preg_match_all('/<script( type="text\/javascript")? src=("|\')(.*?)("|\')>(.*?)<\/script>/is', $remoteFileContent, $matches);
                    if(isset($matches[3])) {
        
                        $assets['js'] = $matches[3];
                    }
    
                    $matches = [];
                    preg_match_all('/<link(.*?)href="(.*?)" type=(.*?)>/is', $remoteFileContent, $matches);
    
                    if(isset($matches[2])) {
                        $assets['css'] = $matches[2];
                    }
                } catch (\Exception $e) {
                    throw new \Exception("Error getting the assets");
                }
            } else {
//                throw new \Exception("Trying to parse empty file for assets");
                return $assets;
            }

        }catch (\Exception $e) {
//            throw new \Exception("Trying to parse empty file for assets");
            return $assets;
        }

        return $assets;
    }

    /**
     * @param Shop   $shop
     * @param string $url
     * @param string $type
     * @return false|string
     * @throws \Exception
     */
    private function getExternalFileContent(Shop $shop, string $url, string $type)
    {
        $filesystem = new Filesystem();
        $request = $this->requestStack->getCurrentRequest();

        $templateDir = $this->projectDir.'/templates';
        $shopTemplatePath = '/web/shop/'.$shop->getCode();

        $fileName = $type.'_'.$request->getLocale().'.html.twig';

        try {
            $temporaryPath = $shopTemplatePath.'/'.self::REMOTE_TEMPORARY.'/';
            if (!$filesystem->exists($templateDir.$temporaryPath)) {
                $filesystem->mkdir($templateDir.$temporaryPath);
            }
        } catch (\Exception $e) {
            throw new \Exception("Trying to create temp directory to save remote content");
        }

        $remoteTemplate = $temporaryPath.$fileName;

        if ($filesystem->exists($templateDir.$remoteTemplate)) {
            $yesterday = new \DateTime('-1 day');
            $fileModifiedDateTime = new \DateTime(date('Y-m-d H:i:s', filemtime($templateDir.$remoteTemplate)));

            // If remote file exists and is older than 1 day
            if ($fileModifiedDateTime < $yesterday) {
                try {
                    $content = $this->getUrlContent($url);

                    // update existing remote file
                    $filesystem->dumpFile($templateDir.$remoteTemplate, $content);
                } catch (GuzzleException $exception) {
                    
                    $remoteTemplate = $shopTemplatePath.'/'.self::REMOTE_DEFAULT.'/'.$fileName;
                }
            }
        } else {
            try {
                $content = $this->getUrlContent($url);

                // create a new remote file
                $filesystem->dumpFile($templateDir.$remoteTemplate, $content);
            } catch (GuzzleException $exception) {
                // return default remote file on url request error
                $remoteTemplate = $shopTemplatePath.'/'.self::REMOTE_DEFAULT.'/'.$fileName;
            }
        }

        try {
            return file_get_contents($templateDir.$remoteTemplate);
        } catch (\Exception $e) {
            throw new \Exception("No content found");
        }
    }

    /**
     * @param string $url
     * @return string
     * @throws GuzzleException
     */
    private function getUrlContent(string $url): string
    {
        $client = new Client();
        
        preg_match('/\/\/(.*)\:(.*)@(.*)/', $url, $matches);
        
        $curlOptions = [
            CURLOPT_USERAGENT      => 'Mobilosoft; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)',
            CURLOPT_FAILONERROR    => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_AUTOREFERER    => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_BINARYTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_CONNECTTIMEOUT => 5,
        ];
        if(isset($matches[1]) && isset($matches[2]) && !empty($matches[1]) && !empty($matches[2]) ) {
            
            $curlOptions[CURLOPT_USERPWD] = "{$matches[1]}:{$matches[2]}";
        }
        
        $response = $client->request(
            Request::METHOD_GET,
            $url,
            [
                'curl' => $curlOptions,
                RequestOptions::CONNECT_TIMEOUT => 2,
                RequestOptions::TIMEOUT => 5,
                RequestOptions::READ_TIMEOUT => 5
            ]
        );

        return $response->getBody()->getContents();
    }
    
    /**
     * @param Shop   $shop
     * @param string $content
     * @param string $url
     * @param string $type
     *
     * @return string
     */
    private function getContentByShop(Shop $shop, string $content, string $url, string  $type = 'header'): string
    {
        $request = $this->requestStack->getCurrentRequest();
        
        
        switch ($shop->getCode()) {
            
            case Shop::CODE_CARREFOUR:
                if($type == "header") {
                    
                    $content = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $content);
                    $content = preg_replace('#<link(.*?)(.*?) type=(.*?)>#is', '', $content);
                }
                
                break;
            
            case Shop::CODE_MEDIMARKET:
                $nextLanguage = $request->getLocale() === Language::FRENCH ? Language::DUTCH : Language::FRENCH;
                $currentUrl = $this->router->generate(
                    $request->attributes->get('_route'),
                    ['_locale' => $nextLanguage] + $request->attributes->get('_route_params'),
                    UrlGeneratorInterface::ABSOLUTE_URL
                );

                $content = str_replace(
                    [$url, str_replace($request->getLocale().'/', $nextLanguage.'/', $url)],
                    $currentUrl,
                    $content
                );
                $content = str_replace(["<h1", "</h1>"], ["<div", "</div>"], $content);
                break;
                
            case Shop::CODE_LEONDEBRUXELLES:
                
                switch ($type) {
                    case 'footer':
                        $crawler = new Crawler($content);
    
                        $footerContent = "";
    
                        $footer = $crawler->filter('div[class="fb-block"]');
                        $footerHtml = $footer->html();
                        $footerHtml = preg_replace('#(href|src)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$footerHtml);
                        $footerContent.='<div class="fb-block">'.$footerHtml.'</div>';
    
    
                        $footer = $crawler->filter('div[class="footer"]');
                        $footerHtml = $footer->html();
                        $footerHtml = preg_replace('#(href|src)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$footerHtml);
                        $footerContent.='<div class="footer">'.$footerHtml.'</div>';
    
                        $footer = $crawler->filter('div[class="footer-bottom"]');
                        $footerHtml = $footer->html();
                        $footerHtml = preg_replace('#(href|src)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$footerHtml);
                        $footerContent.='<div class="footer-bottom">'.$footerHtml.'</div>';
                        
                        $content = $footerContent;
    
    
                        break;
                        
                    case 'header':
                        $crawler = new Crawler($content);
                        
                        //save header
                        $headerContent  = "";
                        $headerContentDesktop = "";
                        $header = $crawler->filter('div[class="wrapper-header-mobile visible-xs visible-sm"]');
                        $headerHtml = $header->html();
                        $headerHtml = preg_replace('#(href|src)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                        $headerContent .= '<div class="wrapper-header-mobile visible-xs visible-sm">'.$headerHtml.'</div>';
    
    
                        $header = $crawler->filter('div[class="topbar"]');
                        $headerHtml = $header->html();
                        $headerHtml = preg_replace('#(href|src)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                        $headerContentDesktop .= '<div class="topbar">'.$headerHtml.'</div>';
    
    
                        $header = $crawler->filter('div[id="sticky-menu"]');
                        $headerHtml = $header->html();
                        $headerHtml = preg_replace('#(href|src)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                        $headerContentDesktop .= '<div id="sticky-menu-sticky-wrapper" class="sticky-wrapper"><div class="sticky-menu nav clearfix" id="sticky-menu">'.$headerHtml.'</div></div>';
    
    
                        $headerContentDesktop = '<div class="header hidden-sm hidden-xs homepage"><div class="wrapper wrapper-topbar">'.$headerContentDesktop.'</div></div>';
    
                        $headerContent .= $headerContentDesktop;
                        
                        
                        $content = $headerContent;
    
                        $nextLanguage = $request->getLocale() === Language::FRENCH ? Language::ENGLISH : Language::FRENCH;
                        $currentUrl = $this->router->generate(
                            $request->attributes->get('_route'),
                            ['_locale' => $nextLanguage] + $request->attributes->get('_route_params'),
                            UrlGeneratorInterface::ABSOLUTE_URL
                        );
                        
                        //language switch
                        if($nextLanguage == Language::FRENCH) {
                            
                            $content = str_replace(["https://www.leon-de-bruxelles.fr"], [$currentUrl], $content);
                        }
                        else {
                            $content = str_replace(["https://www.leon-de-bruxelles.fr/".strtolower($nextLanguage)."/"], [$currentUrl], $content);
                        }
                        
                        
                        break;
                }
                
                break;
                
            case Shop::CODE_LLOYDSPHARMA:
                switch ($type) {
                    
                    case 'header':
                        
                        
                        $crawler = new Crawler($content);
    
                        $urlLinks = str_replace("?lang=".$request->getLocale()."_BE", "", $url);
                        
                        //save header
                        $headerContent  = "";
                        $header = $crawler->filter('header[class="main-header"]');
                        $headerHtml = $header->html();
                        $headerHtml = preg_replace('#(href|src)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$urlLinks.'$2$3',$headerHtml);
                        $headerHtml = str_replace('data-src', 'src', $headerHtml);
                        $headerContent .= '<header id="main-header" class="main-header">'.$headerHtml.'</header>';
    
                        
                        $headerContent .= '<div class="header-placeholder"></div><div class="yCmsContentSlot container bottom-header-slot"></div>';
                        
                        
                        $content = $headerContent;
                        
                        
                        break;
                        
                    case 'footer':
    
                        $crawler = new Crawler($content);
    
                        $urlLinks = str_replace("?lang=".$request->getLocale()."_BE", "", $url);
                        
                        $footerContent = "";
                        
                        $footer = $crawler->filter('footer[class="main-footer"]');
                        $footerHtml = $footer->html();
                        $footerHtml = str_replace('data-src', 'src', $footerHtml);
                        $footerHtml = preg_replace('#(href|src)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$urlLinks.'$2$3',$footerHtml);
                        $footerContent.='<footer class="main-footer">'.$footerHtml.'</footer>';
    
                        $content = $footerContent;
    
                        break;
                }
                break;
                
            case Shop::CODE_LOLALIZA:
    
                switch ($type) {
    
                    case 'header':
                        $crawler = new Crawler($content);
                        
                        $url = "https://www.lolaliza.com";
                        
                        $headerContent  = "";
                        $header = $crawler->filter('header[class="header-site header-desktop"]');
                        $headerHtml = $header->html();
                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                        $headerContent .= '<header  class="header-site header-desktop">'.$headerHtml.'</header>';
                        
                        $content = $headerContent;
                        
                        break;
                        
                        
                    case 'footer':
                        $crawler = new Crawler($content);
    
                        $url = "https://www.lolaliza.com";
                        
                        
                        $headerContent  = "";
                        $header = $crawler->filter('footer[class="footer-site footer-desktop container-full"]');
                        $headerHtml = $header->html();
                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                        $headerContent .= '<footer  class="footer-site footer-desktop container-full">'.$headerHtml.'</footer>';
    
                        $content = $headerContent;
                        break;
                }
                
                break;
                
            case Shop::CODE_MOBILOSOFT_TEST:
                
                switch ($type) {
        
                    case 'header':
                        $crawler = new Crawler($content);
            
            
                        $headerContent  = "";
                        $header = $crawler->filter('div[class="header-container-wrapper"]');
                        $headerHtml = $header->html();
                        $headerContent .= '<div  class="header-container-wrapper">'.$headerHtml.'</div>';
            
                        $content = $headerContent;
            
                        break;
        
        
                    case 'footer':
                        $crawler = new Crawler($content);
            
                        $headerContent  = "";
                        $header = $crawler->filter('div[class="footer-container-wrapper"]');
                        $headerHtml = $header->html();
                        $headerContent .= '<div  class="footer-container-wrapper">'.$headerHtml.'</div>';
            
                        $content = $headerContent;
                        break;
                }
                
                break;
    
            case Shop::CODE_LES3BRASSEURS:
        
                switch ($type) {
            
                    case 'header':
                        $crawler = new Crawler($content);
                
                
                        $headerContent  = "";
                        $header = $crawler->filter('header[class="header"]');
                        $headerHtml = $header->html();
                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                        $headerContent .= '<header class="header">'.$headerHtml.'</header>';
                
                        $content = $headerContent;
                
                        break;
            
            
                    case 'footer':
                        $crawler = new Crawler($content);
                
                        $headerContent  = "";
                        $header = $crawler->filter('footer[class="footer"]');
                        $headerHtml = $header->html();
                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                        $headerContent .= '<footer class="footer">'.$headerHtml.'</footer>';
                
                        $content = $headerContent;
                        break;
                }
        
                break;
    
            case Shop::CODE_PV:
        
                switch ($type) {
            
                    case 'header':
                        $crawler = new Crawler($content);
                
                
                        $headerContent  = "";
                        $header = $crawler->filter('header[id="header"]');
                        $headerHtml = $header->html();
//                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                        $headerContent .= '<header id="header">'.$headerHtml.'</header>';
                
                        $content = $headerContent;
                
                        break;
            
            
                    case 'footer':
                        
                        break;
                }
        
                break;
    
            case Shop::CODE_WORKFORCESOLUTIONS:
        
                switch ($type) {
            
                    case 'header':
                        $crawler = new Crawler($content);
                
                
                        $headerContent  = "";
                        $header = $crawler->filter('section[class="top-banner"]');
                        
                        $headerHtml = $header->html();
                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                        $headerContent .= '<section class="top-banner">'.$headerHtml.'</section>';
    
    
                        $header = $crawler->filter('section[class="top-bar"]');
    
                        $headerHtml = $header->html();
                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                        $headerContent .= '<section class="bar">'.$headerHtml.'</section>';
    
                        $header = $crawler->filter('header[class="printviewnone"]');
    
                        $headerHtml = $header->html();
                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                        $headerContent .= '<header class="printviewnone">'.$headerHtml.'</header>';
                
                        $content = $headerContent;
                
                        break;
            
            
                    case 'footer':
                        $crawler = new Crawler($content);
                
                        $headerContent  = "";
                        $header = $crawler->filter('footer[class="printviewnone five-column"]');
                        $headerHtml = $header->html();
                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                        $headerContent .= '<footer class="printviewnone five-column">'.$headerHtml.'</footer>';
                
                        $content = $headerContent;
                        break;
                }
        
                break;
                
            case Shop::CODE_LOUISDELHAIZE:
    
                switch ($type) {
        
                    case 'header':
    
                        
                        $currentUrlFr = $this->router->generate(
                            $request->attributes->get('_route'),
                            ['_locale' => 'fr'] + $request->attributes->get('_route_params'),
                            UrlGeneratorInterface::ABSOLUTE_URL
                        );
                        $currentUrlNl = $this->router->generate(
                            $request->attributes->get('_route'),
                            ['_locale' => 'nl'] + $request->attributes->get('_route_params'),
                            UrlGeneratorInterface::ABSOLUTE_URL
                        );
                        
                        $crawler = new Crawler($content);
            
            
                        $headerContent  = "";
                        $header = $crawler->filter('header[class="main-header"]');
                        $headerHtml = $header->html();
                        
                        
//                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                    
                        //href="https://louisdelhaize.be/nl/">NL
                    
                        $headerHtml = str_replace(['href="https://louisdelhaize.be/nl/">NL', 'href="https://louisdelhaize.be/">FR'], ['href="'.$currentUrlNl.'">NL', 'href="'.$currentUrlFr.'">FR'], $headerHtml);
                        $headerContent .= '<header class="main-header" role="banner" itemscope="itemscope" itemtype="http://schema.org/WPHeader">'.$headerHtml.'</header>';
            
                        $content = $headerContent;
                        
            
                        break;
        
        
                    case 'footer':
                        $crawler = new Crawler($content);
                        
                        
                        $headerContent  = "";
                        $header = $crawler->filter('div[class="banner--social-medias hidden-print wow fadeInDown"]');
                        $headerHtml = $header->html();

                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);

                        $headerContent .= '<div class="banner--social-medias hidden-print wow fadeInDown animated" style="visibility: visible;">'.$headerHtml.'</div>';
                        
                        
                        $header = $crawler->filter('footer[class="content-info footer hidden-print"]');
                        $headerHtml = $header->html();
                        
    
                        $headerContent .= '<footer class="content-info footer hidden-print" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
'.$headerHtml.'</footer>';
    
                        $content = $headerContent;
            
                        break;
                }
                
                break;
    
            case Shop::CODE_MANPOWER:
        
                $url = str_replace(['/all-vacancies', '/tous-les-emplois', '/alle-vacatures'], '', $url);
                $url = str_replace('/'.$request->getLocale(), '', $url);
                
                
                
                switch ($type) {
            
                    case 'header':
                        $crawler = new Crawler($content);
    
    
                        $currentUrlEn = $this->router->generate(
                            $request->attributes->get('_route'),
                            ['_locale' => 'en'] + $request->attributes->get('_route_params'),
                            UrlGeneratorInterface::ABSOLUTE_URL
                        );
                        $currentUrlFr = $this->router->generate(
                            $request->attributes->get('_route'),
                            ['_locale' => 'fr'] + $request->attributes->get('_route_params'),
                            UrlGeneratorInterface::ABSOLUTE_URL
                        );
                        $currentUrlNl = $this->router->generate(
                            $request->attributes->get('_route'),
                            ['_locale' => 'nl'] + $request->attributes->get('_route_params'),
                            UrlGeneratorInterface::ABSOLUTE_URL
                        );
                        
                
                        $headerContent  = "";
                        $header = $crawler->filter('header[class="section clearfix header"]');
                
                        $headerHtml = $header->html();
                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                        $headerContent .= '<header class="section clearfix header" role="banner">'.$headerHtml.'</header>';
                        
                        $headerContent = str_replace(['<a href="https://manpower.be/nl/alle-vacatures" class="', '<a href="https://manpower.be/fr/tous-les-emplois" class="', '<a href="https://manpower.be/all-vacancies" class="'], ['<a href="'.$currentUrlNl.'" class="', '<a href="'.$currentUrlFr.'" class="', '<a href="'.$currentUrlEn.'" class="'], $headerContent);
                        
                        $content = $headerContent;
                
                        break;
            
            
                    case 'footer':
                        $crawler = new Crawler($content);
                
                        $headerContent  = "";
                        $header = $crawler->filter('div[class="panel-pane pane-footer-copyright"]');
                        $headerHtml = $header->html();
                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                        $headerContent .= '<div class="panel-pane pane-footer-copyright">'.$headerHtml.'</div>';
                
                        $content = $headerContent;
                        break;
                }
        
                break;

            case Shop::CODE_FARM:

                switch ($type) {

                    case 'header':
    
                        $currentUrlFr = $this->router->generate(
                            $request->attributes->get('_route'),
                            ['_locale' => 'fr'] + $request->attributes->get('_route_params'),
                            UrlGeneratorInterface::ABSOLUTE_URL
                        );
                        $currentUrlNl = $this->router->generate(
                            $request->attributes->get('_route'),
                            ['_locale' => 'nl'] + $request->attributes->get('_route_params'),
                            UrlGeneratorInterface::ABSOLUTE_URL
                        );
                        
                        $crawler = new Crawler($content);

                        

                        $headerContent  = "";
                        $header = $crawler->filter('div[data-elementor-type="header"]');

                        $headerHtml = $header->html();
//                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                    
                        $headerHtml = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $headerHtml);
                        
                        $headerContent .= '<div data-elementor-type="header">'.$headerHtml.'</div>';
                        
                        $headerContent = str_replace(['<a title="FR" href="https://www.farm.coop"', '<a title="NL" href="https://www.farm.coop/nl/"'], ['<a title="FR" href="'.$currentUrlFr.'"', '<a title="NL" href="'.$currentUrlNl.'"'], $headerContent);
                        

                        $content = $headerContent;

                        break;


                    case 'footer':
                        $crawler = new Crawler($content);


                        $headerContent  = "";
                        $header = $crawler->filter('div[data-elementor-type="footer"]');

                        $headerHtml = $header->html();
//                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                        $headerHtml = str_replace('<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">', '', $headerHtml);
                        $headerHtml = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $headerHtml);
                        $headerContent .= '<div data-elementor-type="footer">'.$headerHtml.'</div>';

                        $content = $headerContent;

                        break;
                }

                break;
                
            case Shop::CODE_PICKLES:
    
                switch ($type) {
        
                    case 'header':
                        $crawler = new Crawler($content);
            
            
                        $headerContent  = "";
                        $header = $crawler->filter('div[class="p-0 col-xl-12 header"]');
            
                        $headerHtml = $header->html();
                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$headerHtml);
                        $headerContent .= '<div class="p-0 col-xl-12 header header-active">'.$headerHtml.'</div>';
    
                        $menuSidebar = $crawler->filter( 'div[class="menu-sidebar"]');
                        $menuSideBarHtml = $menuSidebar->html();
                        $menuSideBarHtml = preg_replace('#(href|src)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$menuSideBarHtml);
                        $headerContent.='<div class="menu-sidebar">'.$menuSideBarHtml.'</div>';
                        
            
                        $content = $headerContent;
            
                        break;
        
        
                    case 'footer':
                        $crawler = new Crawler($content);
    
    
                        $footerContent = "";
                        $footer = $crawler->filter('div[class="p-0 col-xl-12 footer"]');
                        $footerHtml = $footer->html();
                        $footerHtml = preg_replace('#(href|src)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$footerHtml);
                        $footerContent.='<div class="row"><div class="p-0 col-xl-12 footer">'.$footerHtml.'</div></div>';
    
                        $footer = $crawler->filter('div[class="footer-short"]');
                        $footerHtml = $footer->html();
                        $footerHtml = preg_replace('#(href|src)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$url.'$2$3',$footerHtml);
                        $footerContent.='<div class="row"><div class="p-0 col-xl-12 footer"><div class="footer-short">'.$footerHtml.'</div></div></div>';
                        
                        $content = $footerContent;
            
                        break;
                }
                
                break;
    
            case Shop::CODE_MULTIPHARMA:
        
                switch ($type) {
            
                    case 'header':
                
                        $currentUrlFr = $this->router->generate(
                            $request->attributes->get('_route'),
                            ['_locale' => 'fr'] + $request->attributes->get('_route_params'),
                            UrlGeneratorInterface::ABSOLUTE_URL
                        );
                        $currentUrlNl = $this->router->generate(
                            $request->attributes->get('_route'),
                            ['_locale' => 'nl'] + $request->attributes->get('_route_params'),
                            UrlGeneratorInterface::ABSOLUTE_URL
                        );
                
                        $crawler = new Crawler($content);
                
                
                
                        $headerContent  = "";
                        $header = $crawler->filter('header');
                
                        $headerHtml = $header->html();
    
                        $urlReplace = "https://www.multipharma.be";
    
    
                        $headerHtml = str_replace(['<a href="#" data-locale="fr_BE"', '<a href="#" data-locale="nl_BE"'], ['<a href="'.$currentUrlFr.'" data-locale="fr_BE"', '<a href="'.$currentUrlNl.'" data-locale="nl_BE"'], $headerHtml);
                    
                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$urlReplace.'$2$3',$headerHtml);
                        $headerContent .= '<header>'.$headerHtml.'</header>';
                
                        
                
                
                        $content = $headerContent;
                
                        break;
            
            
                    case 'footer':
                        $crawler = new Crawler($content);
    
                        $urlReplace = "https://www.multipharma.be";
                
                        $headerContent  = "";
                        $header = $crawler->filter('footer[class="text-center text-sm-left"]');
                
                        $headerHtml = $header->html();
                        $headerHtml = preg_replace('#(href|src|srcset)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#','$1="'.$urlReplace.'$2$3',$headerHtml);
                        $headerContent .= '<footer class="text-center text-sm-left">'.$headerHtml.'</footer>';
                
                        $content = $headerContent;
                
                        break;
                }
        
                break;
                
        }

        return $content;
    }
}
