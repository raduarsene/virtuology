<?php

namespace App\Event\Entity;

/**
 * An interface used for configuration Doctrine Entity Listeners.
 * Implement this interface for every Entity Listener Event.
 *
 * @link https://symfony.com/doc/current/bundles/DoctrineBundle/entity-listeners.html
 */
interface EntityListenerInterface
{
}
