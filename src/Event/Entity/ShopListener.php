<?php

namespace App\Event\Entity;

use App\Entity\Shop;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class ShopListener
 *
 * @package App\Event\Entity
 */
class ShopListener implements EntityListenerInterface
{
    /** @var string */
    private $projectDir;

    /**
     * ShopListener constructor.
     *
     * @param string $projectDir
     */
    public function __construct(string $projectDir)
    {
        $this->projectDir = $projectDir;
    }

    /**
     * @param Shop               $shop
     * @param LifecycleEventArgs $args
     */
    public function prePersist(Shop $shop, LifecycleEventArgs $args)
    {
        $shop->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime());

        // If shop is demo
        if ($shop->getDemoShop()) {
            $filesystem = new Filesystem();

            // Add images
            $imageDir = $this->projectDir.'/assets/images/web/shop/'.$shop->getCode();
            if (!$filesystem->exists($imageDir)) {
                try {
                    $filesystem->mkdir($imageDir);

                    $filesystem->mirror($this->projectDir.'/assets/images/web/default', $imageDir);
                } catch (\Exception $e) {}
            }

            // Add templates
            $templateDir = $this->projectDir.'/templates/web/shop/'.$shop->getCode();
            if (!$filesystem->exists($templateDir)) {
                try {
                    $filesystem->mkdir($templateDir);

                    $filesystem->mirror($this->projectDir.'/templates/web/default', $templateDir);
                } catch (\Exception $e) {}
            }

            // Add translations
            $translationsDir = $this->projectDir.'/translations/';
            foreach ($shop->getLanguageDomains() as $language => $domain) {
                $translationFile = $translationsDir.$shop->getCode().'.'.$language.'.yml';

                if (!$filesystem->exists($translationFile)) {
                    try {
                        $filesystem->copy($translationsDir.'default.yml', $translationFile);
                    } catch (\Exception $e) {}
                }
            }
        }
    }

    /**
     * @param Shop               $shop
     * @param PreUpdateEventArgs $args
     */
    public function preUpdate(Shop $shop, PreUpdateEventArgs $args)
    {
        $shop->setUpdatedAt(new \DateTime());
    }
}
