<?php

namespace App\Event\Entity;

use App\Entity\User;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserListener
 *
 * @package App\Event\Entity
 */
class UserListener implements EntityListenerInterface
{
    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;

    /**
     * UserListener constructor.
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param User               $user
     * @param LifecycleEventArgs $args
     */
    public function prePersist(User $user, LifecycleEventArgs $args)
    {
        $this->updatePassword($user);

        $user->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime());
    }

    /**
     * @param User               $user
     * @param PreUpdateEventArgs $args
     */
    public function preUpdate(User $user, PreUpdateEventArgs $args)
    {
        $this->updatePassword($user);

        $user->setUpdatedAt(new \DateTime());
    }

    /**
     * @param User $user
     */
    private function updatePassword(User $user)
    {
        if (!empty($user->getPlainPassword())) {
            $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPlainPassword()));
        }
    }
}
