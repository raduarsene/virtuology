<?php

namespace App\Event;

use App\Entity\Language;
use App\Entity\Shop;
use App\Entity\ShopDomain;
use App\Entity\ShopLocation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

/**
 * Class RequestSubscriber
 *
 * @package App\Event
 */
class RequestSubscriber implements EventSubscriberInterface
{
    const SHOP_DOMAIN_ATTRIBUTE = '_shop_domain';

    /** @var RouterInterface */
    private $router;

    /** @var EntityManagerInterface */
    private $em;

    /** @var Environment */
    private $twig;

    /** @var bool|null */
    private $inMaintenance;

    public function __construct(
        RouterInterface $router,
        EntityManagerInterface $em,
        Environment $twig,
        ?bool $inMaintenance
    ) {
        $this->router = $router;
        $this->em = $em;
        $this->twig = $twig;
        $this->inMaintenance = $inMaintenance;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest', 200],
        ];
    }

    /**
     * @param GetResponseEvent $event
     * @return GetResponseEvent|void
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            // don't do anything if it's not the master request
            return;
        }

        $request = $this->getRequest($event);

        // On admin|profiler|wdt|twig error request, do nothing
        $requestUri = $request->getRequestUri();
        if ($requestUri && preg_match('#^/(_(profiler|wdt|error)|admin)/#i', $requestUri)) {
            return;
        }

        $shopDomain = $this->em->getRepository(ShopDomain::class)->findOneBy(['name' => $request->getHttpHost()]);

        // On API request don't do locale actions
        if (preg_match('#^/api/#i', $requestUri)) {
            // Set the current shop domain in attributes
            $shopDomain && $request->attributes->set(self::SHOP_DOMAIN_ATTRIBUTE, $shopDomain);

            return;
        }

        if (!$shopDomain || !$shop = $shopDomain->getShop()) {
            // Redirect to admin when no shop domain found
            return $event->setResponse(new RedirectResponse($this->router->generate('sonata_admin_dashboard')));
        }

        // Check maintenance
        if ($this->inMaintenance || $shop->getMaintenance()) {
            return $this->addMaintenance($event, $request, $shopDomain);
        }

        // Don't do locale actions on exception
        if ($request->attributes->has('exception')) {
            return;
        }

        
        $locale = $defaultLocale = $shopDomain->getLanguage()->getCode();
        
        // Only for routes with locale
        if ($request->attributes->has('_locale')) {
            if ($locale = $request->attributes->get('_locale')) {
                $language = $this->em->getRepository(Language::class)->findOneBy(['code' => $locale]);

                // Redirect to respective domain with locale
                if ($locale !== $defaultLocale && $language && $shopDomainLanguage = $shop->getDomainByLanguage($language)) {
                    
                    return $event->setResponse(new RedirectResponse(
                        $request->getScheme().'://'.$shopDomainLanguage->getName().$this->getCurrentUrl($request, $shop, $locale)
                    ));
                }

                // Redirect with default locale when locale isn't supported
                if (!in_array($locale, $shopDomain->getAvailableLanguagesArray())) {
                    return $event->setResponse(new RedirectResponse($this->getCurrentUrl($request, $shop, $defaultLocale), Response::HTTP_MOVED_PERMANENTLY));
                }

                // Redirect to specific shop route path if specified
                if ($redirectUrl = $this->getShopRedirectUrl($request, $shop, $locale)) {
                    
                    return $event->setResponse(new RedirectResponse($redirectUrl, Response::HTTP_MOVED_PERMANENTLY));
                }
            } else {
                // Redirect with default locale when locale is missing
                return $event->setResponse(new RedirectResponse($this->getCurrentUrl($request, $shop, $defaultLocale), Response::HTTP_MOVED_PERMANENTLY));
            }
        }

        // Set locale
        $request->setLocale($locale);
        $request->getSession()->set('_locale', $locale);

        // Set the current shop domain in attributes
        $request->attributes->set(self::SHOP_DOMAIN_ATTRIBUTE, $shopDomain);
    }

    /**
     * @param GetResponseEvent $event
     * @return Request
     */
    private function getRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        // update server https
        if (false !== strpos($request->server->get('HTTP_X_FORWARDED_PROTO'), 'https')
            || false !== strpos($request->server->get('HTTP_CF_VISITOR'), 'https')
        ) {
            $request->server->set('HTTPS', 'on');
        }

        // Always trust incoming request on using load balancer (e.g. an AWS Elastic Load Balancing) or a reverse proxy (e.g. Varnish for caching)
        $trustedProxies = ['127.0.0.1'];
        if ($request->server->has('TRUSTED_PROXIES')) {
            $trustedProxies += explode(',', $request->server->get('TRUSTED_PROXIES'));
        }

        if ($request->server->get('APP_ENV') === 'prod') {
            $trustedProxies[] = $request->server->get('REMOTE_ADDR');
        }

        Request::setTrustedProxies($trustedProxies, Request::HEADER_X_FORWARDED_ALL);

        return $request;
    }

    /**
     * @param GetResponseEvent $event
     * @param Request          $request
     * @param ShopDomain       $shopDomain
     * @return GetResponseEvent
     */
    private function addMaintenance(GetResponseEvent $event, Request $request, ShopDomain $shopDomain)
    {
        $shop = $shopDomain->getShop();

        $view = 'web/shop/'.$shop->getCode().'/maintenance.html.twig';
        if (!$this->twig->getLoader()->exists($view)) {
            $view = 'web/index/maintenance.html.twig';
        }

        $request->setLocale($request->attributes->get('_locale') ?: $shopDomain->getLanguage()->getCode());
        $event->setResponse(new Response($this->twig->render($view, ['shop' => $shop]), Response::HTTP_SERVICE_UNAVAILABLE));
        $event->stopPropagation();

        return $event;
    }

    /**
     * @param Request $request
     * @param Shop    $shop
     * @param string  $locale
     * @return string|null
     */
    private function getShopRedirectUrl(Request $request, Shop $shop, string $locale)
    {
        $redirectUrl = null;

        if ($this->getCurrentUrl($request, $shop, $locale, true)) {
            $redirectUrl = $this->getCurrentUrl($request, $shop, $locale);
        }

        return $redirectUrl;
    }

    /**
     * @param Request $request
     * @param Shop    $shop
     * @param string  $locale
     * @param bool    $hasSpecificChanges
     * @return string|bool
     */
    private function getCurrentUrl(Request $request, Shop $shop, string $locale, bool $hasSpecificChanges = false)
    {
        $parameters = ['_locale' => $locale] + $request->query->all();

        if ($request->attributes->has('_localization')) {
            $parameters['_localization'] = $request->attributes->get('_localization');
        }

        if ($request->attributes->has('slug')) {
            $parameters['slug'] = $request->attributes->get('slug');
        }

        if ($request->attributes->has('externalId')) {
            $parameters['externalId'] = $request->attributes->get('externalId');
        }

        $routeName = $request->attributes->get('_route');

        $specificChanges = $this->setUrlSpecificParameters($shop, $routeName, $parameters);
        if ($hasSpecificChanges) {
            return $specificChanges;
        }
    
        $regionParameters = ['countryCode', 'state', 'region', 'zipCode', 'city'];
        //support for region parameters - eg: carrefour
        foreach ($regionParameters as $param) {
            if ($request->attributes->has($param)) {
                $parameters[$param] = $request->attributes->get($param);
            }
        }

        return $this->router->generate($routeName, $parameters);
    }

    /**
     * @param Shop   $shop
     * @param string $routeName
     * @param array  $parameters
     * @return bool true - when there are specific changes related to shop
     */
    private function setUrlSpecificParameters(Shop $shop, string &$routeName, array &$parameters)
    {
        $specificChanges = false;

        switch ($shop->getCode()) {
            case Shop::CODE_VOO:
                if (false !== strpos($routeName, 'app_web_shop_')) {
                    $routeName = str_replace('app_web_shop_', 'app_web_voo_', $routeName);

                    $specificChanges = true;
                }
                if (!empty($parameters['slug']) && false !== strpos($parameters['slug'], '---')) {
                    
                    $parameters['slug'] = preg_replace('/-+/', '-', $parameters['slug']);
                    $specificChanges = true;
                }
                break;
            case Shop::CODE_LEONDEBRUXELLES:
                if (!empty($parameters['slug']) && false === stripos($parameters['slug'], 'leon-de-bruxelles')) {
                    $parameters['slug'] = 'leon-de-bruxelles-'.$parameters['slug'];

                    $specificChanges = true;
                }

                if ('app_web_leondebruxelles_show' === $routeName && Language::ENGLISH === $parameters['_locale']
                    && !empty($parameters['_localization']) && 'mussels-fries' !== $parameters['_localization']
                ) {
                    $parameters['_localization'] = 'mussels-fries';

                    $specificChanges = true;
                }
                break;
            case Shop::CODE_CARREFOUR:
                if ('app_web_carrefour_show' === $routeName && !empty($parameters['externalId'])
                    && !empty($parameters['slug']) && 'carrefour' === $parameters['slug']
                ) {
                    $shopLocation = $this->em->getRepository(ShopLocation::class)->findOneBy([
                        'shop' => $shop,
                        'externalId' => $parameters['externalId'],
                    ]);

                    if ($shopLocation) {
                        $parameters['slug'] = $shopLocation->getSlug();

                        $specificChanges = true;
                    }
                }
                break;
                
        }

        return $specificChanges;
    }
}
