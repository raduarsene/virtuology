<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200309121525 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE shop_domain_region (shop_domain_id INT NOT NULL, region_id INT NOT NULL, INDEX IDX_7E1D6AB08E8806E (shop_domain_id), INDEX IDX_7E1D6AB098260155 (region_id), PRIMARY KEY(shop_domain_id, region_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE shop_domain_region ADD CONSTRAINT FK_7E1D6AB08E8806E FOREIGN KEY (shop_domain_id) REFERENCES shop_domain (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE shop_domain_region ADD CONSTRAINT FK_7E1D6AB098260155 FOREIGN KEY (region_id) REFERENCES region (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE shop_domain_region');
    }
}
