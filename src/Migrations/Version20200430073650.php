<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200430073650 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('UPDATE attribute_value SET image_name = "ed62fcaad00c166a0f502772e87c85f8.png" WHERE value="with_map" AND attribute_id = "15" ');
        $this->addSql('UPDATE attribute_value SET image_name = "55233cf9065bb97bee64bafbcfd352cc.png" WHERE value="with_image_and_map" AND attribute_id = "15"');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
