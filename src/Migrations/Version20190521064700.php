<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190521064700 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE shop_domain (id INT AUTO_INCREMENT NOT NULL, shop_id INT NOT NULL, language_id INT NOT NULL, name VARCHAR(255) NOT NULL, g_client_code VARCHAR(50) DEFAULT NULL, g_mobi_code VARCHAR(50) DEFAULT NULL, gtm_code VARCHAR(50) DEFAULT NULL, g_meta_tag VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_220B38894D16C4DD (shop_id), INDEX IDX_220B388982F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shop_domain_language (shop_domain_id INT NOT NULL, language_id INT NOT NULL, INDEX IDX_2EA63ACE8E8806E (shop_domain_id), INDEX IDX_2EA63ACE82F1BAF4 (language_id), PRIMARY KEY(shop_domain_id, language_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE attribute_value (id INT AUTO_INCREMENT NOT NULL, attribute_id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, value VARCHAR(100) NOT NULL, description LONGTEXT DEFAULT NULL, image_name VARCHAR(100) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_FE4FBB82B6E62EFA (attribute_id), UNIQUE INDEX UNIQ_FE4FBB82B6E62EFA1D775834 (attribute_id, value), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, password VARCHAR(255) NOT NULL, roles JSON NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shop (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, api_url VARCHAR(255) NOT NULL, api_version VARCHAR(16) NOT NULL, api_site_id VARCHAR(100) DEFAULT NULL, map_type VARCHAR(20) DEFAULT NULL, map_credentials JSON DEFAULT NULL, allow_robots TINYINT(1) DEFAULT NULL, demo_shop TINYINT(1) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE attribute (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, block VARCHAR(20) NOT NULL, description LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_FA7AEFFB831B9722 (block), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE api_error (id INT AUTO_INCREMENT NOT NULL, shop_id INT NOT NULL, error_code VARCHAR(16) NOT NULL, api_url VARCHAR(255) NOT NULL, api_query_string JSON DEFAULT NULL, error_message LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_7948E8A14D16C4DD (shop_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shop_template (id INT AUTO_INCREMENT NOT NULL, shop_id INT NOT NULL, attribute_value_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_4CD8E5154D16C4DD (shop_id), INDEX IDX_4CD8E51565A22152 (attribute_value_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE language (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) DEFAULT NULL, code VARCHAR(8) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_D4DB71B577153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shop_location (id INT AUTO_INCREMENT NOT NULL, shop_id INT NOT NULL, name VARCHAR(100) DEFAULT NULL, brand VARCHAR(50) DEFAULT NULL, city VARCHAR(100) DEFAULT NULL, lat NUMERIC(10, 8) DEFAULT NULL, lng NUMERIC(10, 8) DEFAULT NULL, details JSON DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_8526735D4D16C4DD (shop_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lexik_trans_unit (id INT AUTO_INCREMENT NOT NULL, key_name VARCHAR(191) NOT NULL, domain VARCHAR(191) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX key_domain_idx (key_name, domain), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lexik_translation_file (id INT AUTO_INCREMENT NOT NULL, domain VARCHAR(191) NOT NULL, locale VARCHAR(191) NOT NULL, extention VARCHAR(191) NOT NULL, path VARCHAR(191) NOT NULL, hash VARCHAR(191) NOT NULL, UNIQUE INDEX hash_idx (hash), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lexik_trans_unit_translations (id INT AUTO_INCREMENT NOT NULL, file_id INT DEFAULT NULL, trans_unit_id INT DEFAULT NULL, locale VARCHAR(191) NOT NULL, content LONGTEXT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, modified_manually TINYINT(1) NOT NULL, INDEX IDX_B0AA394493CB796C (file_id), INDEX IDX_B0AA3944C3C583C9 (trans_unit_id), UNIQUE INDEX trans_unit_locale_idx (trans_unit_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE shop_domain ADD CONSTRAINT FK_220B38894D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id)');
        $this->addSql('ALTER TABLE shop_domain ADD CONSTRAINT FK_220B388982F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE shop_domain_language ADD CONSTRAINT FK_2EA63ACE8E8806E FOREIGN KEY (shop_domain_id) REFERENCES shop_domain (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE shop_domain_language ADD CONSTRAINT FK_2EA63ACE82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE attribute_value ADD CONSTRAINT FK_FE4FBB82B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id)');
        $this->addSql('ALTER TABLE api_error ADD CONSTRAINT FK_7948E8A14D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id)');
        $this->addSql('ALTER TABLE shop_template ADD CONSTRAINT FK_4CD8E5154D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id)');
        $this->addSql('ALTER TABLE shop_template ADD CONSTRAINT FK_4CD8E51565A22152 FOREIGN KEY (attribute_value_id) REFERENCES attribute_value (id)');
        $this->addSql('ALTER TABLE shop_location ADD CONSTRAINT FK_8526735D4D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id)');
        $this->addSql('ALTER TABLE lexik_trans_unit_translations ADD CONSTRAINT FK_B0AA394493CB796C FOREIGN KEY (file_id) REFERENCES lexik_translation_file (id)');
        $this->addSql('ALTER TABLE lexik_trans_unit_translations ADD CONSTRAINT FK_B0AA3944C3C583C9 FOREIGN KEY (trans_unit_id) REFERENCES lexik_trans_unit (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE shop_domain_language DROP FOREIGN KEY FK_2EA63ACE8E8806E');
        $this->addSql('ALTER TABLE shop_template DROP FOREIGN KEY FK_4CD8E51565A22152');
        $this->addSql('ALTER TABLE shop_domain DROP FOREIGN KEY FK_220B38894D16C4DD');
        $this->addSql('ALTER TABLE api_error DROP FOREIGN KEY FK_7948E8A14D16C4DD');
        $this->addSql('ALTER TABLE shop_template DROP FOREIGN KEY FK_4CD8E5154D16C4DD');
        $this->addSql('ALTER TABLE shop_location DROP FOREIGN KEY FK_8526735D4D16C4DD');
        $this->addSql('ALTER TABLE attribute_value DROP FOREIGN KEY FK_FE4FBB82B6E62EFA');
        $this->addSql('ALTER TABLE shop_domain DROP FOREIGN KEY FK_220B388982F1BAF4');
        $this->addSql('ALTER TABLE shop_domain_language DROP FOREIGN KEY FK_2EA63ACE82F1BAF4');
        $this->addSql('ALTER TABLE lexik_trans_unit_translations DROP FOREIGN KEY FK_B0AA3944C3C583C9');
        $this->addSql('ALTER TABLE lexik_trans_unit_translations DROP FOREIGN KEY FK_B0AA394493CB796C');
        $this->addSql('DROP TABLE shop_domain');
        $this->addSql('DROP TABLE shop_domain_language');
        $this->addSql('DROP TABLE attribute_value');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE shop');
        $this->addSql('DROP TABLE attribute');
        $this->addSql('DROP TABLE api_error');
        $this->addSql('DROP TABLE shop_template');
        $this->addSql('DROP TABLE language');
        $this->addSql('DROP TABLE shop_location');
        $this->addSql('DROP TABLE lexik_trans_unit');
        $this->addSql('DROP TABLE lexik_translation_file');
        $this->addSql('DROP TABLE lexik_trans_unit_translations');
    }
}
