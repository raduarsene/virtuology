<?php


namespace App\DependencyInjection\CompilerPass;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class TagRendererCompilerPass implements CompilerPassInterface
{
    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('webpack_encore.tag_renderer')) {
            return;
        }

        $definition = $container->getDefinition('webpack_encore.tag_renderer');

        $defaultAttributes = $definition->getArgument(2);

//        $defaultAttributes['async'] = '';

        $definition->replaceArgument(2, $defaultAttributes);
    }
}
