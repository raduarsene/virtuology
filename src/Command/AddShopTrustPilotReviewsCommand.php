<?php

namespace App\Command;

use App\Entity\Shop;
use App\Entity\ShopLocation;
use App\Entity\ShopLocationSlugHistory;
use App\Service\Api\ApiInterface;
use App\Service\Api\Request\Location\V2Request;
use App\Service\Api\TrustpilotService;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Class AddShopLocationCommand
 *
 * @package App\Command
 */
class AddShopTrustPilotReviewsCommand extends Command
{
    // to make your command lazily loaded, configure the $defaultName static property,
    // so it will be instantiated only when the command is actually called.
    protected static $defaultName = 'app:add-shop-trustpilot-reviews';

    /** @var EntityManagerInterface */
    private $em;

    /** @var TrustpilotService */
    private $trustpilotService;
    
    /**
     * AddShopTrustPilotReviewsCommand constructor.
     *
     * @param EntityManagerInterface $em
     * @param TrustpilotService      $trustpilotService
     */
    public function __construct( EntityManagerInterface $em, TrustpilotService $trustpilotService)
    {
        parent::__construct();

        $this->em = $em;
        $this->trustpilotService = $trustpilotService;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Add/update shop(s) trustpilot reviews, depending on options')
            ->addArgument('shop', InputArgument::OPTIONAL, 'The shop code. Will add locations to this shop, if empty will add to all found shops')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $shopCode = $input->getArgument('shop');
        
        $output->writeln("Command started\n");

        $shops = $this->em->getRepository(Shop::class)->getShopsByLocation($shopCode, true);
        if (empty($shops)) {
            if ($shopCode) {
                $output->writeln("<error>Not found shop with CODE: ".$shopCode."</error>");

                return;
            } else {
                $output->writeln("<info>Not found any shops</info>");

                return;
            }
        }

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion(sprintf("<info>Found %d shop(s) to add locations, do you want to continue ?  (yes/no)</info>  [<info>yes</info>]:\n", count($shops)));

        if (!$helper->ask($input, $output, $question)) {
            return;
        }

        $output->writeln("Started to search for shop(s) locations:");
        
        /** @var Shop $shop */
        foreach ($shops as $shop) {
    
            $output->writeln("Processing shop: ".$shop->getCode());
            
            list($locations, $statusCode) = $this->trustpilotService->getBusinessUnitLocations($shop);
            //process locations
            if($statusCode == 200 && isset($locations['locations'])) {
                
                foreach ($locations['locations'] as $locationDetails) {
                    
//                    dump($locationDetails);
        
                    $parameters = [
                        'shop' => $shop,
                        'externalId' => $locationDetails['customerLocationId'],
                        'tpLocationId' => $locationDetails['id'],
                    ];
                    
                    
                    $updateResult = $this->em->getRepository(ShopLocation::class)->updateShopLocationTrustpilotId($parameters);
                    
                    switch ($shop->getCode())
                    {
                        case Shop::CODE_LEONDEBRUXELLES:
                            if($updateResult == 0) {
                                
                                $parameters['externalId'] = "0".$parameters['externalId'];
                                $updateResult = $this->em->getRepository(ShopLocation::class)->updateShopLocationTrustpilotId($parameters);
                            }
                            break;
                    }
                    
                }
    
            }
            
            $reviews = $this->trustpilotService->getShopReviews($shop);
            foreach($reviews as $tpLocationId => $review) {
                
                $parameters = [];
                $parameters['shop'] = $shop;
                $parameters['tpLocationId'] = $tpLocationId;
                $parameters['tpReviewsNo'] = $review['reviewsNo'];
                $parameters['tpScore'] = round($review['reviewsScore'] / $review['reviewsNo'], 2);
                
                $this->em->getRepository(ShopLocation::class)->updateShopLocationTrustpilotReviews($parameters);
                
            }
            
    
            $output->writeln("End Processing shop: ".$shop->getCode());
            
        }

        $output->writeln("\nCommand executed successfully.");
    }
    
  
}
