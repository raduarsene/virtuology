<?php

namespace App\Command;

use App\Entity\Shop;
use App\Entity\ShopLocation;
use App\Service\Api\PicklesAuctionService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\PicklesAuction;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class AddShopPicklesAuctionsCommand
 *
 * @package App\Command
 */
class AddShopPicklesAuctionsCommand extends Command
{
    // to make your command lazily loaded, configure the $defaultName static property,
    // so it will be instantiated only when the command is actually called.
    protected static $defaultName = 'app:add-shop-pickles-auctions';
    
    /** @var EntityManagerInterface */
    private $em;

    /** @var PicklesAuctionService */
    private $picklesAuctionService;
    
    /**
     * AddShopPicklesAuctionsCommand constructor.
     *
     * @param EntityManagerInterface $em
     * @param PicklesAuctionService  $picklesAuctionService
     */
    public function __construct( EntityManagerInterface $em, PicklesAuctionService $picklesAuctionService)
    {
        parent::__construct();

        $this->em = $em;
        $this->picklesAuctionService = $picklesAuctionService;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Add Pickles auctions available for each location')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        
        
        $output->writeln("Command started\n");
        $output->writeln("Started to search for Pickles auctions");
        
       
        list($response, $statusCode) = $this->picklesAuctionService->getAuctions();
        if($statusCode == Response::HTTP_OK && count($response)) {
        
            $conn = $this->em->getConnection();
            $conn->query("TRUNCATE TABLE `pickles_auction`");
            
            foreach ($response as $value) {
                
                $picklesAuction = new PicklesAuction();
                
                foreach ($value as $attr => $attrValue) {
                
                    $methodName = "set".ucfirst($attr);
                    $picklesAuction->{$methodName}($attrValue);
                
                    
                }
                $this->em->persist($picklesAuction);
                $this->em->flush();
            }
    
            $output->writeln("\nCommand executed successfully.");
        
        }
        else {
            $output->writeln("\nCommand not executed  - status ".$statusCode);
        }
        

        
    }
    
  
}
