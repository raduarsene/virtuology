<?php

namespace App\Command;

use App\Entity\Shop;
use App\Entity\ShopLocation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ExportTomandcoUrlCommand
 *
 * @package App\Command
 */
class ExportTomandcoUrlCommand extends Command
{
    const DEFAULT_ITEMS_SIZE = 100;

    protected static $defaultName = 'app:export-tomandco-url';

    /** @var EntityManagerInterface */
    private $em;

    /**
     * ExportTomandcoUrlCommand constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();

        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Add/update shop(s) location, depending on options');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {

        $shopObj = $this->em->getRepository(Shop::class)->findOneBy(['code' => 'tomandco']);

        $newUrlStart = "https://stores.tomandco.com/%s/%s";
        $oldUrlStart = "https://www.tomandco.be/%s/stores/%s";

        $newUrlLanguage = 'fr';
        $oldUrlLanguage = 'be_fr';

        $fh = fopen("tomandco_urls.csv", "w+");

        fputcsv($fh, ["oldUrl", "newUrl"]);

        $locations = $this->em->getRepository(ShopLocation::class)->findBy(['shop' => $shopObj]);

        /** @var ShopLocation $location */
        foreach ($locations as $location) {
            $locationDetails = $location->getDetailsArray();
            $slug = $locationDetails['slug'];

            $oldSlug = preg_replace('/-+/', '-', $slug);

            $newUrl = sprintf($newUrlStart, $newUrlLanguage, $slug);
            $oldUrl = sprintf($oldUrlStart, $oldUrlLanguage, $oldSlug);

            fputcsv($fh, [$oldUrl, $newUrl]);
        }

        fclose($fh);

        $output->writeln("\nCommand executed successfully.");
    }
}
