<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ExportTomandcoUrlCommand
 *
 * @package App\Command
 */
class ImportCsvTranslationsCommand extends Command
{
    const DEFAULT_ITEMS_SIZE = 100;

    protected static $defaultName = 'app:import-csv-translations';

    /** @var EntityManagerInterface */
    private $em;

    /**
     * ExportTomandcoUrlCommand constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();

        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Add/update shop(s) location, depending on options');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $conn = $this->em->getConnection();

        $fileName = "./translations_csv/trans.csv";

        $row = 1;
        if (($handle = fopen($fileName, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
        
        
                /*
                 *  1 => "content_fr"
                      2 => "inhoud_nl"
                      3 => "content_en"
                      4 => "Inhalt_de"

                 */
                if ($data[0] == 'key_name') {
            
                } else {
            
                    $query  = "SELECT `id` from `lexik_trans_unit` WHERE `domain`='medimarket-be' AND `key_name`='{$data[0]}'";
                    $result = $conn->fetchArray($query);
            
                    if (isset($result[0])) {
                        $trans_unit_id = $result[0];
                
                        $quoted = $conn->quote($data[1]);
                        $query  = "UPDATE `lexik_trans_unit_translations` SET `content`={$quoted} WHERE `trans_unit_id`=$trans_unit_id AND `locale`='fr' ";
                        $s      = $conn->prepare($query);
                        $s->execute();
                
                
                        $quoted = $conn->quote($data[2]);
                        $query  = "UPDATE `lexik_trans_unit_translations` SET `content`={$quoted} WHERE `trans_unit_id`=$trans_unit_id AND `locale`='nl' ";
                        $s      = $conn->prepare($query);
                        $s->execute();
                
                
                    }
            
            
                }
        
                //                exit();
        
                $row++;
            }
        }
            fclose($handle);

        $output->writeln("\nCommand executed successfully.");
    }
}
