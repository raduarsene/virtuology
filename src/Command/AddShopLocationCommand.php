<?php

namespace App\Command;

use App\Entity\Shop;
use App\Entity\ShopLocation;
use App\Entity\ShopLocationSlugHistory;
use App\Service\Api\ApiInterface;
use App\Service\Api\Request\Location\V2Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Class AddShopLocationCommand
 *
 * @package App\Command
 */
class AddShopLocationCommand extends Command
{
    // to make your command lazily loaded, configure the $defaultName static property,
    // so it will be instantiated only when the command is actually called.
    protected static $defaultName = 'app:add-shop-location';

    /** @var EntityManagerInterface */
    private $em;

    /** @var ApiInterface */
    private $apiService;

    /**
     * AddShopLocationCommand constructor.
     *
     * @param EntityManagerInterface $em
     * @param ApiInterface           $apiService
     */
    public function __construct( EntityManagerInterface $em, ApiInterface $apiService)
    {
        parent::__construct();

        $this->em = $em;
        $this->apiService = $apiService;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Add/update shop(s) location, depending on options')
            ->addArgument('shop', InputArgument::OPTIONAL, 'The shop code. Will add locations to this shop, if empty will add to all found shops')
            ->addOption('projection', 'p', InputOption::VALUE_OPTIONAL, 'Projection to set on API query, default: StoreLocation. Possible values: StoreLocation, LocationFull.')
            ->addOption('update', 'u', InputOption::VALUE_NONE, 'If set, will update location no matter if the shop has a location or not, otherwise will add location only if the shop doesnt have one')
            ->addOption('slugHistory', 'sh', InputOption::VALUE_NONE, 'If set, will add also the slug history from old route /s/ to the new one')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $shopCode = $input->getArgument('shop');
        $projection = $input->getOption('projection') !== null ? $input->getOption('projection') : V2Request::DEFAULT_PROJECTION;
        $toUpdate = (bool)$input->getOption('update');
        $addSlugHistory = (bool)$input->getOption('slugHistory');

        $output->writeln("Command started\n");

        $shops = $this->em->getRepository(Shop::class)->getShopsByLocation($shopCode, $toUpdate);
        if (empty($shops)) {
            if ($shopCode) {
                $output->writeln("<error>Not found shop with CODE: ".$shopCode."</error>");

                return;
            } else {
                $output->writeln("<info>Not found any shops</info>");

                return;
            }
        }

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion(sprintf("<info>Found %d shop(s) to add locations, do you want to continue ?  (yes/no)</info>  [<info>yes</info>]:\n", count($shops)));

        if (!$helper->ask($input, $output, $question)) {
            return;
        }

        $output->writeln("Started to search for shop(s) locations:");
        
        if($shopCode == Shop::CODE_CARREFOUR) {
    
            $this->getLocalizedShopData($shops, $addSlugHistory);
            
            $output->writeln("\nCommand executed successfully.");
            exit();
        
        }

        /** @var Shop $shop */
        foreach ($shops as $shop) {
            // Set default Shop Domain and API version
            $this->apiService->init($shop->getApiVersion(), $shop->getDomains()->first());
    
            if ($addSlugHistory) {
                $this->em->getRepository(ShopLocationSlugHistory::class)->removeByShop($shop->getId());
            }

            $parameters = ['siteId' => $shop->getApiSiteId()];
            $projection && $parameters['projection'] = $projection;
            
           
            list($data) = $this->apiService->getLocations($parameters, false);

            $output->writeln(sprintf("\n<info>Found %d locations from API, adding them to Shop</info>", count($data)));

            $this->addDataToShopLocation($data, $shop, $toUpdate, $addSlugHistory);

            $output->writeln(sprintf("<info>Added %d locations to shop with CODE: %s</info>", count($data), $shop->getCode()));
        }

        $output->writeln("\nCommand executed successfully.");
    }
    
    /**
     * @param array $data
     * @param Shop  $shop
     * @param bool  $removeOld
     * @param bool  $addSlugHistory
     */
    private function addDataToShopLocation($data, Shop $shop, $removeOld, $addSlugHistory)
    {
        if (!empty($data)) {
            // Remove old Locations before adding new one
            if ($removeOld) {
                $this->em->getRepository(ShopLocation::class)->removeLocationsByShop($shop->getId());
            }

            foreach ($data as $index => $details) {
    
                $shopLocation = new ShopLocation();
                
                
                if($shop->getCode() == Shop::CODE_CARREFOUR && !$removeOld) {
                    try {
                        $shopLocation = $this->em->getRepository(ShopLocation::class)->findOneBy(['shop'=>$shop, 'externalId'=>$details['externalId']]);
                    }catch (\Exception $e) {
                        $shopLocation = new ShopLocation();
                    }
                    
                    if(empty($shopLocation)) {
                        $shopLocation = new ShopLocation();
                    }
                    
                    if(!empty($shopLocation)) {
                        
                        $details['translatedName'][] = [
                            'language' => 'fr',
                            'value' => $shopLocation->getName()
                        ];
                        $details['translatedSlug'][] = [
                            'language' => 'fr',
                            'value' => $shopLocation->getSlug()
                        ];
                        $details['translatedBrand'][] = [
                            'language' => 'fr',
                            'value' => $shopLocation->getDetailsArray()['brand']
                        ];
    
                        $details['translatedName'][] = [
                            'language' => 'nl',
                            'value' => $details['name']
                        ];
                        $details['translatedSlug'][] = [
                            'language' => 'nl',
                            'value' => $details['slug']
                        ];
                        $details['translatedBrand'][] = [
                            'language' => 'nl',
                            'value' => $details['brand']
                        ];
                    }
                }
                
                $shopLocation->setShop($shop)
                    ->setName($details['name'])
                    ->setExternalId($details['externalId'])
                    ->setSlug($details['slug'])
                    ->setBrand(!empty($details['brandSlug']) ? $details['brandSlug'] : $details['brand'])
                    ->setDetails($details);

                if (!empty($details['address'])) {
                    $shopLocation->setCity($details['address']['locality'])
                        ->setLat($details['address']['latitude'])
                        ->setLng($details['address']['longitude']);
                }

                $this->em->persist($shopLocation);
                
                if ($addSlugHistory) {
                    $this->addSlugHistory($shop, $details);
                }

                if (($index + 1) % 100 === 0) {
                    $this->em->flush();
                }
            }

            $this->em->flush();
        }
    }

    /**
     * @param Shop  $shop
     * @param array $details
     */
    private function addSlugHistory(Shop $shop, array $details)
    {
        $existingUrl = '';
        switch ($shop->getApiVersion()) {
            case Shop::API_VERSION_3:
                $existingUrl = isset($details['contact']['url']) ? $details['contact']['url'] : '';
                break;
        }

        if ($existingUrl) {
            preg_match("/^.+?\/s\/(.+)$/is", $existingUrl, $match);

            if (!empty($match[1])) {
                $shopLocationSlugHistory = new ShopLocationSlugHistory();
                $shopLocationSlugHistory->setShop($shop)
                    ->setSlug($match[1])
                    ->setExternalId($details['externalId']);

                $this->em->persist($shopLocationSlugHistory);
            }
        }
    }
    
    /**
     * @param $shops
     * @param $addSlugHistory
     */
    private function getLocalizedShopData($shops, $addSlugHistory)
    {
        $shop = $shops[0];
        $this->apiService->init($shop->getApiVersion(), $shop->getDomains()->first());
    
        $parameters = ['siteId' => $shop->getApiSiteId()];
        $parameters['near'] = '50.8467,4.3524';
        $parameters['language'] = 'fr';
    
        //add fr data
        list($data) = $this->apiService->getLocations($parameters, false);
        $this->addDataToShopLocation($data, $shop, true, $addSlugHistory);
    
    
        //complete nl data
        $parameters['language'] = 'nl';
        list($data) = $this->apiService->getLocations($parameters, false);
        $this->addDataToShopLocation($data, $shop, false, $addSlugHistory);
    
    }
}
