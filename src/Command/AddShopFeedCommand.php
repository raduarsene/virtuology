<?php

namespace App\Command;

use App\Entity\Shop;
use App\Entity\ShopFeedData;
use App\Service\Api\ApiInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AddShopFeedCommand
 *
 * @package App\Command
 */
class AddShopFeedCommand extends Command
{
    // to make your command lazily loaded, configure the $defaultName static property,
    // so it will be instantiated only when the command is actually called.
    protected static $defaultName = 'app:add-shop-feed';

    /** @var EntityManagerInterface */
    private $em;

    /** @var ApiInterface */
    private $apiService;

    /**
     * AddShopFeedCommand constructor.
     *
     * @param EntityManagerInterface $em
     * @param ApiInterface           $apiService
     */
    public function __construct( EntityManagerInterface $em, ApiInterface $apiService)
    {
        parent::__construct();

        $this->em = $em;
        $this->apiService = $apiService;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Add/update shop(s) feed data, depending on options (use ex: bin/console app:add-shop-feed smartphone 1 2 -u)')
            ->addArgument('type', InputArgument::REQUIRED, 'The type of feed to add. Available: smartphone')
            ->addArgument('shops', InputArgument::IS_ARRAY | InputArgument::REQUIRED, 'The shops id to add feed (separate multiple ids with a space)!')
            ->addOption('update', 'u', InputOption::VALUE_NONE, 'If set, will update feed data no matter if the shop has feed or not, otherwise will add feed data only if the shop doesnt have')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $type = $input->getArgument('type');
        $shopsId = $input->getArgument('shops');
        $toUpdate = (bool)$input->getOption('update');

        $output->writeln("Command started\n");

        $availableTypes = [ShopFeedData::TYPE_SMARTPHONE];
        if (!in_array($type, $availableTypes)) {
            $output->writeln("<error>Type '".$type."' does not exist. Available types: ".implode(', ', $availableTypes)."</error>");

            return;
        }

        $shops = $this->em->getRepository(Shop::class)->getShopsByFeedData($shopsId, $toUpdate);
        if (empty($shops)) {
            $output->writeln("<error>Not found any shops</error>");

            return;
        }

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion(sprintf("<info>Found %d shop(s) to add feed data, do you want to continue ?  (yes/no)</info>  [<info>yes</info>]:\n", count($shops)));

        if (!$helper->ask($input, $output, $question)) {
            return;
        }

        $output->writeln("Started to search for shop(s) feed data:");

        foreach ($shops as $shopObj) {
            // Set default Shop Domain and API version
            $this->apiService->init($shopObj->getApiVersion(), $shopObj->getDomains()->first());

            list($apiData, $statusCode) = $this->apiService->getFeedData([
                'page'       => null,
                'size'       => null,
                'shopId'     => $shopObj->getId(),
                'type'       => $type,
            ], false);

            if (Response::HTTP_OK === $statusCode) {
                $output->writeln(sprintf("\n<info>Found %d feed data from API, adding them to Shop</info>", count($apiData)));

                $this->addDataToShopFeed($apiData, $shopObj, $type, $toUpdate);

                $output->writeln(sprintf("<info>Added %d feed data to shop with ID: %d</info>", count($apiData), $shopObj->getId()));
            } else {
                $output->writeln(sprintf("\n<error>Not found any feed data for Shop ID: %d</error>", $shopObj->getId()));
            }
        }

        $output->writeln("\nCommand executed successfully.");
    }

    /**
     * @param array  $apiData
     * @param Shop   $shop
     * @param string $type
     * @param bool   $removeOld
     */
    private function addDataToShopFeed($apiData, $shop, $type, $removeOld)
    {
        if (!empty($apiData)) {
            // Remove old Shop Feed Data before adding new one
            if ($removeOld) {
                $this->em->getRepository(ShopFeedData::class)->removeFeedDataByShop($shop->getId());
            }

            foreach ($apiData as $index => $data) {
                $shopFeedData = new ShopFeedData();
                $shopFeedData->setShop($shop)
                    ->setName($data['name'])
                    ->setIndexKey($data['visibilityWeb'])
                    ->setType($type)
                    ->setData($data);

                $this->em->persist($shopFeedData);

                if ($index % 100 === 0) {
                    $this->em->flush();
                }
            }

            $this->em->flush();
        }
    }
}
