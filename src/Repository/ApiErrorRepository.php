<?php

namespace App\Repository;

use App\Entity\ApiError;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ApiError|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApiError|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApiError[]    findAll()
 * @method ApiError[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApiErrorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ApiError::class);
    }
}
