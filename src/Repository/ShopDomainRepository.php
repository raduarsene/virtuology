<?php

namespace App\Repository;

use App\Entity\ShopDomain;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShopDomain|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopDomain|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopDomain[]    findAll()
 * @method ShopDomain[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopDomainRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShopDomain::class);
    }
}
