<?php

namespace App\Repository;

use App\Entity\ShopLocationSlugHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShopLocationSlugHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopLocationSlugHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopLocationSlugHistory[]    findAll()
 * @method ShopLocationSlugHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopLocationSlugHistoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShopLocationSlugHistory::class);
    }

    /**
     * @param int $shopId
     * @return mixed
     */
    public function removeByShop(int $shopId)
    {
        return $this->createQueryBuilder('slsh')
            ->delete()
            ->where('slsh.shop = :shopId')
            ->setParameter('shopId', $shopId)
            ->getQuery()
            ->execute();
    }
}
