<?php

namespace App\Repository;

use App\Entity\Shop;
use App\Entity\ShopLocation;
use App\Service\Api\Request\Location\LocalRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShopLocation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopLocation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopLocation[]    findAll()
 * @method ShopLocation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopLocationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShopLocation::class);
    }

    /**
     * @param int   $shop
     * @param array $parameters
     * @return ShopLocation[]
     */
    public function searchLocationsByCriteria($shop, array $parameters = array())
    {
        $queryBuilder = $this->createQueryBuilder('sl')
            ->where('sl.shop = :shop')
            ->setParameter('shop', $shop);

        if (!empty($parameters['within']) || !empty($parameters['near'])) {
            if (!empty($parameters['near'])) {
                list($northLat, $westLong, $southLat, $eastLong) = $this->getBoundsOfNearAndRadius($parameters);
            } else {
                list($northLat, $westLong, $southLat, $eastLong) = explode(',', $parameters['within']);
            }

            $queryBuilder->andWhere('sl.lat BETWEEN :southLat AND :northLat')
                ->andWhere('sl.lng BETWEEN :westLong AND :eastLong')
                ->setParameter('northLat', $northLat)
                ->setParameter('southLat', $southLat)
                ->setParameter('westLong', $westLong)
                ->setParameter('eastLong', $eastLong);
        }

        if (!empty($parameters['search']) && $search = trim($parameters['search'])) {
            $where = [];

            $searches = explode(',', $search);
            foreach ($searches as $key => $search) {
                if ($search = trim($search)) {
                    $where[] = 'sl.name LIKE :search_'.$key;
                    $where[] = 'sl.brand LIKE :search_'.$key;
                    $where[] = 'sl.city LIKE :search_'.$key;

                    $queryBuilder->setParameter('search_'.$key, '%'.$search.'%');
                }
            }

            $queryBuilder->andWhere(implode(' OR ', $where));
        }

        if (isset($parameters['filter']) && $filter = $parameters['filter']) {
            if (!empty($filter['brandSlugs'])) {
                $where = [];

                foreach ($filter['brandSlugs'] as $key => $brandSlug) {
                    $where[] = 'sl.brand = :brand_slug_'.$key;

                    $queryBuilder->setParameter('brand_slug_'.$key, $brandSlug);
                }

                $queryBuilder->andWhere(implode(' OR ', $where));
            }

            // filter by items key from details (locationItemIds)
            if (!empty($filter['serviceKeys'])) {
                foreach ($filter['serviceKeys'] as $key => $serviceKey) {
                    $queryBuilder->andWhere('sl.details LIKE :service_key_'.$key)
                        ->setParameter('service_key_'.$key, '%"'.$serviceKey.'"%');
                }
            }
        }

        if (!empty($parameters['page'])) {
            $queryBuilder->setFirstResult($parameters['page']);
        }

        if (!empty($parameters['size'])) {
            $queryBuilder->setMaxResults($parameters['size']);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param Shop   $shop
     * @param string $slug
     * @return ShopLocation|null
     */
    public function getLocationBySlug($shop, $slug)
    {
        return $this->createQueryBuilder('sl')
            ->where('sl.shop = :shop')
            ->andWhere('sl.details LIKE :slug')
            ->setParameter('shop', $shop)
            ->setParameter('slug', '%"slug": "'.$slug.'"%')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param Shop  $shop
     * @param array $parameters
     * @return ShopLocation[]
     */
    public function searchNearLocations($shop, array $parameters = array())
    {
        $lat = $lng = null;
        if (!empty($parameters['near'])) {
            list($lat, $lng) = explode(',', $parameters['near']);
        }

        $queryBuilder = $this->createQueryBuilder('sl')
            ->select('sl')
            ->addSelect("(6371 * ACOS(SIN(RADIANS({$lat})) * SIN(RADIANS(sl.lat)) + COS(RADIANS({$lat})) * COS(RADIANS(sl.lat)) * COS(RADIANS(sl.lng) - RADIANS({$lng})))) AS HIDDEN distance")
            ->where('sl.shop = :shop');
        
        $queryBuilder->setParameter('shop', $shop)
            ->orderBy('distance');

        if (!empty($parameters['slug'])) {
            $queryBuilder->andWhere('sl.details NOT LIKE :slug')
                ->setParameter('slug', '%"slug": "'.$parameters['slug'].'"%');
        }

        if (!empty($parameters['radius'])) {
            $queryBuilder->having('distance <= :distance')
                ->setParameter('distance', $parameters['radius'] / 1000); // distance in km
        }

        if (!empty($parameters['page'])) {
            $queryBuilder->setFirstResult($parameters['page']);
        }

        if (!empty($parameters['size'])) {
            $queryBuilder->setMaxResults($parameters['size']);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param int   $shop
     * @param array $parameters
     * @return ShopLocation[]
     */
    public function searchLocationsByShop($shop, array $parameters = array())
    {
        $queryBuilder = $this->createQueryBuilder('sl')
            ->where('sl.shop = :shop')
            ->setParameter('shop', $shop)
            ->orderBy('sl.city');

        if (!empty($parameters['search'])) {
            $queryBuilder->andWhere('sl.name LIKE :search OR sl.city LIKE :search')
                ->setParameter('search', '%'.$parameters['search'].'%');
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param int $shopId
     * @return mixed
     */
    public function removeLocationsByShop(int $shopId)
    {
        return $this->createQueryBuilder('sl')
            ->delete()
            ->where('sl.shop = :shopId')
            ->setParameter('shopId', $shopId)
            ->getQuery()
            ->execute();
    }

    /**
     * @param array $parameters
     * @return array
     */
    private function getBoundsOfNearAndRadius(array $parameters)
    {
        list($lat, $long) = explode(',', $parameters['near']);
        $radiusMeter = !empty($parameters['radius']) ? $parameters['radius'] : LocalRequest::DEFAULT_RADIUS_ON_NEAR;

        // Calculate boundaries
        $earthRadius = 6378.137; // radius of the earth in kilometer
        $pi = pi(); // Math pi
        $meterDegree = (1 / ((2 * $pi / 360) * $earthRadius)) / 1000; // 1 meter in degree

        // Latitudes
        $northLat = $lat + ($radiusMeter * $meterDegree);
        $southLat = $lat - ($radiusMeter * $meterDegree);

        // Longitudes
        $westLong = $long - ($radiusMeter * $meterDegree) / cos($lat * ($pi / 180));
        $eastLong = $long + ($radiusMeter * $meterDegree) / cos($lat * ($pi / 180));

        return [$northLat, $westLong, $southLat, $eastLong];
    }
    
    /**
     * @param array $parameters
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function updateShopLocationTrustpilotId(array $parameters)
    {
        $q = $this->createQueryBuilder('sl')
                    ->update(ShopLocation::class, 'sl')
                    ->set('sl.tpLocationId', ":tpLocationId")
                    ->where('sl.shop = :shop')
                    ->andWhere('sl.externalId = :externalId')
            
                    ->setParameter('shop', $parameters['shop'])
                    ->setParameter('externalId', $parameters['externalId'])
                    ->setParameter('tpLocationId', $parameters['tpLocationId'])
                    ->getQuery();
            ;
            
        return $q->execute();
    }
    
    /**
     * @param array $parameters
     *
     * @return mixed
     */
    public function updateShopLocationTrustpilotReviews(array $parameters)
    {
        $q = $this->createQueryBuilder('sl')
                  ->update(ShopLocation::class, 'sl')
                  ->set('sl.tpScore', ":tpScore")
                  ->set('sl.tpReviewsNo', ":tpReviewsNo")
                  ->where('sl.shop = :shop')
                  ->andWhere('sl.tpLocationId = :tpLocationId')
    
                  ->setParameter('shop', $parameters['shop'])
                  ->setParameter('tpScore', $parameters['tpScore'])
                  ->setParameter('tpReviewsNo', $parameters['tpReviewsNo'])
                  ->setParameter('tpLocationId', $parameters['tpLocationId'])
                  ->getQuery();
        ;
    
        return $q->execute();
    }
}
