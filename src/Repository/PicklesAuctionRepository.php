<?php

namespace App\Repository;

use App\Entity\PicklesAuction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PicklesAuction|null find($id, $lockMode = null, $lockVersion = null)
 * @method PicklesAuction|null findOneBy(array $criteria, array $orderBy = null)
 * @method PicklesAuction[]    findAll()
 * @method PicklesAuction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PicklesAuctionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PicklesAuction::class);
    }
    
    /**
     * @param array $params
     *
     * @return mixed
     */
    public function getCountByFilters($params = [])
    {
        $qb = $this->createQueryBuilder('p')
                   ->select('COUNT(p.id)');
        
        $this->applyFilters($qb, $params);
        
        return $qb
            ->getQuery()
            ->getSingleScalarResult();
    }
    
    public function getByFilters($params = [])
    {
        
        if(!isset($params['page']) || empty($params['page'])) $params['page'] = 1;
        if(!isset($params['n']) || empty($params['n'])) $params['n'] = 10;
        
        $qb = $this->createQueryBuilder('p')
                   ->select('p')
                   ->setFirstResult(($params['page']-1)*$params['n'])
                   ->setMaxResults($params['n']);
        
        $this->applyFilters($qb, $params);
        
        return $qb
            ->getQuery()
            ->getArrayResult();
    }
    
    
    /**
     * @param       $qb
     * @param array $params
     */
    protected function applyFilters($qb, $params=[])
    {
        
        if(isset($params['organizationCode']) && !empty($params['organizationCode'])) {
            $qb
                ->andWhere("p.organizationCode = :organizationCode")
                ->setParameter('organizationCode', $params['organizationCode']);
        }
    }
}
