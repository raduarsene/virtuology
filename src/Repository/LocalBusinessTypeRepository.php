<?php

namespace App\Repository;

use App\Entity\LocalBusinessType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method LocalBusinessType|null find($id, $lockMode = null, $lockVersion = null)
 * @method LocalBusinessType|null findOneBy(array $criteria, array $orderBy = null)
 * @method LocalBusinessType[]    findAll()
 * @method LocalBusinessType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocalBusinessTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LocalBusinessType::class);
    }

    // /**
    //  * @return LocalBusinessType[] Returns an array of LocalBusinessType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LocalBusinessType
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
