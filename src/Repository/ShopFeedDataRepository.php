<?php

namespace App\Repository;

use App\Entity\ShopFeedData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShopFeedData|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopFeedData|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopFeedData[]    findAll()
 * @method ShopFeedData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopFeedDataRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShopFeedData::class);
    }

    /**
     * @param array $parameters
     * @return ShopFeedData[]
     */
    public function getFeedByParameters(array $parameters = array())
    {
        $queryBuilder = $this->createQueryBuilder('fd');

        if (!empty($parameters['shopId'])) {
            $queryBuilder->where('fd.shop = :shopId')
                ->setParameter('shopId', $parameters['shopId']);
        }

        if (!empty($parameters['type'])) {
            $queryBuilder->andWhere('fd.type = :type')
                ->setParameter('type', $parameters['type']);
        }

        if (isset($parameters['isVisible'])) {
            $queryBuilder->andWhere('fd.indexKey = :isVisible')
                ->setParameter('isVisible', $parameters['isVisible']);
        }

        if (!empty($parameters['page'])) {
            $queryBuilder->setFirstResult($parameters['page']);
        }

        if (!empty($parameters['size'])) {
            $queryBuilder->setMaxResults($parameters['size']);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param null|int $shopId
     * @return mixed
     */
    public function removeFeedDataByShop($shopId = null)
    {
        $queryBuilder = $this->createQueryBuilder('fd')
            ->delete();

        if ($shopId) {
            $queryBuilder->where('fd.shop = :shopId')
                ->setParameter('shopId', $shopId);
        }

        return $queryBuilder->getQuery()->execute();
    }
}
