<?php

namespace App\Repository;

use App\Entity\Shop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Shop|null find($id, $lockMode = null, $lockVersion = null)
 * @method Shop|null findOneBy(array $criteria, array $orderBy = null)
 * @method Shop[]    findAll()
 * @method Shop[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Shop::class);
    }

    /**
     * @param string|null $shopCode
     * @param bool        $withLocations
     * @return Shop[]
     */
    public function getShopsByLocation(string $shopCode = null, bool $withLocations = false)
    {
        $queryBuilder = $this->createQueryBuilder('s')
            ->join('s.domains', 'd');

        if ($shopCode) {
            $queryBuilder->where('s.code = :shopCode')
                ->setParameter('shopCode', $shopCode);
        }

        if (!$withLocations) {
            $queryBuilder->leftJoin('s.locations', 'l')
                ->andWhere('l.id IS NULL');
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param array $shopsId
     * @param bool  $withFeedData
     * @return array
     */
    public function getShopsByFeedData(array $shopsId = array(), bool $withFeedData = false)
    {
        $queryBuilder = $this->createQueryBuilder('s')
            ->join('s.domains', 'd');

        if (!empty($shopsId)) {
            $queryBuilder->where('s.id IN (:shopsId)')
                ->setParameter('shopsId', $shopsId);
        }

        if (!$withFeedData) {
            $queryBuilder->leftJoin('s.feedData', 'fd')
                ->andWhere('fd.id IS NULL');
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
