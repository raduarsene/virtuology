<?php

namespace App\Repository;

use App\Entity\ShopTemplate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShopTemplate|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopTemplate|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopTemplate[]    findAll()
 * @method ShopTemplate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopTemplateRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShopTemplate::class);
    }

    /**
     * @param int    $shopId
     * @param string $type
     * @param bool   $valueAsKey
     * @return ShopTemplate[]
     */
    public function getShopsByType(int $shopId, string $type, bool $valueAsKey = false)
    {
        $shopTemplates = $this->createQueryBuilder('st')
            ->join('st.attributeValue', 'av')
            ->join('av.attribute', 'a')
            ->where('st.shop = :shop')
            ->andWhere('a.type = :type')
            ->setParameter('shop', $shopId)
            ->setParameter('type', $type)
            ->orderBy('st.position')
            ->getQuery()
            ->getResult();

        if ($valueAsKey) {
            $result = [];
            /** @var ShopTemplate $shopTemplate */
            foreach ($shopTemplates as $shopTemplate) {
                $result[$shopTemplate->getAttributeValue()->getValue()] = $shopTemplate;
            }

            $shopTemplates = $result;
        }

        return $shopTemplates;
    }
}
