<?php

namespace App\Validator\Constraints;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class UniqueInCollectionValidator extends ConstraintValidator
{
    public function validate($entities, Constraint $constraint)
    {
        if (!$constraint instanceof UniqueInCollection) {
            throw new UnexpectedTypeException($constraint, UniqueInCollection::class);
        }

        if ($entities) {
            $arrayCollection = new ArrayCollection();
            foreach ($entities as $entity) {
                if (!empty($constraint->method) && method_exists($entity, 'get'.$constraint->method)) {
                    $entity = $entity->{'get'.$constraint->method}();
                }

                if ($arrayCollection->contains($entity)) {
                    $this->context->buildViolation($constraint->message)
                        ->setParameter('{{ field }}', $constraint->method)
                        ->addViolation();

                    return;
                }

                $arrayCollection->add($entity);
            }
        }
    }
}
