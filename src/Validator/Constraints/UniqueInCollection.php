<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueInCollection extends Constraint
{
    public $method = "";

    public $message = 'You can\'t add the same "{{ field }}" multiple times.';
}
