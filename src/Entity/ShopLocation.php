<?php

namespace App\Entity;

use App\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="shop_location", indexes={@ORM\Index(name="external_id_idx", columns={"external_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\ShopLocationRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ShopLocation
{
    /** Timestampable trait */
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="locations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $externalId;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $brand;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $city;

    /**
     * @var float|null
     *
     * @ORM\Column(type="decimal", precision=11, scale=8, nullable=true)
     */
    private $lat;

    /**
     * @var float|null
     *
     * @ORM\Column(type="decimal", precision=11, scale=8, nullable=true)
     */
    private $lng;

    /**
     * @var array|string|null
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private $details;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $tpLocationId;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=2, nullable=true)
     */
    private $tpScore;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tpReviewsNo;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set shop.
     *
     * @param Shop $shop
     * @return $this
     */
    public function setShop(Shop $shop)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop.
     *
     * @return Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string|null $externalId
     * @return $this
     */
    public function setExternalId(?string $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @param string|null $slug
     * @return ShopLocation
     */
    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Set brand.
     *
     * @param string|null $brand
     * @return $this
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand.
     *
     * @return string|null
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set city.
     *
     * @param string|null $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set lat.
     *
     * @param float|null $lat
     * @return $this
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat.
     *
     * @return float|null
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng.
     *
     * @param float|null $lng
     * @return $this
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng.
     *
     * @return float|null
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Set details.
     *
     * @param array|string|null $details
     * @return $this
     */
    public function setDetails($details)
    {
        // If is string json then change to array
        if (!empty($details) && is_string($details) && $array = json_decode($details, true)) {
            $details = $array;
        }

        $this->details = $details;

        return $this;
    }

    /**
     * Get details as string.
     *
     * @return string|null
     */
    public function getDetails()
    {
        $details = $this->details;

        if (is_array($details)) {
            $details = json_encode($details);
        }

        return $details;
    }

    /**
     * Get details as array.
     *
     * @return array|null
     */
    public function getDetailsArray()
    {
        return $this->details;
    }
    
    /**
     * @return mixed
     */
    public function getTpLocationId()
    {
        return $this->tpLocationId;
    }
    
    /**
     * @param mixed $tpLocationId
     */
    public function setTpLocationId($tpLocationId): void
    {
        $this->tpLocationId = $tpLocationId;
    }
    
    /**
     * @return mixed
     */
    public function getTpScore()
    {
        return $this->tpScore;
    }
    
    /**
     * @param mixed $tpScore
     */
    public function setTpScore($tpScore): void
    {
        $this->tpScore = $tpScore;
    }
    
    /**
     * @return mixed
     */
    public function getTpReviewsNo()
    {
        return $this->tpReviewsNo;
    }
    
    /**
     * @param mixed $tpReviewsNo
     */
    public function setTpReviewsNo($tpReviewsNo): void
    {
        $this->tpReviewsNo = $tpReviewsNo;
    }
    
    

   
}
