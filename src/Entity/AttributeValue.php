<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="attribute_value", uniqueConstraints={@ORM\UniqueConstraint(columns={"attribute_id", "value"})})
 * @UniqueEntity(fields={"attribute", "value"})
 * @ORM\Entity(repositoryClass="App\Repository\AttributeValueRepository")
 * @ORM\EntityListeners({"App\Event\Entity\AttributeValueListener"})
 */
class AttributeValue
{
    // Attribute default blocks
    const VALUE_MAP_SEARCH = 'map_search';
    const VALUE_MAP_SEARCH_LIST = 'map_search_list';
    const VALUE_SEARCH_NO_MAP = 'search_no_map';
    const VALUE_SIMPLE = 'simple';
    const VALUE_WITH_OPENING_HOURS = 'with_opening_hours';
    const VALUE_WITH_SEARCH = 'with_search';
    const VALUE_SEARCH_AND_OPENING_HOURS = 'search_and_opening_hours';
    const VALUE_WITH_MAP = 'with_map';
    const VALUE_WITH_IMAGE = 'with_image';
    const VALUE_WITH_IMAGE_INFO = 'with_image_info';
    const VALUE_WITH_IMAGE_AND_MAP = 'with_image_and_map';
    const VALUE_WITH_TEXT = 'with_text';
    const VALUE_IMAGE_LEFT = 'image_left';
    const VALUE_IMAGE_RIGHT = 'image_right';
    const VALUE_WITH_SLIDESHOW = 'with_slideshow';

    // Global Attribute values
    const VALUE_OPEN_HOURS_WEEK = 'open_hours_week';
    const VALUE_OPEN_HOURS_EXCEPTION = 'open_hours_exception';
    const VALUE_LOCATION_ROUTE_MAP = 'location_route_map';
    const VALUE_LOCATION_ROUTE_REDIRECT = 'location_route_redirect';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Attribute
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Attribute", inversedBy="attributeValues")
     * @ORM\JoinColumn(nullable=false)
     */
    private $attribute;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     * @Assert\Regex("/^[a-zA-Z][\w\-]*$/")
     */
    private $value;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $imageName;

    /**
     * Unmapped property to handle image uploads
     */
    private $image;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDefault;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set Attribute.
     *
     * @param Attribute $attribute
     * @return $this
     */
    public function setAttribute(Attribute $attribute): self
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Get Attribute.
     *
     * @return Attribute|null
     */
    public function getAttribute(): ?Attribute
    {
        return $this->attribute;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set value.
     *
     * @param string $value
     * @return $this
     */
    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set imageName.
     *
     * @param string|null $imageName
     * @return $this
     */
    public function setImageName(?string $imageName): self
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get imageName.
     *
     * @return string|null
     */
    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    /**
     * Set image file.
     *
     * @param UploadedFile|File|null $image
     * @return $this
     */
    public function setImage($image)
    {
        $this->image = $image;

        // Update field to persist entity
        $this->setUpdatedAt(new \DateTime());

        return $this;
    }

    /**
     * Get image file.
     *
     * @return UploadedFile|File|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set isDefault.
     *
     * @param bool|null $isDefault
     * @return $this
     */
    public function setIsDefault($isDefault): self
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault.
     *
     * @return bool|null
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
