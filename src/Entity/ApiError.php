<?php

namespace App\Entity;

use App\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="api_error")
 * @ORM\Entity(repositoryClass="App\Repository\ApiErrorRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ApiError
{
    /** Timestampable trait */
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="apiErrors")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=16)
     * @Assert\NotBlank()
     */
    private $errorCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $apiUrl;

    /**
     * @var array|string|null
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private $apiQueryString;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $errorMessage;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->errorCode.' - '.$this->apiUrl;
    }

    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set shop.
     *
     * @param Shop $shop
     * @return $this
     */
    public function setShop(Shop $shop)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop.
     *
     * @return Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set errorCode.
     *
     * @param string $errorCode
     * @return $this
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;

        return $this;
    }

    /**
     * Get errorCode.
     *
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * Set apiUrl.
     *
     * @param string $apiUrl
     * @return $this
     */
    public function setApiUrl($apiUrl)
    {
        $this->apiUrl = $apiUrl;

        return $this;
    }

    /**
     * Get apiUrl.
     *
     * @return string
     */
    public function getApiUrl()
    {
        return $this->apiUrl;
    }

    /**
     * Set apiQueryString.
     *
     * @param array|string|null $apiQueryString
     * @return $this
     */
    public function setApiQueryString($apiQueryString)
    {
        // If is string json then change to array
        if (!empty($apiQueryString) && is_string($apiQueryString) && $array = json_decode($apiQueryString, true)) {
            $apiQueryString = $array;
        }

        $this->apiQueryString = $apiQueryString;

        return $this;
    }

    /**
     * Get apiQueryString as string.
     *
     * @return string|null
     */
    public function getApiQueryString()
    {
        $apiQueryString = $this->apiQueryString;

        if (is_array($apiQueryString)) {
            $apiQueryString = json_encode($apiQueryString);
        }

        return $apiQueryString;
    }

    /**
     * Get apiQueryString as array.
     *
     * @return array|null
     */
    public function getApiQueryStringArray()
    {
        return $this->apiQueryString;
    }

    /**
     * Set errorMessage.
     *
     * @param string|null $errorMessage
     * @return $this
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }

    /**
     * Get errorMessage.
     *
     * @return string|null
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }
}
