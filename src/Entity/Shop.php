<?php

namespace App\Entity;

use App\Validator\Constraints as AppAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="shop")
 * @ORM\Entity(repositoryClass="App\Repository\ShopRepository")
 * @ORM\EntityListeners({"App\Event\Entity\ShopListener"})
 */
class Shop
{
    /** VERSIONS for API */
    const API_VERSION_LOCAL = 'v0'; // for local database
    const API_VERSION_2 = 'v2';
    const API_VERSION_3 = 'v3';

    /** Types of Map */
    const MAP_TYPE_GOOGLE = 'google';
    const MAP_TYPE_HERE = 'here';

    /** Shop codes */
    const CODE_CARREFOUR = 'carrefour-be';
    const CODE_MEDIMARKET = 'medimarket-be';
    const CODE_LEONDEBRUXELLES = 'leondebruxelles-fr';
    const CODE_LLOYDSPHARMA = 'lloydspharma-be';
    const CODE_VOO = 'voo-be';
    const CODE_LOLALIZA = 'lolaliza-be';
    const CODE_COVID = 'covid-solidarity';
    const CODE_MOBILOSOFT_TEST = 'mobilosoft-test';
    const CODE_LES3BRASSEURS = 'les3brasseurs-fr';
    const CODE_PV = 'pv-be';
    const CODE_WORKFORCESOLUTIONS = 'workforcesolutions-us';
    const CODE_LOUISDELHAIZE = 'louisdelhaize-be';
    const CODE_FARM = 'farm-be';

    const CODE_MANPOWER = 'manpower-be';
    const CODE_HACKETT = 'hackett-int';
    const CODE_FACONNABLE = 'faconnable-int';
    const CODE_PICKLES = 'pickles-au';
    const CODE_MULTIPHARMA = 'multipharma';
    
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * This is a unique code that identifies this shop (used for icons, translations, assets ...)
     *
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     * @Assert\NotBlank()
     * @Assert\Regex("/^[a-z_][\w\-]+$/i")
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=16)
     * @Assert\NotBlank()
     */
    private $apiVersion;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $apiSiteId;

    /**
     * Token used for API v3
     *
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $apiToken;

    /**
     * Key param used for cloudinary API images
     *
     * @var string|null
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $apiKey;

    /**
     * Type of map to show. One of the values from const MAP_TYPE
     *
     * @var string|null
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $mapType;

    /**
     * Map credentials.
     * Ex. for GOOGLE with 'key' or 'key_premium':
     * [
     *  'key'         => 'google_map_key',
     *  'key_premium' => 'google_map_key_premium'
     * ]
     *
     * Ex. for HERE with 'id' and 'code':
     * [
     *  'id'   => 'here_map_id',
     *  'code' => 'here_map_code'
     * ]
     *
     * @var array|string|null
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private $mapCredentials;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $allowRobots;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $demoShop;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $maintenance;

    /**
     * @var ShopDomain[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ShopDomain", mappedBy="shop", orphanRemoval=true, cascade={"all"},
     *                                                      fetch="EAGER")
     */
    private $domains;

    /**
     * @var ShopLocation[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ShopLocation", mappedBy="shop", cascade={"all"})
     */
    private $locations;

    /**
     * @var ShopFeedData[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ShopFeedData", mappedBy="shop", cascade={"all"})
     */
    private $feedData;

    /**
     * @var ApiError[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ApiError", mappedBy="shop", cascade={"all"})
     */
    private $apiErrors;

    /**
     * @var ShopTemplate[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ShopTemplate", mappedBy="shop", orphanRemoval=true, cascade={"all"},
     *                                                        fetch="EAGER")
     * @AppAssert\UniqueInCollection(method="attribute")
     */
    private $templates;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * used for V3 because in staging we have shops on live and on staging for mobilosoft
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $apiUrl;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LocalBusinessType", mappedBy="shop", orphanRemoval=true, cascade={"all"})
     */
    private $localBusinessTypes;

    /**
     * @var array|string|null
     * @ORM\Column(type="json", nullable=true)
     */
    private $trustPilotConf;
    

    /**
     * Shop constructor.
     */
    public function __construct()
    {
        $this->apiVersion = self::API_VERSION_2;

        $this->domains = new ArrayCollection();
        $this->locations = new ArrayCollection();
        $this->feedData = new ArrayCollection();
        $this->apiErrors = new ArrayCollection();
        $this->templates = new ArrayCollection();
        $this->localBusinessTypes = new ArrayCollection();

    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code.
     *
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set apiVersion.
     *
     * @param string $apiVersion
     * @return $this
     */
    public function setApiVersion($apiVersion)
    {
        $this->apiVersion = $apiVersion;

        return $this;
    }

    /**
     * Get apiVersion.
     *
     * @return string
     */
    public function getApiVersion()
    {
        return $this->apiVersion;
    }

    /**
     * Set apiSiteId.
     *
     * @param string|null $apiSiteId
     * @return $this
     */
    public function setApiSiteId($apiSiteId)
    {
        $this->apiSiteId = $apiSiteId;

        return $this;
    }

    /**
     * Get apiSiteId.
     *
     * @return string|null
     */
    public function getApiSiteId()
    {
        return $this->apiSiteId;
    }

    /**
     * Set apiToken.
     *
     * @param string|null $apiToken
     * @return $this
     */
    public function setApiToken($apiToken)
    {
        $this->apiToken = $apiToken;

        return $this;
    }

    /**
     * Get apiToken.
     *
     * @return string|null
     */
    public function getApiToken()
    {
        return $this->apiToken;
    }

    /**
     * Set apiKey.
     *
     * @param string|null $apiKey
     * @return $this
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Get apiKey.
     *
     * @return string|null
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Set mapType.
     *
     * @param string|null $mapType
     * @return $this
     */
    public function setMapType($mapType)
    {
        $this->mapType = $mapType;

        return $this;
    }

    /**
     * Get mapType.
     *
     * @return string|null
     */
    public function getMapType()
    {
        return $this->mapType;
    }

    /**
     * Set mapCredentials.
     *
     * @param array|string|null $mapCredentials
     * @return $this
     */
    public function setMapCredentials($mapCredentials)
    {
        // If is string json then change to array
        if (!empty($mapCredentials) && is_string($mapCredentials) && $array = json_decode($mapCredentials, true)) {
            $mapCredentials = $array;
        }

        $this->mapCredentials = $mapCredentials;

        return $this;
    }

    /**
     * Get mapCredentials as json encoded string.
     *
     * @return string|null
     */
    public function getMapCredentials()
    {
        $mapCredentials = $this->mapCredentials;

        if (is_array($mapCredentials)) {
            $mapCredentials = json_encode($mapCredentials);
        }

        return $mapCredentials;
    }

    /**
     * Get mapCredentials as array.
     *
     * @return array|null
     */
    public function getMapCredentialsArray()
    {
        return $this->mapCredentials;
    }

    /**
     * Set allowRobots.
     *
     * @param bool|null $allowRobots
     * @return $this
     */
    public function setAllowRobots($allowRobots)
    {
        $this->allowRobots = $allowRobots;

        return $this;
    }

    /**
     * Get allowRobots.
     *
     * @return bool|null
     */
    public function getAllowRobots()
    {
        return $this->allowRobots;
    }

    /**
     * Set demoShop.
     *
     * @param bool|null $demoShop
     * @return $this
     */
    public function setDemoShop($demoShop)
    {
        $this->demoShop = $demoShop;

        return $this;
    }

    /**
     * Get demoShop.
     *
     * @return bool|null
     */
    public function getDemoShop()
    {
        return $this->demoShop;
    }

    /**
     * Set maintenance.
     *
     * @param bool|null $maintenance
     * @return $this
     */
    public function setMaintenance($maintenance)
    {
        $this->maintenance = $maintenance;

        return $this;
    }

    /**
     * Get maintenance.
     *
     * @return bool|null
     */
    public function getMaintenance()
    {
        return $this->maintenance;
    }

    /**
     * Add domain to Shop.
     *
     * @param ShopDomain $domain
     * @return $this
     */
    public function addDomain(ShopDomain $domain)
    {
        if (!$this->domains->contains($domain)) {
            $domain->setShop($this);
            $this->domains->add($domain);
        }

        return $this;
    }

    /**
     * Add domains to Shop.
     *
     * @param ShopDomain[] $domains
     * @return $this
     */
    public function setDomains(ShopDomain ...$domains)
    {
        foreach ($domains as $domain) {
            $this->addDomain($domain);
        }

        return $this;
    }

    /**
     * Get Shop domains.
     *
     * @return Collection
     */
    public function getDomains()
    {
        return $this->domains;
    }
    
    /**
     * Get default domain of this shop
     *
     * @return ShopDomain|null
     */
    public function getDefaultDomain()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('defaultDomain', true));
        $defaultDomain = $this->domains->matching($criteria)->first();

        if (!$defaultDomain) {
            $defaultDomain = $this->domains->first();
        }

        return $defaultDomain;
    }

    /**
     * Get Shop domain by Language.
     *
     * @param Language $language
     * @return ShopDomain|bool
     */
    public function getDomainByLanguage(Language $language)
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('language', $language));

        return $this->domains->matching($criteria)->first();
    }

    /**
     * Get domains for each available language.
     *
     * @return array
     */
    public function getLanguageDomains()
    {
        $languageDomains = [];

        foreach ($this->domains as $domain) {
            $languageDomains[$domain->getLanguage()->getCode()] = $domain->getName();

            foreach ($domain->getAvailableLanguages() as $language) {
                if (!isset($languageDomains[$language->getCode()])) {
                    $languageDomains[$language->getCode()] = $domain->getName();
                }
            }
        }

        return $languageDomains;
    }

    /**
     * Remove domain from Shop.
     *
     * @param ShopDomain $domain
     * @return $this
     */
    public function removeDomain(ShopDomain $domain)
    {
        $this->domains->removeElement($domain);

        return $this;
    }

    /**
     * Add location to Shop.
     *
     * @param ShopLocation $location
     * @return $this
     */
    public function addLocation(ShopLocation $location)
    {
        if (!$this->locations->contains($location)) {
            $location->setShop($this);
            $this->locations->add($location);
        }

        return $this;
    }

    /**
     * Get Shop locations.
     *
     * @return Collection
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Remove location from Shop.
     *
     * @param ShopLocation $location
     * @return $this
     */
    public function removeLocation(ShopLocation $location)
    {
        $this->locations->removeElement($location);

        return $this;
    }

    /**
     * Add feedData to Shop.
     *
     * @param ShopFeedData $feedData
     * @return $this
     */
    public function addFeedData(ShopFeedData $feedData)
    {
        if (!$this->feedData->contains($feedData)) {
            $feedData->setShop($this);
            $this->feedData->add($feedData);
        }

        return $this;
    }

    /**
     * Get Shop feedData.
     *
     * @return Collection
     */
    public function getFeedData()
    {
        return $this->feedData;
    }

    /**
     * Remove feedData from Shop.
     *
     * @param ShopFeedData $feedData
     * @return $this
     */
    public function removeFeedData(ShopFeedData $feedData)
    {
        $this->feedData->removeElement($feedData);

        return $this;
    }

    /**
     * Add ApiError to Shop.
     *
     * @param ApiError $apiError
     * @return $this
     */
    public function addApiError(ApiError $apiError)
    {
        if (!$this->apiErrors->contains($apiError)) {
            $apiError->setShop($this);
            $this->apiErrors->add($apiError);
        }

        return $this;
    }

    /**
     * Get Shop apiErrors.
     *
     * @return Collection
     */
    public function getApiErrors()
    {
        return $this->apiErrors;
    }

    /**
     * Remove ApiError from Shop.
     *
     * @param ApiError $apiError
     * @return $this
     */
    public function removeApiError(ApiError $apiError)
    {
        $this->apiErrors->removeElement($apiError);

        return $this;
    }

    /**
     * Add template to Shop.
     *
     * @param ShopTemplate $template
     * @return $this
     */
    public function addTemplate(ShopTemplate $template): self
    {
        if (!$this->templates->contains($template)) {
            $template->setShop($this);
            $this->templates->add($template);
        }

        return $this;
    }

    /**
     * Get Shop templates
     *
     * @return Collection|ShopTemplate[]
     */
    public function getTemplates()
    {
        $templates = $this->templates->toArray();

        usort($templates, function (ShopTemplate $first, ShopTemplate $second) {
            if (null === $first->getPosition() && null !== $second->getPosition()) {
                return 1;
            }

            if (null !== $first->getPosition() && null === $second->getPosition()) {
                return -1;
            }

            $firstAttributeValue = $first->getAttributeValue();
            $secondAttributeValue = $second->getAttributeValue();
            if ($firstAttributeValue === $secondAttributeValue) {
                return 0;
            } elseif (null === $firstAttributeValue) {
                return 1;
            } elseif (null === $secondAttributeValue) {
                return -1;
            }

            $firstAttributeType = $first->getAttribute()->getType();
            $secondAttributeType = $second->getAttribute()->getType();
            if ($firstAttributeType === Attribute::TYPE_PAGE_HOME && $secondAttributeType === Attribute::TYPE_PAGE_SHOW) {
                return -1;
            }
            if ($firstAttributeType === Attribute::TYPE_PAGE_SHOW && $secondAttributeType === Attribute::TYPE_PAGE_HOME) {
                return 1;
            }

            return $first->getPosition() <=> $second->getPosition();
        });

        return $templates;
    }

    /**
     * Remove template from Shop
     *
     * @param ShopTemplate $template
     * @return $this
     */
    public function removeTemplate(ShopTemplate $template): self
    {
        if ($this->templates->contains($template)) {
            $this->templates->removeElement($template);
        }

        return $this;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getApiUrl(): ?string
    {
        return $this->apiUrl;
    }

    public function setApiUrl(?string $apiUrl): self
    {
        $this->apiUrl = $apiUrl;

        return $this;
    }

    /**
     * @return Collection|LocalBusinessType[]
     */
    public function getLocalBusinessTypes(): Collection
    {
        return $this->localBusinessTypes;
    }

    public function addLocalBusinessType(LocalBusinessType $localBusinessType): self
    {
        if (!$this->localBusinessTypes->contains($localBusinessType)) {
            $this->localBusinessTypes[] = $localBusinessType;
            $localBusinessType->setShop($this);
        }

        return $this;
    }

    public function removeLocalBusinessType(LocalBusinessType $localBusinessType): self
    {
        if ($this->localBusinessTypes->contains($localBusinessType)) {
            $this->localBusinessTypes->removeElement($localBusinessType);
            // set the owning side to null (unless already changed)
            if ($localBusinessType->getShop() === $this) {
                $localBusinessType->setShop(null);
            }
        }

        return $this;
    }
    
    /**
     * @param string|null $brandSlug
     *
     * @return string|null
     */
    public function getLocalBusinessType($brandSlug = null)
    {
        $structuredDataType =  "LocalBusiness";
        $localBusinessTypes = $this->getLocalBusinessTypes();
        
        /** @var LocalBusinessType $localBusinessType */
        foreach ($localBusinessTypes as $localBusinessType) {
            $structuredDataType = $localBusinessType->getStructuredDataType();
            if(!empty($brandSlug) && $localBusinessType->getBrandSlug() == $brandSlug) {
                return $structuredDataType;
            }
        }
    
        return $structuredDataType;
    }
    
    /**
     * @return array|false|string|null
     */
    public function getTrustPilotConf()
    {
        
        $trustPilotConf =  $this->trustPilotConf;
    
        if (is_array($trustPilotConf)) {
            $trustPilotConf = json_encode($trustPilotConf);
        }
    
        return $trustPilotConf;
    }
    
    /**
     * @param $trustPilotConf
     *
     * @return Shop
     */
    public function setTrustPilotConf($trustPilotConf): self
    {
        // If is string json then change to array
        if (!empty($trustPilotConf) && is_string($trustPilotConf) && $array = json_decode($trustPilotConf, true)) {
            $trustPilotConf = $array;
        }
        
        $this->trustPilotConf = $trustPilotConf;

        return $this;
    }
    
}
