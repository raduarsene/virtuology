<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LocalBusinessTypeRepository")
 * @ORM\HasLifecycleCallbacks
 */
class LocalBusinessType
{
    /** Timestampable trait */
    use Timestampable;
    
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="localBusinessTypes")
     */
    private $shop;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $brandSlug;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $structuredDataType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    public function getBrandSlug(): ?string
    {
        return $this->brandSlug;
    }

    public function setBrandSlug(?string $brandSlug): self
    {
        $this->brandSlug = $brandSlug;

        return $this;
    }

    public function getStructuredDataType(): ?string
    {
        return $this->structuredDataType;
    }

    public function setStructuredDataType(string $structuredDataType): self
    {
        $this->structuredDataType = $structuredDataType;

        return $this;
    }
}
