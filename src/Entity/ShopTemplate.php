<?php

namespace App\Entity;

use App\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="shop_template")
 * @UniqueEntity(fields={"shop", "attributeValue"})
 * @ORM\Entity(repositoryClass="App\Repository\ShopTemplateRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ShopTemplate
{
    /** Timestampable trait */
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="templates")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    /**
     * Non entity field used for form creation
     *
     * @var Attribute
     */
    private $attribute;

    /**
     * @var AttributeValue
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\AttributeValue")
     * @ORM\JoinColumn(nullable=false)
     */
    private $attributeValue;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $position;

    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set shop.
     *
     * @param Shop|null $shop
     * @return $this
     */
    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop.
     *
     * @return Shop|null
     */
    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    /**
     * Set attribute.
     *
     * @param Attribute|null $attribute
     * @return $this
     */
    public function setAttribute(?Attribute $attribute): self
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Get attribute.
     *
     * @return Attribute|null
     */
    public function getAttribute(): ?Attribute
    {
        return $this->attributeValue ? $this->attributeValue->getAttribute() : $this->attribute;
    }

    /**
     * Set attributeValue.
     *
     * @param AttributeValue|null $attributeValue
     * @return $this
     */
    public function setAttributeValue(?AttributeValue $attributeValue): self
    {
        $this->attributeValue = $attributeValue;

        return $this;
    }

    /**
     * Get attributeValue.
     *
     * @return AttributeValue|null
     */
    public function getAttributeValue(): ?AttributeValue
    {
        return $this->attributeValue;
    }

    /**
     * Set position.
     *
     * @param int|null $position
     * @return $this
     */
    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }
}
