<?php

namespace App\Entity;

use App\Entity\Traits\Timestampable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="attribute")
 * @UniqueEntity("block")
 * @ORM\Entity(repositoryClass="App\Repository\AttributeRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Attribute
{
    // Attribute types
    const TYPE_PAGE_HOME = 'page_home';
    const TYPE_PAGE_SHOW = 'page_show';
    const TYPE_GLOBAL = 'global';

    // Attribute default blocks
    const BLOCK_MAP = 'map';
    const BLOCK_LOCATIONS = 'locations';
    const BLOCK_LOCATION_DETAILS = 'location_details';
    const BLOCK_LOCATION_BANNER = 'location_banner';
    const BLOCK_LOCATION_DESCRIPTION = 'location_description';
    const BLOCK_LOCATION_PROMOTIONS = 'location_promotions';
    const BLOCK_LOCATION_EVENTS = 'location_events';
    const BLOCK_LOCATION_SERVICES = 'location_services';
    const BLOCK_NEAR_LOCATION = 'near_location';
    const BLOCK_OPEN_HOURS = 'open_hours';
    const BLOCK_LOCATION_ROUTE = 'location_route';

    /** Timestampable trait */
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    private $type;

    /**
     * Name of the directory and block of the attribute
     *
     * @var string
     *
     * @ORM\Column(type="string", length=20, unique=true)
     * @Assert\NotBlank()
     * @Assert\Regex("/^[a-zA-Z][\w\-]*$/")
     */
    private $block;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var AttributeValue[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\AttributeValue", mappedBy="attribute", orphanRemoval=true, cascade={"all"}, fetch="EAGER")
     */
    private $attributeValues;

    /**
     * Attribute constructor.
     */
    public function __construct()
    {
        $this->attributeValues = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set type.
     *
     * @param string $type
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * Set block.
     *
     * @param string $block
     * @return $this
     */
    public function setBlock(string $block): self
    {
        $this->block = $block;

        return $this;
    }

    /**
     * Get block.
     *
     * @return string|null
     */
    public function getBlock(): ?string
    {
        return $this->block;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Add value to Attribute.
     *
     * @param AttributeValue $attributeValue
     * @return $this
     */
    public function addAttributeValue(AttributeValue $attributeValue)
    {
        $attributeValue->setAttribute($this);
        if (!$this->attributeValues->contains($attributeValue)) {
            $this->attributeValues->add($attributeValue);
        }

        return $this;
    }

    /**
     * Add values to Attribute.
     *
     * @param AttributeValue[] $attributeValues
     * @return $this
     */
    public function setAttributeValues(AttributeValue ...$attributeValues)
    {
        $this->attributeValues = new ArrayCollection();
        foreach ($attributeValues as $attributeValue) {
            $this->addAttributeValue($attributeValue);
        }

        return $this;
    }

    /**
     * Get AttributeValue's.
     *
     * @return Collection
     */
    public function getAttributeValues()
    {
        return $this->attributeValues;
    }

    /**
     * Remove AttributeValue from Attribute.
     *
     * @param AttributeValue $attributeValue
     * @return $this
     */
    public function removeAttributeValue(AttributeValue $attributeValue)
    {
        $this->attributeValues->removeElement($attributeValue);

        return $this;
    }
}
