<?php

namespace App\Entity;

use App\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="shop_feed_data", indexes={@ORM\Index(name="indexKey", columns={"index_key"})})
 * @ORM\Entity(repositoryClass="App\Repository\ShopFeedDataRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ShopFeedData
{
    const TYPE_SMARTPHONE = 'smartphone';

    /** Timestampable trait */
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="feedData")
     */
    private $shop;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $indexKey;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $type;

    /**
     * @var array|null
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private $data;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set shop.
     *
     * @param Shop $shop
     * @return $this
     */
    public function setShop(Shop $shop)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop.
     *
     * @return Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set indexKey.
     *
     * @param string|null $indexKey
     * @return $this
     */
    public function setIndexKey($indexKey)
    {
        $this->indexKey = $indexKey;

        return $this;
    }

    /**
     * Get indexKey.
     *
     * @return string|null
     */
    public function getIndexKey()
    {
        return $this->indexKey;
    }

    /**
     * Set type.
     *
     * @param string|null $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set data.
     *
     * @param array|null $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data.
     *
     * @return array|null
     */
    public function getData()
    {
        return $this->data;
    }
}
