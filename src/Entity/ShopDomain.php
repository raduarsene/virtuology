<?php

namespace App\Entity;

use App\Entity\Traits\Timestampable;
use App\Helper\DateTimeFormat;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="shop_domain")
 * @ORM\Entity(repositoryClass="App\Repository\ShopDomainRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ShopDomain
{
    /** Timestampable trait */
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="domains")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    /**
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Language")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    /**
     * @var Language[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Language", cascade={"persist"})
     * @ORM\JoinTable(name="shop_domain_language")
     * @Assert\Count(max="10")
     */
    private $availableLanguages;

    /**
     * Google analytics code from the client
     *
     * @var string|null
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $gClientCode;

    /**
     * Google analytics code used from mobilosoft
     *
     * @var string|null
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $gMobiCode;

    /**
     * Google tag manager code
     *
     * @var string|null
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $gtmCode;

    /**
     * Google meta tag used for search console
     *
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $gMetaTag;

    /**
     * Format of datetime to show on page
     *
     * @var string|null
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $datetimeFormat;

    /**
     * Default domain which we should use it's attached language for metas
     *
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $defaultDomain;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasGdpr;

    /**
     * ShopDomain constructor.
     */
    public function __construct()
    {
        $this->availableLanguages = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shop.
     *
     * @param Shop $shop
     * @return $this
     */
    public function setShop(Shop $shop)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop.
     *
     * @return Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set language.
     *
     * @param Language $language
     * @return $this
     */
    public function setLanguage(Language $language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language.
     *
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Add available Language to ShopDomain.
     *
     * @param Language $language
     * @return $this
     */
    public function addAvailableLanguage(Language $language)
    {
        if (!$this->availableLanguages->contains($language)) {
            $this->availableLanguages->add($language);
        }

        return $this;
    }

    /**
     * Set available Language's to ShopDomain.
     *
     * @param Language[] $languages
     * @return $this
     */
    public function setAvailableLanguages(array $languages)
    {
        foreach ($languages as $language) {
            $this->addAvailableLanguage($language);
        }

        return $this;
    }

    /**
     * Get ShopDomain available languages.
     *
     * @return Collection
     */
    public function getAvailableLanguages()
    {
        return $this->availableLanguages;
    }

    /**
     * Get ShopDomain available languages as array of codes.
     *
     * @return array
     */
    public function getAvailableLanguagesArray()
    {
        $availableLanguages = [];

        /** @var Language $availableLanguage */
        foreach ($this->availableLanguages->toArray() as $availableLanguage) {
            $availableLanguages[] = $availableLanguage->getCode();
        }

        return $availableLanguages;
    }

    /**
     * Remove Language from ShopDomain.
     *
     * @param Language $language
     * @return $this
     */
    public function removeAvailableLanguage(Language $language)
    {
        $this->availableLanguages->removeElement($language);

        return $this;
    }

    /**
     * Set gClientCode.
     *
     * @param string|null $gClientCode
     * @return $this
     */
    public function setGClientCode($gClientCode)
    {
        $this->gClientCode = $gClientCode;

        return $this;
    }

    /**
     * Get gClientCode.
     *
     * @return string|null
     */
    public function getGClientCode()
    {
        return $this->gClientCode;
    }

    /**
     * Set gMobiCode.
     *
     * @param string|null $gMobiCode
     * @return $this
     */
    public function setGMobiCode($gMobiCode)
    {
        $this->gMobiCode = $gMobiCode;

        return $this;
    }

    /**
     * Get gMobiCode.
     *
     * @return string|null
     */
    public function getGMobiCode()
    {
        return $this->gMobiCode;
    }

    /**
     * Set gtmCode.
     *
     * @param string|null $gtmCode
     * @return $this
     */
    public function setGTMCode($gtmCode)
    {
        $this->gtmCode = $gtmCode;

        return $this;
    }

    /**
     * Get gtmCode.
     *
     * @return string|null
     */
    public function getGTMCode()
    {
        return $this->gtmCode;
    }

    /**
     * Set gMetaTag.
     *
     * @param string|null $gMetaTag
     * @return $this
     */
    public function setGMetaTag($gMetaTag)
    {
        $this->gMetaTag = $gMetaTag;

        return $this;
    }

    /**
     * Get gMetaTag.
     *
     * @return string|null
     */
    public function getGMetaTag()
    {
        return $this->gMetaTag;
    }

    /**
     * Set datetimeFormat.
     *
     * @param string|null $datetimeFormat
     * @return $this
     */
    public function setDatetimeFormat($datetimeFormat)
    {
        $this->datetimeFormat = $datetimeFormat;

        return $this;
    }

    /**
     * Get datetimeFormat.
     *
     * @return string|null
     */
    public function getDatetimeFormat()
    {
        return $this->datetimeFormat;
    }

    /**
     * Get date format for moment in javascript.
     *
     * @return string
     */
    public function getMomentDateFormat()
    {
        return DateTimeFormat::getMomentDateFormat($this->datetimeFormat);
    }

    /**
     * Get time format for moment in javascript.
     *
     * @return string
     */
    public function getMomentTimeFormat()
    {
        return DateTimeFormat::getMomentTimeFormat($this->datetimeFormat);
    }

    /**
     * Set default domain.
     *
     * @param bool|null $defaultDomain
     * @return $this
     */
    public function setDefaultDomain(?bool $defaultDomain): self
    {
        $this->defaultDomain = $defaultDomain;

        return $this;
    }

    /**
     * Is default domain.
     *
     * @return bool|null
     */
    public function isDefaultDomain(): ?bool
    {
        return $this->defaultDomain;
    }

    public function getHasGdpr(): ?bool
    {
        return $this->hasGdpr;
    }

    public function setHasGdpr(?bool $hasGdpr): self
    {
        $this->hasGdpr = $hasGdpr;

        return $this;
    }
}
