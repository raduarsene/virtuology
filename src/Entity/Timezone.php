<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="timezone")
 * @ORM\Entity(repositoryClass="App\Repository\TimezoneRepository")
 */
class Timezone
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Zone
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Zone")
     * @ORM\JoinColumn(nullable=false)
     */
    private $zone;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $abbreviation;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $timeStart;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $gmtOffset;

    /**
     * Daylight Saving Time
     *
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $dst;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set zone.
     *
     * @param Zone $zone
     * @return $this
     */
    public function setZone(Zone $zone)
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * Get zone.
     *
     * @return Zone
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * Set abbreviation.
     *
     * @param string|null $abbreviation
     * @return $this
     */
    public function setAbbreviation($abbreviation)
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    /**
     * Get abbreviation.
     *
     * @return string|null
     */
    public function getAbbreviation()
    {
        return $this->abbreviation;
    }

    /**
     * Set timeStart.
     *
     * @param int $timeStart
     * @return $this
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;

        return $this;
    }

    /**
     * Get timeStart.
     *
     * @return int
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * Set gmtOffset.
     *
     * @param int $gmtOffset
     * @return $this
     */
    public function setGmtOffset($gmtOffset)
    {
        $this->gmtOffset = $gmtOffset;

        return $this;
    }

    /**
     * Get gmtOffset.
     *
     * @return int
     */
    public function getGmtOffset()
    {
        return $this->gmtOffset;
    }

    /**
     * Set dst.
     *
     * @param bool $dst
     * @return $this
     */
    public function setDst($dst)
    {
        $this->dst = $dst;

        return $this;
    }

    /**
     * Get dst.
     *
     * @return bool
     */
    public function getDst()
    {
        return $this->dst;
    }
}
