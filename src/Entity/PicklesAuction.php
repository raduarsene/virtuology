<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PicklesAuctionRepository")
 * @ORM\HasLifecycleCallbacks
 */
class PicklesAuction
{
    
    /** Timestampable trait */
    use Timestampable;
    
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $organizationName;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $organizationCode;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $lotId;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $nvicCode;

    /**
     * @ORM\Column(type="json")
     */
    private $vehicleDetails = [];
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrganizationName(): ?string
    {
        return $this->organizationName;
    }

    public function setOrganizationName(?string $organizationName): self
    {
        $this->organizationName = $organizationName;

        return $this;
    }

    public function getOrganizationCode(): ?string
    {
        return $this->organizationCode;
    }

    public function setOrganizationCode(?string $organizationCode): self
    {
        $this->organizationCode = $organizationCode;

        return $this;
    }

    public function getLotId(): ?string
    {
        return $this->lotId;
    }

    public function setLotId(?string $lotId): self
    {
        $this->lotId = $lotId;

        return $this;
    }

    public function getNvicCode(): ?string
    {
        return $this->nvicCode;
    }

    public function setNvicCode(?string $nvicCode): self
    {
        $this->nvicCode = $nvicCode;

        return $this;
    }

    public function getVehicleDetails(): ?array
    {
        return $this->vehicleDetails;
    }

    public function setVehicleDetails(array $vehicleDetails): self
    {
        $this->vehicleDetails = $vehicleDetails;

        return $this;
    }
    
}
