<?php

namespace App\Entity;

use App\Entity\Traits\Timestampable;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="language")
 * @UniqueEntity("code")
 * @ORM\Entity(repositoryClass="App\Repository\LanguageRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Language
{
    const ENGLISH = 'en';
    const FRENCH = 'fr';
    const DUTCH = 'nl';
    const GERMAN = 'de';

    /** Timestampable trait */
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=8, unique=true)
     * @Assert\NotBlank()
     * @Assert\Locale(canonicalize = true)
     */
    private $code;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code.
     *
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get language code + '_' + country code if selected, ex: en, en_US.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get language + country code for html (with '-'), ex: en-US.
     *
     * @return string
     */
    public function getLangHtml()
    {
        return str_replace('_', '-', $this->code);
    }

    /**
     * Get language code (2 chars), ex: en, fr.
     *
     * @return string
     */
    public function getLangCode()
    {
        return current(explode('_', $this->code));
    }
}
