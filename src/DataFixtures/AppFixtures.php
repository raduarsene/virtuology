<?php

namespace App\DataFixtures;

use App\Entity\Attribute;
use App\Entity\AttributeValue;
use App\Entity\Language;
use App\Entity\Shop;
use App\Entity\ShopDomain;
use App\Entity\ShopTemplate;
use App\Entity\User;
use App\Entity\Zone;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Class AppFixtures
 *
 * @package App\DataFixtures
 */
class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->loadLanguages($manager);
        $this->loadZones($manager);
        $this->loadUsers($manager);
        $this->loadAttributes($manager);
        $this->loadShops($manager);
    }

    /**
     * @param ObjectManager $manager
     */
    private function loadLanguages(ObjectManager $manager)
    {
        foreach ($this->getLanguageData() as list($name, $code)) {
            $language = new Language();
            $language->setName($name)
                ->setCode($code);

            $manager->persist($language);
            $this->addReference('lang_'.$code, $language);
        }

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     */
    private function loadZones(ObjectManager $manager)
    {
        foreach ($this->getZoneData() as list($countryCode, $zoneName, $main)) {
            $zone = new Zone();
            $zone->setCountryCode($countryCode)
                ->setZoneName($zoneName)
                ->setMain($main);

            $manager->persist($zone);
        }

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     */
    private function loadUsers(ObjectManager $manager)
    {
        $user = new User();
        $user->setName('Admin');
        $user->setPlainPassword('admin');
        $user->setEmail('admin@vir.loc');
        $user->setRoles([User::ROLE_SUPER_ADMIN]);

        $manager->persist($user);
        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     */
    private function loadAttributes(ObjectManager $manager)
    {
        foreach ($this->getAttributeData() as [$name, $type, $block, $values]) {
            $attribute = new Attribute();
            $attribute->setName($name)
                ->setType($type)
                ->setBlock($block);

            foreach ($values as [$valueName, $value, $default, $imageName]) {
                $attributeValue = new AttributeValue();
                $attributeValue->setName($valueName)
                    ->setValue($value)
                    ->setIsDefault($default)
                    ->setImageName($imageName);

                $attribute->addAttributeValue($attributeValue);

                $this->addReference($block.'_'.$value, $attributeValue);
            }

            $manager->persist($attribute);
        }

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     */
    private function loadShops(ObjectManager $manager)
    {
        foreach ($this->getShopData() as list($name, $code, $apiSiteId, $apiKey, $mapType, $mapCredentials, $domains, $templates)) {
            $shop = new Shop();
            $shop->setName($name)
                ->setCode($code)
                ->setApiSiteId($apiSiteId)
                ->setApiKey($apiKey)
                ->setMapType($mapType)
                ->setMapCredentials($mapCredentials);

            // Add shop domains
            foreach ($domains as list($domainName, $language, $availableLanguages)) {
                $shopDomain = new ShopDomain();
                $shopDomain->setName($domainName)
                    ->setLanguage($language)
                    ->setAvailableLanguages($availableLanguages);

                $shop->addDomain($shopDomain);
            }

            // Add shop templates
            foreach ($templates as list($reference, $position)) {
                $shopTemplate = new ShopTemplate();
                $shopTemplate->setAttributeValue($this->getReference($reference))
                    ->setPosition($position);

                $shop->addTemplate($shopTemplate);
            }

            $manager->persist($shop);
        }

        $manager->flush();
    }

    private function getLanguageData()
    {
        return [
            ['English', 'en'],
            ['French', 'fr'],
        ];
    }

    private function getAttributeData()
    {
        return [
            ['Map/Search', Attribute::TYPE_PAGE_HOME, Attribute::BLOCK_MAP, [
                ['Map with Search', AttributeValue::VALUE_MAP_SEARCH, false, 'f208b06534b8682344b76c58b1308b49.png'],
                ['Map with Search and List', AttributeValue::VALUE_MAP_SEARCH_LIST, true, 'c9591fa9ee096f0cfca37b504cc59f67.png'],
                ['Search without Map', AttributeValue::VALUE_SEARCH_NO_MAP, false, '00577ab81c5f4d0d00324b3fd5e6987e.png'],
            ]],
            ['Location/City list', Attribute::TYPE_PAGE_HOME, Attribute::BLOCK_LOCATIONS, [
                ['Simple', AttributeValue::VALUE_SIMPLE, false, 'e893d3cff18bd890cb9afe3c1e4e3cc3.png'],
                ['With opening hours', AttributeValue::VALUE_WITH_OPENING_HOURS, true, '66922219e246379c7ed37085bddfaa38.png'],
                ['With search', AttributeValue::VALUE_WITH_SEARCH, false, 'e52467260c982b7d86c5eab0df01ab6a.png'],
                ['Search and opening hours', AttributeValue::VALUE_SEARCH_AND_OPENING_HOURS, false, '165faa6f89542a489492ed3b4c00e741.png'],
            ]],
            ['Location details', Attribute::TYPE_PAGE_SHOW, Attribute::BLOCK_LOCATION_DETAILS, [
                ['Simple', AttributeValue::VALUE_SIMPLE, true, '07c4ffd793ec56d6e2e9ad5971457519.png'],
                ['With map', AttributeValue::VALUE_WITH_MAP, false, 'ed62fcaad00c166a0f502772e87c85f8.png'],
                ['With image', AttributeValue::VALUE_WITH_IMAGE, false, '9f525d43a3dea73c2c2dcd7b29b13b41.png'],
                ['With image and map', AttributeValue::VALUE_WITH_IMAGE_AND_MAP, false, '55233cf9065bb97bee64bafbcfd352cc.png'],
            ]],
            ['Location banner', Attribute::TYPE_PAGE_SHOW, Attribute::BLOCK_LOCATION_BANNER, [
                ['With map', AttributeValue::VALUE_WITH_MAP, true, '963121ff7b8f4a56563a14f967bbf434.png'],
                ['With image', AttributeValue::VALUE_WITH_IMAGE, false, '278d46e1def8a5a2b86c0fecea9a97b8.png'],
            ]],
            ['Location description', Attribute::TYPE_PAGE_SHOW, Attribute::BLOCK_LOCATION_DESCRIPTION, [
                ['Image left', AttributeValue::VALUE_IMAGE_LEFT, true, 'e602a098fcaedfa49c2b77706814bfe0.png'],
                ['Image right', AttributeValue::VALUE_IMAGE_RIGHT, false, '2b9e355ea03a12a54a07c45102644898.png'],
            ]],
            ['Location promotions', Attribute::TYPE_PAGE_SHOW, Attribute::BLOCK_LOCATION_PROMOTIONS, [
                ['With slideshow', AttributeValue::VALUE_WITH_SLIDESHOW, false, '28504a204c8c788174305645ec3e5923.png'],
            ]],
            ['Location events', Attribute::TYPE_PAGE_SHOW, Attribute::BLOCK_LOCATION_EVENTS, [
                ['With slideshow', AttributeValue::VALUE_WITH_SLIDESHOW, false, '0bda1e85bca92bc74e09000b7f8a4e03.png'],
            ]],
            ['Location services', Attribute::TYPE_PAGE_SHOW, Attribute::BLOCK_LOCATION_SERVICES, [
                ['With text', AttributeValue::VALUE_WITH_TEXT, true, '23dc9b5c29f486d40f2ef674ae8a70c2.png'],
                ['With image', AttributeValue::VALUE_WITH_IMAGE, false, '63332314d9f74212a2c7abf84a438373.png'],
                ['With image and info', AttributeValue::VALUE_WITH_IMAGE_INFO, false, '051a487de9872bde409d511d269f01b4.png'],
            ]],
            ['Near location', Attribute::TYPE_PAGE_SHOW, Attribute::BLOCK_NEAR_LOCATION, [
                ['Simple', AttributeValue::VALUE_SIMPLE, true, 'd1697ccc29361f6b17dc316f98f84409.png'],
            ]],
            ['Open hours', Attribute::TYPE_GLOBAL, Attribute::BLOCK_OPEN_HOURS, [
                ['Open hours in week type', AttributeValue::VALUE_OPEN_HOURS_WEEK, false, null],
                ['Open hours with exceptions', AttributeValue::VALUE_OPEN_HOURS_EXCEPTION, true, null],
            ]],
            ['Location route', Attribute::TYPE_GLOBAL, Attribute::BLOCK_LOCATION_ROUTE, [
                ['Location route map', AttributeValue::VALUE_LOCATION_ROUTE_MAP, false, null],
                ['Location route redirect', AttributeValue::VALUE_LOCATION_ROUTE_REDIRECT, false, null],
            ]],
        ];
    }

    private function getShopData()
    {
        return [
            [
                'Demo Mobilosoft',
                'mobilosoft-test',
                '3fde860c-8137-4ae9-98f4-511de0fab8d3',
                'mobilosoft-test',
                Shop::MAP_TYPE_GOOGLE,
                ['key' => 'AIzaSyDyUXWy_egoNttDXn0f-ZXPik2PnZvtnV8'],
                [
                    ['demo-mobilosoft.vir.loc', $this->getReference('lang_en'), [$this->getReference('lang_en'), $this->getReference('lang_fr')]],
                ],
                [
                    [Attribute::BLOCK_MAP.'_'.AttributeValue::VALUE_MAP_SEARCH_LIST, 1],
                    [Attribute::BLOCK_LOCATIONS.'_'.AttributeValue::VALUE_WITH_SEARCH, 2],
                    [Attribute::BLOCK_LOCATION_DETAILS.'_'.AttributeValue::VALUE_WITH_MAP, 1],
                    [Attribute::BLOCK_LOCATION_BANNER.'_'.AttributeValue::VALUE_WITH_MAP, 2],
                    [Attribute::BLOCK_LOCATION_DESCRIPTION.'_'.AttributeValue::VALUE_IMAGE_RIGHT, 3],
                    [Attribute::BLOCK_LOCATION_PROMOTIONS.'_'.AttributeValue::VALUE_WITH_SLIDESHOW, 4],
                    [Attribute::BLOCK_LOCATION_EVENTS.'_'.AttributeValue::VALUE_WITH_SLIDESHOW, 5],
                    [Attribute::BLOCK_LOCATION_SERVICES.'_'.AttributeValue::VALUE_WITH_IMAGE_INFO, 6],
                    [Attribute::BLOCK_NEAR_LOCATION.'_'.AttributeValue::VALUE_SIMPLE, 7],
                    [Attribute::BLOCK_OPEN_HOURS.'_'.AttributeValue::VALUE_OPEN_HOURS_WEEK, null],
                    [Attribute::BLOCK_LOCATION_ROUTE.'_'.AttributeValue::VALUE_LOCATION_ROUTE_REDIRECT, null],
                ],
            ],
        ];
    }

    /**
     * @link https://timezonedb.com/time-zones
     * @return array
     */
    private function getZoneData()
    {
        return [
            ['AD', 'Europe/Andorra', 1],
            ['AE', 'Asia/Dubai', 1],
            ['AF', 'Asia/Kabul', 1],
            ['AG', 'America/Antigua', 1],
            ['AI', 'America/Anguilla', 1],
            ['AL', 'Europe/Tirane', 1],
            ['AM', 'Asia/Yerevan', 1],
            ['AO', 'Africa/Luanda', 1],
            ['AQ', 'Antarctica/McMurdo', 0],
            ['AQ', 'Antarctica/Casey', 0],
            ['AQ', 'Antarctica/Davis', 0],
            ['AQ', 'Antarctica/DumontDUrville', 0],
            ['AQ', 'Antarctica/Mawson', 0],
            ['AQ', 'Antarctica/Palmer', 0],
            ['AQ', 'Antarctica/Rothera', 0],
            ['AQ', 'Antarctica/Syowa', 1],
            ['AQ', 'Antarctica/Troll', 0],
            ['AQ', 'Antarctica/Vostok', 0],
            ['AR', 'America/Argentina/Buenos_Aires', 1],
            ['AR', 'America/Argentina/Cordoba', 0],
            ['AR', 'America/Argentina/Salta', 0],
            ['AR', 'America/Argentina/Jujuy', 0],
            ['AR', 'America/Argentina/Tucuman', 0],
            ['AR', 'America/Argentina/Catamarca', 0],
            ['AR', 'America/Argentina/La_Rioja', 0],
            ['AR', 'America/Argentina/San_Juan', 0],
            ['AR', 'America/Argentina/Mendoza', 0],
            ['AR', 'America/Argentina/San_Luis', 0],
            ['AR', 'America/Argentina/Rio_Gallegos', 0],
            ['AR', 'America/Argentina/Ushuaia', 0],
            ['AS', 'Pacific/Pago_Pago', 1],
            ['AT', 'Europe/Vienna', 1],
            ['AU', 'Australia/Lord_Howe', 0],
            ['AU', 'Antarctica/Macquarie', 0],
            ['AU', 'Australia/Hobart', 0],
            ['AU', 'Australia/Currie', 0],
            ['AU', 'Australia/Melbourne', 0],
            ['AU', 'Australia/Sydney', 1],
            ['AU', 'Australia/Broken_Hill', 0],
            ['AU', 'Australia/Brisbane', 0],
            ['AU', 'Australia/Lindeman', 0],
            ['AU', 'Australia/Adelaide', 0],
            ['AU', 'Australia/Darwin', 0],
            ['AU', 'Australia/Perth', 0],
            ['AU', 'Australia/Eucla', 0],
            ['AW', 'America/Aruba', 1],
            ['AX', 'Europe/Mariehamn', 1],
            ['AZ', 'Asia/Baku', 1],
            ['BA', 'Europe/Sarajevo', 1],
            ['BB', 'America/Barbados', 1],
            ['BD', 'Asia/Dhaka', 1],
            ['BE', 'Europe/Brussels', 1],
            ['BF', 'Africa/Ouagadougou', 1],
            ['BG', 'Europe/Sofia', 1],
            ['BH', 'Asia/Bahrain', 1],
            ['BI', 'Africa/Bujumbura', 1],
            ['BJ', 'Africa/Porto-Novo', 1],
            ['BL', 'America/St_Barthelemy', 1],
            ['BM', 'Atlantic/Bermuda', 1],
            ['BN', 'Asia/Brunei', 1],
            ['BO', 'America/La_Paz', 1],
            ['BQ', 'America/Kralendijk', 1],
            ['BR', 'America/Noronha', 0],
            ['BR', 'America/Belem', 0],
            ['BR', 'America/Fortaleza', 0],
            ['BR', 'America/Recife', 0],
            ['BR', 'America/Araguaina', 0],
            ['BR', 'America/Maceio', 0],
            ['BR', 'America/Bahia', 0],
            ['BR', 'America/Sao_Paulo', 1],
            ['BR', 'America/Campo_Grande', 0],
            ['BR', 'America/Cuiaba', 0],
            ['BR', 'America/Santarem', 0],
            ['BR', 'America/Porto_Velho', 0],
            ['BR', 'America/Boa_Vista', 0],
            ['BR', 'America/Manaus', 0],
            ['BR', 'America/Eirunepe', 0],
            ['BR', 'America/Rio_Branco', 0],
            ['BS', 'America/Nassau', 1],
            ['BT', 'Asia/Thimphu', 1],
            ['BW', 'Africa/Gaborone', 1],
            ['BY', 'Europe/Minsk', 1],
            ['BZ', 'America/Belize', 1],
            ['CA', 'America/St_Johns', 0],
            ['CA', 'America/Halifax', 0],
            ['CA', 'America/Glace_Bay', 0],
            ['CA', 'America/Moncton', 0],
            ['CA', 'America/Goose_Bay', 0],
            ['CA', 'America/Blanc-Sablon', 0],
            ['CA', 'America/Toronto', 1],
            ['CA', 'America/Nipigon', 0],
            ['CA', 'America/Thunder_Bay', 0],
            ['CA', 'America/Iqaluit', 0],
            ['CA', 'America/Pangnirtung', 0],
            ['CA', 'America/Atikokan', 0],
            ['CA', 'America/Winnipeg', 0],
            ['CA', 'America/Rainy_River', 0],
            ['CA', 'America/Resolute', 0],
            ['CA', 'America/Rankin_Inlet', 0],
            ['CA', 'America/Regina', 0],
            ['CA', 'America/Swift_Current', 0],
            ['CA', 'America/Edmonton', 0],
            ['CA', 'America/Cambridge_Bay', 0],
            ['CA', 'America/Yellowknife', 0],
            ['CA', 'America/Inuvik', 0],
            ['CA', 'America/Creston', 0],
            ['CA', 'America/Dawson_Creek', 0],
            ['CA', 'America/Fort_Nelson', 0],
            ['CA', 'America/Vancouver', 0],
            ['CA', 'America/Whitehorse', 0],
            ['CA', 'America/Dawson', 0],
            ['CC', 'Indian/Cocos', 1],
            ['CD', 'Africa/Kinshasa', 0],
            ['CD', 'Africa/Lubumbashi', 1],
            ['CF', 'Africa/Bangui', 1],
            ['CG', 'Africa/Brazzaville', 1],
            ['CH', 'Europe/Zurich', 1],
            ['CI', 'Africa/Abidjan', 1],
            ['CK', 'Pacific/Rarotonga', 1],
            ['CL', 'America/Santiago', 1],
            ['CL', 'America/Punta_Arenas', 0],
            ['CL', 'Pacific/Easter', 0],
            ['CM', 'Africa/Douala', 1],
            ['CN', 'Asia/Shanghai', 1],
            ['CN', 'Asia/Urumqi', 0],
            ['CO', 'America/Bogota', 1],
            ['CR', 'America/Costa_Rica', 1],
            ['CU', 'America/Havana', 1],
            ['CV', 'Atlantic/Cape_Verde', 1],
            ['CW', 'America/Curacao', 1],
            ['CX', 'Indian/Christmas', 1],
            ['CY', 'Asia/Nicosia', 1],
            ['CY', 'Asia/Famagusta', 0],
            ['CZ', 'Europe/Prague', 1],
            ['DE', 'Europe/Berlin', 1],
            ['DE', 'Europe/Busingen', 0],
            ['DJ', 'Africa/Djibouti', 1],
            ['DK', 'Europe/Copenhagen', 1],
            ['DM', 'America/Dominica', 1],
            ['DO', 'America/Santo_Domingo', 1],
            ['DZ', 'Africa/Algiers', 1],
            ['EC', 'America/Guayaquil', 1],
            ['EC', 'Pacific/Galapagos', 0],
            ['EE', 'Europe/Tallinn', 1],
            ['EG', 'Africa/Cairo', 1],
            ['EH', 'Africa/El_Aaiun', 1],
            ['ER', 'Africa/Asmara', 1],
            ['ES', 'Europe/Madrid', 1],
            ['ES', 'Africa/Ceuta', 0],
            ['ES', 'Atlantic/Canary', 0],
            ['ET', 'Africa/Addis_Ababa', 1],
            ['FI', 'Europe/Helsinki', 1],
            ['FJ', 'Pacific/Fiji', 1],
            ['FK', 'Atlantic/Stanley', 1],
            ['FM', 'Pacific/Chuuk', 0],
            ['FM', 'Pacific/Pohnpei', 1],
            ['FM', 'Pacific/Kosrae', 0],
            ['FO', 'Atlantic/Faroe', 1],
            ['FR', 'Europe/Paris', 1],
            ['GA', 'Africa/Libreville', 1],
            ['GB', 'Europe/London', 1],
            ['GD', 'America/Grenada', 1],
            ['GE', 'Asia/Tbilisi', 1],
            ['GF', 'America/Cayenne', 1],
            ['GG', 'Europe/Guernsey', 1],
            ['GH', 'Africa/Accra', 1],
            ['GI', 'Europe/Gibraltar', 1],
            ['GL', 'America/Godthab', 1],
            ['GL', 'America/Danmarkshavn', 0],
            ['GL', 'America/Scoresbysund', 0],
            ['GL', 'America/Thule', 0],
            ['GM', 'Africa/Banjul', 1],
            ['GN', 'Africa/Conakry', 1],
            ['GP', 'America/Guadeloupe', 1],
            ['GQ', 'Africa/Malabo', 1],
            ['GR', 'Europe/Athens', 1],
            ['GS', 'Atlantic/South_Georgia', 1],
            ['GT', 'America/Guatemala', 1],
            ['GU', 'Pacific/Guam', 1],
            ['GW', 'Africa/Bissau', 1],
            ['GY', 'America/Guyana', 1],
            ['HK', 'Asia/Hong_Kong', 1],
            ['HN', 'America/Tegucigalpa', 1],
            ['HR', 'Europe/Zagreb', 1],
            ['HT', 'America/Port-au-Prince', 1],
            ['HU', 'Europe/Budapest', 1],
            ['ID', 'Asia/Jakarta', 1],
            ['ID', 'Asia/Pontianak', 0],
            ['ID', 'Asia/Makassar', 0],
            ['ID', 'Asia/Jayapura', 0],
            ['IE', 'Europe/Dublin', 1],
            ['IL', 'Asia/Jerusalem', 1],
            ['IM', 'Europe/Isle_of_Man', 1],
            ['IN', 'Asia/Kolkata', 1],
            ['IO', 'Indian/Chagos', 1],
            ['IQ', 'Asia/Baghdad', 1],
            ['IR', 'Asia/Tehran', 1],
            ['IS', 'Atlantic/Reykjavik', 1],
            ['IT', 'Europe/Rome', 1],
            ['JE', 'Europe/Jersey', 1],
            ['JM', 'America/Jamaica', 1],
            ['JO', 'Asia/Amman', 1],
            ['JP', 'Asia/Tokyo', 1],
            ['KE', 'Africa/Nairobi', 1],
            ['KG', 'Asia/Bishkek', 1],
            ['KH', 'Asia/Phnom_Penh', 1],
            ['KI', 'Pacific/Tarawa', 1],
            ['KI', 'Pacific/Enderbury', 0],
            ['KI', 'Pacific/Kiritimati', 0],
            ['KM', 'Indian/Comoro', 1],
            ['KN', 'America/St_Kitts', 1],
            ['KP', 'Asia/Pyongyang', 1],
            ['KR', 'Asia/Seoul', 1],
            ['KW', 'Asia/Kuwait', 1],
            ['KY', 'America/Cayman', 1],
            ['KZ', 'Asia/Almaty', 1],
            ['KZ', 'Asia/Qyzylorda', 0],
            ['KZ', 'Asia/Qostanay', 0],
            ['KZ', 'Asia/Aqtobe', 0],
            ['KZ', 'Asia/Aqtau', 0],
            ['KZ', 'Asia/Atyrau', 0],
            ['KZ', 'Asia/Oral', 0],
            ['LA', 'Asia/Vientiane', 1],
            ['LB', 'Asia/Beirut', 1],
            ['LC', 'America/St_Lucia', 1],
            ['LI', 'Europe/Vaduz', 1],
            ['LK', 'Asia/Colombo', 1],
            ['LR', 'Africa/Monrovia', 1],
            ['LS', 'Africa/Maseru', 1],
            ['LT', 'Europe/Vilnius', 1],
            ['LU', 'Europe/Luxembourg', 1],
            ['LV', 'Europe/Riga', 1],
            ['LY', 'Africa/Tripoli', 1],
            ['MA', 'Africa/Casablanca', 1],
            ['MC', 'Europe/Monaco', 1],
            ['MD', 'Europe/Chisinau', 1],
            ['ME', 'Europe/Podgorica', 1],
            ['MF', 'America/Marigot', 1],
            ['MG', 'Indian/Antananarivo', 1],
            ['MH', 'Pacific/Majuro', 1],
            ['MH', 'Pacific/Kwajalein', 0],
            ['MK', 'Europe/Skopje', 1],
            ['ML', 'Africa/Bamako', 1],
            ['MM', 'Asia/Yangon', 1],
            ['MN', 'Asia/Ulaanbaatar', 1],
            ['MN', 'Asia/Hovd', 0],
            ['MN', 'Asia/Choibalsan', 0],
            ['MO', 'Asia/Macau', 1],
            ['MP', 'Pacific/Saipan', 1],
            ['MQ', 'America/Martinique', 1],
            ['MR', 'Africa/Nouakchott', 1],
            ['MS', 'America/Montserrat', 1],
            ['MT', 'Europe/Malta', 1],
            ['MU', 'Indian/Mauritius', 1],
            ['MV', 'Indian/Maldives', 1],
            ['MW', 'Africa/Blantyre', 1],
            ['MX', 'America/Mexico_City', 1],
            ['MX', 'America/Cancun', 0],
            ['MX', 'America/Merida', 0],
            ['MX', 'America/Monterrey', 0],
            ['MX', 'America/Matamoros', 0],
            ['MX', 'America/Mazatlan', 0],
            ['MX', 'America/Chihuahua', 0],
            ['MX', 'America/Ojinaga', 0],
            ['MX', 'America/Hermosillo', 0],
            ['MX', 'America/Tijuana', 0],
            ['MX', 'America/Bahia_Banderas', 0],
            ['MY', 'Asia/Kuala_Lumpur', 1],
            ['MY', 'Asia/Kuching', 0],
            ['MZ', 'Africa/Maputo', 1],
            ['NA', 'Africa/Windhoek', 1],
            ['NC', 'Pacific/Noumea', 1],
            ['NE', 'Africa/Niamey', 1],
            ['NF', 'Pacific/Norfolk', 1],
            ['NG', 'Africa/Lagos', 1],
            ['NI', 'America/Managua', 1],
            ['NL', 'Europe/Amsterdam', 1],
            ['NO', 'Europe/Oslo', 1],
            ['NP', 'Asia/Kathmandu', 1],
            ['NR', 'Pacific/Nauru', 1],
            ['NU', 'Pacific/Niue', 1],
            ['NZ', 'Pacific/Auckland', 1],
            ['NZ', 'Pacific/Chatham', 0],
            ['OM', 'Asia/Muscat', 1],
            ['PA', 'America/Panama', 1],
            ['PE', 'America/Lima', 1],
            ['PF', 'Pacific/Tahiti', 1],
            ['PF', 'Pacific/Marquesas', 0],
            ['PF', 'Pacific/Gambier', 0],
            ['PG', 'Pacific/Port_Moresby', 1],
            ['PG', 'Pacific/Bougainville', 0],
            ['PH', 'Asia/Manila', 1],
            ['PK', 'Asia/Karachi', 1],
            ['PL', 'Europe/Warsaw', 1],
            ['PM', 'America/Miquelon', 1],
            ['PN', 'Pacific/Pitcairn', 1],
            ['PR', 'America/Puerto_Rico', 1],
            ['PS', 'Asia/Gaza', 1],
            ['PS', 'Asia/Hebron', 0],
            ['PT', 'Europe/Lisbon', 1],
            ['PT', 'Atlantic/Madeira', 0],
            ['PT', 'Atlantic/Azores', 0],
            ['PW', 'Pacific/Palau', 1],
            ['PY', 'America/Asuncion', 1],
            ['QA', 'Asia/Qatar', 1],
            ['RE', 'Indian/Reunion', 1],
            ['RO', 'Europe/Bucharest', 1],
            ['RS', 'Europe/Belgrade', 1],
            ['RU', 'Europe/Kaliningrad', 0],
            ['RU', 'Europe/Moscow', 1],
            ['UA', 'Europe/Simferopol', 0],
            ['RU', 'Europe/Kirov', 0],
            ['RU', 'Europe/Astrakhan', 0],
            ['RU', 'Europe/Volgograd', 0],
            ['RU', 'Europe/Saratov', 0],
            ['RU', 'Europe/Ulyanovsk', 0],
            ['RU', 'Europe/Samara', 0],
            ['RU', 'Asia/Yekaterinburg', 0],
            ['RU', 'Asia/Omsk', 0],
            ['RU', 'Asia/Novosibirsk', 0],
            ['RU', 'Asia/Barnaul', 0],
            ['RU', 'Asia/Tomsk', 0],
            ['RU', 'Asia/Novokuznetsk', 0],
            ['RU', 'Asia/Krasnoyarsk', 0],
            ['RU', 'Asia/Irkutsk', 0],
            ['RU', 'Asia/Chita', 0],
            ['RU', 'Asia/Yakutsk', 0],
            ['RU', 'Asia/Khandyga', 0],
            ['RU', 'Asia/Vladivostok', 0],
            ['RU', 'Asia/Ust-Nera', 0],
            ['RU', 'Asia/Magadan', 0],
            ['RU', 'Asia/Sakhalin', 0],
            ['RU', 'Asia/Srednekolymsk', 0],
            ['RU', 'Asia/Kamchatka', 0],
            ['RU', 'Asia/Anadyr', 0],
            ['RW', 'Africa/Kigali', 1],
            ['SA', 'Asia/Riyadh', 1],
            ['SB', 'Pacific/Guadalcanal', 1],
            ['SC', 'Indian/Mahe', 1],
            ['SD', 'Africa/Khartoum', 1],
            ['SE', 'Europe/Stockholm', 1],
            ['SG', 'Asia/Singapore', 1],
            ['SH', 'Atlantic/St_Helena', 1],
            ['SI', 'Europe/Ljubljana', 1],
            ['SJ', 'Arctic/Longyearbyen', 1],
            ['SK', 'Europe/Bratislava', 1],
            ['SL', 'Africa/Freetown', 1],
            ['SM', 'Europe/San_Marino', 1],
            ['SN', 'Africa/Dakar', 1],
            ['SO', 'Africa/Mogadishu', 1],
            ['SR', 'America/Paramaribo', 1],
            ['SS', 'Africa/Juba', 1],
            ['ST', 'Africa/Sao_Tome', 1],
            ['SV', 'America/El_Salvador', 1],
            ['SX', 'America/Lower_Princes', 1],
            ['SY', 'Asia/Damascus', 1],
            ['SZ', 'Africa/Mbabane', 1],
            ['TC', 'America/Grand_Turk', 1],
            ['TD', 'Africa/Ndjamena', 1],
            ['TF', 'Indian/Kerguelen', 1],
            ['TG', 'Africa/Lome', 1],
            ['TH', 'Asia/Bangkok', 1],
            ['TJ', 'Asia/Dushanbe', 1],
            ['TK', 'Pacific/Fakaofo', 1],
            ['TL', 'Asia/Dili', 1],
            ['TM', 'Asia/Ashgabat', 1],
            ['TN', 'Africa/Tunis', 1],
            ['TO', 'Pacific/Tongatapu', 1],
            ['TR', 'Europe/Istanbul', 1],
            ['TT', 'America/Port_of_Spain', 1],
            ['TV', 'Pacific/Funafuti', 1],
            ['TW', 'Asia/Taipei', 1],
            ['TZ', 'Africa/Dar_es_Salaam', 1],
            ['UA', 'Europe/Kiev', 1],
            ['UA', 'Europe/Uzhgorod', 0],
            ['UA', 'Europe/Zaporozhye', 0],
            ['UG', 'Africa/Kampala', 1],
            ['UM', 'Pacific/Midway', 1],
            ['UM', 'Pacific/Wake', 0],
            ['US', 'America/New_York', 1],
            ['US', 'America/Detroit', 0],
            ['US', 'America/Kentucky/Louisville', 0],
            ['US', 'America/Kentucky/Monticello', 0],
            ['US', 'America/Indiana/Indianapolis', 0],
            ['US', 'America/Indiana/Vincennes', 0],
            ['US', 'America/Indiana/Winamac', 0],
            ['US', 'America/Indiana/Marengo', 0],
            ['US', 'America/Indiana/Petersburg', 0],
            ['US', 'America/Indiana/Vevay', 0],
            ['US', 'America/Chicago', 0],
            ['US', 'America/Indiana/Tell_City', 0],
            ['US', 'America/Indiana/Knox', 0],
            ['US', 'America/Menominee', 0],
            ['US', 'America/North_Dakota/Center', 0],
            ['US', 'America/North_Dakota/New_Salem', 0],
            ['US', 'America/North_Dakota/Beulah', 0],
            ['US', 'America/Denver', 0],
            ['US', 'America/Boise', 0],
            ['US', 'America/Phoenix', 0],
            ['US', 'America/Los_Angeles', 0],
            ['US', 'America/Anchorage', 0],
            ['US', 'America/Juneau', 0],
            ['US', 'America/Sitka', 0],
            ['US', 'America/Metlakatla', 0],
            ['US', 'America/Yakutat', 0],
            ['US', 'America/Nome', 0],
            ['US', 'America/Adak', 0],
            ['US', 'Pacific/Honolulu', 0],
            ['UY', 'America/Montevideo', 1],
            ['UZ', 'Asia/Samarkand', 1],
            ['UZ', 'Asia/Tashkent', 0],
            ['VA', 'Europe/Vatican', 1],
            ['VC', 'America/St_Vincent', 1],
            ['VE', 'America/Caracas', 1],
            ['VG', 'America/Tortola', 1],
            ['VI', 'America/St_Thomas', 1],
            ['VN', 'Asia/Ho_Chi_Minh', 1],
            ['VU', 'Pacific/Efate', 1],
            ['WF', 'Pacific/Wallis', 1],
            ['WS', 'Pacific/Apia', 1],
            ['YE', 'Asia/Aden', 1],
            ['YT', 'Indian/Mayotte', 1],
            ['ZA', 'Africa/Johannesburg', 1],
            ['ZM', 'Africa/Lusaka', 1],
            ['ZW', 'Africa/Harare', 1],
        ];
    }
}
