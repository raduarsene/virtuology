<?php

namespace App\Controller\Admin;

use App\Entity\AttributeValue;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ShopController
 *
 * @Route("/admin")
 */
class ShopController extends AbstractController
{
    /**
     * @Route("/attribute/{id}/values", name="app_admin_values_by_attribute", methods={"POST"}, requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param int     $id
     * @return JsonResponse
     */
    public function getValuesByAttribute(Request $request, $id)
    {
        $data = [];

        $attributeValues = $this->getDoctrine()->getRepository(AttributeValue::class)->findBy([
            'attribute' => $id
        ]);

        foreach ($attributeValues as $attributeValue) {
            $data[] = [
                'id'    => $attributeValue->getId(),
                'name'  => $attributeValue->getName(),
                'image' => $attributeValue->getImageName(),
                'url'   => $attributeValue->getImageName()
                    ? $request->getUriForPath('/'.$this->getParameter('app.attribute_value.image.path').$attributeValue->getImageName())
                    : '',
            ];
        }

        return new JsonResponse($data);
    }
}
