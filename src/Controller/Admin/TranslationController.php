<?php

namespace App\Controller\Admin;

use App\Admin\TranslationAdmin;
use Lexik\Bundle\TranslationBundle\Entity\Translation;
use Lexik\Bundle\TranslationBundle\Entity\TransUnit;
use Lexik\Bundle\TranslationBundle\Manager\TranslationInterface;
use Lexik\Bundle\TranslationBundle\Manager\TransUnitManager;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Yaml\Dumper;
use App\Service\Translation\SyncManagerService;

class TranslationController extends CRUDController
{
    const TRANSLATION_FILE_PATH = '../translations';
    const TRANSLATION_FILE_TYPE = 'yml';
    
    /** @var SyncManagerService */
    private $syncManagerService;
    
    /**
     * TranslationController constructor.
     *
     * @param SyncManagerService $syncManagerService
     */
    public function __construct(SyncManagerService $syncManagerService)
    {
        $this->syncManagerService = $syncManagerService;
    }
    
    /**
     * Edit translation action
     *
     * @param int|string|null $id
     * @param Request|null    $request
     * @return Response|RedirectResponse
     */
    public function editAction($id = null, Request $request = null)
    {
        /** @var TranslationAdmin $translationAdmin */
        $translationAdmin = $this->admin;

        if (!$request->isMethod('POST')) {
            return $this->redirect($translationAdmin->generateUrl('list'));
        }

        /** @var TransUnit $transUnit */
        $transUnit = $translationAdmin->getEntityManager()->getRepository(TransUnit::class)->find($id);
        if (!$transUnit) {
            throw new NotFoundHttpException(sprintf('Unable to find translation unit with id : %s', $id));
        }

        $locale = $request->request->get('locale');
        $content = $request->request->get('value');

        if (!$locale) {
            return new JsonResponse(['message' => 'locale missing'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $translationAdmin->setSubject($transUnit);

        /** @var TransUnitManager $transUnitManager */
        $transUnitManager = $translationAdmin->getTransUnitManager();

        /* @var $translation Translation */
        if ($request->request->get('pk')) {
            $translation = $transUnitManager->updateTranslation($transUnit, $locale, $content, true);
        } else {
            $file = $transUnitManager->getTranslationFile($transUnit, $locale);
            if (!$file) {
                $file = $translationAdmin->getFileManager()->getFor(
                    sprintf('%s.%s.%s', $transUnit->getDomain(), $locale, self::TRANSLATION_FILE_TYPE),
                    $this->getParameter('kernel.root_dir').DIRECTORY_SEPARATOR.self::TRANSLATION_FILE_PATH
                );
            }

            $translation = $transUnitManager->addTranslation($transUnit, $locale, $content, $file, true);
        }

        return new JsonResponse([
            'key'    => $transUnit->getKey(),
            'domain' => $transUnit->getDomain(),
            'pk'     => $translation->getId(),
            'locale' => $translation->getLocale(),
            'value'  => $translation->getContent(),
        ]);
    }

    /**
     * @return RedirectResponse
     */
    public function clearCacheAction()
    {
        /** @var TranslationAdmin $translationAdmin */
        $translationAdmin = $this->admin;

        $managedLocales = $translationAdmin->getAppLocales();

        $this->get('translator')->removeLocalesCacheFiles($managedLocales);

        $this->addFlash('sonata_flash_success', 'translations.cache_removed');

        return $this->redirect($translationAdmin->generateUrl('list'));
    }

    /**
     * Execute a batch download
     *
     * @param ProxyQueryInterface $queryProxy
     * @return StreamedResponse
     */
    public function batchActionDownload(ProxyQueryInterface $queryProxy)
    {
        $callback = function () use ($queryProxy): void {
            $dumper = new Dumper(4);

            foreach ($queryProxy->getQuery()->iterate() as $pos => $object) {
                /** @var TransUnit $transUnit */
                foreach ($object as $transUnit) {
                    $chunkPrefix = $transUnit->getDomain().'__'.$transUnit->getKey().'__'.$transUnit->getId().'__';

                    $chunk = array();
                    /** @var TranslationInterface $translation */
                    foreach ($transUnit->getTranslations() as $translation) {
                        $chunk[$chunkPrefix.$translation->getLocale()] = $translation->getContent();
                    }

                    echo $dumper->dump($chunk, 2);
                    flush();
                }
            }
        };

        return new StreamedResponse($callback, Response::HTTP_OK, [
            'Content-Disposition' => sprintf('attachment; filename="%s"', 'translations.'.self::TRANSLATION_FILE_TYPE),
            'Content-Type'        => 'text/x-yaml',
        ]);
    }
    
    
    /**
     * @return RedirectResponse
     */
    public function syncTranslationsAction(Request $request)
    {
        /** @var TranslationAdmin $translationAdmin */
        $translationAdmin = $this->admin;
    
        $filter = $request->get('filter');
        $currentTranslationDomain = isset($filter['domain']['value'])?$filter['domain']['value']:null;
        
        if(empty($currentTranslationDomain)) {
//            $this->addFlash('sonata_flash_success', 'translations.sync_error');
//            return $this->redirect($translationAdmin->generateUrl('list'));
            $currentTranslationDomain = 'messages';
        }
        
        
        $this->syncManagerService->syncTranslationDomain($currentTranslationDomain);
        
        $this->addFlash('sonata_flash_success', 'Translations have been synced. Please clear live cache');
    
        
        return $this->redirect($translationAdmin->generateUrl('list', ['filter' => $filter]));
    }
}
