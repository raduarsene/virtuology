<?php

namespace App\Controller\Api;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AssetController extends AbstractApiController
{
    /**
     * @Rest\QueryParam(name="siteId", description="The ID of the site this entity belongs to.")
     * @Rest\QueryParam(name="locationId", description="The ID of the location.")
     *
     * @param Request               $request
     * @param ParamFetcherInterface $paramFetcher
     * @return Response
     */
    public function getLocationAssetAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        list($data, $statusCode) = $this->getApiService($request)->getLocationAssets($paramFetcher->all());

        $view = $this->view($data, $statusCode);

        return $this->handleView($view);
    }
}
