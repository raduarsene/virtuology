<?php

namespace App\Controller\Api;

use App\Entity\Shop;
use App\Entity\ShopDomain;
use App\Event\RequestSubscriber;
use App\Service\Api\ApiInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractApiController extends AbstractFOSRestController
{
    /** @var ApiInterface */
    protected $apiService;

    /**
     * AbstractApiController constructor.
     *
     * @param ApiInterface $apiService
     */
    public function __construct(ApiInterface $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * @param Request $request
     * @return Shop|null
     */
    protected function getCurrentShop(Request $request)
    {
        $shop = null;

        if ($shopDomain = $this->getCurrentShopDomain($request)) {
            $shop = $shopDomain->getShop();
        }

        return $shop;
    }

    /**
     * @param Request $request
     * @return ShopDomain|null
     */
    protected function getCurrentShopDomain(Request $request)
    {
        return $request->attributes->get(RequestSubscriber::SHOP_DOMAIN_ATTRIBUTE);
    }

    /**
     * @param Request $request
     * @return ApiInterface
     */
    protected function getApiService(Request $request)
    {
        $version = $request->get('version');
        if (!$version && $shop = $this->getCurrentShop($request)) {
            $version = $shop->getApiVersion();
        }

        return $this->apiService->init($version, $this->getCurrentShopDomain($request));
    }
}
