<?php

namespace App\Controller\Api;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ItemController extends AbstractApiController
{
    /**
     * @Rest\QueryParam(name="page", requirements="\d+", allowBlank=false, default="0", description="Page you want to retrieve, 0 indexed.")
     * @Rest\QueryParam(name="size", requirements="\d+", allowBlank=false, default="1000", description="Size of the page you want to retrieve.")
     * @Rest\QueryParam(name="siteId", description="The ID of the site this entity belongs to.")
     * @Rest\QueryParam(name="locationId", description="The ID/GUID of the location this entity belongs to.")
     * @Rest\QueryParam(name="language", requirements="[a-z]{2}", description="The language of the item.")
     * @Rest\QueryParam(name="statusList", default="PUBLISHED", description="")
     *
     * @param Request               $request
     * @param ParamFetcherInterface $paramFetcher
     * @return Response
     */
    public function getItemsAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        list($data, $statusCode) = $this->getApiService($request)->getLocationItems($paramFetcher->all());

        $view = $this->view($data, $statusCode);

        return $this->handleView($view);
    }
}
