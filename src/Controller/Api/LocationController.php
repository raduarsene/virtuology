<?php

namespace App\Controller\Api;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LocationController extends AbstractApiController
{
    /**
     * @Rest\QueryParam(name="page", requirements="\d+", nullable=true, description="Page you want to retrieve, 0 indexed.")
     * @Rest\QueryParam(name="size", requirements="\d+", nullable=true, description="Size of the page you want to retrieve.")
     * @Rest\QueryParam(name="siteId", nullable=true, description="The ID of the site this entity belongs to.")
     * @Rest\QueryParam(name="language", requirements="[a-z]{2}", nullable=true, description="The language of the item.")
     * @Rest\QueryParam(name="projection", nullable=true, description="Type of locations.")
     * @Rest\QueryParam(name="within", nullable=true, description="Search for locations within map boundries.")
     * @Rest\QueryParam(name="near", nullable=true, description="Lat and Long of the location in format: 'lat,long'.")
     * @Rest\QueryParam(name="radius", requirements="\d+", nullable=true, description="The radius to search in metters.")
     * @Rest\QueryParam(name="onEmptyRadius", requirements="\d+", nullable=true, description="The radius to search in metters if no results found with default radius.")
     * @Rest\QueryParam(name="itemList", nullable=true, description="Filtering the results by some of the attributes/items attached to a location.")
     * @Rest\QueryParam(name="search", nullable=true, description="Search by location name, city or brand (used only on local API).")
     * @Rest\QueryParam(name="serviceKeys", nullable=true, description="V3 array of filterable services ids.")
     * @Rest\QueryParam(name="orServiceKeys", nullable=true, description="V3 array of filterable services ids.")
     * @Rest\QueryParam(name="brandSlugs", nullable=true, description="V3 array of filterable brand slugs.")
     * @Rest\QueryParam(name="openNow", nullable=true, description="V3 a location is opened now or not")
     * @Rest\QueryParam(name="openOn", nullable=true, description="V3 a location is opened on a certain date or not")
     * @Rest\QueryParam(name="openOnSundays", nullable=true, description="V3 a location is opened on sundays or not")
     * @Rest\QueryParam(name="fitAll", nullable=true, default=false,  description="V3 if is a fitAll request or not")
     * @Rest\QueryParam(name="country", nullable=true, description="filter locations by country")
     *
     * @param Request               $request
     * @param ParamFetcherInterface $paramFetcher
     * @return Response
     */
    public function getLocationsAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        
        list($data, $statusCode) = $this->getApiService($request)->getLocations($paramFetcher->all());

        $view = $this->view($data, $statusCode);

        return $this->handleView($view);
    }

    /**
     * @Rest\QueryParam(name="page", requirements="\d+", allowBlank=false, default="0", description="Page you want to retrieve, 0 indexed.")
     * @Rest\QueryParam(name="size", requirements="\d+", allowBlank=false, default="1", description="Size of the page you want to retrieve.")
     * @Rest\QueryParam(name="siteId", description="The ID of the site this entity belongs to.")
     * @Rest\QueryParam(name="slug", allowBlank=false, description="Slug of the location to retrieve.")
     * @Rest\QueryParam(name="language", requirements="[a-z]{2}", nullable=true, description="The language of the item.")
     * @Rest\QueryParam(name="projection", allowBlank=false, default="LocationFull", description="Type of locations.")
     * @Rest\QueryParam(name="services", description="Get location services/items, default false.")
     *
     * @param Request               $request
     * @param ParamFetcherInterface $paramFetcher
     * @return Response
     */
    public function getLocationBySlugAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        list($data, $statusCode) = $this->getApiService($request)->getLocationBySlug($paramFetcher->all());

        $view = $this->view($data, $statusCode);

        return $this->handleView($view);
    }

    /**
     * @Rest\QueryParam(name="search", description="String to search in locations.")
     *
     * @param Request               $request
     * @param ParamFetcherInterface $paramFetcher
     * @return Response
     */
    public function getLocationsLetterGroupedAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        list($data, $statusCode) = $this->getApiService($request)->getLocationsLetterGrouped($paramFetcher->all());

        $view = $this->view($data, $statusCode);

        return $this->handleView($view);
    }

    /**
     * @Rest\QueryParam(name="page", requirements="\d+", allowBlank=false, default="0", description="Page you want to retrieve, 0 indexed.")
     * @Rest\QueryParam(name="size", requirements="\d+", allowBlank=false, default="9", description="Size of the page you want to retrieve.")
     * @Rest\QueryParam(name="siteId", description="The ID of the site this entity belongs to.")
     * @Rest\QueryParam(name="slug", allowBlank=false, description="Slug of the location to retrieve.")
     * @Rest\QueryParam(name="language", requirements="[a-z]{2}", nullable=true, description="The language of the item.")
     * @Rest\QueryParam(name="near", nullable=true, description="Lat and Long of the location in format: 'lat,long'.")
     * @Rest\QueryParam(name="radius", requirements="\d+", nullable=true, description="The radius to search in metters.")
     * @Rest\QueryParam(name="onEmptyRadius", requirements="\d+", nullable=true, description="The radius to search in metters if no results found with old radius.")
     * @Rest\QueryParam(name="projection", allowBlank=false, default="StoreLocation", description="Type of locations.")
     * @Rest\QueryParam(name="brandSlugs", nullable=true, description="V3 array of filterable brand slugs.")
     *
     * @param Request               $request
     * @param ParamFetcherInterface $paramFetcher
     * @return Response
     */
    public function getNearLocationsBySlugAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        list($data, $statusCode) = $this->getApiService($request)->getNearLocationsBySlug($paramFetcher->all());

        $view = $this->view($data, $statusCode);

        return $this->handleView($view);
    }
}
