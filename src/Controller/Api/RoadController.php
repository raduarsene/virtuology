<?php

namespace App\Controller\Api;

use App\Service\Api\RoadService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Response;

class RoadController extends AbstractFOSRestController
{
    /** @var RoadService */
    protected $roadService;

    /**
     * RoadController constructor.
     *
     * @param RoadService $roadService
     */
    public function __construct(RoadService $roadService)
    {
        $this->roadService = $roadService;
    }

    /**
     * @Rest\QueryParam(name="language", default="fr", description="default language")
     * @Rest\QueryParam(name="zipcity", allowBlank=false, description="search item")
     *
     * @param ParamFetcherInterface $paramFetcher
     * @return Response
     */
    public function getRoadLocationsAction(ParamFetcherInterface $paramFetcher)
    {
        list($data, $statusCode) = $this->roadService->searchLocationsByCriteria($paramFetcher->all());

        $view = $this->view($data, $statusCode);

        return $this->handleView($view);
    }
}
