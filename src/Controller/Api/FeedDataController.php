<?php

namespace App\Controller\Api;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FeedDataController extends AbstractApiController
{
    /**
     * @Rest\QueryParam(name="page", requirements="\d+", default="0", description="Page you want to retrieve, 0 indexed.")
     * @Rest\QueryParam(name="size", requirements="\d+", default="100", description="Size of the page you want to retrieve.")
     * @Rest\QueryParam(name="shopId", description="The ID of the shop this entity belongs to.")
     * @Rest\QueryParam(name="type", description="The TYPE of this entity.")
     * @Rest\QueryParam(name="isVisible", description="Entity is visible.")
     * @Rest\QueryParam(name="inStock", description="Entity is in stock.")
     *
     * @param Request               $request
     * @param ParamFetcherInterface $paramFetcher
     * @return Response
     */
    public function getFeedDataAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        list($data, $statusCode) = $this->getApiService($request)->getFeedData($paramFetcher->all());

        $view = $this->view($data, $statusCode);

        return $this->handleView($view);
    }
}
