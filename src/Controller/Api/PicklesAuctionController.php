<?php

namespace App\Controller\Api;


use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\Api\PicklesAuctionService;
use App\Entity\PicklesAuction;

class PicklesAuctionController extends AbstractFOSRestController
{
    
    /** @var EntityManagerInterface  */
    private $em;
    
    /** @var PicklesAuctionService */
    private $picklesAuctionService;
    
    /**
     * PicklesAuctionController constructor.
     *
     * @param EntityManagerInterface $em
     * @param PicklesAuctionService  $picklesAuctionService
     */
    
    public function __construct(EntityManagerInterface $em, PicklesAuctionService $picklesAuctionService)
    {
        $this->em = $em;
        $this->picklesAuctionService = $picklesAuctionService;
    }

    
    /**
     * @Rest\QueryParam(name="n", requirements="\d+", default="10")
     * @Rest\QueryParam(name="page", requirements="\d+", default="1")
     * @Rest\QueryParam(name="organizationCode", requirements="[a-zA-Z\_\-\)\(\ ]*", nullable=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     * @return Response
     */
    public function getPicklesAuctionsAction(ParamFetcherInterface $paramFetcher)
    {
    
        $n = $paramFetcher->get('n', 0);
        $page = $paramFetcher->get('page', 0);
        $organizationCode = $paramFetcher->get('organizationCode');
        
        if(!empty($organizationCode)) {
            $organizationCode = $this->picklesAuctionService->slugify($organizationCode);
        }
    
        $params = [ 'organizationCode'=> $organizationCode ];
    
        $total = $this->em->getRepository(PicklesAuction::class)->getCountByFilters($params);
        $totalPages = ceil( $total/ $n ); //calculate total pages
        $page = max($page, 1); //get 1 page when $_GET['page'] <= 0
        $page = min($page, $totalPages); //get last page when $_GET['page'] > $totalPages
        
    
        $params = array_merge($params, ['n'=>$n, 'page'=>$page]);
        
    
        $data = $this->em->getRepository(PicklesAuction::class)->getByFilters($params);
        
        if(!empty($data)) {
    
            $statusCode = Response::HTTP_OK;
    
            $resultApi = [
        
                'results' => $data,
                'pages' => [
                    'totalResults'=>$total,
                    'totalPages' => $totalPages,
                    'currentPage' => $page
                ]
            ];
            
        }
        else {
            $statusCode = Response::HTTP_NOT_FOUND;
    
            $resultApi = [
        
                'results' => $data,
                'pages' => [
                    'totalResults'=> 0,
                    'totalPages' => 0,
                    'currentPage' => 0
                ]
            ];
        }
        

        $view = $this->view($resultApi, $statusCode);

        return $this->handleView($view);
    }
}
