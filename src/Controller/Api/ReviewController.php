<?php

namespace App\Controller\Api;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReviewController extends AbstractApiController
{
    /**
     * @Rest\QueryParam(name="siteId", description="The ID of the site this entity belongs to.")
     * @Rest\QueryParam(name="locationId", description="The ID/GUID of the location this entity belongs to.")
     * @Rest\QueryParam(name="language", requirements="[a-z]{2}", description="The language of the item.")
     *
     * @param Request               $request
     * @param ParamFetcherInterface $paramFetcher
     * @return Response
     */
    public function getLocationReviewsAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        list($data, $statusCode) = $this->getApiService($request)->getLocationReviews($paramFetcher->all());

        $view = $this->view($data, $statusCode);

        return $this->handleView($view);
    }
}
