<?php

namespace App\Controller\Web;

use App\Entity\Attribute;
use App\Entity\Shop;
use App\Entity\ShopTemplate;
use App\Service\ShopService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

/**
 * Class ShopController
 *
 * @package App\Controller\Web
 */
class ShopController extends AbstractController
{
    /** @var Environment */
    private $twig;

    /** @var ShopService */
    private $shopService;

    /**
     * ShopController constructor.
     *
     * @param Environment $twig
     * @param ShopService $shopService
     */
    public function __construct(Environment $twig, ShopService $shopService)
    {
        $this->twig = $twig;
        $this->shopService = $shopService;
    }

    /**
     * @Route("/{_locale}", defaults={"_locale": null}, requirements={"_locale"="[a-z]{2}(_[A-Z]{2})?"}, methods={"GET"}, name="app_web_shop_home")
     * @Route({
     *     "fr": "/fr/boutique",
     *     "en": "/en/shop",
     *     "nl": "/nl/winkel",
     *     "de": "/de/geschaft"
     * }, methods={"GET"}, name="app_web_voo_home")
     *
     * @param Request $request
     * @return Response
     */
    public function home(Request $request): Response
    {
        $shop = $this->getCurrentShop($request);
        $shopDomain = $this->getCurrentShopDomain($request);

        // Render Shop template if exists, otherwise base template
        $view = 'web/shop/'.$shop->getCode().'/home.html.twig';
        if (!$this->twig->getLoader()->exists($view)) {
            $view = 'web/base/home.html.twig';
        }

        $shopTemplateRepository = $this->getDoctrine()->getRepository(ShopTemplate::class);
        $templates = $shopTemplateRepository->getShopsByType($shop->getId(), Attribute::TYPE_PAGE_HOME);

        return $this->render($view, [
            'shop'            => $shop,
            'shopDomain'      => $shopDomain,
            'templates'       => $templates,
            'globalTemplates' => $shopTemplateRepository->getShopsByType($shop->getId(), Attribute::TYPE_GLOBAL, true),
            'data'            => $this->shopService->getHomeData($shop, $shopDomain, $templates),
        ]);
    }
    

    /**
     * @Route("/{_locale}/z/{countryCode}/{state}/{region}/{zipCode}/{city}",
     *     defaults={"_locale": null, "countryCode": null, "state": null, "region": null, "zipCode": null, "city": null},
     *     requirements={"_locale"="[a-z]{2}(_[A-Z]{2})?"},
     *     methods={"GET"},
     *     name="app_web_shop_home_redirect"
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function homeRedirect(Request $request): Response
    {
        return $this->home($request);
    }
    
    /**
     * @Route("/{_locale}/filters", requirements={"_locale"="[a-z]{2}"}, methods={"GET"}, name="app_web_shop_filters")
     *
     * @param Request $request
     * @return Response
     */
    public function filters(Request $request): Response
    {
        
        $shop = $this->getCurrentShop($request);
        $shopDomain = $this->getCurrentShopDomain($request);
        
        // Render Shop template if exists, otherwise base template
        $view = 'web/shop/'.$shop->getCode().'/block/filters.html.twig';
        if (!$this->twig->getLoader()->exists($view)) {
            
            $view = 'web/block/map/filters/dynamic_checkbox.html.twig';
        }
        $language = $request->getLocale();
        
        $filters = $this->shopService->getFiltersData($shopDomain, $language);
    
        return $this->render($view, [
            
            'shop'            => $shop,
            'shopDomain'      => $shopDomain,
            'filters'         => $filters,
            
        ]);
        
    }

    /**
     * @Route("/{_locale}/{slug}", requirements={"_locale"="[a-z]{2}(_[A-Z]{2})?"}, methods={"GET"}, name="app_web_shop_show")
     * @Route("/{_locale}/s/carrefour/{slug}/{externalId}", requirements={"_locale"="[a-z]{2}(_[A-Z]{2})?"}, methods={"GET"}, name="app_web_carrefour_show")
     * @Route("/{_locale}/s/t/{slug}/{externalId}", requirements={"_locale"="[a-z]{2}(_[A-Z]{2})?"}, methods={"GET"}, name="app_web_carrefour_show_variant")
     * @Route("/{_locale}/pharma/{slug}/{externalId}", defaults={"externalId": null}, requirements={"_locale"="[a-z]{2}(_[A-Z]{2})?"}, methods={"GET"}, name="app_web_lloydspharma_show")
     * @Route({
     *     "fr": "/fr/boutique/{slug}",
     *     "en": "/en/shop/{slug}",
     *     "nl": "/nl/winkel/{slug}",
     *     "de": "/de/geschaft/{slug}"
     * }, methods={"GET"}, name="app_web_voo_show")
     * @Route({
     *     "fr": "/fr/s/moules-frites/{slug}/{externalId}",
     *     "en": "/en/s/{_localization}/{slug}/{externalId}"
     * }, defaults={"_localization": "mussels-fries"}, requirements={"_localization"="mussels-fries|Mussels and french fries"}, methods={"GET"}, name="app_web_leondebruxelles_show")
     *
     * @param Request     $request
     * @param string      $slug
     * @param string|null $externalId
     * @return Response
     */
    public function show(Request $request, string $slug, ?string $externalId = null): Response
    {
        $shop = $this->getCurrentShop($request);
        $shopDomain = $this->getCurrentShopDomain($request);

        // Render Shop template if exists, otherwise base template
        $view = 'web/shop/'.$shop->getCode().'/show.html.twig';
        if (!$this->twig->getLoader()->exists($view)) {
            $view = 'web/base/show.html.twig';
        }

        $shopTemplateRepository = $this->getDoctrine()->getRepository(ShopTemplate::class);

        return $this->render($view, [
            'shop'            => $shop,
            'shopDomain'      => $shopDomain,
            'templates'       => $shopTemplateRepository->getShopsByType($shop->getId(), Attribute::TYPE_PAGE_SHOW),
            'globalTemplates' => $shopTemplateRepository->getShopsByType($shop->getId(), Attribute::TYPE_GLOBAL, true),
            'location'        => $this->shopService->getShowData($shopDomain, $slug, $request->getLocale(), $externalId),
        ]);
    }

    /**
     * @Route("/{_locale}/s/{slug}", methods={"GET"}, name="app_web_shop_show_redirect")
     * @Route("/{_locale}/s/pharma/{slug}/{externalId}", defaults={"externalId": null}, requirements={"_locale"="[a-z]{2}(_[A-Z]{2})?"}, methods={"GET"}, name="app_web_lloydspharma_show_redirect")
     *
     *  @Route("/{_locale}/s/brasseurs/{slug}/{externalId}", defaults={"externalId": null}, requirements={"_locale"="[a-z]{2}(_[A-Z]{2})?"}, methods={"GET"}, name="app_web_les3brasseurs_show_redirect")
     * @param Request $request
     * @param string  $slug
     * @param string|null  $externalId
     * @return Response
     */
    public function showRedirect(Request $request, string $slug, ?string $externalId): Response
    {
        $shop = $this->getCurrentShop($request);

        switch ($shop->getCode()) {
            //in this case we need to handle the redirect
            case Shop::CODE_MEDIMARKET:
                $shopNewSlug = $this->shopService->getShopLocationSlugFromHistory($shop, $slug);

                if ($shopNewSlug) {
                    if ($request->getHost() == 'stores.pharmaclic.be') {
                        $url = $this->generateUrl('app_web_shop_show', ['slug' => $shopNewSlug]);

                        return $this->redirect('https://stores.medi-market.be'.$url, Response::HTTP_MOVED_PERMANENTLY);
                    }

                    return $this->redirectToRoute('app_web_shop_show', ['slug' => $shopNewSlug], Response::HTTP_MOVED_PERMANENTLY);
                }

                throw $this->createNotFoundException('This location does not exist');
                break;
                
            case Shop::CODE_LOLALIZA:
            case Shop::CODE_LOUISDELHAIZE:
            
                
                $shopNewSlug = $this->shopService->getShopLocationSlugFromHistory($shop, $slug);
    
                if ($shopNewSlug) {
                    return $this->redirectToRoute('app_web_shop_show', ['slug' => $shopNewSlug], Response::HTTP_MOVED_PERMANENTLY);
                }
    
                throw $this->createNotFoundException('This location does not exist');
                
                
                break;
                
            case Shop::CODE_LES3BRASSEURS:
    
                $slug = '3-brasseurs-'.$slug;
                
                $shopNewSlug = $this->shopService->getShopLocationSlugFromHistory($shop, $slug, $externalId);
    
                if ($shopNewSlug) {
                    return $this->redirectToRoute('app_web_shop_show', ['slug' => $shopNewSlug], Response::HTTP_MOVED_PERMANENTLY);
                }
    
                throw $this->createNotFoundException('This location does not exist');
                
                break;
    
            case Shop::CODE_LLOYDSPHARMA:
    
                $slug = 'pharma/'.$slug.'/'.$externalId;
    
                $shopNewSlug = $this->shopService->getShopLocationSlugFromHistory($shop, $slug);
    
                if ($shopNewSlug) {
                    return $this->redirectToRoute('app_web_lloydspharma_show', ['slug' => $shopNewSlug], Response::HTTP_MOVED_PERMANENTLY);
                }
                
                exit();
                break;
        }

        return $this->show($request, $slug);
    }
}
