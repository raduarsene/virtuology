<?php

namespace App\Controller\Web;

use App\Service\Api\Entity\Item\AssetEntity;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use GuzzleHttp\RequestOptions;

/**
 * Class ImageController
 *
 * @package App\Controller\Web
 */
class ImageController extends AbstractController
{
    const CLOUDINARY_PATH = '/mobilosoft/image/upload/';
    const CLOUDINARY_KEY_PATH = '/image/upload/';
    const CLOUDINARY_KEY_PATH_VIDEO = '/video/upload/';

    const IMAGE_TYPE_ITEM = 'item';
    const IMAGE_TYPE_LOCATION = 'location';

    const DEFAULT_IMAGE_TYPE = self::IMAGE_TYPE_ITEM;
    const DEFAULT_IMAGE_FORMAT = ''; // file format/extension, exemple: .webp or .jpg
    const DEFAULT_IMAGE_QUALITY = 'q_auto';
    const DEFAULT_IMAGE_PATH = '/build/images/web/one-pixel.png';

    
    /**
     * @Route("/image/{apiKey}", methods={"GET"}, name="app_web_image_cloudinary_path")
     * @param Request $request
     * @param string  $apiKey
     * @param string  $type
     *
     * @return Response
     */
    public function showCloudinaryImageByPath(Request $request, string $apiKey): Response
    {
        $imageSize = $request->get('imageSize');
        $imageParams = ($imageSize ? $imageSize.',' : '').self::DEFAULT_IMAGE_QUALITY;

        $type = $request->query->get("type", AssetEntity::TYPE_IMAGE);
        
        switch ($type) {
            case AssetEntity::TYPE_VIDEO:
                $apiUrl = $this->getParameter('app.cloudinary_api').'/'.$apiKey.self::CLOUDINARY_KEY_PATH_VIDEO.$imageParams.'/'.$request->get('apiPath');
                break;
            default:
                $apiUrl = $this->getParameter('app.cloudinary_api').'/'.$apiKey.self::CLOUDINARY_KEY_PATH.$imageParams.'/'.$request->get('apiPath');
                break;
        }
        
        $httpClient = new \GuzzleHttp\Client();
        try {
            $httpResponse = $httpClient->request(Request::METHOD_GET, $apiUrl, [
    
                RequestOptions::CONNECT_TIMEOUT => 2,
                RequestOptions::TIMEOUT => 4,
            
            ]);

            $statusCode = $httpResponse->getStatusCode();
        } catch (GuzzleException $e) {
            $statusCode = $e->getCode();
        }

        if ($statusCode === Response::HTTP_OK && !empty($httpResponse)) {
            $response = new Response();

            $response->headers->set('Content-Type', $httpResponse->getHeaderLine('content-type'));

            $response->setContent($httpResponse->getBody()->getContents());
        } else {
            $response = new BinaryFileResponse($this->getParameter('kernel.project_dir').'/public'.$request->get('defaultImage', self::DEFAULT_IMAGE_PATH));
        }

        return $response;
    }

    /**
     * @Route("/image/{apiKey}/{id}", methods={"GET"}, name="app_web_image_cloudinary")
     *
     * @param Request $request
     * @param string  $apiKey
     * @param string  $id
     * @return Response
     */
    public function showCloudinaryImage(Request $request, string $apiKey, string $id): Response
    {
        $format = $request->get('format', self::DEFAULT_IMAGE_FORMAT);
        $imageType = $request->get('imageType', self::DEFAULT_IMAGE_TYPE);
        $imageSize = $request->get('imageSize');

        $imageParams = ($imageSize ? $imageSize.',' : '').self::DEFAULT_IMAGE_QUALITY;
        $imageName = $imageType.'_'.str_replace('-', '_', $id).$format;

        $apiUrl = $this->getParameter('app.cloudinary_api').self::CLOUDINARY_PATH.$imageParams.'/'.$apiKey.'/';
        
        

        $httpClient = new \GuzzleHttp\Client();
        try {
            $httpResponse = $httpClient->request(Request::METHOD_GET, $apiUrl.$imageName,
    
                [
        
                    RequestOptions::CONNECT_TIMEOUT => 2,
                    RequestOptions::TIMEOUT => 4,
    
                ]
                
            );

            $statusCode = $httpResponse->getStatusCode();
        } catch (GuzzleException $e) {
            $statusCode = $e->getCode();

            if ($imageType === self::IMAGE_TYPE_ITEM) {
                $imageName = $imageType.'_'.$request->getLocale().'_'.str_replace('-', '_', $id).$format;

                try {
                    $httpResponse = $httpClient->request(Request::METHOD_GET, $apiUrl.$imageName);

                    $statusCode = $httpResponse->getStatusCode();
                } catch (GuzzleException $e) {
                    $statusCode = $e->getCode();
                }
            }

            // Get image using brand if is set
            $brand = $request->get('brand');
            if ($brand && $brand !== 'null') {
                $imageName = self::DEFAULT_IMAGE_TYPE.'_'.$brand.$format;

                try {
                    $httpResponse = $httpClient->request(Request::METHOD_GET, $apiUrl.$imageName);

                    $statusCode = $httpResponse->getStatusCode();
                } catch (GuzzleException $e) {
                    $statusCode = $e->getCode();
                }
            }
        }

        if ($statusCode === Response::HTTP_OK && !empty($httpResponse)) {
            $response = new Response();

            $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $imageName);
            $response->headers->set('Content-Disposition', $disposition);
            $response->headers->set('Content-Type', $httpResponse->getHeaderLine('content-type'));

            $response->setContent($httpResponse->getBody()->getContents());
        } else {
            $response = new BinaryFileResponse($this->getParameter('kernel.project_dir').'/public'.$request->get('defaultImage', self::DEFAULT_IMAGE_PATH));
        }

        return $response;
    }
}
