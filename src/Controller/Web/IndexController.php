<?php

namespace App\Controller\Web;

use App\Entity\ShopLocation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

/**
 * Class IndexController
 *
 * @package App\Controller\Web
 */
class IndexController extends AbstractController
{
    /** @var Environment */
    private $twig;

    /**
     * IndexController constructor.
     *
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/robots.txt", name="app_web_robots")
     *
     * @param Request $request
     * @return Response
     */
    public function robots(Request $request): Response
    {
        $shop = $this->getCurrentShop($request);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/plain');

        $view = 'web/shop/'.$shop->getCode().'/robots.txt.twig';
        if (!$this->twig->getLoader()->exists($view)) {
            $view = 'web/index/robots.txt.twig';
        }

        return $this->render($view, [
            'allowRobots' => $shop->getAllowRobots(),
        ], $response);
    }

    /**
     * @Route("/sitemap.xml", name="app_web_sitemap")
     * @Route("/sitemap.{_locale}.xml", requirements={"_locale"="[a-z]{2}"}, name="app_web_sitemap_locale")
     * @Route("/sitemap-stores-{_locale}.xml", requirements={"_locale"="[a-z]{2}"}, name="app_web_sitemap_locale_medimarket")
     * @Route("/sitemap.locations.{_locale}.xml", requirements={"_locale"="[a-z]{2}"}, name="app_web_sitemap_locale_carrefour")
     *
     * @param Request $request
     * @return Response
     */
    public function sitemap(Request $request): Response
    {
        $shop = $this->getCurrentShop($request);
        $shopDomain = $this->getCurrentShopDomain($request);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml');

        $view = 'web/shop/'.$shop->getCode().'/sitemap.xml.twig';
        if (!$this->twig->getLoader()->exists($view)) {
            $view = 'web/index/sitemap.xml.twig';
        }

        /** @var ShopLocation[] $shopLocations */
        $shopLocations = $this->getDoctrine()->getRepository(ShopLocation::class)->findBy(['shop' => $shop]);

        return $this->render($view, [
            'shopDomain'      => $shopDomain,
            'shopLocations'   => $shopLocations,
            'languageDomains' => $shop->getLanguageDomains(),
        ], $response);
    }

    /**
     * @Route("/sitemap-index.xml", name="app_web_sitemap_index")
     *
     * @param Request $request
     * @return Response
     */
    public function sitemapIndex(Request $request): Response
    {
        $shop = $this->getCurrentShop($request);
        $shopDomain = $this->getCurrentShopDomain($request);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml');

        $view = 'web/shop/'.$shop->getCode().'/sitemap_index.xml.twig';
        if (!$this->twig->getLoader()->exists($view)) {
            $view = 'web/index/sitemap_index.xml.twig';
        }

        return $this->render($view, [
            'shopDomain'      => $shopDomain,
            'languageDomains' => $shop->getLanguageDomains(),
        ], $response);
    }
}
