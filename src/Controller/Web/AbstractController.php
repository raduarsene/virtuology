<?php

namespace App\Controller\Web;

use App\Entity\Shop;
use App\Entity\ShopDomain;
use App\Event\RequestSubscriber;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as AbstractBaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

abstract class AbstractController extends AbstractBaseController
{
    /**
     * @param Request $request
     * @return Shop
     */
    protected function getCurrentShop(Request $request)
    {
        /** @var Shop $shop */
        $shop = $this->getCurrentShopDomain($request)->getShop();

        if (!$shop) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'Request can be performed only with shop');
        }

        return $shop;
    }

    /**
     * @param Request $request
     * @return ShopDomain
     */
    protected function getCurrentShopDomain(Request $request)
    {
        /** @var ShopDomain $shopDomain */
        $shopDomain = $request->attributes->get(RequestSubscriber::SHOP_DOMAIN_ATTRIBUTE);

        if (!$shopDomain) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'Request can be performed only with shop domain');
        }

        return $shopDomain;
    }
}
